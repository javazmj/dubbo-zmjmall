# zmjmalldemo
## 项目介绍

   zmjmall是基于springboot + dubbo 构建的一款在线商城系统,整合了 
   springboot + dubbo + mybatis + jpa + redis 等主流技术,
   是为了研究电商架构做的个人项目.
   
## 项目演示地址

    https://m.youmias.com
    https://m.zmjmall.com
   
## 搭建环境

java: jdk 1.8 + 

maven:3.3.9 

springboot : 1.5.9.RELEASE

zookeeper: 3.4.11 +

mysql: 5.7
 

## 软件架构说明

## 安装教程

1. https://gitee.com/javazmj/dubbo-zmjmall 下载项目

2. 启动zookeeper 客户端 

3. 启动redis 客户端

## 使用说明

1.  导入/sql/zmjmall.sql 到数据库

2.  启动 ZmjmallServiceApplication

3.  启动 ZmjmalldemoWebApplication

## 参与贡献

  周美军  536304123@qq.com 
  
  张盼伟
  
## 项目git地址

  后端接口: https://gitee.com/javazmj/dubbo-zmjmall（前后端暂未联调）
  
  前端静态: https://gitee.com/javazmj/youmiasStatic
  
  后台管理: https://gitee.com/javazmj/zmjmalldemo-cms(基于Guns搭建)

## 相关网址

  http://x.zmjmall.com    (接口测试地址)  

  例  测试api:        http://x.zmjmall.com/test/test/12               method=get

      分类列表api:    http://x.zmjmall.com/category                    method=get  

                     http://x.zmjmall.com/category/{categoryId}       method=get   
  
 http://erp.zmjmall.com    (后台管理系统)   name: guest   password:123456 (仅提供来宾权限)

 http://mq.zmjmall.com    (rabbitmq可视化界面)   
 
 http://el.zmjmall.com    (elasticsearch可视化界面)

 https://zmjmall.com    (测试秒杀商品并发优化的一个demo)   

      