package com.zmjmall.demo.base;

import java.io.Serializable;

public interface GenericMapper<T,PK extends Serializable> {

	int deleteByPrimaryKey(PK id);


	int insert(T t);


	int insertSelective(T t);


	T selectByPrimaryKey(PK id);


	int updateByPrimaryKeySelective(T t);


	int updateByPrimaryKey(T t);


	int getCount(T record);

	
}
