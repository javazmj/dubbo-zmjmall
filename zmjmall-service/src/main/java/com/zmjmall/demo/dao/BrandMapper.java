package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.Brand;

public interface BrandMapper extends GenericMapper<Brand,String> {

}