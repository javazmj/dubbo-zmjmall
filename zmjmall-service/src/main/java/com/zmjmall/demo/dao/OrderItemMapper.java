package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.OrderItem;

public interface OrderItemMapper extends GenericMapper<OrderItem,String> {

}