package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.Cart;
import com.zmjmall.demo.vo.CartVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CartMapper extends GenericMapper<Cart,String> {

    List<CartVO> findByUserIdEquals(@Param("userId") String userId,@Param("checked") Integer checked);

    CartVO findByUserIdAndProductId(Cart cart);

    int update(Cart cart);

    int deleteByUserIdAndProductId(Cart cart);

    int updateChecked(Map<String,Object> map);
}