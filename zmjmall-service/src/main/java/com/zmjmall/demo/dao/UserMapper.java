package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.User;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserMapper extends GenericMapper<User,String> {

    @Select({"select id,nick_name,head,phone from user "})
    @ResultMap("BaseResultMap")
    User toLogin();


    @Select({"select * from user"})
    @ResultMap("BaseResultMap")
    List<User> findAllPage();

    int updateLoginInfo(User user);


    @Select({"select * from user"})
    @ResultMap("BaseResultMap")
    List<User> findAll();

    User getUser(User user);
}