package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.Category;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface CategoryMapper extends GenericMapper<Category,String> {

    @Select({"select id,pid,category_name,sort from category where id = #{id,jdbcType=VARCHAR} order by sort"})
    @ResultMap("BaseResultMap")
    Category findById(@Param("id")String id);

    @Select({"select * from category where pid = #{pid,jdbcType=VARCHAR}"})
    @ResultMap("BaseResultMap")
    List<Category> findMenuLeave(Category category);

    //    @Select({"select * from category where pid = #{cid,jdbcType=VARCHAR}"})
    @Select({"select * from category"})
    @ResultMap("BaseResultMap")
    List<Category> findChildCategory();
}