package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.MallOrder;

public interface MallOrderMapper extends GenericMapper<MallOrder,String> {

}