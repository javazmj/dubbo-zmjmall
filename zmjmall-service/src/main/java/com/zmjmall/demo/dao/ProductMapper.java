package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.Product;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ProductMapper extends GenericMapper<Product,String> {

    @Select({"select * from product"})
    @ResultMap("BaseResultMap")
    List<Product> findAll();


    @Select({"select id from product where id = #{id,jdbcType=VARCHAR}"})
    @ResultMap("BaseResultMap")
    Product findById(@Param("id")String id);

    @Select({"select * from product"})
    @ResultMap("BaseResultMap")
    List<Product> findAllPage();

    Product save(Product product);
}