package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.PayInfo;

public interface PayInfoMapper extends GenericMapper<PayInfo,String> {

}