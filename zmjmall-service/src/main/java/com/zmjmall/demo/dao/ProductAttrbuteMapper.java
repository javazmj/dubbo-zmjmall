package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.ProductAttrbute;

public interface ProductAttrbuteMapper extends GenericMapper<ProductAttrbute,String> {

}