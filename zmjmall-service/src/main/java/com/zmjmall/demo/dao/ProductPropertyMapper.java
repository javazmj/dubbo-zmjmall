package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.Product;
import com.zmjmall.demo.model.ProductProperty;
import com.zmjmall.demo.vo.SkuAttributesVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ProductPropertyMapper extends GenericMapper<Product,String> {

    @Select({"SELECT pp.id as 'propertyId',pp.property_name as 'propertyName',pa.id as 'attributeId',pa.attribute_name as 'attributeName' FROM product_attrbute pa LEFT JOIN product_property pp ON " +
            "pa.property_id = pp.id WHERE pp.category_id = #{categoryId,jdbcType=VARCHAR}"})
    @ResultType(SkuAttributesVO.class)
    List<SkuAttributesVO> findByCategoryId(@Param("categoryId")String categoryId);
}