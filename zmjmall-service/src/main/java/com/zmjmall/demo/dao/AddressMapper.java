package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.Address;

public interface AddressMapper extends GenericMapper<Address,String> {

}