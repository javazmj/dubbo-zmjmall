package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.Shop;

public interface ShopMapper extends GenericMapper<Shop,String> {

}