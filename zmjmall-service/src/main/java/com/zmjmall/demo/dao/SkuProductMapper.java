package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.dto.SkuProductDTO;
import com.zmjmall.demo.model.SkuProduct;
import com.zmjmall.demo.vo.SkuProductVO;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SkuProductMapper extends GenericMapper<SkuProduct,String> {

    @Select({"SELECT sp.id,sp.sku_product_name,sp.title,p.category_id,p.shop_id,p.brand_id,\n" +
            "p.main_image,p.product_type,p.detail,p.sub_images,sp.attribute_values FROM sku_product sp LEFT JOIN product p ON p.id = sp.product_id WHERE sp.product_id = #{id,jdbcType=VARCHAR} AND ground = #{ground,jdbcType=VARCHAR}"})
    @Results(id = "skuProductVO", value = {
            @Result(column = "sku_product_name", property = "productName", javaType = String.class),
            })
    List<SkuProductVO> findByProductId(String id, int ground);


    List<SkuProductVO> findAllByPage(SkuProductDTO skuProductDTO);


    SkuProductVO findById(SkuProductDTO skuProductDTO);

    List<SkuProductVO> findAll();
}