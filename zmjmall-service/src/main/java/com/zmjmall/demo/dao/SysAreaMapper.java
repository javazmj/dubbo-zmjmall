package com.zmjmall.demo.dao;

import com.zmjmall.demo.base.GenericMapper;
import com.zmjmall.demo.model.SysArea;

import java.util.List;

public interface SysAreaMapper extends GenericMapper<SysArea,String> {

    SysArea findNameByCodeList(List<String> strings);
}