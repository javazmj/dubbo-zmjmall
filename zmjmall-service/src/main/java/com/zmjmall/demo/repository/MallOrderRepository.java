package com.zmjmall.demo.repository;

import com.zmjmall.demo.model.MallOrder;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zmj
 * @version 2018/7/16
 */
public interface MallOrderRepository extends JpaRepository<MallOrder,String> {
}
