package com.zmjmall.demo.repository;

import com.zmjmall.demo.model.SkuProduct;
import com.zmjmall.demo.vo.SkuProductVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Map;

/**
 * @author zmj
 * @version 2018/7/9
 */
public interface SkuProductRepository extends JpaRepository<SkuProduct,String> {

    @Query(value = "SELECT new com.zmjmall.demo.vo.SkuProductVO(sp.id,sp.sku,sp.skuProductName,) FROM sku_product sp LEFT JOIN product p ON p.id = sp.product_id" +
            " WHERE sp.product_id=:id AND sp.ground='0'" +
            "p.id = sp.product_id",nativeQuery = true)
    List<SkuProductVO> findByProductId(String id);
}
