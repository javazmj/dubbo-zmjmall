package com.zmjmall.demo.repository;

import com.zmjmall.demo.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zmj
 * @version 2018/7/12
 */
public interface CartRepository extends JpaRepository<Cart,String> {

}
