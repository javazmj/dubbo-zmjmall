package com.zmjmall.demo.repository;

import com.zmjmall.demo.model.Shop;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zmj
 * @version 2018/7/3
 */
public interface ShopRepository extends JpaRepository<Shop,String> {

    Shop findByUserIdEquals(String userId);
}
