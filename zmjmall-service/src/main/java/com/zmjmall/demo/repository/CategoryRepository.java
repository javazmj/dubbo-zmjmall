package com.zmjmall.demo.repository;

import com.zmjmall.demo.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/2
 */
public interface CategoryRepository extends JpaRepository<Category,String> {

    List<Category> findByPid(String pid);
}
