package com.zmjmall.demo.repository;

import com.zmjmall.demo.model.ProductProperty;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/9
 */
public interface ProductPropertyRepository extends JpaRepository<ProductProperty,String> {

    List<ProductProperty> findByCategoryId(String categoryId);
}
