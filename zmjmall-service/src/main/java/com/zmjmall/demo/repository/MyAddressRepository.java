package com.zmjmall.demo.repository;

import com.zmjmall.demo.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/12
 */
public interface MyAddressRepository extends JpaRepository<Address,String> {

    List<Address> findByUserIdEquals(String userId);

    Address findById(String addressId);
}
