package com.zmjmall.demo.repository;

import com.zmjmall.demo.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zmj
 * @version 2018/6/29
 */
public interface ProductRepository extends JpaRepository<Product,String> {

    Product findByIdEquals(String id);

    List<Product> findByBrandId(String id);

}

