package com.zmjmall.demo.repository;

import com.zmjmall.demo.model.SysArea;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/12
 */
public interface SysAreaRepository extends JpaRepository<SysArea,String> {

    List<SysArea> findByParentId(String parentId);

    SysArea findByCode(String addressCode);
}
