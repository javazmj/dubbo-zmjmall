package com.zmjmall.demo.repository;

import com.zmjmall.demo.model.ProductAttrbute;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zmj
 * @version 2018/7/9
 */
public interface ProductAttrbuteRepository extends JpaRepository<ProductAttrbute,String> {
}
