package com.zmjmall.demo.repository;

import com.zmjmall.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zmj
 * @version 2018/7/3
 */
public interface UserRepository extends JpaRepository<User,String> {

    User findByPhone(String phone);

}
