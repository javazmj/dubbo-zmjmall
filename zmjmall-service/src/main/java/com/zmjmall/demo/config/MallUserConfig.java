package com.zmjmall.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author zmj
 * @version 2018/7/17
 */
@Configuration
@ConfigurationProperties(prefix = "web.login")
@Data
public class MallUserConfig {

    private String salt;
}
