package com.zmjmall.demo.api.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmjmall.demo.api.OrderItemService;
import lombok.extern.slf4j.Slf4j;

/**
 * @author zmj
 * @version 2018/7/16
 */
@Service
@Slf4j
public class OrderItemServiceImpl implements OrderItemService {
}
