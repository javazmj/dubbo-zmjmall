package com.zmjmall.demo.api.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmjmall.demo.api.ProductPropertyService;
import com.zmjmall.demo.dao.ProductPropertyMapper;
import com.zmjmall.demo.vo.SkuAttributesVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/9
 */
@Service
@Slf4j
public class ProductPropertyServiceImpl implements ProductPropertyService {

    @Autowired
    private ProductPropertyMapper propertyMapper;

    @Override
    public List<SkuAttributesVO> findByCategoryId(String categoryId) {
        return propertyMapper.findByCategoryId(categoryId);
    }
}
