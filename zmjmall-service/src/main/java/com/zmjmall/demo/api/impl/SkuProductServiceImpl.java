package com.zmjmall.demo.api.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import com.zmjmall.demo.api.SkuProductService;
import com.zmjmall.demo.dao.SkuProductMapper;
import com.zmjmall.demo.dto.SkuProductDTO;
import com.zmjmall.demo.model.SkuProduct;
import com.zmjmall.demo.vo.SkuProductVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/9
 */
@Service
@Slf4j
public class SkuProductServiceImpl implements SkuProductService {

    @Autowired
    private SkuProductMapper skuProductMapper;

    @Override
    @Transactional
    public boolean insert(SkuProduct skuProduct) {
        return skuProductMapper.insert(skuProduct) == 1?true:false;
    }

    @Override
    public List<SkuProductVO> findAllByPage(SkuProductDTO skuProductDTO, int pageNum, int pageSize, String sort) {
        PageHelper.startPage(pageNum,pageSize,sort);
        return skuProductMapper.findAllByPage(skuProductDTO);
    }

    @Override
    public SkuProductVO findById(SkuProductDTO skuProductVO) {
        return skuProductMapper.findById(skuProductVO);
    }

    @Override
    public List<SkuProductVO> findAll() {
        return skuProductMapper.findAll();
    }
}
