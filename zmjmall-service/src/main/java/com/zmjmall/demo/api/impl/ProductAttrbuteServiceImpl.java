package com.zmjmall.demo.api.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmjmall.demo.api.ProductAttrbuteService;
import com.zmjmall.demo.model.ProductAttrbute;
import com.zmjmall.demo.repository.ProductAttrbuteRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/9
 */
@Service
@Slf4j
public class ProductAttrbuteServiceImpl implements ProductAttrbuteService{

    @Autowired
    private ProductAttrbuteRepository productAttrbuteRepository;

    @Override
    public List<ProductAttrbute> findAll() {
        return productAttrbuteRepository.findAll();
    }
}
