package com.zmjmall.demo.api.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.zmjmall.demo.api.CartService;
import com.zmjmall.demo.dao.CartMapper;
import com.zmjmall.demo.dao.ProductMapper;
import com.zmjmall.demo.model.Cart;
import com.zmjmall.demo.vo.CartVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zmj
 * @version 2018/7/11
 */
@Service
@Slf4j
public class CartServiceImpl implements CartService {

    @Autowired
    private CartMapper cartMapper;

    @Override
    public List<CartVO> findByUserId(String userId) {
        return cartMapper.findByUserIdEquals(userId,null);
    }

    @Override
    public CartVO findByUserIdAndProductId(Cart cart) {
        return cartMapper.findByUserIdAndProductId(cart);
    }

    @Override
    @Transactional
    public int insert(Cart cart) {
        cart.setId(IdWorker.getIdStr());
        return cartMapper.insert(cart);
    }

    @Override
    @Transactional
    public int update(Cart cart) {
        return cartMapper.update(cart);
    }

    @Override
    @Transactional
    public int delete(Cart cart) {
        return cartMapper.deleteByUserIdAndProductId(cart);
    }

    @Override
    @Transactional
    public int updateChecked(String userId,List<String> idsList) {
        Map<String,Object> map = new HashMap<>();
        map.put("userId",userId);
        map.put("list",idsList);
        return cartMapper.updateChecked(map);
    }
}
