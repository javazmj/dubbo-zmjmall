package com.zmjmall.demo.api.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmjmall.demo.api.CategoryService;
import com.zmjmall.demo.dao.CategoryMapper;
import com.zmjmall.demo.model.Category;
import com.zmjmall.demo.repository.CategoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/2
 */
@Service
@Slf4j
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private CategoryRepository categoryRepository;


    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category findById(String id) {
        return categoryMapper.findById(id);
    }

    @Override
    public List<Category> findByPid(String pid) {
        return categoryRepository.findByPid(pid);
    }

    @Override
    public  List<Category> findMenuLeave(Category category) { return categoryMapper.findMenuLeave(category);}

    @Override
    public List<Category> findChildCategory() {
        return categoryMapper.findChildCategory();
    }
}
