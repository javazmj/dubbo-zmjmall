package com.zmjmall.demo.api.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmjmall.demo.api.SysAreaService;
import com.zmjmall.demo.model.SysArea;
import com.zmjmall.demo.repository.SysAreaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/11
 */
@Service
@Slf4j
public class SysAreaServiceImpl implements SysAreaService {

    @Autowired
    private SysAreaRepository sysAreaRepository;

    @Override
    public List<SysArea> findByParentId(String id) {
        return sysAreaRepository.findByParentId(id);
    }

    @Override
    public SysArea findByCode(String addressCode) {
        return sysAreaRepository.findByCode(addressCode);
    }
}
