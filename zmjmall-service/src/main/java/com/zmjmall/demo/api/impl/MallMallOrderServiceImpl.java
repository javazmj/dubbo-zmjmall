package com.zmjmall.demo.api.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.toolkit.IdWorker;
import com.zmjmall.demo.api.MallOrderService;
import com.zmjmall.demo.constant.GlobalConstant;
import com.zmjmall.demo.dao.CartMapper;
import com.zmjmall.demo.dao.OrderItemMapper;
import com.zmjmall.demo.dao.MallOrderMapper;
import com.zmjmall.demo.dto.OrderDTO;
import com.zmjmall.demo.model.Address;
import com.zmjmall.demo.model.MallOrder;
import com.zmjmall.demo.model.OrderItem;
import com.zmjmall.demo.repository.MyAddressRepository;
import com.zmjmall.demo.vo.CartVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author zmj
 * @version 2018/7/16
 */
@Service
@Slf4j
public class MallMallOrderServiceImpl extends BaseImpl implements MallOrderService {

    @Autowired
    private MallOrderMapper mallOrderMapper;

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Autowired
    private MyAddressRepository myAddressRepository;

    @Autowired
    private CartMapper cartMapper;
    

    @Override
    @Transactional
    public boolean createOrder(OrderDTO orderDTO) {
        MallOrder order = new MallOrder();
        /**
         * 根据收货地址获取收货信息
         * 根据购物车选中列表获取商品信息
         * 生成实际付款金额，运费金额
         * 生成订单
         * 生成订单详情
         *
         *
         */
        Address address = myAddressRepository.findById(orderDTO.getAddressId());
        order.setReceiverName(address.getReceiverName());
        order.setReceiverMobile(address.getReceiverMobile());
        order.setReceiverAddress(addressNameByCode(address));
        //订单号
        order.setId(IdWorker.getIdStr());
        order.setUserId(orderDTO.getUserId());
        order.setOrderNo(IdWorker.getIdStr());

        List<CartVO> cartVOList = cartMapper.findByUserIdEquals(orderDTO.getUserId(), GlobalConstant.CART_CHECKED);

        BigDecimal payment = BigDecimal.ZERO;

        for (CartVO cartVO:cartVOList) {
            //生成订单详情
            OrderItem orderItem = new OrderItem();
            orderItem.setId(IdWorker.getIdStr());
            orderItem.setOrderNo(order.getOrderNo());
            orderItem.setUserId(orderDTO.getUserId());
            orderItem.setProductId(cartVO.getProductId());
            orderItem.setProductName(cartVO.getProductName());
            orderItem.setCurrentUnitPrice(cartVO.getPrice());
            orderItem.setQuantity(cartVO.getQuantity());
            //保留两位小数
            BigDecimal price = cartVO.getPrice().multiply(new BigDecimal(cartVO.getQuantity()));
            BigDecimal totalPrice = price.setScale(2, BigDecimal.ROUND_HALF_UP);
            orderItem.setTotalPrice(totalPrice);

            orderItemMapper.insertSelective(orderItem);
            //所有商品总价
            payment = payment.add(totalPrice);
        }
        order.setPayment(payment);
        //运费先默认0元
        order.setPostage(GlobalConstant.POSTAGE);
        order.setStatus(10);

        return mallOrderMapper.insertSelective(order) == 1 ? true:false;
    }

}
