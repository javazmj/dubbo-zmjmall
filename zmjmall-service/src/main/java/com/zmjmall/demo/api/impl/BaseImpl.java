package com.zmjmall.demo.api.impl;

import com.zmjmall.demo.dao.SysAreaMapper;
import com.zmjmall.demo.model.Address;
import com.zmjmall.demo.model.SysArea;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

/**
 * @author zmj
 * @version 2018/7/16
 */
public class BaseImpl {

    @Autowired
    private SysAreaMapper sysAreaMapper;

    public String addressNameByCode(Address add) {
        String code1 = add.getReceiverProvince() + add.getReceiverCity();
        String code2 = add.getReceiverProvince() + add.getReceiverCity() + add.getReceiverDistrict();

        List<String> codeList = Arrays.asList(add.getReceiverProvince(), code1, code2);
        SysArea sysArea = sysAreaMapper.findNameByCodeList(codeList);
        return sysArea.getName().replace(",","") +  add.getReceiverAddress();
    }
}
