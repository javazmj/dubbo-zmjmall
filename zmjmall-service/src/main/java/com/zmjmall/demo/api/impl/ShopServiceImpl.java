package com.zmjmall.demo.api.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zmjmall.demo.api.ShopService;
import com.zmjmall.demo.model.Shop;
import com.zmjmall.demo.repository.ShopRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by: meijun
 * Date: 2018/7/8 16:33
 */
@Service
@Slf4j
public class ShopServiceImpl implements ShopService {

    @Autowired
    private ShopRepository shopRepository;

    @Override
    public Shop findByUserId(String userId) {
        return shopRepository.findByUserIdEquals(userId);
    }
}
