package com.zmjmall.demo.repository;

import com.zmjmall.demo.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/7/2 7:45
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository repository;


    @Test
    public void findByIdEquals() throws Exception {

        Product product = repository.findByIdEquals("1");
        log.info("product ==== {}",product.toString());
        Assert.assertNotNull(product);
    }

}