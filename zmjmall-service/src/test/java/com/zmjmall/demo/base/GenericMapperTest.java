package com.zmjmall.demo.base;

import com.zmjmall.demo.dao.ProductMapper;
import com.zmjmall.demo.model.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by: meijun
 * Date: 2018/7/2 7:38
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GenericMapperTest {

    @Autowired
    private ProductMapper mapper;

    @Test
    public void insert() throws Exception {
        Product product = new Product();
        product.setId("09894");
        product.setBrandId("5313");
        product.setCategoryId("654113");
        product.setShopId("53433");
        product.setMainImage("/***.img");
        product.setDelFlag("0");
        product.setProductName("adidas 风衣TH11");
        product.setProdcutCode(new Integer(1315));

        mapper.insert(product);
    }

}