package com.zmjmall.demo.api.impl;

import com.zmjmall.demo.api.ProductService;
import com.zmjmall.demo.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by: meijun
 * Date: 2018/6/30 20:48
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ProductServiceImplTest {

    @Autowired
    private ProductService productService;

    @Test
    public void findAll() throws Exception {
        /*List<Product> list = productService.findAll();
        Assert.assertNotEquals(0,list.size());*/
        Sort sort = new Sort(Sort.Direction.DESC, "prodcutCode");
        Pageable pageable = new PageRequest(0, 2, sort);
        Page<Product> productPage = productService.findAllByPage(pageable);
        System.out.println("==================================");
        log.info(productPage.getContent().toString());
        System.out.println("==================================");
    }

    @Test
    public void findById() throws Exception {
        Product product = productService.findById("1");
        Assert.assertNotNull(product);

    }

}