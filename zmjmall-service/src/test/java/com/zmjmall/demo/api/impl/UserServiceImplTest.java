package com.zmjmall.demo.api.impl;

import com.zmjmall.demo.api.UserService;
import com.zmjmall.demo.model.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * @author zmj
 * @version 2018/7/10
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @Test
    public void insert() throws Exception {
        User user = new User();
        user.setPhone("13803502520");
        boolean b = userService.insert(user);
        Assert.assertEquals(true,b);
    }

}