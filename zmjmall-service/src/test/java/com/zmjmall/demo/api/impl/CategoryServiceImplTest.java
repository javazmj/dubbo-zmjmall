package com.zmjmall.demo.api.impl;

import com.zmjmall.demo.api.CategoryService;
import com.zmjmall.demo.model.Category;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author zmj
 * @version 2018/7/2
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class CategoryServiceImplTest {

    @Autowired
    private CategoryService categoryService;

    @Test
    public void findAll() throws Exception {
        List<Category> list = categoryService.findAll();
        log.info("分类总长度：size = {}",list.size());
        Assert.assertNotEquals(1,list.size());
    }

    @Test
    public void findById() throws Exception {
        Category category = categoryService.findById("2");
        log.info(category.toString());
        Assert.assertNotNull(category);
    }

}