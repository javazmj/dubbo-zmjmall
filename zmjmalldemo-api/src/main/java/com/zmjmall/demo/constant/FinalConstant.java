package com.zmjmall.demo.constant;

/**
 * @author zmj
 * @version 2018/7/4
 */
public interface FinalConstant {

    String MOGILE_REGEX = "^1[3|4|5|7|8]\\d{9}$";

    String SORT_ASC = "ASC";

    String SORT_DESC = "DESC";

}
