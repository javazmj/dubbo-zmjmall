package com.zmjmall.demo.constant;


public interface RedisConstant {

    String TOKEN_PREFIX = "token_%s";

    Integer EXPIRE = 7200; //2小时

    String LOGIN_CODE_PREFIX = "logincode"; //登录验证码

    String REGISTER_CODE_PREFIX = "registercode"; //注册验证码

    Integer CODE_EXPIRE = 60 * 2; //失效时间120秒
}
