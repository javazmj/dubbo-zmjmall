package com.zmjmall.demo.constant;

/**
 * Created by: meijun
 * Date: 2018/7/16 10:15
 */
public interface GlobalConstant {
    //收获地址默认不选中（1不选中，0选中）
    Integer ADDRESS_DEFAULT = 1;
    //购物车是否选中（1选中，0未选中）
    Integer CART_CHECKED = 1;
    //运费
    Integer POSTAGE = 0;
    //店铺是否审核(1审核，0通过)
    Integer SHOP_AUDIT =  1;
}
