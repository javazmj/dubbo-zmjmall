package com.zmjmall.demo.api;

import com.zmjmall.demo.model.Shop;

/**
 * Created by: meijun
 * Date: 2018/7/8 16:33
 */
public interface ShopService {

    Shop findByUserId(String userId);
}
