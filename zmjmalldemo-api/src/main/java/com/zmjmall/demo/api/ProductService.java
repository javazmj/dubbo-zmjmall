package com.zmjmall.demo.api;

import com.zmjmall.demo.dto.ProductDTO;
import com.zmjmall.demo.model.Product;
import com.zmjmall.demo.model.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author zmj
 * @version 2018/6/29
 */
public interface ProductService{

    List<Product>  findAll();

    Product findById(String id);

    Page<Product> findAllByPage(@Param("pageable") Pageable pageable);

    List<Product> findAllPage(int pageNum, int pageSize,String sort);

    int insert(ProductDTO productDTO);

    void delete(String id);
}
