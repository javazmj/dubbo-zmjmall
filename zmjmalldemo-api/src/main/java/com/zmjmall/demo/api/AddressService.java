package com.zmjmall.demo.api;

import com.zmjmall.demo.dto.AddressDTO;
import com.zmjmall.demo.vo.AddressVO;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/11
 */
public interface AddressService {

    List<AddressVO> findByUserId(String userId);

    int save(AddressDTO addressDTO);
}
