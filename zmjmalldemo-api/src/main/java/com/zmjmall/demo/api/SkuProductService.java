package com.zmjmall.demo.api;

import com.zmjmall.demo.dto.SkuProductDTO;
import com.zmjmall.demo.model.SkuProduct;
import com.zmjmall.demo.vo.SkuProductVO;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/9
 */
public interface SkuProductService {

    boolean insert(SkuProduct skuProduct);

    List<SkuProductVO> findAllByPage(SkuProductDTO skuProductDTO, int pageNum, int pageSize, String sort);

    SkuProductVO findById(SkuProductDTO skuProductDTO);

    List<SkuProductVO> findAll();
}
