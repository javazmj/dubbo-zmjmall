package com.zmjmall.demo.api;

import com.zmjmall.demo.dto.LoginPasswordDTO;
import com.zmjmall.demo.model.User;

/**
 * @author zmj
 * @version 2018/7/3
 */
public interface UserService {

    User getByPhone(String iphone);

    User getUser(User user);

    User toLogin(User user);

    void updateLoginInfo(User user);

    boolean insert(User user);

    boolean updateUserInfo(User user);

    User loginByPssword(LoginPasswordDTO loginPasswordDTO);
}
