package com.zmjmall.demo.api;

import com.zmjmall.demo.model.SysArea;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/12
 */
public interface SysAreaService {

    List<SysArea> findByParentId(String id);

    SysArea findByCode(String addressCode);
}
