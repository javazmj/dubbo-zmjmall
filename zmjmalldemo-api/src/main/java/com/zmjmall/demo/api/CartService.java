package com.zmjmall.demo.api;

import com.zmjmall.demo.model.Cart;
import com.zmjmall.demo.vo.CartVO;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/11
 */
public interface CartService {

    List<CartVO> findByUserId(String userId);

    CartVO findByUserIdAndProductId(Cart cart);

    int insert(Cart cart);

    int update(Cart cart);

    int delete(Cart cart);

    int updateChecked(String userId,List<String> idsList);
}
