package com.zmjmall.demo.api;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/2
 */

public interface BaseService<T,P,K>{

    /**
     * 查询所有分类
     */
    List<T> findAll();

    /**
     * 根据ID查询分类
     * @param id
     * @return
     */
    T findById(K id);

    /**
     * 分页查询
     * @param pageNum
     * @param pageSize
     * @param sort
     * @return
     */
    List<T> findAllPage(P pageNum, P pageSize, K sort);
}
