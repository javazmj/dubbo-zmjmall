package com.zmjmall.demo.api;

import com.zmjmall.demo.dto.OrderDTO;

/**
 * @author zmj
 * @version 2018/7/11
 */
public interface MallOrderService {

    boolean createOrder(OrderDTO orderDTO);
}
