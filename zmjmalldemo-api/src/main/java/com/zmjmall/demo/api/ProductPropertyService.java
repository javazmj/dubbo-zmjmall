package com.zmjmall.demo.api;

import com.zmjmall.demo.vo.SkuAttributesVO;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/9
 */
public interface ProductPropertyService {

    List<SkuAttributesVO> findByCategoryId(String categoryId);
}
