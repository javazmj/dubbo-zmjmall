package com.zmjmall.demo.api;

import com.zmjmall.demo.model.Category;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/2
 */
public interface CategoryService{

    /**
     * 查询所有分类
     */
    List<Category> findAll();

    /**
     * 根据ID查询分类
     * @param id
     * @return
     */
    Category findById(String id);

    /**
     * 根据父类id查询分类
     * @param pid
     * @return
     */
    List<Category> findByPid(String pid);

    List<Category> findMenuLeave(Category category);

    List<Category> findChildCategory();
}
