package com.zmjmall.demo.api;

import com.zmjmall.demo.model.ProductAttrbute;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/9
 */
public interface ProductAttrbuteService {

    List<ProductAttrbute> findAll();
}
