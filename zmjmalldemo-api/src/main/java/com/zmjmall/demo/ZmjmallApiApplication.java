package com.zmjmall.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zmj
 * @version 2018/7/18
 */
@SpringBootApplication
public class ZmjmallApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZmjmallApiApplication.class, args);
    }
}
