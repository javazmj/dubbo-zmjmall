package com.zmjmall.demo.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author zmj
 * @version 2018/1/22
 */
public class MyConstraintVlaidator implements ConstraintValidator<MyConstraint,Object> {


    public void initialize(MyConstraint constraintAnnotation) {
        System.out.println("my validator init...");
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        System.out.println(value);

        return false;
    }
}
