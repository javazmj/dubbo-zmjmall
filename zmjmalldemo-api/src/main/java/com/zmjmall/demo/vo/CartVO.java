package com.zmjmall.demo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author zmj
 * @version 2018/7/12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CartVO implements Serializable{

    private static final long serialVersionUID = 7768866903954853667L;

    /**
     *  SKU id
     */
    private String id;
    /**
     * 商品id
     */
    private String productId;
    /**
     * 商品名称(返回的是skuProductName)
     */
    private String productName;
    /**
     * 分类ID
     */
    private String categoryId;
    /**
     * 商品主图
     */
    private String mainImage;
    /**
     * 商品属性值 格式：
     * 属性id:属性值id1/属性值id2,属性id:属性值id1/属性值id2
     */
    private String attributeValues;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 数量
     */
    private Integer quantity;
    /**
     * 是否选择,1=已勾选,0=未勾选
     */
    private Integer checked;
}
