package com.zmjmall.demo.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author zmj
 * @version 2018/7/10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SkuProductVO implements Serializable {

    private static final long serialVersionUID = 5625060164242881529L;
    /**
     *  id
     */
    private String id;
    /**
     *
     */
    private String productId;
    /**
     * 商品sku
     */
    @JsonIgnore
    @JSONField(serialize = false)
    private String sku;
    /**
     * 商品名称(返回的是skuProductName)
     */
    private String productName;
    /**
     * 商品副标题
     */
    private String title;
    /**
     * 分类ID
     */
    private String categoryId;
    /**
     * 分类名称
     */
    private String categoryName;

    /**
     * 店铺ID
     */
    private String shopId;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 品牌ID
     */
    private String brandId;


    private String brandName;
    /**
     * 商品主图
     */
    private String mainImage;
    /**
     * 商品类型
     * （1单品2赠品3套餐）
     */
    private Integer productType;
    /**
     * 是否审核通过
     */
    private Integer audit;
    /**
     *
     * 商品详情补充
     */
    private String detail;
    /**
     * 图片地址,json格式,扩展用
     */
    private String subImages;
    /**
     * 商品属性值 格式：
     * 属性id:属性值id1/属性值id2,属性id:属性值id1/属性值id2
     */
    private String attributeValues;
    /**
     * 是否上架
     */
    private String ground;
    /**
     * 库存量
     */
    private String stock;

    private BigDecimal price;

}
