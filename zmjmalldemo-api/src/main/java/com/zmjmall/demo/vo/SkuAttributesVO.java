package com.zmjmall.demo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zmj
 * @version 2018/7/11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SkuAttributesVO implements Serializable {

    private static final long serialVersionUID = 8224796469460004110L;

    private String attributeId;

    private String propertyName;

    private String attributeName;

    private String propertyId;
}
