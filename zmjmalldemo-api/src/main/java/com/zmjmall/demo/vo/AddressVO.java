package com.zmjmall.demo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zmj
 * @version 2018/7/12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressVO implements Serializable{

    private static final long serialVersionUID = -3863454248797654301L;

    private String id;


    private String userId;


    private String receiverName;


    private String receiverMobile;

    /**
     * 省市区 + 详细地址拼接
     */
    private String addressInfo;

    /**
     * 是否默认选中
     */
    private Integer isDefault;
}
