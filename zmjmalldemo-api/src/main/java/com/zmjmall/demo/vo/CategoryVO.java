package com.zmjmall.demo.vo;

import com.zmjmall.demo.api.TreeEntity;
import lombok.Data;

import java.util.List;

/**
 * Created by yide on 2018/7/9.
 */
@Data
public class CategoryVO implements TreeEntity<CategoryVO> {

    public String id;

    public String name;

    public String parentId;

    public String img;

    public List<CategoryVO> children;

}
