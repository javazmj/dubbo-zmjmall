package com.zmjmall.demo.vo;

import com.zmjmall.demo.model.Product;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by: meijun
 * Date: 2018/7/8 17:13
 */
@Data
public class ShopVO implements Serializable {

    private static final long serialVersionUID = -1957591938384877813L;

    private String id;

    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 所属用户
     */
    private String userId;
    /**
     * 店铺编码
     */
    private Integer shopCode;
    /**
     * 是否审核通过(0未审核通过,1审核通过)
     */
    private Integer audit;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 商品总数
     */
    private Integer productSum;
    /**
     * 上架商品总数
     */
    private Integer productGroundSum;
    /**
     * 待审核商品总数
     */
    private Integer productWaitGroundSum;
    /**
     * 商品列表
     */
    private List<Product> productList;
}
