package com.zmjmall.demo.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author zmj
 * @version 2018/7/10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SkuProductDTO implements Serializable {

    private static final long serialVersionUID = -8792046192278246486L;
    /**
     *  商品id
     */
    private String id;
    /**
     *  商品基本信息id
     */
    private String productId;
    /**
     * 商品sku
     */
    private String sku;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 商品副标题
     */
    private String title;
    /**
     * 分类ID
     */
    private String categoryId;
    /**
     * 分类名称
     */
    private String categoryName;

    /**
     * 店铺ID
     */
    private String shopId;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 品牌ID
     */
    private String brandId;


    private String brandName;
    /**
     * 商品类型
     * （1单品2赠品3套餐）
     */
    private Integer productType;
    /**
     * 商品属性值 格式：
     * 属性id:属性值id1/属性值id2,属性id:属性值id1/属性值id2
     */
    private String attributeValues;
    /**
     * 价格排序
     */
    private BigDecimal price;
    /**
     * 商品最低价格
     */
    private BigDecimal minPrice;
    /**
     * 商品最高价格
     */
    private BigDecimal maxPrice;
}
