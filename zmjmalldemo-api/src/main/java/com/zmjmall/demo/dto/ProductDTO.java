package com.zmjmall.demo.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.zmjmall.demo.model.Product;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *  商品信息传输实体类
 * @author zmj
 * @version 2018/7/3
 */
@Data
@NoArgsConstructor
@ToString
public class ProductDTO implements Serializable {

    private static final long serialVersionUID = -4856177066037025950L;

    /**
     * 商品名称
     */
    private String productName;
    /**
     * 分类ID
     */
    private String categoryId;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 店铺ID
     */
    private String shopId;
    /**
     * 品牌ID
     */
    private String brandId;
    /**
     * 商品主图
     */
    private String mainImage;
    /**
     * 商品类型
     * （1单品2赠品3套餐）
     */
    private Integer productType;
    /**
     * 商品详情补充
     */
    private String detail;
    /**
     * 图片地址,json格式,扩展用
     */
    private String subImages;
    /**
     * 商品属性值 格式：
     * 属性id:属性值id1/属性值id2,属性id:属性值id1/属性值id2
     */
    private String attributeValues;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 库存
     */

    private Integer stock;
}
