package com.zmjmall.demo.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zmj
 * @version 2018/7/3
 */
@Data
@ToString
public class UserDTO implements Serializable{

    private static final long serialVersionUID = 5835407928083498720L;

    public interface UserDTOSimpleView {};
    public interface UserDTODetailView extends UserDTO.UserDTOSimpleView {};

    @NotNull(message = "id不能为空")
    private String id;
    /**
     * 用户昵称
     */
    @NotBlank(message = "")
    @Length(min = 1,max = 32,message = "用户昵称长度不合法")
    private String nickName;
    /**
     * 用户头像
     */
    private String head;
    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空")
    @Length(min = 11,max = 11,message = "手机号长度错误")
    @Pattern(regexp = "^1[3|4|5|7|8]\\d{9}$",message = "手机号格式不正确")
    private String phone;

    /**
     * 邮箱
     */
    @Email(message = "不是一个合法的邮箱")
    private String email;
    /**
     * 登录密码
     */
    @JSONField(serialize = false)
    @JsonIgnore
    private String password;
}
