package com.zmjmall.demo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 订单传输实体类
 * @author zmj
 * @version 2018/7/16
 */
@Data
@NoArgsConstructor
@ToString
public class OrderDTO implements Serializable{

    private static final long serialVersionUID = -8861078198406029114L;

    /**
     * 收货地址id
     */
    @NotNull(message = "收货地址不能为空")
    private String addressId;
    /**
     * 用户id
     */
    @NotNull(message = "用户id不能为空")
    private String userId;
    /**
     * 实际付款金额,单位是元,保留两位小数
     */
    private BigDecimal payment;
    /**
     * 运费,单位是元
     */
    private Integer postage;
}
