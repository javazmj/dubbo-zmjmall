package com.zmjmall.demo.dto;

import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author zmj
 * @version 2018/7/3
 */
@Data
@ToString
public class LoginPasswordDTO implements Serializable{

    private static final long serialVersionUID = 5835407928083498720L;

    public interface UserDTOSimpleView {};
    public interface UserDTODetailView extends LoginPasswordDTO.UserDTOSimpleView {};

    @NotBlank(message = "手机号不能为空")
    @Length(min = 11,max = 11,message = "手机号长度错误")
    @Pattern(regexp = "^1[3|4|5|7|8]\\d{9}$",message = "手机号格式不正确")
    private String phone;


    private String email;

    @NotBlank(message = "密码不能为空")
    @Length(min = 6,max = 16,message = "密码长度为6-16位")
    private String password;


    private String code;
}
