package com.zmjmall.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author zmj
 * @version 2018/7/12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressDTO implements Serializable{

    private static final long serialVersionUID = 2121964427954205408L;

    private String userId;

    @NotNull(message = "收货人不能为空")
    private String receiverName;

    @Length(min = 11,max = 11,message = "手机号长度错误")
    @Pattern(regexp = "^1[3|4|5|7|8]\\d{9}$",message = "手机号格式不正确")
    private String receiverMobile;

    /**
     * 省市区编码
     */
    @Length(min = 6,max = 6,message = "地区编码错误")
    private String addressCode;

    /**
     * 详细地址
     */
    @NotNull(message = "详细地址不能为空")
    private String receiverAddress;

    /**
     * 是否默认选中
     */
    private Integer isDefault;
}
