package com.zmjmall.demo.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString
public class Address extends BaseModel implements Serializable {

    private static final long serialVersionUID = -9032496436401122643L;

    @Id
    @GeneratedValue
    private String id;


    @JsonIgnore
    @JSONField(serialize = false)
    private String userId;


    private String receiverName;

    @JsonIgnore
    @JSONField(serialize = false)
    private String receiverPhone;


    private String receiverMobile;


    private String receiverProvince;


    private String receiverCity;


    private String receiverDistrict;


    private String receiverAddress;

    @JsonIgnore
    @JSONField(serialize = false)
    private String receiverZip;


    private Integer isDefault;
}