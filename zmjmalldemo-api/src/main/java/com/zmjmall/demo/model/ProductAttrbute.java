package com.zmjmall.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString
public class ProductAttrbute extends BaseModel implements Serializable {

    private static final long serialVersionUID = 683547483845661768L;

    @Id
    @GeneratedValue
    private String id;


    private String propertyId;


    private String attributeName;


    private String attributeVale;


    private Integer attributeCode;


    private String description;
}