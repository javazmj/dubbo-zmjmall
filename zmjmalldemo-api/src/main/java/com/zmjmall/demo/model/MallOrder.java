package com.zmjmall.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString
public class MallOrder extends BaseModel implements Serializable {

    private static final long serialVersionUID = -1842593870294267601L;

    @Id
    @GeneratedValue
    private String id;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 收货人
     */
    private String receiverName;
    /**
     * 收货电话
     */
    private String receiverMobile;
    /**
     * 详细地址
     */
    private String receiverAddress;
    /**
     * 实际付款金额,单位是元,保留两位小数
     */
    private BigDecimal payment;
    /**
     * 支付类型,1-在线支付
     */
    private Integer paymentType;
    /**
     * 运费,单位是元
     */
    private Integer postage;
    /**
     * 订单状态:0-已取消-10-未付款，20-已付款，40-已发货，50-交易成功，60-交易关闭
     */
    private Integer status;
    /**
     * 支付时间
     */
    private Date paymentTime;
    /**
     * 发货时间
     */
    private Date sendTime;
    /**
     * 交易完成时间
     */
    private Date endTime;
    /**
     *交易关闭时间
     */
    private Date closeTime;

}