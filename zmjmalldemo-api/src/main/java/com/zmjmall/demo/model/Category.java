package com.zmjmall.demo.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString
public class Category extends BaseModel implements Serializable {

    private static final long serialVersionUID = 2311693906459079995L;

    @Id
    @GeneratedValue
    public String id;
    /**
     * 父级ID
     */
    private String pid;
    /**
     * 分类名称
     */
    private String categoryName;
    /**
     * 分类类型
     */
    private String categoryType;
    /**
     * 是否显示
     */
    private Integer isShow;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 分类图片
     */
    private String image;
}