package com.zmjmall.demo.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
//@Table(name = "mall_user")
public class User extends BaseModel implements Serializable {

    private static final long serialVersionUID = 3782183930278628182L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private String id;
    /**
     * 用户昵称
     */
    private String nickName;
    /**
     * 用户头像
     */
    private String head;
    /**
     * 手机号
     */
    @Length(min = 11,max = 11,message = "手机号长度错误")
    @Pattern(regexp = "^1[3|4|5|7|8]\\d{9}$",message = "手机号格式不正确")
    private String phone;

    /**
     * 邮箱
     */
    private String email;
    /**
     * 登录密码
     */
    @JSONField(serialize = false)
    @JsonIgnore
    private String password;
    /**
     * 盐
     */
    @JsonIgnore
    @JSONField(serialize = false)
    private String salt;
    /**
     * 登录方式
     * 1PC端，2安卓，3ios
     */
    @JSONField(serialize = false)
    @JsonIgnore
    private Integer loginType;
    /**
     * 最后一次登录时间
     */
    @JSONField(serialize = false)
    @JsonIgnore
    private Date lastLoginTime;
    /**
     * 登录次数
     */
    @JSONField(serialize = false)
    @JsonIgnore
    private Integer loginCount;

}