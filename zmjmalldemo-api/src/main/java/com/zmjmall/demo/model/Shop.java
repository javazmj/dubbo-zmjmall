package com.zmjmall.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString
public class Shop extends BaseModel implements Serializable{

    private static final long serialVersionUID = 6521625863177949745L;

    @Id
    @GeneratedValue
    private String id;
    /**
     * 店铺名称
     */
    private String shopName;
    /**
     * 所属用户
     */
    private String userId;
    /**
     * 店铺编码
     */
    private Integer shopCode;
    /**
     * 是否审核通过(0未审核通过,1审核通过)
     */
    private Integer audit;
}