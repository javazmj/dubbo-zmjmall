package com.zmjmall.demo.model;

import com.alibaba.fastjson.annotation.JSONType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString
@JSONType(includes = {"id","parentId","name","sort","code"})
public class SysArea extends BaseModel implements Serializable {

    private static final long serialVersionUID = 5078888855083496713L;

    @Id
    @GeneratedValue
    private String id;


    private String parentId;


    private String parentIds;


    private String name;


    private Long sort;


    private String code;


    private String type;


    private String createBy;


    private String updateBy;


    private String remarks;


}