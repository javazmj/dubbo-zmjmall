package com.zmjmall.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString
public class OrderItem extends BaseModel implements Serializable {

    private static final long serialVersionUID = -6045557550998851326L;

    @Id
    @GeneratedValue
    private String id;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 商品id(sku id)
     */
    private String productId;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 商品图片地址
     */
    private String productImage;
    /**
     * 生成订单时的商品单价，单位是元,保留两位小数
     */
    private BigDecimal currentUnitPrice;
    /**
     * 商品数量
     */
    private Integer quantity;
    /**
     * 商品总价,单位是元,保留两位小数
     */
    private BigDecimal totalPrice;
}