package com.zmjmall.demo.model;


import com.fasterxml.jackson.annotation.JsonView;
import com.zmjmall.demo.validator.MyConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString
public class Product extends BaseModel implements Serializable {


    private static final long serialVersionUID = -513971319628721704L;

    public interface ProductSimpleView {};
    public interface ProductDetailView extends ProductSimpleView {};

    /**
     * 主键ID
     */
    @Id
    @GeneratedValue
    @JsonView(ProductSimpleView.class)
    private String id;

    /**
     * 商品名称
     */
    @MyConstraint(message = "测试自定义校验...............")
    @JsonView(ProductSimpleView.class)
    private String productName;
    /**
     * 商品编码
     */
    private Integer prodcutCode;
    /**
     * 分类ID
     */
    @JsonView(ProductSimpleView.class)
    private String categoryId;
    /**
     * 店铺ID
     */
    @JsonView(ProductSimpleView.class)
    private String shopId;
    /**
     * 品牌ID
     */
    private String brandId;
    /**
     * 商品主图
     */
    @JsonView(ProductSimpleView.class)
    private String mainImage;
    /**
     * 商品类型
     * （1单品2赠品3套餐）
     */
    private Integer productType;
    /**
     * 商品详情补充
     */
    private String detail;

    /**
     * 图片地址,json格式,扩展用
     */
    private String subImages;
}