package com.zmjmall.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString
public class SkuProduct extends BaseModel implements Serializable {

    private static final long serialVersionUID = 7799776043285546389L;

    @Id
    @GeneratedValue
    private String id;


    private String productId;


    private String sku;


    private Integer skuCode;


    private String skuProductName;


    private String attributeValues;


    private String title;


    private BigDecimal price;


    private Integer stock;


    private String ground;

}