package com.zmjmall.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString
public class Cart extends BaseModel implements Serializable {

    private static final long serialVersionUID = 7256620750666400348L;

    @Id
    @GeneratedValue
    private String id;

//    @NotNull(message = "用户ID不能为空")
    private String userId;
    /**
     * sku商品id
     */
    @NotNull(message = "商品ID不能为空")
    private String productId;
    /**
     * 数量
     */
    @Min(value = 1,message = "商品数量不能少于1个" )
    private Integer quantity;
    /**
     * 是否选择,1=已勾选,0=未勾选
     */
    private Integer checked;
}