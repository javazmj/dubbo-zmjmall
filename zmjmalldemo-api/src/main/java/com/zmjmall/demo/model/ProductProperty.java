package com.zmjmall.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString
public class ProductProperty extends BaseModel implements Serializable {

    private static final long serialVersionUID = -275773293743310342L;

    @Id
    @GeneratedValue
    private String id;


    private String categoryId;


    private String propertyName;


    private Integer propertyCode;


    private String description;


    private Integer sort;


    private Integer issku;


    private Integer issearch;


    private Integer isshow;

}