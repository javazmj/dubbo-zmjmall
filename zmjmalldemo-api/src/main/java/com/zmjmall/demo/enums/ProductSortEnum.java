package com.zmjmall.demo.enums;

import lombok.Getter;

/**
 * 排序字典
 * Created by: meijun
 * Date: 2017/12/20 16:36
 */
@Getter
public enum ProductSortEnum implements CodeEnum {

    SORT_UPDATE_TIME(0,"sp.update_time"),
    SORT_STOCK(1,"sp.stock"),
    SORT_PRICE(2,"sp.price"),
    ;

    private Integer code;

    private String msg;

    ProductSortEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
