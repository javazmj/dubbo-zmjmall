package com.zmjmall.demo.enums;

import lombok.Getter;

/**
 * Created by: meijun
 * Date: 2017/12/20 16:36
 */
@Getter
public enum DictEnum {
    SORT_ASC(0,"ASC"),
    SORT_DICT(1,"DESC"),
    ;

    private Integer code;

    private String msg;

    DictEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
