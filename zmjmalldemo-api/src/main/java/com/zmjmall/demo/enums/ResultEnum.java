package com.zmjmall.demo.enums;

import lombok.Getter;

/**
 * Created by: meijun
 * Date: 2017/12/20 16:36
 */
@Getter
public enum ResultEnum {

    SUCCESS(200,"成功"),
    RESTFUL_API_NOTFOUND(404,"未提供该接口"),
    RESTFUL_API_ERROR(500,"服务器内部错误"),



    PARAM_ERROR(1,"参数不正确"),
    PRODUCT_NOT_EXIST(10,"该商品不存在"),
    PRODUCT_STOCK_ERROR(11,"库存不足"),
    ORDER_NOT_EXIST(12,"订单不存在"),
    ORDER_DETAIL_EXIST(13,"订单详情不存在"),
    ORDER_STATUS_ERROR(14,"订单状态不正确"),
    ORDER_UPDATE_ERROR(15,"订单更新失败"),
    ORDER_DETAIL_EMPTY(16,"订单详情为空"),
    ORDER_PAY_STATUS_ERROR(17,"订单支付状态不正确"),
    CART_EMPTY(18,"购物车为空"),
    ORDER_OWNER_ERROR(19,"该订单不属于当前用户"),
    WX_MP_ERRROR(20,"微信公众账号方面错误"),
    WXPAY_NOTIFY_MONEY_VERIFY_ERROR(21,"微信支付异步通知校验不通过"),
    ORDER_CANCEL_SUCCESS(22,"订单取消成功"),
    ORDER_FINISH_SUCCESS(23,"订单完结成功"),
    PRODUCT_STATUS_ERROR(24,"商品状态不正确"),
    LOGIN_FAIL(25,"登录失败,登录信息不正确"),
    SHOP_NOTCREATE(26,"您还未创建店铺"),
    SHOP_WAIT_AUDITING(27,"您的店铺正在审核中"),
    CART_NOT_PRODECT(28,"购物车为空"),
    NOT_ADDRESS(29,"您还未添加收货地址"),
    LOGIN_ERROR(30,"账号或密码错误"),
    HEAD_NOT_IMG(31,"请上传图片"),




    FAILURE_NOTCOOKILE_TOKEN(-1,"请先登录"),
    FAILURE_NOTREDIS_TOKEN(-2,"请先登录"),
    LOGIN_NOT_REGISTER(-3,"该账号未注册"),
    LOGIN_CODE_ERROR(-4,"验证码错误或已失效"),
    REGISTER_ISHAVE(-5,"该手机号已注册"),
    MOBILE_EERROR(-6,"手机号格式错误"),
    OPERATE_ERROR(-10,"失败"),

    ;



    private Integer code;

    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
