package com.zmjmall.demo.enums;

/**
 * Created by: meijun
 * Date: 2018/3/12 20:04
 */
public interface CodeEnum {

    Integer getCode();
}
