package com.zmjmall.demo.util;

import java.util.UUID;

/**
 * @author zmj
 * @version 2018/6/21
 */
public class UUIDUtil {

    public static String uuid() {
        return UUID.randomUUID().toString().replace("-","");
    }

}


