package com.zmjmall.demo.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.zmjmall.demo.vo.SkuAttributesVO;

/**
 * Created by: meijun
 * Date: 2017/12/22 20:31
 */
public class JsonUtil {

    /**
     * @param object
     * @return
     */
    public  static  String  toJson (Object object) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        return  gson.toJson(object);
    }


    public static Object toJsonIgnore (Object object) {
        return JSON.toJSON(object);
    }
    /**
     *
     * @param str
     * @param t
     * @return
     */
    public static String toListString(String str,Class t) {
        return JSONArray.parseArray(str, t).toString();
    }
}
