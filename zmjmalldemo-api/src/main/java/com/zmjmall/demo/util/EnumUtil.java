package com.zmjmall.demo.util;


import com.zmjmall.demo.enums.CodeEnum;

/**
 * Created by: meijun
 * Date: 2018/3/12 20:07
 */
public class EnumUtil {

    public static <T extends CodeEnum> T getByCode(Integer code, Class<T> enumClass) {
        for (T each: enumClass.getEnumConstants()) {
            if (code.equals(each.getCode())) {
                return each;
            }
        }
        return null;
    }
}
