package com.zmjmall.demo.util;

import org.springframework.util.Base64Utils;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *  商品SKU生成策略
 * @author zmj
 * @version 2018/7/10
 */
public class SKUGenerator {

    private static final String SPLIT = "/";

    /**
     * sku生成规则
     * shopId/ + 分类ID/ + productID/ + 属性ID(排序asc)
     * @param strings
     * @return
     */
    public static String createSKU(String ...strings){
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < strings.length; i++) {
            sb.append(strings[i] + SPLIT);
        }
        try {
           return Base64Utils.encodeToString(sb.toString().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 产品标题截取 -
     * @param name
     * @return
     */
    public static String nameSpilt(String name) {
        return name.substring(0,name.lastIndexOf("-"));
    }

    /**
     * 将属性asc排序
     * @param attributes
     * @return
     */
    public static String attributesASC (String attributes) {
        String[] split = attributes.split("/");
        List<String> stringList = Arrays.asList(split);
        Collections.sort(stringList);
        StringBuilder sb = new StringBuilder();
        for (String s:stringList) {
            sb.append(s + "/") ;
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(createSKU("110","101201","1016940866624417793","1005/1008/"));
    }
}
