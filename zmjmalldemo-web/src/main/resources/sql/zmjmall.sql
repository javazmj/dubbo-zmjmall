/*
Navicat MySQL Data Transfer

Source Server         : 阿里云
Source Server Version : 50713
Source Host           : 47.95.4.224:3306
Source Database       : zmjmall

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2018-07-19 23:15:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for address
-- ----------------------------
DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` varchar(32) NOT NULL,
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `receiver_name` varchar(20) DEFAULT NULL COMMENT '收货姓名',
  `receiver_phone` varchar(20) DEFAULT NULL COMMENT '收货固定电话',
  `receiver_mobile` varchar(20) DEFAULT NULL COMMENT '收货移动电话',
  `receiver_province` varchar(20) DEFAULT NULL COMMENT '省份',
  `receiver_city` varchar(20) DEFAULT NULL COMMENT '城市',
  `receiver_district` varchar(20) DEFAULT NULL COMMENT '区/县',
  `receiver_address` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `receiver_zip` varchar(6) DEFAULT NULL COMMENT '邮编',
  `is_default` int(11) DEFAULT NULL COMMENT '是否为默认收货地址（0默认，1不默认）',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `del_flag` varchar(255) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of address
-- ----------------------------
INSERT INTO `address` VALUES ('1', '1', '周周', '', '15313601723', '山西省', '太原市', '方山县', '方山县啊啊啊啊', null, '0', null, null, '0');

-- ----------------------------
-- Table structure for brand
-- ----------------------------
DROP TABLE IF EXISTS `brand`;
CREATE TABLE `brand` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `brand_name` varchar(128) DEFAULT NULL COMMENT '品牌名称',
  `brand_logo` varchar(128) DEFAULT NULL COMMENT '品牌logo图片地址',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `img` varchar(255) DEFAULT NULL COMMENT '默认图标',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of brand
-- ----------------------------
INSERT INTO `brand` VALUES ('1', 'adidas', '', null, null);
INSERT INTO `brand` VALUES ('2', '七匹狼', null, null, null);
INSERT INTO `brand` VALUES ('3', '美的（Media）', null, null, null);
INSERT INTO `brand` VALUES ('4', 'Haier', null, null, null);
INSERT INTO `brand` VALUES ('5', 'oppo', null, null, null);
INSERT INTO `brand` VALUES ('6', '华为(HuaWei)', null, null, null);
INSERT INTO `brand` VALUES ('7', 'Apple', null, null, null);

-- ----------------------------
-- Table structure for cart
-- ----------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` varchar(32) NOT NULL,
  `user_id` varchar(32) NOT NULL,
  `product_id` varchar(32) DEFAULT NULL COMMENT '商品id',
  `quantity` int(11) DEFAULT NULL COMMENT '数量',
  `checked` int(11) DEFAULT '1' COMMENT '是否选择,1=已勾选,0=未勾选',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cart
-- ----------------------------
INSERT INTO `cart` VALUES ('1', '1', '1016940867291312129', '1', '1', '2018-07-08 16:43:41', '2018-07-08 16:43:43');
INSERT INTO `cart` VALUES ('1017646771254919169', '1017614559998414849', '1016940867291312129', '57', '0', '2018-07-13 13:47:51', '2018-07-13 13:49:47');
INSERT INTO `cart` VALUES ('1017650602747408385', '1017614559998414849', '1016942276418072577', '-11', '0', '2018-07-13 14:03:05', '2018-07-13 14:04:35');
INSERT INTO `cart` VALUES ('2', '1', '1016942028396294145', '10', '1', '2018-07-12 13:23:35', '2018-07-12 13:23:38');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `pid` varchar(32) DEFAULT NULL COMMENT '上级id',
  `category_name` varchar(32) DEFAULT NULL COMMENT '分类名称',
  `category_type` varchar(32) DEFAULT NULL COMMENT '分类类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `image` varchar(255) DEFAULT NULL,
  `is_show` int(11) DEFAULT NULL COMMENT '是否显示(0显示，1不显示)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `del_flag` varchar(255) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1011', '0', '家用电器', null, '2', null, null, null, null, null);
INSERT INTO `category` VALUES ('101101', '1011', '电水壶', null, '1', 'https://img30.360buyimg.com/focus/jfs/t12559/262/969294499/3436/8c0ce9c9/5a17f1d2N8078d5e6.jpg', null, null, null, null);
INSERT INTO `category` VALUES ('101102', '1011', '微波炉', null, '2', 'https://img11.360buyimg.com/focus/jfs/t13267/86/981023661/1871/6fae5f11/5a17f203N50016f64.jpg', null, null, null, null);
INSERT INTO `category` VALUES ('101103', '1011', '冰箱', null, '3', 'https://img12.360buyimg.com/focus/jfs/t13153/44/964603695/1011/21d660d2/5a17f6aeN280056ea.jpg', null, null, null, null);
INSERT INTO `category` VALUES ('1012', '0', '男装', null, '3', null, null, null, null, null);
INSERT INTO `category` VALUES ('101201', '1012', 'T恤', null, '1', 'https://img13.360buyimg.com/focus/jfs/t18436/155/1324938407/6646/1a66cfa0/5ac47fffNe7a93aca.jpg', null, null, null, null);
INSERT INTO `category` VALUES ('101202', '1012', '夹克', null, '2', 'https://img11.360buyimg.com/focus/jfs/t11590/82/2013872051/5874/83b5772d/5a0e947eN67f0e537.jpg', null, null, null, null);
INSERT INTO `category` VALUES ('101203', '1012', '西装', null, '3', 'https://img13.360buyimg.com/focus/jfs/t13489/68/552491077/2495/7b517e4b/5a0e9483Na6231535.jpg', null, null, null, null);
INSERT INTO `category` VALUES ('1013', '0', '女装', null, '4', null, null, null, null, null);
INSERT INTO `category` VALUES ('101301', '1013', '连衣裙', null, '1', 'https://img10.360buyimg.com/focus/jfs/t11791/249/2180710603/1984/b8d02ddf/5a128463Nce1ae193.jpg', null, null, null, null);
INSERT INTO `category` VALUES ('101302', '1013', '卫衣', null, '2', 'https://img11.360buyimg.com/focus/jfs/t15667/299/2238226291/4404/19e817ba/5a9fbff4N02a4be7b.jpg', null, null, null, null);
INSERT INTO `category` VALUES ('101303', '1013', '针织衫', null, '3', 'https://img20.360buyimg.com/focus/jfs/t19030/113/1250466624/3376/b981fe07/5ac48007Nb30b2118.jpg', null, null, null, null);
INSERT INTO `category` VALUES ('1014', '0', '手机数码', null, '1', null, null, null, null, null);
INSERT INTO `category` VALUES ('101401', '1014', '手机', null, '1', 'https://img10.360buyimg.com/focus/jfs/t11503/241/2246064496/4783/cea2850e/5a169216N0701c7f1.jpg', null, null, null, null);
INSERT INTO `category` VALUES ('101402', '1014', '照相机', null, '2', 'https://img11.360buyimg.com/focus/jfs/t11470/45/2362968077/2689/fb36d9a0/5a169238Nc8f0882b.jpg', null, null, null, null);
INSERT INTO `category` VALUES ('101403', '1014', '平板电脑', null, '3', 'https://img11.360buyimg.com/focus/jfs/t11470/45/2362968077/2689/fb36d9a0/5a169238Nc8f0882b.jpg', null, null, null, null);

-- ----------------------------
-- Table structure for mall_order
-- ----------------------------
DROP TABLE IF EXISTS `mall_order`;
CREATE TABLE `mall_order` (
  `id` varchar(32) NOT NULL COMMENT '订单id',
  `order_no` varchar(32) DEFAULT NULL COMMENT '订单号',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `receiver_name` varchar(32) DEFAULT NULL COMMENT '收货人',
  `receiver_mobile` varchar(11) DEFAULT NULL COMMENT '收货手机',
  `receiver_address` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `payment` decimal(20,2) DEFAULT NULL COMMENT '实际付款金额,单位是元,保留两位小数',
  `payment_type` int(4) DEFAULT NULL COMMENT '支付类型,1-在线支付',
  `postage` int(10) DEFAULT NULL COMMENT '运费,单位是元',
  `status` int(10) DEFAULT NULL COMMENT '订单状态:0-已取消-10-未付款，20-已付款，40-已发货，50-交易成功，60-交易关闭',
  `payment_time` datetime DEFAULT NULL COMMENT '支付时间',
  `send_time` datetime DEFAULT NULL COMMENT '发货时间',
  `end_time` datetime DEFAULT NULL COMMENT '交易完成时间',
  `close_time` datetime DEFAULT NULL COMMENT '交易关闭时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of mall_order
-- ----------------------------

-- ----------------------------
-- Table structure for mall_user
-- ----------------------------
DROP TABLE IF EXISTS `mall_user`;
CREATE TABLE `mall_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `head` varchar(255) DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  `login_count` int(11) DEFAULT NULL,
  `login_type` int(11) DEFAULT NULL,
  `nick_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mall_user
-- ----------------------------

-- ----------------------------
-- Table structure for order_item
-- ----------------------------
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item` (
  `id` varchar(32) NOT NULL COMMENT '订单子表id',
  `user_id` varchar(32) DEFAULT NULL,
  `order_no` bigint(20) DEFAULT NULL,
  `product_id` varchar(32) DEFAULT NULL COMMENT '商品id',
  `product_name` varchar(100) DEFAULT NULL COMMENT '商品名称',
  `product_image` varchar(500) DEFAULT NULL COMMENT '商品图片地址',
  `current_unit_price` decimal(20,2) DEFAULT NULL COMMENT '生成订单时的商品单价，单位是元,保留两位小数',
  `quantity` int(10) DEFAULT NULL COMMENT '商品数量',
  `total_price` decimal(20,2) DEFAULT NULL COMMENT '商品总价,单位是元,保留两位小数',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_no_index` (`order_no`) USING BTREE,
  KEY `order_no_user_id_index` (`user_id`,`order_no`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order_item
-- ----------------------------

-- ----------------------------
-- Table structure for pay_info
-- ----------------------------
DROP TABLE IF EXISTS `pay_info`;
CREATE TABLE `pay_info` (
  `id` varchar(32) NOT NULL,
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `order_no` bigint(20) DEFAULT NULL COMMENT '订单号',
  `pay_platform` int(10) DEFAULT NULL COMMENT '支付平台:1-支付宝,2-微信',
  `platform_number` varchar(200) DEFAULT NULL COMMENT '支付宝支付流水号',
  `platform_status` varchar(20) DEFAULT NULL COMMENT '支付宝支付状态',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_info
-- ----------------------------

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `product_name` varchar(100) DEFAULT NULL COMMENT '商品名称',
  `prodcut_code` int(11) DEFAULT NULL COMMENT '商品编码',
  `category_id` varchar(32) DEFAULT NULL COMMENT '分类Id',
  `shop_id` varchar(32) DEFAULT NULL COMMENT '店铺Id',
  `brand_id` varchar(32) DEFAULT NULL COMMENT '品牌id',
  `main_image` varchar(500) DEFAULT NULL COMMENT '商品主图',
  `sub_images` text COMMENT '图片地址,json格式,扩展用',
  `product_type` int(11) DEFAULT NULL COMMENT '商品类型（1单品2赠品3套餐）',
  `detail` text COMMENT '商品详情',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `del_flag` varchar(255) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1016940866624417793', 'adidas 夏天清凉运动T恤 速干 ', null, '101201', '110', '1', '//m.360buyimg.com/mobilecms/s750x750_jfs/t21688/164/1354068148/329651/777b96b0/5b2664b4N813cd54d.jpg!q80.dpg', null, '1', null, null, null, '0');
INSERT INTO `product` VALUES ('1016941362055606274', '七匹狼 纯棉印花T恤 舒适 大方', null, '101201', '110', '2', '//m.360buyimg.com/mobilecms/s750x750_jfs/t19399/222/773216014/143905/78b329c3/5aa66eaeN4cb60e4d.jpg!q80.dpg', null, '1', null, null, null, '0');
INSERT INTO `product` VALUES ('1016942028371128322', '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA', null, '101103', '110', '4', '//m.360buyimg.com/mobilecms/s750x750_jfs/t22777/278/818979203/144223/31aaa355/5b44176cNda84b3de.jpg!q80.dpg', null, '1', null, null, null, '0');
INSERT INTO `product` VALUES ('1016942276371935234', '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)', null, '101103', '110', '3', '//m.360buyimg.com/mobilecms/s750x750_jfs/t20434/158/2012122248/3633970/3fc69dd4/5b440aa9Nf6a0c7a9.jpg!q80.dpg', null, '1', null, null, null, '0');
INSERT INTO `product` VALUES ('1016942961473744897', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机', null, '101401', '110', '7', '//m.360buyimg.com/mobilecms/s750x750_jfs/t17383/125/1470478028/71155/1cb53bc2/5acc5248N6a5f81cd.jpg!q80.dpg', null, '1', null, null, null, '0');
INSERT INTO `product` VALUES ('1016943072513748993', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待', null, '101401', '110', '6', '//m.360buyimg.com/mobilecms/s750x750_jfs/t10315/227/1754541026/256693/980afae7/59e5bdf4Nb6b9904a.jpg!q80.dpg', null, '1', null, null, null, '0');
INSERT INTO `product` VALUES ('1016943209159979009', 'OPPO R15 全面屏双摄拍照手机', null, '101401', '110', '5', '//m.360buyimg.com/mobilecms/s750x750_jfs/t18271/36/780866793/186084/ba760109/5aa63eb3N5cdd612b.jpg!q80.dpg', null, '1', null, null, null, '0');

-- ----------------------------
-- Table structure for product_attrbute
-- ----------------------------
DROP TABLE IF EXISTS `product_attrbute`;
CREATE TABLE `product_attrbute` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `property_id` varchar(32) DEFAULT NULL COMMENT '属性id',
  `attribute_name` varchar(255) DEFAULT NULL,
  `attribute_vale` varchar(32) DEFAULT NULL COMMENT '属性值',
  `attribute_code` int(11) DEFAULT NULL COMMENT '属性值编码',
  `description` varchar(32) DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `del_flag` varchar(255) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of product_attrbute
-- ----------------------------
INSERT INTO `product_attrbute` VALUES ('1000', '11', '三门', '三门', null, '三门', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1001', '11', '十字对开门', '十字对开门', null, '十字对开门', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1002', '11', '双门', '双门', null, '双门', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1003', '12', '风冷（无霜）', '风冷（无霜）', null, '风冷（无霜）', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1004', '12', '风直冷（混冷）', '风直冷（混冷）', null, '风直冷（混冷）', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1005', '13', '修身型', '修身型', null, '修身型', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1006', '13', '标准型', '标准型', null, '标准型', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1007', '13', '宽松型', '宽松型', null, '宽松型', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1008', '14', '长袖', '长袖', null, '长袖', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1009', '14', '七分袖', '七分袖', null, '七分袖', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1010', '14', '短袖', '短袖', null, '短袖', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1011', '15', '4G', '4G', null, '4G', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1012', '15', '8G', '8G', null, '8G', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1013', '15', '6G', '6G', null, '6G', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1014', '16', '64G', '64G', null, '64G', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1015', '16', '128G', '128G', null, '128G', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1016', '16', '256G', '256G', null, '256G', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1017', '17', '5.6英寸及以上', '5.6英寸及以上', null, '5.6英寸及以上', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1018', '17', '5.5-5.1英寸', '5.5-5.1英寸', null, '5.5-5.1英寸', null, null, null);
INSERT INTO `product_attrbute` VALUES ('1019', '18', '5.0-4.6英寸', '5.0-4.6英寸', null, '5.0-4.6英寸', null, null, null);

-- ----------------------------
-- Table structure for product_property
-- ----------------------------
DROP TABLE IF EXISTS `product_property`;
CREATE TABLE `product_property` (
  `id` varchar(32) NOT NULL,
  `category_id` varchar(32) DEFAULT NULL COMMENT '分类id',
  `property_name` varchar(32) DEFAULT NULL COMMENT '属性名',
  `property_code` int(11) DEFAULT NULL COMMENT '属性名编码',
  `description` varchar(32) DEFAULT NULL COMMENT '描述',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `isSKU` int(11) DEFAULT NULL COMMENT '是否是sku属性(0是sku属性，1是销售属性)',
  `isSearch` int(11) DEFAULT NULL COMMENT '属性是否可搜索（0可搜索，1不可搜索）',
  `isShow` int(11) DEFAULT NULL COMMENT '是否显示（0显示，1不显示）',
  `del_flag` varchar(255) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of product_property
-- ----------------------------
INSERT INTO `product_property` VALUES ('11', '101103', '门款式', '11', '', '1', '0', '0', '0', '0');
INSERT INTO `product_property` VALUES ('12', '101103', '制冷', '12', '', '1', '0', '0', '0', '0');
INSERT INTO `product_property` VALUES ('13', '101201', '版型', '13', null, '1', '0', '0', '0', '0');
INSERT INTO `product_property` VALUES ('14', '101201', '袖长', '14', null, '1', '0', '0', '0', '0');
INSERT INTO `product_property` VALUES ('15', '101401', '运行内存', '15', null, '1', '0', '0', '0', '0');
INSERT INTO `product_property` VALUES ('16', '101401', '机身内存', '16', null, '1', '0', '0', '0', '0');
INSERT INTO `product_property` VALUES ('17', '101401', '屏幕尺寸', '17', null, '1', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for shop
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `shop_name` varchar(100) DEFAULT NULL COMMENT '店铺名称',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `shop_code` int(11) DEFAULT NULL COMMENT '店铺编码',
  `audit` int(11) DEFAULT NULL COMMENT '是否审核(0未审核通过，1审核通过)',
  `create_time` datetime DEFAULT NULL COMMENT '注册时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `del_flag` varchar(255) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of shop
-- ----------------------------
INSERT INTO `shop` VALUES ('110', '杂货铺', '1', '244', '0', '2018-07-11 14:17:08', '2018-07-11 14:17:11', '0');
INSERT INTO `shop` VALUES ('111', '八号当铺', '2', '255', '0', '2018-07-09 10:27:43', '2018-07-09 10:27:45', '0');

-- ----------------------------
-- Table structure for sku_product
-- ----------------------------
DROP TABLE IF EXISTS `sku_product`;
CREATE TABLE `sku_product` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `product_id` varchar(32) DEFAULT NULL COMMENT '商品id',
  `sku` varchar(64) DEFAULT NULL COMMENT '商品sku',
  `sku_code` int(11) DEFAULT NULL COMMENT '商品sku编码',
  `sku_product_name` varchar(128) DEFAULT NULL COMMENT '商品组合sku名称（商品名称 + titile）',
  `title` varchar(100) DEFAULT NULL COMMENT '商品副标题',
  `attribute_values` varchar(500) DEFAULT NULL COMMENT '商品属性',
  `price` decimal(10,2) DEFAULT NULL COMMENT '商品价格',
  `stock` int(11) DEFAULT '1' COMMENT '库存量',
  `ground` varchar(255) DEFAULT '0' COMMENT '是否上架',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `del_flag` varchar(255) DEFAULT '0' COMMENT '删除标记'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of sku_product
-- ----------------------------
INSERT INTO `sku_product` VALUES ('1016940866699915266', '1016940866624417793', 'MTEwLzEwMTIwMS8xMDE2OTQwODY2NjI0NDE3NzkzLzEwMDUvMTAwOC8v', null, 'adidas 夏天清凉运动T恤 速干   修身型-长袖', 'adidas 夏天清凉运动T恤 速干   修身型-长袖', '[{\"attributeId\":\"1005\",\"propertyName\":\"版型\",\"attributeName\":\"修身型\"},{\"attributeId\":\"1008\",\"propertyName\":\"袖长\",\"attributeName\":\"长袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016940866817355777', '1016940866624417793', 'MTEwLzEwMTIwMS8xMDE2OTQwODY2NjI0NDE3NzkzLzEwMDUvMTAwOS8v', null, 'adidas 夏天清凉运动T恤 速干   修身型-七分袖', 'adidas 夏天清凉运动T恤 速干   修身型-七分袖', '[{\"attributeId\":\"1005\",\"propertyName\":\"版型\",\"attributeName\":\"修身型\"},{\"attributeId\":\"1009\",\"propertyName\":\"袖长\",\"attributeName\":\"七分袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016940866825744386', '1016940866624417793', 'MTEwLzEwMTIwMS8xMDE2OTQwODY2NjI0NDE3NzkzLzEwMDUvMTAxMC8v', null, 'adidas 夏天清凉运动T恤 速干   修身型-短袖', 'adidas 夏天清凉运动T恤 速干   修身型-短袖', '[{\"attributeId\":\"1005\",\"propertyName\":\"版型\",\"attributeName\":\"修身型\"},{\"attributeId\":\"1010\",\"propertyName\":\"袖长\",\"attributeName\":\"短袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016940867245174786', '1016940866624417793', 'MTEwLzEwMTIwMS8xMDE2OTQwODY2NjI0NDE3NzkzLzEwMDYvMTAwOC8v', null, 'adidas 夏天清凉运动T恤 速干   标准型-长袖', 'adidas 夏天清凉运动T恤 速干   标准型-长袖', '[{\"attributeId\":\"1006\",\"propertyName\":\"版型\",\"attributeName\":\"标准型\"},{\"attributeId\":\"1008\",\"propertyName\":\"袖长\",\"attributeName\":\"长袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016940867291312129', '1016940866624417793', 'MTEwLzEwMTIwMS8xMDE2OTQwODY2NjI0NDE3NzkzLzEwMDYvMTAwOS8v', null, 'adidas 夏天清凉运动T恤 速干   标准型-七分袖', 'adidas 夏天清凉运动T恤 速干   标准型-七分袖', '[{\"attributeId\":\"1006\",\"propertyName\":\"版型\",\"attributeName\":\"标准型\"},{\"attributeId\":\"1009\",\"propertyName\":\"袖长\",\"attributeName\":\"七分袖\"}]', '238.00', '1000', '0', null, '2018-07-12 14:02:32', '0');
INSERT INTO `sku_product` VALUES ('1016940867308089345', '1016940866624417793', 'MTEwLzEwMTIwMS8xMDE2OTQwODY2NjI0NDE3NzkzLzEwMDYvMTAxMC8v', null, 'adidas 夏天清凉运动T恤 速干   标准型-短袖', 'adidas 夏天清凉运动T恤 速干   标准型-短袖', '[{\"attributeId\":\"1006\",\"propertyName\":\"版型\",\"attributeName\":\"标准型\"},{\"attributeId\":\"1010\",\"propertyName\":\"袖长\",\"attributeName\":\"短袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016940867324866561', '1016940866624417793', 'MTEwLzEwMTIwMS8xMDE2OTQwODY2NjI0NDE3NzkzLzEwMDcvMTAwOC8v', null, 'adidas 夏天清凉运动T恤 速干   宽松型-长袖', 'adidas 夏天清凉运动T恤 速干   宽松型-长袖', '[{\"attributeId\":\"1007\",\"propertyName\":\"版型\",\"attributeName\":\"宽松型\"},{\"attributeId\":\"1008\",\"propertyName\":\"袖长\",\"attributeName\":\"长袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016940867333255170', '1016940866624417793', 'MTEwLzEwMTIwMS8xMDE2OTQwODY2NjI0NDE3NzkzLzEwMDcvMTAwOS8v', null, 'adidas 夏天清凉运动T恤 速干   宽松型-七分袖', 'adidas 夏天清凉运动T恤 速干   宽松型-七分袖', '[{\"attributeId\":\"1007\",\"propertyName\":\"版型\",\"attributeName\":\"宽松型\"},{\"attributeId\":\"1009\",\"propertyName\":\"袖长\",\"attributeName\":\"七分袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016940867345838082', '1016940866624417793', 'MTEwLzEwMTIwMS8xMDE2OTQwODY2NjI0NDE3NzkzLzEwMDcvMTAxMC8v', null, 'adidas 夏天清凉运动T恤 速干   宽松型-短袖', 'adidas 夏天清凉运动T恤 速干   宽松型-短袖', '[{\"attributeId\":\"1007\",\"propertyName\":\"版型\",\"attributeName\":\"宽松型\"},{\"attributeId\":\"1010\",\"propertyName\":\"袖长\",\"attributeName\":\"短袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016941362076577793', '1016941362055606274', 'MTEwLzEwMTIwMS8xMDE2OTQxMzYyMDU1NjA2Mjc0LzEwMDUvMTAwOC8v', null, '七匹狼 纯棉印花T恤 舒适 大方  修身型-长袖', '七匹狼 纯棉印花T恤 舒适 大方  修身型-长袖', '[{\"attributeId\":\"1005\",\"propertyName\":\"版型\",\"attributeName\":\"修身型\"},{\"attributeId\":\"1008\",\"propertyName\":\"袖长\",\"attributeName\":\"长袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016941362294681602', '1016941362055606274', 'MTEwLzEwMTIwMS8xMDE2OTQxMzYyMDU1NjA2Mjc0LzEwMDUvMTAwOS8v', null, '七匹狼 纯棉印花T恤 舒适 大方  修身型-七分袖', '七匹狼 纯棉印花T恤 舒适 大方  修身型-七分袖', '[{\"attributeId\":\"1005\",\"propertyName\":\"版型\",\"attributeName\":\"修身型\"},{\"attributeId\":\"1009\",\"propertyName\":\"袖长\",\"attributeName\":\"七分袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016941362307264513', '1016941362055606274', 'MTEwLzEwMTIwMS8xMDE2OTQxMzYyMDU1NjA2Mjc0LzEwMDUvMTAxMC8v', null, '七匹狼 纯棉印花T恤 舒适 大方  修身型-短袖', '七匹狼 纯棉印花T恤 舒适 大方  修身型-短袖', '[{\"attributeId\":\"1005\",\"propertyName\":\"版型\",\"attributeName\":\"修身型\"},{\"attributeId\":\"1010\",\"propertyName\":\"袖长\",\"attributeName\":\"短袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016941362328236034', '1016941362055606274', 'MTEwLzEwMTIwMS8xMDE2OTQxMzYyMDU1NjA2Mjc0LzEwMDYvMTAwOC8v', null, '七匹狼 纯棉印花T恤 舒适 大方  标准型-长袖', '七匹狼 纯棉印花T恤 舒适 大方  标准型-长袖', '[{\"attributeId\":\"1006\",\"propertyName\":\"版型\",\"attributeName\":\"标准型\"},{\"attributeId\":\"1008\",\"propertyName\":\"袖长\",\"attributeName\":\"长袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016941362537951233', '1016941362055606274', 'MTEwLzEwMTIwMS8xMDE2OTQxMzYyMDU1NjA2Mjc0LzEwMDYvMTAwOS8v', null, '七匹狼 纯棉印花T恤 舒适 大方  标准型-七分袖', '七匹狼 纯棉印花T恤 舒适 大方  标准型-七分袖', '[{\"attributeId\":\"1006\",\"propertyName\":\"版型\",\"attributeName\":\"标准型\"},{\"attributeId\":\"1009\",\"propertyName\":\"袖长\",\"attributeName\":\"七分袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016941362554728450', '1016941362055606274', 'MTEwLzEwMTIwMS8xMDE2OTQxMzYyMDU1NjA2Mjc0LzEwMDYvMTAxMC8v', null, '七匹狼 纯棉印花T恤 舒适 大方  标准型-短袖', '七匹狼 纯棉印花T恤 舒适 大方  标准型-短袖', '[{\"attributeId\":\"1006\",\"propertyName\":\"版型\",\"attributeName\":\"标准型\"},{\"attributeId\":\"1010\",\"propertyName\":\"袖长\",\"attributeName\":\"短袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016941362563117057', '1016941362055606274', 'MTEwLzEwMTIwMS8xMDE2OTQxMzYyMDU1NjA2Mjc0LzEwMDcvMTAwOC8v', null, '七匹狼 纯棉印花T恤 舒适 大方  宽松型-长袖', '七匹狼 纯棉印花T恤 舒适 大方  宽松型-长袖', '[{\"attributeId\":\"1007\",\"propertyName\":\"版型\",\"attributeName\":\"宽松型\"},{\"attributeId\":\"1008\",\"propertyName\":\"袖长\",\"attributeName\":\"长袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016941362579894273', '1016941362055606274', 'MTEwLzEwMTIwMS8xMDE2OTQxMzYyMDU1NjA2Mjc0LzEwMDcvMTAwOS8v', null, '七匹狼 纯棉印花T恤 舒适 大方  宽松型-七分袖', '七匹狼 纯棉印花T恤 舒适 大方  宽松型-七分袖', '[{\"attributeId\":\"1007\",\"propertyName\":\"版型\",\"attributeName\":\"宽松型\"},{\"attributeId\":\"1009\",\"propertyName\":\"袖长\",\"attributeName\":\"七分袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016941362588282881', '1016941362055606274', 'MTEwLzEwMTIwMS8xMDE2OTQxMzYyMDU1NjA2Mjc0LzEwMDcvMTAxMC8v', null, '七匹狼 纯棉印花T恤 舒适 大方  宽松型-短袖', '七匹狼 纯棉印花T恤 舒适 大方  宽松型-短袖', '[{\"attributeId\":\"1007\",\"propertyName\":\"版型\",\"attributeName\":\"宽松型\"},{\"attributeId\":\"1010\",\"propertyName\":\"袖长\",\"attributeName\":\"短袖\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942028396294145', '1016942028371128322', 'MTEwLzEwMTEwMy8xMDE2OTQyMDI4MzcxMTI4MzIyLzEwMDEvMTAwNC8v', null, '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  十字对开门-风直冷（混冷）', '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  十字对开门-风直冷（混冷）', '[{\"attributeId\":\"1001\",\"propertyName\":\"门款式\",\"attributeName\":\"十字对开门\"},{\"attributeId\":\"1004\",\"propertyName\":\"制冷\",\"attributeName\":\"风直冷（混冷）\"}]', '2688.00', '1000', '0', null, '2018-07-12 14:02:44', '0');
INSERT INTO `sku_product` VALUES ('1016942028643758081', '1016942028371128322', 'MTEwLzEwMTEwMy8xMDE2OTQyMDI4MzcxMTI4MzIyLzEwMDEvMTAwNS8v', null, '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  十字对开门-修身型', '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  十字对开门-修身型', '[{\"attributeId\":\"1001\",\"propertyName\":\"门款式\",\"attributeName\":\"十字对开门\"},{\"attributeId\":\"1005\",\"attributeName\":\"修身型\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942028677312513', '1016942028371128322', 'MTEwLzEwMTEwMy8xMDE2OTQyMDI4MzcxMTI4MzIyLzEwMDEvMTAwNi8v', null, '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  十字对开门-标准型', '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  十字对开门-标准型', '[{\"attributeId\":\"1001\",\"propertyName\":\"门款式\",\"attributeName\":\"十字对开门\"},{\"attributeId\":\"1006\",\"attributeName\":\"标准型\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942028698284033', '1016942028371128322', 'MTEwLzEwMTEwMy8xMDE2OTQyMDI4MzcxMTI4MzIyLzEwMDIvMTAwNC8v', null, '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  双门-风直冷（混冷）', '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  双门-风直冷（混冷）', '[{\"attributeId\":\"1002\",\"propertyName\":\"门款式\",\"attributeName\":\"双门\"},{\"attributeId\":\"1004\",\"propertyName\":\"制冷\",\"attributeName\":\"风直冷（混冷）\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942028706672642', '1016942028371128322', 'MTEwLzEwMTEwMy8xMDE2OTQyMDI4MzcxMTI4MzIyLzEwMDIvMTAwNS8v', null, '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  双门-修身型', '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  双门-修身型', '[{\"attributeId\":\"1002\",\"propertyName\":\"门款式\",\"attributeName\":\"双门\"},{\"attributeId\":\"1005\",\"attributeName\":\"修身型\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942028719255553', '1016942028371128322', 'MTEwLzEwMTEwMy8xMDE2OTQyMDI4MzcxMTI4MzIyLzEwMDIvMTAwNi8v', null, '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  双门-标准型', '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  双门-标准型', '[{\"attributeId\":\"1002\",\"propertyName\":\"门款式\",\"attributeName\":\"双门\"},{\"attributeId\":\"1006\",\"attributeName\":\"标准型\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942028727644162', '1016942028371128322', 'MTEwLzEwMTEwMy8xMDE2OTQyMDI4MzcxMTI4MzIyLzEwMDMvMTAwNC8v', null, '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  风冷（无霜）-风直冷（混冷）', '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  风冷（无霜）-风直冷（混冷）', '[{\"attributeId\":\"1003\",\"propertyName\":\"制冷\",\"attributeName\":\"风冷（无霜）\"},{\"attributeId\":\"1004\",\"propertyName\":\"制冷\",\"attributeName\":\"风直冷（混冷）\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942028736032769', '1016942028371128322', 'MTEwLzEwMTEwMy8xMDE2OTQyMDI4MzcxMTI4MzIyLzEwMDMvMTAwNS8v', null, '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  风冷（无霜）-修身型', '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  风冷（无霜）-修身型', '[{\"attributeId\":\"1003\",\"propertyName\":\"制冷\",\"attributeName\":\"风冷（无霜）\"},{\"attributeId\":\"1005\",\"attributeName\":\"修身型\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942028744421377', '1016942028371128322', 'MTEwLzEwMTEwMy8xMDE2OTQyMDI4MzcxMTI4MzIyLzEwMDMvMTAwNi8v', null, '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  风冷（无霜）-标准型', '海尔（Haier）471升双变频冰箱 时尚外取水Water cooler 系列 BCD-471WDEA  风冷（无霜）-标准型', '[{\"attributeId\":\"1003\",\"propertyName\":\"制冷\",\"attributeName\":\"风冷（无霜）\"},{\"attributeId\":\"1006\",\"attributeName\":\"标准型\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942276409683969', '1016942276371935234', 'MTEwLzEwMTEwMy8xMDE2OTQyMjc2MzcxOTM1MjM0LzEwMDEvMTAwNC8v', null, '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  十字对开门-风直冷（混冷）', '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  十字对开门-风直冷（混冷）', '[{\"attributeId\":\"1001\",\"propertyName\":\"门款式\",\"attributeName\":\"十字对开门\"},{\"attributeId\":\"1004\",\"propertyName\":\"制冷\",\"attributeName\":\"风直冷（混冷）\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942276418072577', '1016942276371935234', 'MTEwLzEwMTEwMy8xMDE2OTQyMjc2MzcxOTM1MjM0LzEwMDEvMTAwNS8v', null, '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  十字对开门-修身型', '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  十字对开门-修身型', '[{\"attributeId\":\"1001\",\"propertyName\":\"门款式\",\"attributeName\":\"十字对开门\"},{\"attributeId\":\"1005\",\"attributeName\":\"修身型\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942276422266881', '1016942276371935234', 'MTEwLzEwMTEwMy8xMDE2OTQyMjc2MzcxOTM1MjM0LzEwMDEvMTAwNi8v', null, '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  十字对开门-标准型', '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  十字对开门-标准型', '[{\"attributeId\":\"1001\",\"propertyName\":\"门款式\",\"attributeName\":\"十字对开门\"},{\"attributeId\":\"1006\",\"attributeName\":\"标准型\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942276426461186', '1016942276371935234', 'MTEwLzEwMTEwMy8xMDE2OTQyMjc2MzcxOTM1MjM0LzEwMDIvMTAwNC8v', null, '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  双门-风直冷（混冷）', '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  双门-风直冷（混冷）', '[{\"attributeId\":\"1002\",\"propertyName\":\"门款式\",\"attributeName\":\"双门\"},{\"attributeId\":\"1004\",\"propertyName\":\"制冷\",\"attributeName\":\"风直冷（混冷）\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942276430655490', '1016942276371935234', 'MTEwLzEwMTEwMy8xMDE2OTQyMjc2MzcxOTM1MjM0LzEwMDIvMTAwNS8v', null, '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  双门-修身型', '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  双门-修身型', '[{\"attributeId\":\"1002\",\"propertyName\":\"门款式\",\"attributeName\":\"双门\"},{\"attributeId\":\"1005\",\"attributeName\":\"修身型\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942276434849793', '1016942276371935234', 'MTEwLzEwMTEwMy8xMDE2OTQyMjc2MzcxOTM1MjM0LzEwMDIvMTAwNi8v', null, '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  双门-标准型', '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  双门-标准型', '[{\"attributeId\":\"1002\",\"propertyName\":\"门款式\",\"attributeName\":\"双门\"},{\"attributeId\":\"1006\",\"attributeName\":\"标准型\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942276648759298', '1016942276371935234', 'MTEwLzEwMTEwMy8xMDE2OTQyMjc2MzcxOTM1MjM0LzEwMDMvMTAwNC8v', null, '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  风冷（无霜）-风直冷（混冷）', '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  风冷（无霜）-风直冷（混冷）', '[{\"attributeId\":\"1003\",\"propertyName\":\"制冷\",\"attributeName\":\"风冷（无霜）\"},{\"attributeId\":\"1004\",\"propertyName\":\"制冷\",\"attributeName\":\"风直冷（混冷）\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942276669730818', '1016942276371935234', 'MTEwLzEwMTEwMy8xMDE2OTQyMjc2MzcxOTM1MjM0LzEwMDMvMTAwNS8v', null, '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  风冷（无霜）-修身型', '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  风冷（无霜）-修身型', '[{\"attributeId\":\"1003\",\"propertyName\":\"制冷\",\"attributeName\":\"风冷（无霜）\"},{\"attributeId\":\"1005\",\"attributeName\":\"修身型\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942276690702337', '1016942276371935234', 'MTEwLzEwMTEwMy8xMDE2OTQyMjc2MzcxOTM1MjM0LzEwMDMvMTAwNi8v', null, '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  风冷（无霜）-标准型', '美的(Midea)429升 玻璃面板智能双变频冰箱温湿精控一级能效伯爵咖BCD-429WGPZM(E)  风冷（无霜）-标准型', '[{\"attributeId\":\"1003\",\"propertyName\":\"制冷\",\"attributeName\":\"风冷（无霜）\"},{\"attributeId\":\"1006\",\"attributeName\":\"标准型\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942961498910722', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTEvMTAxNC8xMDE3Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-64G-5.6英寸及以上', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-64G-5.6英寸及以上', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942961725403137', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTEvMTAxNC8xMDE4Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-64G-5.5-5.1英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-64G-5.5-5.1英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942961956089857', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTEvMTAxNC8xMDE5Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-64G-5.0-4.6英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-64G-5.0-4.6英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942961985449986', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTEvMTAxNS8xMDE3Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-128G-5.6英寸及以上', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-128G-5.6英寸及以上', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962006421505', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTEvMTAxNS8xMDE4Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-128G-5.5-5.1英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-128G-5.5-5.1英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962031587330', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTEvMTAxNS8xMDE5Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-128G-5.0-4.6英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-128G-5.0-4.6英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962052558849', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTEvMTAxNi8xMDE3Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-256G-5.6英寸及以上', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-256G-5.6英寸及以上', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962081918977', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTEvMTAxNi8xMDE4Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-256G-5.5-5.1英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-256G-5.5-5.1英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962102890497', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTEvMTAxNi8xMDE5Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-256G-5.0-4.6英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  4G-256G-5.0-4.6英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962115473409', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTIvMTAxNC8xMDE3Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-64G-5.6英寸及以上', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-64G-5.6英寸及以上', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962216136705', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTIvMTAxNC8xMDE4Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-64G-5.5-5.1英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-64G-5.5-5.1英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962228719617', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTIvMTAxNC8xMDE5Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-64G-5.0-4.6英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-64G-5.0-4.6英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962237108226', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTIvMTAxNS8xMDE3Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-128G-5.6英寸及以上', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-128G-5.6英寸及以上', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962245496833', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTIvMTAxNS8xMDE4Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-128G-5.5-5.1英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-128G-5.5-5.1英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962258079745', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTIvMTAxNS8xMDE5Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-128G-5.0-4.6英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-128G-5.0-4.6英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962266468354', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTIvMTAxNi8xMDE3Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-256G-5.6英寸及以上', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-256G-5.6英寸及以上', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962279051266', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTIvMTAxNi8xMDE4Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-256G-5.5-5.1英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-256G-5.5-5.1英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962287439874', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTIvMTAxNi8xMDE5Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-256G-5.0-4.6英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  8G-256G-5.0-4.6英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962300022785', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTMvMTAxNC8xMDE3Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-64G-5.6英寸及以上', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-64G-5.6英寸及以上', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962308411393', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTMvMTAxNC8xMDE4Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-64G-5.5-5.1英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-64G-5.5-5.1英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962320994306', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTMvMTAxNC8xMDE5Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-64G-5.0-4.6英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-64G-5.0-4.6英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962329382913', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTMvMTAxNS8xMDE3Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-128G-5.6英寸及以上', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-128G-5.6英寸及以上', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962341965825', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTMvMTAxNS8xMDE4Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-128G-5.5-5.1英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-128G-5.5-5.1英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962350354434', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTMvMTAxNS8xMDE5Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-128G-5.0-4.6英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-128G-5.0-4.6英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962362937346', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTMvMTAxNi8xMDE3Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-256G-5.6英寸及以上', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-256G-5.6英寸及以上', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962367131650', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTMvMTAxNi8xMDE4Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-256G-5.5-5.1英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-256G-5.5-5.1英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016942962375520257', '1016942961473744897', 'MTEwLzEwMTQwMS8xMDE2OTQyOTYxNDczNzQ0ODk3LzEwMTMvMTAxNi8xMDE5Ly8=', null, 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-256G-5.0-4.6英寸', 'Apple iPhone 8 Plus (A1864) 64GB 金色 移动联通电信4G手机  6G-256G-5.0-4.6英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072530526210', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTEvMTAxNC8xMDE3Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-64G-5.6英寸及以上', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-64G-5.6英寸及以上', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072538914818', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTEvMTAxNC8xMDE4Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-64G-5.5-5.1英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-64G-5.5-5.1英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072547303426', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTEvMTAxNC8xMDE5Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-64G-5.0-4.6英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-64G-5.0-4.6英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072551497729', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTEvMTAxNS8xMDE3Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-128G-5.6英寸及以上', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-128G-5.6英寸及以上', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072555692034', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTEvMTAxNS8xMDE4Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-128G-5.5-5.1英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-128G-5.5-5.1英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072559886338', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTEvMTAxNS8xMDE5Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-128G-5.0-4.6英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-128G-5.0-4.6英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072559886339', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTEvMTAxNi8xMDE3Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-256G-5.6英寸及以上', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-256G-5.6英寸及以上', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072568274945', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTEvMTAxNi8xMDE4Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-256G-5.5-5.1英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-256G-5.5-5.1英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072572469249', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTEvMTAxNi8xMDE5Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-256G-5.0-4.6英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  4G-256G-5.0-4.6英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072576663553', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTIvMTAxNC8xMDE3Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-64G-5.6英寸及以上', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-64G-5.6英寸及以上', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072861876225', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTIvMTAxNC8xMDE4Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-64G-5.5-5.1英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-64G-5.5-5.1英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072878653442', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTIvMTAxNC8xMDE5Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-64G-5.0-4.6英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-64G-5.0-4.6英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072891236353', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTIvMTAxNS8xMDE3Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-128G-5.6英寸及以上', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-128G-5.6英寸及以上', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072903819266', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTIvMTAxNS8xMDE4Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-128G-5.5-5.1英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-128G-5.5-5.1英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072912207873', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTIvMTAxNS8xMDE5Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-128G-5.0-4.6英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-128G-5.0-4.6英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072928985089', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTIvMTAxNi8xMDE3Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-256G-5.6英寸及以上', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-256G-5.6英寸及以上', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072941568002', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTIvMTAxNi8xMDE4Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-256G-5.5-5.1英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-256G-5.5-5.1英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072954150913', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTIvMTAxNi8xMDE5Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-256G-5.0-4.6英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  8G-256G-5.0-4.6英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072966733825', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTMvMTAxNC8xMDE3Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-64G-5.6英寸及以上', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-64G-5.6英寸及以上', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072975122434', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTMvMTAxNC8xMDE4Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-64G-5.5-5.1英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-64G-5.5-5.1英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943072983511041', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTMvMTAxNC8xMDE5Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-64G-5.0-4.6英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-64G-5.0-4.6英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943073000288257', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTMvMTAxNS8xMDE3Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-128G-5.6英寸及以上', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-128G-5.6英寸及以上', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943073008676865', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTMvMTAxNS8xMDE4Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-128G-5.5-5.1英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-128G-5.5-5.1英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943073021259777', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTMvMTAxNS8xMDE5Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-128G-5.0-4.6英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-128G-5.0-4.6英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943073029648385', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTMvMTAxNi8xMDE3Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-256G-5.6英寸及以上', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-256G-5.6英寸及以上', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943073042231297', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTMvMTAxNi8xMDE4Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-256G-5.5-5.1英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-256G-5.5-5.1英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943073054814209', '1016943072513748993', 'MTEwLzEwMTQwMS8xMDE2OTQzMDcyNTEzNzQ4OTkzLzEwMTMvMTAxNi8xMDE5Ly8=', null, '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-256G-5.0-4.6英寸', '华为 HUAWEI Mate 10 6GB 128GB 香槟金 移动联通电信4G手机 双卡双待  6G-256G-5.0-4.6英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209185144834', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTEvMTAxNC8xMDE3Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  4G-64G-5.6英寸及以上', 'OPPO R15 全面屏双摄拍照手机  4G-64G-5.6英寸及以上', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209529077762', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTEvMTAxNC8xMDE4Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  4G-64G-5.5-5.1英寸', 'OPPO R15 全面屏双摄拍照手机  4G-64G-5.5-5.1英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209545854977', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTEvMTAxNC8xMDE5Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  4G-64G-5.0-4.6英寸', 'OPPO R15 全面屏双摄拍照手机  4G-64G-5.0-4.6英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209558437890', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTEvMTAxNS8xMDE3Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  4G-128G-5.6英寸及以上', 'OPPO R15 全面屏双摄拍照手机  4G-128G-5.6英寸及以上', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209571020802', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTEvMTAxNS8xMDE4Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  4G-128G-5.5-5.1英寸', 'OPPO R15 全面屏双摄拍照手机  4G-128G-5.5-5.1英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209579409410', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTEvMTAxNS8xMDE5Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  4G-128G-5.0-4.6英寸', 'OPPO R15 全面屏双摄拍照手机  4G-128G-5.0-4.6英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209591992321', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTEvMTAxNi8xMDE3Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  4G-256G-5.6英寸及以上', 'OPPO R15 全面屏双摄拍照手机  4G-256G-5.6英寸及以上', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209600380929', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTEvMTAxNi8xMDE4Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  4G-256G-5.5-5.1英寸', 'OPPO R15 全面屏双摄拍照手机  4G-256G-5.5-5.1英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209612963841', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTEvMTAxNi8xMDE5Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  4G-256G-5.0-4.6英寸', 'OPPO R15 全面屏双摄拍照手机  4G-256G-5.0-4.6英寸', '[{\"attributeId\":\"1011\",\"propertyName\":\"运行内存\",\"attributeName\":\"4G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209621352449', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTIvMTAxNC8xMDE3Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  8G-64G-5.6英寸及以上', 'OPPO R15 全面屏双摄拍照手机  8G-64G-5.6英寸及以上', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209701044226', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTIvMTAxNC8xMDE4Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  8G-64G-5.5-5.1英寸', 'OPPO R15 全面屏双摄拍照手机  8G-64G-5.5-5.1英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209717821441', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTIvMTAxNC8xMDE5Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  8G-64G-5.0-4.6英寸', 'OPPO R15 全面屏双摄拍照手机  8G-64G-5.0-4.6英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209730404353', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTIvMTAxNS8xMDE3Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  8G-128G-5.6英寸及以上', 'OPPO R15 全面屏双摄拍照手机  8G-128G-5.6英寸及以上', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209948508161', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTIvMTAxNS8xMDE4Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  8G-128G-5.5-5.1英寸', 'OPPO R15 全面屏双摄拍照手机  8G-128G-5.5-5.1英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209961091074', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTIvMTAxNS8xMDE5Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  8G-128G-5.0-4.6英寸', 'OPPO R15 全面屏双摄拍照手机  8G-128G-5.0-4.6英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209969479682', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTIvMTAxNi8xMDE3Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  8G-256G-5.6英寸及以上', 'OPPO R15 全面屏双摄拍照手机  8G-256G-5.6英寸及以上', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209986256898', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTIvMTAxNi8xMDE4Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  8G-256G-5.5-5.1英寸', 'OPPO R15 全面屏双摄拍照手机  8G-256G-5.5-5.1英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943209998839810', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTIvMTAxNi8xMDE5Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  8G-256G-5.0-4.6英寸', 'OPPO R15 全面屏双摄拍照手机  8G-256G-5.0-4.6英寸', '[{\"attributeId\":\"1012\",\"propertyName\":\"运行内存\",\"attributeName\":\"8G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943210011422721', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTMvMTAxNC8xMDE3Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  6G-64G-5.6英寸及以上', 'OPPO R15 全面屏双摄拍照手机  6G-64G-5.6英寸及以上', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943210019811330', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTMvMTAxNC8xMDE4Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  6G-64G-5.5-5.1英寸', 'OPPO R15 全面屏双摄拍照手机  6G-64G-5.5-5.1英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943210032394242', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTMvMTAxNC8xMDE5Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  6G-64G-5.0-4.6英寸', 'OPPO R15 全面屏双摄拍照手机  6G-64G-5.0-4.6英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1014\",\"propertyName\":\"机身内存\",\"attributeName\":\"64G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943210044977153', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTMvMTAxNS8xMDE3Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  6G-128G-5.6英寸及以上', 'OPPO R15 全面屏双摄拍照手机  6G-128G-5.6英寸及以上', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943210053365761', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTMvMTAxNS8xMDE4Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  6G-128G-5.5-5.1英寸', 'OPPO R15 全面屏双摄拍照手机  6G-128G-5.5-5.1英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943210061754369', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTMvMTAxNS8xMDE5Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  6G-128G-5.0-4.6英寸', 'OPPO R15 全面屏双摄拍照手机  6G-128G-5.0-4.6英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1015\",\"propertyName\":\"机身内存\",\"attributeName\":\"128G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943210070142978', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTMvMTAxNi8xMDE3Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  6G-256G-5.6英寸及以上', 'OPPO R15 全面屏双摄拍照手机  6G-256G-5.6英寸及以上', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1017\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.6英寸及以上\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943210078531586', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTMvMTAxNi8xMDE4Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  6G-256G-5.5-5.1英寸', 'OPPO R15 全面屏双摄拍照手机  6G-256G-5.5-5.1英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1018\",\"propertyName\":\"屏幕尺寸\",\"attributeName\":\"5.5-5.1英寸\"}]', '999.00', '1000', '0', null, null, '0');
INSERT INTO `sku_product` VALUES ('1016943210086920194', '1016943209159979009', 'MTEwLzEwMTQwMS8xMDE2OTQzMjA5MTU5OTc5MDA5LzEwMTMvMTAxNi8xMDE5Ly8=', null, 'OPPO R15 全面屏双摄拍照手机  6G-256G-5.0-4.6英寸', 'OPPO R15 全面屏双摄拍照手机  6G-256G-5.0-4.6英寸', '[{\"attributeId\":\"1013\",\"propertyName\":\"运行内存\",\"attributeName\":\"6G\"},{\"attributeId\":\"1016\",\"propertyName\":\"机身内存\",\"attributeName\":\"256G\"},{\"attributeId\":\"1019\",\"attributeName\":\"5.0-4.6英寸\"}]', '999.00', '1000', '0', null, null, '0');

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `parent_id` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) NOT NULL COMMENT '所有父级编号',
  `name` varchar(100) NOT NULL COMMENT '名称',
  `sort` decimal(10,0) NOT NULL COMMENT '排序',
  `code` varchar(100) DEFAULT NULL COMMENT '区域编码',
  `type` char(1) DEFAULT NULL COMMENT '区域类型',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_area_parent_id` (`parent_id`) USING BTREE,
  KEY `sys_area_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='区域表';

-- ----------------------------
-- Records of sys_area
-- ----------------------------
INSERT INTO `sys_area` VALUES ('1', '0', '0,', '中国', '10', '100000', '1', '1', '2013-05-27 08:00:00', '1', '2013-05-27 08:00:00', null, '0');
INSERT INTO `sys_area` VALUES ('11', '1', '0,1,11,', '北京', '10001', '11', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1101', '11', '0,1,11,1101,', '北京市辖', '10002', '1101', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110101', '1101', '0,1,11,1101,110101,', '东城区', '10003', '110101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110102', '1101', '0,1,11,1101,110102,', '西城区', '10004', '110102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110103', '1101', '0,1,11,1101,110103,', '崇文区', '10005', '110103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110104', '1101', '0,1,11,1101,110104,', '宣武区', '10006', '110104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110105', '1101', '0,1,11,1101,110105,', '朝阳区', '10007', '110105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110106', '1101', '0,1,11,1101,110106,', '丰台区', '10008', '110106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110107', '1101', '0,1,11,1101,110107,', '石景山区', '10009', '110107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110108', '1101', '0,1,11,1101,110108,', '海淀区', '10010', '110108', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110109', '1101', '0,1,11,1101,110109,', '门头沟区', '10011', '110109', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110111', '1101', '0,1,11,1101,110111,', '房山区', '10012', '110111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110112', '1101', '0,1,11,1101,110112,', '通州区', '10013', '110112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110113', '1101', '0,1,11,1101,110113,', '顺义区', '10014', '110113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110114', '1101', '0,1,11,1101,110114,', '昌平区', '10015', '110114', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1102', '11', '0,1,11,1102,', '北京县辖', '10016', '1102', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110224', '1101', '0,1,11,1101,1101,', '大兴区', '10017', '110224', '4', '1', '2017-03-10 15:48:56', '1e98819ba91c48aba3d9d38da6b834ab', '2018-01-04 11:38:44', '', '0');
INSERT INTO `sys_area` VALUES ('110226', '1102', '0,1,11,1102,110226,', '平谷县', '10018', '110226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110227', '1102', '0,1,11,1102,110227,', '怀柔县', '10019', '110227', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110228', '1102', '0,1,11,1102,110228,', '密云县', '10020', '110228', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('110229', '1102', '0,1,11,1102,110229,', '延庆县', '10021', '110229', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('12', '1', '0,1,12,', '天津', '10022', '12', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1201', '12', '0,1,12,1201,', '天津市辖', '10023', '1201', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120101', '1201', '0,1,12,1201,120101,', '和平区', '10024', '120101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120102', '1201', '0,1,12,1201,120102,', '河东区', '10025', '120102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120103', '1201', '0,1,12,1201,120103,', '河西区', '10026', '120103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120104', '1201', '0,1,12,1201,120104,', '南开区', '10027', '120104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120105', '1201', '0,1,12,1201,120105,', '河北区', '10028', '120105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120106', '1201', '0,1,12,1201,120106,', '红桥区', '10029', '120106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120107', '1201', '0,1,12,1201,120107,', '塘沽区', '10030', '120107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120108', '1201', '0,1,12,1201,120108,', '汉沽区', '10031', '120108', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120109', '1201', '0,1,12,1201,120109,', '大港区', '10032', '120109', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120110', '1201', '0,1,12,1201,120110,', '东丽区', '10033', '120110', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120111', '1201', '0,1,12,1201,120111,', '西青区', '10034', '120111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120112', '1201', '0,1,12,1201,120112,', '津南区', '10035', '120112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120113', '1201', '0,1,12,1201,120113,', '北辰区', '10036', '120113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120114', '1201', '0,1,12,1201,120114,', '武清区', '10037', '120114', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1202', '12', '0,1,12,1202,', '天津县辖', '10038', '1202', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120221', '1202', '0,1,12,1202,120221,', '宁河县', '10039', '120221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120223', '1202', '0,1,12,1202,120223,', '静海县', '10040', '120223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120224', '1202', '0,1,12,1202,120224,', '宝坻县', '10041', '120224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('120225', '1202', '0,1,12,1202,120225,', '蓟  县', '10042', '120225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('13', '1', '0,1,13,', '河北', '10043', '13', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1301', '13', '0,1,13,1301,', '石家庄', '10044', '1301', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130101', '1301', '0,1,13,1301,130101,', '市辖区', '10045', '130101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130102', '1301', '0,1,13,1301,130102,', '长安区', '10046', '130102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130103', '1301', '0,1,13,1301,130103,', '桥东区', '10047', '130103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130104', '1301', '0,1,13,1301,130104,', '桥西区', '10048', '130104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130105', '1301', '0,1,13,1301,130105,', '新华区', '10049', '130105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130106', '1301', '0,1,13,1301,130106,', '郊  区', '10050', '130106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130107', '1301', '0,1,13,1301,130107,', '井陉矿区', '10051', '130107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130121', '1301', '0,1,13,1301,130121,', '井陉县', '10052', '130121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130123', '1301', '0,1,13,1301,130123,', '正定县', '10053', '130123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130124', '1301', '0,1,13,1301,130124,', '栾城县', '10054', '130124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130125', '1301', '0,1,13,1301,130125,', '行唐县', '10055', '130125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130126', '1301', '0,1,13,1301,130126,', '灵寿县', '10056', '130126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130127', '1301', '0,1,13,1301,130127,', '高邑县', '10057', '130127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130128', '1301', '0,1,13,1301,130128,', '深泽县', '10058', '130128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130129', '1301', '0,1,13,1301,130129,', '赞皇县', '10059', '130129', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130130', '1301', '0,1,13,1301,130130,', '无极县', '10060', '130130', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130131', '1301', '0,1,13,1301,130131,', '平山县', '10061', '130131', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130132', '1301', '0,1,13,1301,130132,', '元氏县', '10062', '130132', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130133', '1301', '0,1,13,1301,130133,', '赵  县', '10063', '130133', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130181', '1301', '0,1,13,1301,130181,', '辛集市', '10064', '130181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130182', '1301', '0,1,13,1301,130182,', '藁城市', '10065', '130182', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130183', '1301', '0,1,13,1301,130183,', '晋州市', '10066', '130183', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130184', '1301', '0,1,13,1301,130184,', '新乐市', '10067', '130184', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130185', '1301', '0,1,13,1301,130185,', '鹿泉市', '10068', '130185', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1302', '13', '0,1,13,1302,', '唐山', '10069', '1302', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130201', '1302', '0,1,13,1302,130201,', '市辖区', '10070', '130201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130202', '1302', '0,1,13,1302,130202,', '路南区', '10071', '130202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130203', '1302', '0,1,13,1302,130203,', '路北区', '10072', '130203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130204', '1302', '0,1,13,1302,130204,', '古冶区', '10073', '130204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130205', '1302', '0,1,13,1302,130205,', '开平区', '10074', '130205', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130206', '1302', '0,1,13,1302,130206,', '新  区', '10075', '130206', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130221', '1302', '0,1,13,1302,130221,', '丰润县', '10076', '130221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130223', '1302', '0,1,13,1302,130223,', '滦  县', '10077', '130223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130224', '1302', '0,1,13,1302,130224,', '滦南县', '10078', '130224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130225', '1302', '0,1,13,1302,130225,', '乐亭县', '10079', '130225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130227', '1302', '0,1,13,1302,130227,', '迁西县', '10080', '130227', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130229', '1302', '0,1,13,1302,130229,', '玉田县', '10081', '130229', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130230', '1302', '0,1,13,1302,130230,', '唐海县', '10082', '130230', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130281', '1302', '0,1,13,1302,130281,', '遵化市', '10083', '130281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130282', '1302', '0,1,13,1302,130282,', '丰南市', '10084', '130282', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130283', '1302', '0,1,13,1302,130283,', '迁安市', '10085', '130283', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1303', '13', '0,1,13,1303,', '秦皇岛', '10086', '1303', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130301', '1303', '0,1,13,1303,130301,', '市辖区', '10087', '130301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130302', '1303', '0,1,13,1303,130302,', '海港区', '10088', '130302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130303', '1303', '0,1,13,1303,130303,', '山海关区', '10089', '130303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130304', '1303', '0,1,13,1303,130304,', '北戴河区', '10090', '130304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130321', '1303', '0,1,13,1303,130321,', '青龙满族自治县', '10091', '130321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130322', '1303', '0,1,13,1303,130322,', '昌黎县', '10092', '130322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130323', '1303', '0,1,13,1303,130323,', '抚宁县', '10093', '130323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130324', '1303', '0,1,13,1303,130324,', '卢龙县', '10094', '130324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1304', '13', '0,1,13,1304,', '邯郸', '10095', '1304', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130401', '1304', '0,1,13,1304,130401,', '市辖区', '10096', '130401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130402', '1304', '0,1,13,1304,130402,', '邯山区', '10097', '130402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130403', '1304', '0,1,13,1304,130403,', '丛台区', '10098', '130403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130404', '1304', '0,1,13,1304,130404,', '复兴区', '10099', '130404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130406', '1304', '0,1,13,1304,130406,', '峰峰矿区', '10100', '130406', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130421', '1304', '0,1,13,1304,130421,', '邯郸县', '10101', '130421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130423', '1304', '0,1,13,1304,130423,', '临漳县', '10102', '130423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130424', '1304', '0,1,13,1304,130424,', '成安县', '10103', '130424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130425', '1304', '0,1,13,1304,130425,', '大名县', '10104', '130425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130426', '1304', '0,1,13,1304,130426,', '涉  县', '10105', '130426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130427', '1304', '0,1,13,1304,130427,', '磁  县', '10106', '130427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130428', '1304', '0,1,13,1304,130428,', '肥乡县', '10107', '130428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130429', '1304', '0,1,13,1304,130429,', '永年县', '10108', '130429', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130430', '1304', '0,1,13,1304,130430,', '邱  县', '10109', '130430', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130431', '1304', '0,1,13,1304,130431,', '鸡泽县', '10110', '130431', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130432', '1304', '0,1,13,1304,130432,', '广平县', '10111', '130432', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130433', '1304', '0,1,13,1304,130433,', '馆陶县', '10112', '130433', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130434', '1304', '0,1,13,1304,130434,', '魏  县', '10113', '130434', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130435', '1304', '0,1,13,1304,130435,', '曲周县', '10114', '130435', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130481', '1304', '0,1,13,1304,130481,', '武安市', '10115', '130481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1305', '13', '0,1,13,1305,', '邢台', '10116', '1305', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130501', '1305', '0,1,13,1305,130501,', '市辖区', '10117', '130501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130502', '1305', '0,1,13,1305,130502,', '桥东区', '10118', '130502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130503', '1305', '0,1,13,1305,130503,', '桥西区', '10119', '130503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130521', '1305', '0,1,13,1305,130521,', '邢台县', '10120', '130521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130522', '1305', '0,1,13,1305,130522,', '临城县', '10121', '130522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130523', '1305', '0,1,13,1305,130523,', '内丘县', '10122', '130523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130524', '1305', '0,1,13,1305,130524,', '柏乡县', '10123', '130524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130525', '1305', '0,1,13,1305,130525,', '隆尧县', '10124', '130525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130526', '1305', '0,1,13,1305,130526,', '任  县', '10125', '130526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130527', '1305', '0,1,13,1305,130527,', '南和县', '10126', '130527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130528', '1305', '0,1,13,1305,130528,', '宁晋县', '10127', '130528', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130529', '1305', '0,1,13,1305,130529,', '巨鹿县', '10128', '130529', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130530', '1305', '0,1,13,1305,130530,', '新河县', '10129', '130530', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130531', '1305', '0,1,13,1305,130531,', '广宗县', '10130', '130531', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130532', '1305', '0,1,13,1305,130532,', '平乡县', '10131', '130532', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130533', '1305', '0,1,13,1305,130533,', '威  县', '10132', '130533', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130534', '1305', '0,1,13,1305,130534,', '清河县', '10133', '130534', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130535', '1305', '0,1,13,1305,130535,', '临西县', '10134', '130535', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130581', '1305', '0,1,13,1305,130581,', '南宫市', '10135', '130581', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130582', '1305', '0,1,13,1305,130582,', '沙河市', '10136', '130582', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1306', '13', '0,1,13,1306,', '保定', '10137', '1306', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130601', '1306', '0,1,13,1306,130601,', '市辖区', '10138', '130601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130602', '1306', '0,1,13,1306,130602,', '新市区', '10139', '130602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130603', '1306', '0,1,13,1306,130603,', '北市区', '10140', '130603', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130604', '1306', '0,1,13,1306,130604,', '南市区', '10141', '130604', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130621', '1306', '0,1,13,1306,130621,', '满城县', '10142', '130621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130622', '1306', '0,1,13,1306,130622,', '清苑县', '10143', '130622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130623', '1306', '0,1,13,1306,130623,', '涞水县', '10144', '130623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130624', '1306', '0,1,13,1306,130624,', '阜平县', '10145', '130624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130625', '1306', '0,1,13,1306,130625,', '徐水县', '10146', '130625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130626', '1306', '0,1,13,1306,130626,', '定兴县', '10147', '130626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130627', '1306', '0,1,13,1306,130627,', '唐  县', '10148', '130627', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130628', '1306', '0,1,13,1306,130628,', '高阳县', '10149', '130628', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130629', '1306', '0,1,13,1306,130629,', '容城县', '10150', '130629', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130630', '1306', '0,1,13,1306,130630,', '涞源县', '10151', '130630', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130631', '1306', '0,1,13,1306,130631,', '望都县', '10152', '130631', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130632', '1306', '0,1,13,1306,130632,', '安新县', '10153', '130632', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130633', '1306', '0,1,13,1306,130633,', '易  县', '10154', '130633', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130634', '1306', '0,1,13,1306,130634,', '曲阳县', '10155', '130634', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130635', '1306', '0,1,13,1306,130635,', '蠡  县', '10156', '130635', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130636', '1306', '0,1,13,1306,130636,', '顺平县', '10157', '130636', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130637', '1306', '0,1,13,1306,130637,', '博野县', '10158', '130637', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130638', '1306', '0,1,13,1306,130638,', '雄  县', '10159', '130638', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130681', '1306', '0,1,13,1306,130681,', '涿州市', '10160', '130681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130682', '1306', '0,1,13,1306,130682,', '定州市', '10161', '130682', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130683', '1306', '0,1,13,1306,130683,', '安国市', '10162', '130683', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130684', '1306', '0,1,13,1306,130684,', '高碑店市', '10163', '130684', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1307', '13', '0,1,13,1307,', '张家口', '10164', '1307', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130701', '1307', '0,1,13,1307,130701,', '市辖区', '10165', '130701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130702', '1307', '0,1,13,1307,130702,', '桥东区', '10166', '130702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130703', '1307', '0,1,13,1307,130703,', '桥西区', '10167', '130703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130705', '1307', '0,1,13,1307,130705,', '宣化区', '10168', '130705', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130706', '1307', '0,1,13,1307,130706,', '下花园区', '10169', '130706', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130721', '1307', '0,1,13,1307,130721,', '宣化县', '10170', '130721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130722', '1307', '0,1,13,1307,130722,', '张北县', '10171', '130722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130723', '1307', '0,1,13,1307,130723,', '康保县', '10172', '130723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130724', '1307', '0,1,13,1307,130724,', '沽源县', '10173', '130724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130725', '1307', '0,1,13,1307,130725,', '尚义县', '10174', '130725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130726', '1307', '0,1,13,1307,130726,', '蔚  县', '10175', '130726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130727', '1307', '0,1,13,1307,130727,', '阳原县', '10176', '130727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130728', '1307', '0,1,13,1307,130728,', '怀安县', '10177', '130728', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130729', '1307', '0,1,13,1307,130729,', '万全县', '10178', '130729', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130730', '1307', '0,1,13,1307,130730,', '怀来县', '10179', '130730', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130731', '1307', '0,1,13,1307,130731,', '涿鹿县', '10180', '130731', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130732', '1307', '0,1,13,1307,130732,', '赤城县', '10181', '130732', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130733', '1307', '0,1,13,1307,130733,', '崇礼县', '10182', '130733', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1308', '13', '0,1,13,1308,', '承德', '10183', '1308', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130801', '1308', '0,1,13,1308,130801,', '市辖区', '10184', '130801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130802', '1308', '0,1,13,1308,130802,', '双桥区', '10185', '130802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130803', '1308', '0,1,13,1308,130803,', '双滦区', '10186', '130803', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130804', '1308', '0,1,13,1308,130804,', '鹰手营子矿区', '10187', '130804', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130821', '1308', '0,1,13,1308,130821,', '承德县', '10188', '130821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130822', '1308', '0,1,13,1308,130822,', '兴隆县', '10189', '130822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130823', '1308', '0,1,13,1308,130823,', '平泉县', '10190', '130823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130824', '1308', '0,1,13,1308,130824,', '滦平县', '10191', '130824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130825', '1308', '0,1,13,1308,130825,', '隆化县', '10192', '130825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130826', '1308', '0,1,13,1308,130826,', '丰宁满族自治县', '10193', '130826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130827', '1308', '0,1,13,1308,130827,', '宽城满族自治县', '10194', '130827', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130828', '1308', '0,1,13,1308,130828,', '围场满族蒙古族自治县', '10195', '130828', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1309', '13', '0,1,13,1309,', '沧州', '10196', '1309', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130901', '1309', '0,1,13,1309,130901,', '市辖区', '10197', '130901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130902', '1309', '0,1,13,1309,130902,', '新华区', '10198', '130902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130903', '1309', '0,1,13,1309,130903,', '运河区', '10199', '130903', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130921', '1309', '0,1,13,1309,130921,', '沧  县', '10200', '130921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130922', '1309', '0,1,13,1309,130922,', '青  县', '10201', '130922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130923', '1309', '0,1,13,1309,130923,', '东光县', '10202', '130923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130924', '1309', '0,1,13,1309,130924,', '海兴县', '10203', '130924', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130925', '1309', '0,1,13,1309,130925,', '盐山县', '10204', '130925', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130926', '1309', '0,1,13,1309,130926,', '肃宁县', '10205', '130926', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130927', '1309', '0,1,13,1309,130927,', '南皮县', '10206', '130927', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130928', '1309', '0,1,13,1309,130928,', '吴桥县', '10207', '130928', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130929', '1309', '0,1,13,1309,130929,', '献  县', '10208', '130929', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130930', '1309', '0,1,13,1309,130930,', '孟村回族自治县', '10209', '130930', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130981', '1309', '0,1,13,1309,130981,', '泊头市', '10210', '130981', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130982', '1309', '0,1,13,1309,130982,', '任丘市', '10211', '130982', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130983', '1309', '0,1,13,1309,130983,', '黄骅市', '10212', '130983', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('130984', '1309', '0,1,13,1309,130984,', '河间市', '10213', '130984', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1310', '13', '0,1,13,1310,', '廊坊', '10214', '1310', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131001', '1310', '0,1,13,1310,131001,', '市辖区', '10215', '131001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131002', '1310', '0,1,13,1310,131002,', '安次区', '10216', '131002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131003', '1310', '0,1,13,1310,131003,', '廊坊市广阳区', '10217', '131003', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131022', '1310', '0,1,13,1310,131022,', '固安县', '10218', '131022', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131023', '1310', '0,1,13,1310,131023,', '永清县', '10219', '131023', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131024', '1310', '0,1,13,1310,131024,', '香河县', '10220', '131024', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131025', '1310', '0,1,13,1310,131025,', '大城县', '10221', '131025', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131026', '1310', '0,1,13,1310,131026,', '文安县', '10222', '131026', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131028', '1310', '0,1,13,1310,131028,', '大厂回族自治县', '10223', '131028', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131081', '1310', '0,1,13,1310,131081,', '霸州市', '10224', '131081', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131082', '1310', '0,1,13,1310,131082,', '三河市', '10225', '131082', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1311', '13', '0,1,13,1311,', '衡水', '10226', '1311', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131101', '1311', '0,1,13,1311,131101,', '市辖区', '10227', '131101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131102', '1311', '0,1,13,1311,131102,', '桃城区', '10228', '131102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131121', '1311', '0,1,13,1311,131121,', '枣强县', '10229', '131121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131122', '1311', '0,1,13,1311,131122,', '武邑县', '10230', '131122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131123', '1311', '0,1,13,1311,131123,', '武强县', '10231', '131123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131124', '1311', '0,1,13,1311,131124,', '饶阳县', '10232', '131124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131125', '1311', '0,1,13,1311,131125,', '安平县', '10233', '131125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131126', '1311', '0,1,13,1311,131126,', '故城县', '10234', '131126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131127', '1311', '0,1,13,1311,131127,', '景  县', '10235', '131127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131128', '1311', '0,1,13,1311,131128,', '阜城县', '10236', '131128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131181', '1311', '0,1,13,1311,131181,', '冀州市', '10237', '131181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('131182', '1311', '0,1,13,1311,131182,', '深州市', '10238', '131182', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('14', '1', '0,1,14,', '山西', '10239', '14', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1401', '14', '0,1,14,1401,', '太原', '10240', '1401', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140101', '1401', '0,1,14,1401,140101,', '市辖区', '10241', '140101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140105', '1401', '0,1,14,1401,140105,', '小店区', '10242', '140105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140106', '1401', '0,1,14,1401,140106,', '迎泽区', '10243', '140106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140107', '1401', '0,1,14,1401,140107,', '杏花岭区', '10244', '140107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140108', '1401', '0,1,14,1401,140108,', '尖草坪区', '10245', '140108', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140109', '1401', '0,1,14,1401,140109,', '万柏林区', '10246', '140109', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140110', '1401', '0,1,14,1401,140110,', '晋源区', '10247', '140110', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140121', '1401', '0,1,14,1401,140121,', '清徐县', '10248', '140121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140122', '1401', '0,1,14,1401,140122,', '阳曲县', '10249', '140122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140123', '1401', '0,1,14,1401,140123,', '娄烦县', '10250', '140123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140181', '1401', '0,1,14,1401,140181,', '古交市', '10251', '140181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1402', '14', '0,1,14,1402,', '大同', '10252', '1402', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140201', '1402', '0,1,14,1402,140201,', '市辖区', '10253', '140201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140202', '1402', '0,1,14,1402,140202,', '城  区', '10254', '140202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140203', '1402', '0,1,14,1402,140203,', '矿  区', '10255', '140203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140211', '1402', '0,1,14,1402,140211,', '南郊区', '10256', '140211', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140212', '1402', '0,1,14,1402,140212,', '新荣区', '10257', '140212', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140221', '1402', '0,1,14,1402,140221,', '阳高县', '10258', '140221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140222', '1402', '0,1,14,1402,140222,', '天镇县', '10259', '140222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140223', '1402', '0,1,14,1402,140223,', '广灵县', '10260', '140223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140224', '1402', '0,1,14,1402,140224,', '灵丘县', '10261', '140224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140225', '1402', '0,1,14,1402,140225,', '浑源县', '10262', '140225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140226', '1402', '0,1,14,1402,140226,', '左云县', '10263', '140226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140227', '1402', '0,1,14,1402,140227,', '大同县', '10264', '140227', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1403', '14', '0,1,14,1403,', '阳泉', '10265', '1403', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140301', '1403', '0,1,14,1403,140301,', '市辖区', '10266', '140301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140302', '1403', '0,1,14,1403,140302,', '城  区', '10267', '140302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140303', '1403', '0,1,14,1403,140303,', '矿  区', '10268', '140303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140311', '1403', '0,1,14,1403,140311,', '郊  区', '10269', '140311', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140321', '1403', '0,1,14,1403,140321,', '平定县', '10270', '140321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140322', '1403', '0,1,14,1403,140322,', '盂  县', '10271', '140322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1404', '14', '0,1,14,1404,', '长治', '10272', '1404', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140401', '1404', '0,1,14,1404,140401,', '市辖区', '10273', '140401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140402', '1404', '0,1,14,1404,140402,', '城  区', '10274', '140402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140411', '1404', '0,1,14,1404,140411,', '郊  区', '10275', '140411', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140421', '1404', '0,1,14,1404,140421,', '长治县', '10276', '140421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140423', '1404', '0,1,14,1404,140423,', '襄垣县', '10277', '140423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140424', '1404', '0,1,14,1404,140424,', '屯留县', '10278', '140424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140425', '1404', '0,1,14,1404,140425,', '平顺县', '10279', '140425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140426', '1404', '0,1,14,1404,140426,', '黎城县', '10280', '140426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140427', '1404', '0,1,14,1404,140427,', '壶关县', '10281', '140427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140428', '1404', '0,1,14,1404,140428,', '长子县', '10282', '140428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140429', '1404', '0,1,14,1404,140429,', '武乡县', '10283', '140429', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140430', '1404', '0,1,14,1404,140430,', '沁  县', '10284', '140430', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140431', '1404', '0,1,14,1404,140431,', '沁源县', '10285', '140431', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140481', '1404', '0,1,14,1404,140481,', '潞城市', '10286', '140481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1405', '14', '0,1,14,1405,', '晋城', '10287', '1405', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140501', '1405', '0,1,14,1405,140501,', '市辖区', '10288', '140501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140502', '1405', '0,1,14,1405,140502,', '城  区', '10289', '140502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140521', '1405', '0,1,14,1405,140521,', '沁水县', '10290', '140521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140522', '1405', '0,1,14,1405,140522,', '阳城县', '10291', '140522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140524', '1405', '0,1,14,1405,140524,', '陵川县', '10292', '140524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140525', '1405', '0,1,14,1405,140525,', '泽州县', '10293', '140525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140581', '1405', '0,1,14,1405,140581,', '高平市', '10294', '140581', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1406', '14', '0,1,14,1406,', '朔州', '10295', '1406', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140601', '1406', '0,1,14,1406,140601,', '市辖区', '10296', '140601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140602', '1406', '0,1,14,1406,140602,', '朔城区', '10297', '140602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140603', '1406', '0,1,14,1406,140603,', '平鲁区', '10298', '140603', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140621', '1406', '0,1,14,1406,140621,', '山阴县', '10299', '140621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140622', '1406', '0,1,14,1406,140622,', '应  县', '10300', '140622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140623', '1406', '0,1,14,1406,140623,', '右玉县', '10301', '140623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140624', '1406', '0,1,14,1406,140624,', '怀仁县', '10302', '140624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1407', '14', '0,1,14,1407,', '晋中', '10303', '1407', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140701', '1407', '0,1,14,1407,140701,', '市辖区', '10304', '140701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140702', '1407', '0,1,14,1407,140702,', '榆次区', '10305', '140702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140721', '1407', '0,1,14,1407,140721,', '榆社县', '10306', '140721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140722', '1407', '0,1,14,1407,140722,', '左权县', '10307', '140722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140723', '1407', '0,1,14,1407,140723,', '和顺县', '10308', '140723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140724', '1407', '0,1,14,1407,140724,', '昔阳县', '10309', '140724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140725', '1407', '0,1,14,1407,140725,', '寿阳县', '10310', '140725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140726', '1407', '0,1,14,1407,140726,', '太谷县', '10311', '140726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140727', '1407', '0,1,14,1407,140727,', '祁  县', '10312', '140727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140728', '1407', '0,1,14,1407,140728,', '平遥县', '10313', '140728', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140729', '1407', '0,1,14,1407,140729,', '灵石县', '10314', '140729', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140781', '1407', '0,1,14,1407,140781,', '介休市', '10315', '140781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1408', '14', '0,1,14,1408,', '运城', '10316', '1408', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140801', '1408', '0,1,14,1408,140801,', '市辖区', '10317', '140801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140802', '1408', '0,1,14,1408,140802,', '盐湖区', '10318', '140802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140821', '1408', '0,1,14,1408,140821,', '临猗县', '10319', '140821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140822', '1408', '0,1,14,1408,140822,', '万荣县', '10320', '140822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140823', '1408', '0,1,14,1408,140823,', '闻喜县', '10321', '140823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140824', '1408', '0,1,14,1408,140824,', '稷山县', '10322', '140824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140825', '1408', '0,1,14,1408,140825,', '新绛县', '10323', '140825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140826', '1408', '0,1,14,1408,140826,', '绛  县', '10324', '140826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140827', '1408', '0,1,14,1408,140827,', '垣曲县', '10325', '140827', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140828', '1408', '0,1,14,1408,140828,', '夏  县', '10326', '140828', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140829', '1408', '0,1,14,1408,140829,', '平陆县', '10327', '140829', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140830', '1408', '0,1,14,1408,140830,', '芮城县', '10328', '140830', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140881', '1408', '0,1,14,1408,140881,', '永济市', '10329', '140881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140882', '1408', '0,1,14,1408,140882,', '河津市', '10330', '140882', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1409', '14', '0,1,14,1409,', '忻州', '10331', '1409', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140901', '1409', '0,1,14,1409,140901,', '市辖区', '10332', '140901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140902', '1409', '0,1,14,1409,140902,', '忻府区', '10333', '140902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140921', '1409', '0,1,14,1409,140921,', '定襄县', '10334', '140921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140922', '1409', '0,1,14,1409,140922,', '五台县', '10335', '140922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140923', '1409', '0,1,14,1409,140923,', '代  县', '10336', '140923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140924', '1409', '0,1,14,1409,140924,', '繁峙县', '10337', '140924', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140925', '1409', '0,1,14,1409,140925,', '宁武县', '10338', '140925', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140926', '1409', '0,1,14,1409,140926,', '静乐县', '10339', '140926', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140927', '1409', '0,1,14,1409,140927,', '神池县', '10340', '140927', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140928', '1409', '0,1,14,1409,140928,', '五寨县', '10341', '140928', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140929', '1409', '0,1,14,1409,140929,', '岢岚县', '10342', '140929', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140930', '1409', '0,1,14,1409,140930,', '河曲县', '10343', '140930', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140931', '1409', '0,1,14,1409,140931,', '保德县', '10344', '140931', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140932', '1409', '0,1,14,1409,140932,', '偏关县', '10345', '140932', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('140981', '1409', '0,1,14,1409,140981,', '原平市', '10346', '140981', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1410', '14', '0,1,14,1410,', '临汾', '10347', '1410', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141001', '1410', '0,1,14,1410,141001,', '市辖区', '10348', '141001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141002', '1410', '0,1,14,1410,141002,', '尧都区', '10349', '141002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141021', '1410', '0,1,14,1410,141021,', '曲沃县', '10350', '141021', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141022', '1410', '0,1,14,1410,141022,', '翼城县', '10351', '141022', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141023', '1410', '0,1,14,1410,141023,', '襄汾县', '10352', '141023', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141024', '1410', '0,1,14,1410,141024,', '洪洞县', '10353', '141024', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141025', '1410', '0,1,14,1410,141025,', '古  县', '10354', '141025', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141026', '1410', '0,1,14,1410,141026,', '安泽县', '10355', '141026', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141027', '1410', '0,1,14,1410,141027,', '浮山县', '10356', '141027', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141028', '1410', '0,1,14,1410,141028,', '吉  县', '10357', '141028', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141029', '1410', '0,1,14,1410,141029,', '乡宁县', '10358', '141029', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141030', '1410', '0,1,14,1410,141030,', '大宁县', '10359', '141030', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141031', '1410', '0,1,14,1410,141031,', '隰  县', '10360', '141031', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141032', '1410', '0,1,14,1410,141032,', '永和县', '10361', '141032', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141033', '1410', '0,1,14,1410,141033,', '蒲  县', '10362', '141033', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141034', '1410', '0,1,14,1410,141034,', '汾西县', '10363', '141034', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141081', '1410', '0,1,14,1410,141081,', '侯马市', '10364', '141081', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('141082', '1410', '0,1,14,1410,141082,', '霍州市', '10365', '141082', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1423', '14', '0,1,14,1423,', '吕梁地区', '10366', '1423', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142301', '1423', '0,1,14,1423,142301,', '孝义市', '10367', '142301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142302', '1423', '0,1,14,1423,142302,', '离石市', '10368', '142302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142303', '1423', '0,1,14,1423,142303,', '汾阳市', '10369', '142303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142322', '1423', '0,1,14,1423,142322,', '文水县', '10370', '142322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142323', '1423', '0,1,14,1423,142323,', '交城县', '10371', '142323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142325', '1423', '0,1,14,1423,142325,', '兴  县', '10372', '142325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142326', '1423', '0,1,14,1423,142326,', '临  县', '10373', '142326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142327', '1423', '0,1,14,1423,142327,', '柳林县', '10374', '142327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142328', '1423', '0,1,14,1423,142328,', '石楼县', '10375', '142328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142329', '1423', '0,1,14,1423,142329,', '岚  县', '10376', '142329', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142330', '1423', '0,1,14,1423,142330,', '方山县', '10377', '142330', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142332', '1423', '0,1,14,1423,142332,', '中阳县', '10378', '142332', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('142333', '1423', '0,1,14,1423,142333,', '交口县', '10379', '142333', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('15', '1', '0,1,15,', '内蒙古', '10380', '15', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1501', '15', '0,1,15,1501,', '呼和浩特', '10381', '1501', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150101', '1501', '0,1,15,1501,150101,', '市辖区', '10382', '150101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150102', '1501', '0,1,15,1501,150102,', '新城区', '10383', '150102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150103', '1501', '0,1,15,1501,150103,', '回民区', '10384', '150103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150104', '1501', '0,1,15,1501,150104,', '玉泉区', '10385', '150104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150105', '1501', '0,1,15,1501,150105,', '赛罕区', '10386', '150105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150121', '1501', '0,1,15,1501,150121,', '土默特左旗', '10387', '150121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150122', '1501', '0,1,15,1501,150122,', '托克托县', '10388', '150122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150123', '1501', '0,1,15,1501,150123,', '和林格尔县', '10389', '150123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150124', '1501', '0,1,15,1501,150124,', '清水河县', '10390', '150124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150125', '1501', '0,1,15,1501,150125,', '武川县', '10391', '150125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1502', '15', '0,1,15,1502,', '包头', '10392', '1502', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150201', '1502', '0,1,15,1502,150201,', '市辖区', '10393', '150201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150202', '1502', '0,1,15,1502,150202,', '东河区', '10394', '150202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150203', '1502', '0,1,15,1502,150203,', '昆都伦区', '10395', '150203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150204', '1502', '0,1,15,1502,150204,', '青山区', '10396', '150204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150205', '1502', '0,1,15,1502,150205,', '石拐区', '10397', '150205', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150206', '1502', '0,1,15,1502,150206,', '白云矿区', '10398', '150206', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150207', '1502', '0,1,15,1502,150207,', '九原区', '10399', '150207', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150221', '1502', '0,1,15,1502,150221,', '土默特右旗', '10400', '150221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150222', '1502', '0,1,15,1502,150222,', '固阳县', '10401', '150222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150223', '1502', '0,1,15,1502,150223,', '达尔罕茂明安联合旗', '10402', '150223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1503', '15', '0,1,15,1503,', '乌海', '10403', '1503', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150301', '1503', '0,1,15,1503,150301,', '市辖区', '10404', '150301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150302', '1503', '0,1,15,1503,150302,', '海勃湾区', '10405', '150302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150303', '1503', '0,1,15,1503,150303,', '海南区', '10406', '150303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150304', '1503', '0,1,15,1503,150304,', '乌达区', '10407', '150304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1504', '15', '0,1,15,1504,', '赤峰', '10408', '1504', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150401', '1504', '0,1,15,1504,150401,', '市辖区', '10409', '150401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150402', '1504', '0,1,15,1504,150402,', '红山区', '10410', '150402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150403', '1504', '0,1,15,1504,150403,', '元宝山区', '10411', '150403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150404', '1504', '0,1,15,1504,150404,', '松山区', '10412', '150404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150421', '1504', '0,1,15,1504,150421,', '阿鲁科尔沁旗', '10413', '150421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150422', '1504', '0,1,15,1504,150422,', '巴林左旗', '10414', '150422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150423', '1504', '0,1,15,1504,150423,', '巴林右旗', '10415', '150423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150424', '1504', '0,1,15,1504,150424,', '林西县', '10416', '150424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150425', '1504', '0,1,15,1504,150425,', '克什克腾旗', '10417', '150425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150426', '1504', '0,1,15,1504,150426,', '翁牛特旗', '10418', '150426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150428', '1504', '0,1,15,1504,150428,', '喀喇沁旗', '10419', '150428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150429', '1504', '0,1,15,1504,150429,', '宁城县', '10420', '150429', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150430', '1504', '0,1,15,1504,150430,', '敖汉旗', '10421', '150430', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1505', '15', '0,1,15,1505,', '通辽', '10422', '1505', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150501', '1505', '0,1,15,1505,150501,', '市辖区', '10423', '150501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150502', '1505', '0,1,15,1505,150502,', '科尔沁区', '10424', '150502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150521', '1505', '0,1,15,1505,150521,', '科尔沁左翼中旗', '10425', '150521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150522', '1505', '0,1,15,1505,150522,', '科尔沁左翼后旗', '10426', '150522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150523', '1505', '0,1,15,1505,150523,', '开鲁县', '10427', '150523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150524', '1505', '0,1,15,1505,150524,', '库伦旗', '10428', '150524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150525', '1505', '0,1,15,1505,150525,', '奈曼旗', '10429', '150525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150526', '1505', '0,1,15,1505,150526,', '扎鲁特旗', '10430', '150526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('150581', '1505', '0,1,15,1505,150581,', '霍林郭勒市', '10431', '150581', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1521', '15', '0,1,15,1521,', '呼伦贝尔盟', '10432', '1521', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152101', '1521', '0,1,15,1521,152101,', '海拉尔市', '10433', '152101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152102', '1521', '0,1,15,1521,152102,', '满洲里市', '10434', '152102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152103', '1521', '0,1,15,1521,152103,', '扎兰屯市', '10435', '152103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152104', '1521', '0,1,15,1521,152104,', '牙克石市', '10436', '152104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152105', '1521', '0,1,15,1521,152105,', '根河市', '10437', '152105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152106', '1521', '0,1,15,1521,152106,', '额尔古纳市', '10438', '152106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152122', '1521', '0,1,15,1521,152122,', '阿荣旗', '10439', '152122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152123', '1521', '0,1,15,1521,152123,', '莫力达瓦达斡尔族自治', '10440', '152123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152127', '1521', '0,1,15,1521,152127,', '鄂伦春自治旗', '10441', '152127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152128', '1521', '0,1,15,1521,152128,', '鄂温克族自治旗', '10442', '152128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152129', '1521', '0,1,15,1521,152129,', '新巴尔虎右旗', '10443', '152129', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152130', '1521', '0,1,15,1521,152130,', '新巴尔虎左旗', '10444', '152130', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152131', '1521', '0,1,15,1521,152131,', '陈巴尔虎旗', '10445', '152131', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1522', '15', '0,1,15,1522,', '兴安盟', '10446', '1522', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152201', '1522', '0,1,15,1522,152201,', '乌兰浩特市', '10447', '152201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152202', '1522', '0,1,15,1522,152202,', '阿尔山市', '10448', '152202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152221', '1522', '0,1,15,1522,152221,', '科尔沁右翼前旗', '10449', '152221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152222', '1522', '0,1,15,1522,152222,', '科尔沁右翼中旗', '10450', '152222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152223', '1522', '0,1,15,1522,152223,', '扎赉特旗', '10451', '152223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152224', '1522', '0,1,15,1522,152224,', '突泉县', '10452', '152224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1525', '15', '0,1,15,1525,', '锡林郭勒盟', '10453', '1525', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152501', '1525', '0,1,15,1525,152501,', '二连浩特市', '10454', '152501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152502', '1525', '0,1,15,1525,152502,', '锡林浩特市', '10455', '152502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152522', '1525', '0,1,15,1525,152522,', '阿巴嘎旗', '10456', '152522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152523', '1525', '0,1,15,1525,152523,', '苏尼特左旗', '10457', '152523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152524', '1525', '0,1,15,1525,152524,', '苏尼特右旗', '10458', '152524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152525', '1525', '0,1,15,1525,152525,', '东乌珠穆沁旗', '10459', '152525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152526', '1525', '0,1,15,1525,152526,', '西乌珠穆沁旗', '10460', '152526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152527', '1525', '0,1,15,1525,152527,', '太仆寺旗', '10461', '152527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152528', '1525', '0,1,15,1525,152528,', '镶黄旗', '10462', '152528', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152529', '1525', '0,1,15,1525,152529,', '正镶白旗', '10463', '152529', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152530', '1525', '0,1,15,1525,152530,', '正蓝旗', '10464', '152530', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152531', '1525', '0,1,15,1525,152531,', '多伦县', '10465', '152531', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1526', '15', '0,1,15,1526,', '乌兰察布盟', '10466', '1526', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152601', '1526', '0,1,15,1526,152601,', '集宁市', '10467', '152601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152602', '1526', '0,1,15,1526,152602,', '丰镇市', '10468', '152602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152624', '1526', '0,1,15,1526,152624,', '卓资县', '10469', '152624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152625', '1526', '0,1,15,1526,152625,', '化德县', '10470', '152625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152626', '1526', '0,1,15,1526,152626,', '商都县', '10471', '152626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152627', '1526', '0,1,15,1526,152627,', '兴和县', '10472', '152627', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152629', '1526', '0,1,15,1526,152629,', '凉城县', '10473', '152629', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152630', '1526', '0,1,15,1526,152630,', '察哈尔右翼前旗', '10474', '152630', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152631', '1526', '0,1,15,1526,152631,', '察哈尔右翼中旗', '10475', '152631', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152632', '1526', '0,1,15,1526,152632,', '察哈尔右翼后旗', '10476', '152632', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152634', '1526', '0,1,15,1526,152634,', '四子王旗', '10477', '152634', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1527', '15', '0,1,15,1527,', '伊克昭盟', '10478', '1527', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152701', '1527', '0,1,15,1527,152701,', '东胜市', '10479', '152701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152722', '1527', '0,1,15,1527,152722,', '达拉特旗', '10480', '152722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152723', '1527', '0,1,15,1527,152723,', '准格尔旗', '10481', '152723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152724', '1527', '0,1,15,1527,152724,', '鄂托克前旗', '10482', '152724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152725', '1527', '0,1,15,1527,152725,', '鄂托克旗', '10483', '152725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152726', '1527', '0,1,15,1527,152726,', '杭锦旗', '10484', '152726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152727', '1527', '0,1,15,1527,152727,', '乌审旗', '10485', '152727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152728', '1527', '0,1,15,1527,152728,', '伊金霍洛旗', '10486', '152728', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1528', '15', '0,1,15,1528,', '巴彦淖尔盟', '10487', '1528', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152801', '1528', '0,1,15,1528,152801,', '临河市', '10488', '152801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152822', '1528', '0,1,15,1528,152822,', '五原县', '10489', '152822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152823', '1528', '0,1,15,1528,152823,', '磴口县', '10490', '152823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152824', '1528', '0,1,15,1528,152824,', '乌拉特前旗', '10491', '152824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152825', '1528', '0,1,15,1528,152825,', '乌拉特中旗', '10492', '152825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152826', '1528', '0,1,15,1528,152826,', '乌拉特后旗', '10493', '152826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152827', '1528', '0,1,15,1528,152827,', '杭锦后旗', '10494', '152827', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('1529', '15', '0,1,15,1529,', '阿拉善盟', '10495', '1529', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152921', '1529', '0,1,15,1529,152921,', '阿拉善左旗', '10496', '152921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152922', '1529', '0,1,15,1529,152922,', '阿拉善右旗', '10497', '152922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('152923', '1529', '0,1,15,1529,152923,', '额济纳旗', '10498', '152923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('21', '1', '0,1,21,', '辽宁', '10499', '21', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2101', '21', '0,1,21,2101,', '沈阳', '10500', '2101', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210101', '2101', '0,1,21,2101,210101,', '市辖区', '10501', '210101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210102', '2101', '0,1,21,2101,210102,', '和平区', '10502', '210102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210103', '2101', '0,1,21,2101,210103,', '沈河区', '10503', '210103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210104', '2101', '0,1,21,2101,210104,', '大东区', '10504', '210104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210105', '2101', '0,1,21,2101,210105,', '皇姑区', '10505', '210105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210106', '2101', '0,1,21,2101,210106,', '铁西区', '10506', '210106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210111', '2101', '0,1,21,2101,210111,', '苏家屯区', '10507', '210111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210112', '2101', '0,1,21,2101,210112,', '东陵区', '10508', '210112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210113', '2101', '0,1,21,2101,210113,', '新城子区', '10509', '210113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210114', '2101', '0,1,21,2101,210114,', '于洪区', '10510', '210114', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210122', '2101', '0,1,21,2101,210122,', '辽中县', '10511', '210122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210123', '2101', '0,1,21,2101,210123,', '康平县', '10512', '210123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210124', '2101', '0,1,21,2101,210124,', '法库县', '10513', '210124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210181', '2101', '0,1,21,2101,210181,', '新民市', '10514', '210181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2102', '21', '0,1,21,2102,', '大连', '10515', '2102', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210201', '2102', '0,1,21,2102,210201,', '市辖区', '10516', '210201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210202', '2102', '0,1,21,2102,210202,', '中山区', '10517', '210202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210203', '2102', '0,1,21,2102,210203,', '西岗区', '10518', '210203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210204', '2102', '0,1,21,2102,210204,', '沙河口区', '10519', '210204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210211', '2102', '0,1,21,2102,210211,', '甘井子区', '10520', '210211', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210212', '2102', '0,1,21,2102,210212,', '旅顺口区', '10521', '210212', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210213', '2102', '0,1,21,2102,210213,', '金州区', '10522', '210213', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210224', '2102', '0,1,21,2102,210224,', '长海县', '10523', '210224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210281', '2102', '0,1,21,2102,210281,', '瓦房店市', '10524', '210281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210282', '2102', '0,1,21,2102,210282,', '普兰店市', '10525', '210282', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210283', '2102', '0,1,21,2102,210283,', '庄河市', '10526', '210283', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2103', '21', '0,1,21,2103,', '鞍山', '10527', '2103', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210301', '2103', '0,1,21,2103,210301,', '市辖区', '10528', '210301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210302', '2103', '0,1,21,2103,210302,', '铁东区', '10529', '210302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210303', '2103', '0,1,21,2103,210303,', '铁西区', '10530', '210303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210304', '2103', '0,1,21,2103,210304,', '立山区', '10531', '210304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210311', '2103', '0,1,21,2103,210311,', '千山区', '10532', '210311', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210321', '2103', '0,1,21,2103,210321,', '台安县', '10533', '210321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210323', '2103', '0,1,21,2103,210323,', '岫岩满族自治县', '10534', '210323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210381', '2103', '0,1,21,2103,210381,', '海城市', '10535', '210381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2104', '21', '0,1,21,2104,', '抚顺', '10536', '2104', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210401', '2104', '0,1,21,2104,210401,', '市辖区', '10537', '210401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210402', '2104', '0,1,21,2104,210402,', '新抚区', '10538', '210402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210403', '2104', '0,1,21,2104,210403,', '东洲区', '10539', '210403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210404', '2104', '0,1,21,2104,210404,', '望花区', '10540', '210404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210411', '2104', '0,1,21,2104,210411,', '顺城区', '10541', '210411', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210421', '2104', '0,1,21,2104,210421,', '抚顺县', '10542', '210421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210422', '2104', '0,1,21,2104,210422,', '新宾满族自治县', '10543', '210422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210423', '2104', '0,1,21,2104,210423,', '清原满族自治县', '10544', '210423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2105', '21', '0,1,21,2105,', '本溪', '10545', '2105', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210501', '2105', '0,1,21,2105,210501,', '市辖区', '10546', '210501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210502', '2105', '0,1,21,2105,210502,', '平山区', '10547', '210502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210503', '2105', '0,1,21,2105,210503,', '溪湖区', '10548', '210503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210504', '2105', '0,1,21,2105,210504,', '明山区', '10549', '210504', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210505', '2105', '0,1,21,2105,210505,', '南芬区', '10550', '210505', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210521', '2105', '0,1,21,2105,210521,', '本溪满族自治县', '10551', '210521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210522', '2105', '0,1,21,2105,210522,', '桓仁满族自治县', '10552', '210522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2106', '21', '0,1,21,2106,', '丹东', '10553', '2106', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210601', '2106', '0,1,21,2106,210601,', '市辖区', '10554', '210601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210602', '2106', '0,1,21,2106,210602,', '元宝区', '10555', '210602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210603', '2106', '0,1,21,2106,210603,', '振兴区', '10556', '210603', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210604', '2106', '0,1,21,2106,210604,', '振安区', '10557', '210604', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210624', '2106', '0,1,21,2106,210624,', '宽甸满族自治县', '10558', '210624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210681', '2106', '0,1,21,2106,210681,', '东港市', '10559', '210681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210682', '2106', '0,1,21,2106,210682,', '凤城市', '10560', '210682', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2107', '21', '0,1,21,2107,', '锦州', '10561', '2107', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210701', '2107', '0,1,21,2107,210701,', '市辖区', '10562', '210701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210702', '2107', '0,1,21,2107,210702,', '古塔区', '10563', '210702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210703', '2107', '0,1,21,2107,210703,', '凌河区', '10564', '210703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210711', '2107', '0,1,21,2107,210711,', '太和区', '10565', '210711', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210726', '2107', '0,1,21,2107,210726,', '黑山县', '10566', '210726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210727', '2107', '0,1,21,2107,210727,', '义  县', '10567', '210727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210781', '2107', '0,1,21,2107,210781,', '凌海市', '10568', '210781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210782', '2107', '0,1,21,2107,210782,', '北宁市', '10569', '210782', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2108', '21', '0,1,21,2108,', '营口', '10570', '2108', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210801', '2108', '0,1,21,2108,210801,', '市辖区', '10571', '210801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210802', '2108', '0,1,21,2108,210802,', '站前区', '10572', '210802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210803', '2108', '0,1,21,2108,210803,', '西市区', '10573', '210803', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210804', '2108', '0,1,21,2108,210804,', '鲅鱼圈区', '10574', '210804', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210811', '2108', '0,1,21,2108,210811,', '老边区', '10575', '210811', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210881', '2108', '0,1,21,2108,210881,', '盖州市', '10576', '210881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210882', '2108', '0,1,21,2108,210882,', '大石桥市', '10577', '210882', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2109', '21', '0,1,21,2109,', '阜新', '10578', '2109', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210901', '2109', '0,1,21,2109,210901,', '市辖区', '10579', '210901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210902', '2109', '0,1,21,2109,210902,', '海州区', '10580', '210902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210903', '2109', '0,1,21,2109,210903,', '新邱区', '10581', '210903', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210904', '2109', '0,1,21,2109,210904,', '太平区', '10582', '210904', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210905', '2109', '0,1,21,2109,210905,', '清河门区', '10583', '210905', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210911', '2109', '0,1,21,2109,210911,', '细河区', '10584', '210911', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210921', '2109', '0,1,21,2109,210921,', '阜新蒙古族自治县', '10585', '210921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('210922', '2109', '0,1,21,2109,210922,', '彰武县', '10586', '210922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2110', '21', '0,1,21,2110,', '辽阳', '10587', '2110', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211001', '2110', '0,1,21,2110,211001,', '市辖区', '10588', '211001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211002', '2110', '0,1,21,2110,211002,', '白塔区', '10589', '211002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211003', '2110', '0,1,21,2110,211003,', '文圣区', '10590', '211003', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211004', '2110', '0,1,21,2110,211004,', '宏伟区', '10591', '211004', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211005', '2110', '0,1,21,2110,211005,', '弓长岭区', '10592', '211005', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211011', '2110', '0,1,21,2110,211011,', '太子河区', '10593', '211011', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211021', '2110', '0,1,21,2110,211021,', '辽阳县', '10594', '211021', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211081', '2110', '0,1,21,2110,211081,', '灯塔市', '10595', '211081', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2111', '21', '0,1,21,2111,', '盘锦', '10596', '2111', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211101', '2111', '0,1,21,2111,211101,', '市辖区', '10597', '211101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211102', '2111', '0,1,21,2111,211102,', '双台子区', '10598', '211102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211103', '2111', '0,1,21,2111,211103,', '兴隆台区', '10599', '211103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211121', '2111', '0,1,21,2111,211121,', '大洼县', '10600', '211121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211122', '2111', '0,1,21,2111,211122,', '盘山县', '10601', '211122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2112', '21', '0,1,21,2112,', '铁岭', '10602', '2112', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211201', '2112', '0,1,21,2112,211201,', '市辖区', '10603', '211201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211202', '2112', '0,1,21,2112,211202,', '银州区', '10604', '211202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211204', '2112', '0,1,21,2112,211204,', '清河区', '10605', '211204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211221', '2112', '0,1,21,2112,211221,', '铁岭县', '10606', '211221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211223', '2112', '0,1,21,2112,211223,', '西丰县', '10607', '211223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211224', '2112', '0,1,21,2112,211224,', '昌图县', '10608', '211224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211281', '2112', '0,1,21,2112,211281,', '铁法市', '10609', '211281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211282', '2112', '0,1,21,2112,211282,', '开原市', '10610', '211282', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2113', '21', '0,1,21,2113,', '朝阳', '10611', '2113', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211301', '2113', '0,1,21,2113,211301,', '市辖区', '10612', '211301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211302', '2113', '0,1,21,2113,211302,', '双塔区', '10613', '211302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211303', '2113', '0,1,21,2113,211303,', '龙城区', '10614', '211303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211321', '2113', '0,1,21,2113,211321,', '朝阳县', '10615', '211321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211322', '2113', '0,1,21,2113,211322,', '建平县', '10616', '211322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211324', '2113', '0,1,21,2113,211324,', '喀喇沁左翼蒙古族自治', '10617', '211324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211381', '2113', '0,1,21,2113,211381,', '北票市', '10618', '211381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211382', '2113', '0,1,21,2113,211382,', '凌源市', '10619', '211382', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2114', '21', '0,1,21,2114,', '葫芦岛', '10620', '2114', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211401', '2114', '0,1,21,2114,211401,', '市辖区', '10621', '211401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211402', '2114', '0,1,21,2114,211402,', '连山区', '10622', '211402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211403', '2114', '0,1,21,2114,211403,', '龙港区', '10623', '211403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211404', '2114', '0,1,21,2114,211404,', '南票区', '10624', '211404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211421', '2114', '0,1,21,2114,211421,', '绥中县', '10625', '211421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211422', '2114', '0,1,21,2114,211422,', '建昌县', '10626', '211422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('211481', '2114', '0,1,21,2114,211481,', '兴城市', '10627', '211481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('22', '1', '0,1,22,', '吉林', '10628', '22', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2201', '22', '0,1,22,2201,', '长春', '10629', '2201', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220101', '2201', '0,1,22,2201,220101,', '市辖区', '10630', '220101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220102', '2201', '0,1,22,2201,220102,', '南关区', '10631', '220102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220103', '2201', '0,1,22,2201,220103,', '宽城区', '10632', '220103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220104', '2201', '0,1,22,2201,220104,', '朝阳区', '10633', '220104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220105', '2201', '0,1,22,2201,220105,', '二道区', '10634', '220105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220106', '2201', '0,1,22,2201,220106,', '绿园区', '10635', '220106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220112', '2201', '0,1,22,2201,220112,', '双阳区', '10636', '220112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220122', '2201', '0,1,22,2201,220122,', '农安县', '10637', '220122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220181', '2201', '0,1,22,2201,220181,', '九台市', '10638', '220181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220182', '2201', '0,1,22,2201,220182,', '榆树市', '10639', '220182', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220183', '2201', '0,1,22,2201,220183,', '德惠市', '10640', '220183', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2202', '22', '0,1,22,2202,', '吉林', '10641', '2202', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220201', '2202', '0,1,22,2202,220201,', '市辖区', '10642', '220201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220202', '2202', '0,1,22,2202,220202,', '昌邑区', '10643', '220202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220203', '2202', '0,1,22,2202,220203,', '龙潭区', '10644', '220203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220204', '2202', '0,1,22,2202,220204,', '船营区', '10645', '220204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220211', '2202', '0,1,22,2202,220211,', '丰满区', '10646', '220211', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220221', '2202', '0,1,22,2202,220221,', '永吉县', '10647', '220221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220281', '2202', '0,1,22,2202,220281,', '蛟河市', '10648', '220281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220282', '2202', '0,1,22,2202,220282,', '桦甸市', '10649', '220282', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220283', '2202', '0,1,22,2202,220283,', '舒兰市', '10650', '220283', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220284', '2202', '0,1,22,2202,220284,', '磐石市', '10651', '220284', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2203', '22', '0,1,22,2203,', '四平', '10652', '2203', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220301', '2203', '0,1,22,2203,220301,', '市辖区', '10653', '220301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220302', '2203', '0,1,22,2203,220302,', '铁西区', '10654', '220302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220303', '2203', '0,1,22,2203,220303,', '铁东区', '10655', '220303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220322', '2203', '0,1,22,2203,220322,', '梨树县', '10656', '220322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220323', '2203', '0,1,22,2203,220323,', '伊通满族自治县', '10657', '220323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220381', '2203', '0,1,22,2203,220381,', '公主岭市', '10658', '220381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220382', '2203', '0,1,22,2203,220382,', '双辽市', '10659', '220382', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2204', '22', '0,1,22,2204,', '辽源', '10660', '2204', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220401', '2204', '0,1,22,2204,220401,', '市辖区', '10661', '220401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220402', '2204', '0,1,22,2204,220402,', '龙山区', '10662', '220402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220403', '2204', '0,1,22,2204,220403,', '西安区', '10663', '220403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220421', '2204', '0,1,22,2204,220421,', '东丰县', '10664', '220421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220422', '2204', '0,1,22,2204,220422,', '东辽县', '10665', '220422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2205', '22', '0,1,22,2205,', '通化', '10666', '2205', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220501', '2205', '0,1,22,2205,220501,', '市辖区', '10667', '220501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220502', '2205', '0,1,22,2205,220502,', '东昌区', '10668', '220502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220503', '2205', '0,1,22,2205,220503,', '二道江区', '10669', '220503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220521', '2205', '0,1,22,2205,220521,', '通化县', '10670', '220521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220523', '2205', '0,1,22,2205,220523,', '辉南县', '10671', '220523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220524', '2205', '0,1,22,2205,220524,', '柳河县', '10672', '220524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220581', '2205', '0,1,22,2205,220581,', '梅河口市', '10673', '220581', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220582', '2205', '0,1,22,2205,220582,', '集安市', '10674', '220582', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2206', '22', '0,1,22,2206,', '白山', '10675', '2206', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220601', '2206', '0,1,22,2206,220601,', '市辖区', '10676', '220601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220602', '2206', '0,1,22,2206,220602,', '八道江区', '10677', '220602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220621', '2206', '0,1,22,2206,220621,', '抚松县', '10678', '220621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220622', '2206', '0,1,22,2206,220622,', '靖宇县', '10679', '220622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220623', '2206', '0,1,22,2206,220623,', '长白朝鲜族自治县', '10680', '220623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220625', '2206', '0,1,22,2206,220625,', '江源县', '10681', '220625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220681', '2206', '0,1,22,2206,220681,', '临江市', '10682', '220681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2207', '22', '0,1,22,2207,', '松原', '10683', '2207', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220701', '2207', '0,1,22,2207,220701,', '市辖区', '10684', '220701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220702', '2207', '0,1,22,2207,220702,', '宁江区', '10685', '220702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220721', '2207', '0,1,22,2207,220721,', '前郭尔罗斯蒙古族自治', '10686', '220721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220722', '2207', '0,1,22,2207,220722,', '长岭县', '10687', '220722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220723', '2207', '0,1,22,2207,220723,', '乾安县', '10688', '220723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220724', '2207', '0,1,22,2207,220724,', '扶余县', '10689', '220724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2208', '22', '0,1,22,2208,', '白城', '10690', '2208', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220801', '2208', '0,1,22,2208,220801,', '市辖区', '10691', '220801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220802', '2208', '0,1,22,2208,220802,', '洮北区', '10692', '220802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220821', '2208', '0,1,22,2208,220821,', '镇赉县', '10693', '220821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220822', '2208', '0,1,22,2208,220822,', '通榆县', '10694', '220822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220881', '2208', '0,1,22,2208,220881,', '洮南市', '10695', '220881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('220882', '2208', '0,1,22,2208,220882,', '大安市', '10696', '220882', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2224', '22', '0,1,22,2224,', '延边朝鲜族自治州', '10697', '2224', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('222401', '2224', '0,1,22,2224,222401,', '延吉市', '10698', '222401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('222402', '2224', '0,1,22,2224,222402,', '图们市', '10699', '222402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('222403', '2224', '0,1,22,2224,222403,', '敦化市', '10700', '222403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('222404', '2224', '0,1,22,2224,222404,', '珲春市', '10701', '222404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('222405', '2224', '0,1,22,2224,222405,', '龙井市', '10702', '222405', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('222406', '2224', '0,1,22,2224,222406,', '和龙市', '10703', '222406', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('222424', '2224', '0,1,22,2224,222424,', '汪清县', '10704', '222424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('222426', '2224', '0,1,22,2224,222426,', '安图县', '10705', '222426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('23', '1', '0,1,23,', '黑龙江', '10706', '23', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2301', '23', '0,1,23,2301,', '哈尔滨', '10707', '2301', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230101', '2301', '0,1,23,2301,230101,', '市辖区', '10708', '230101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230102', '2301', '0,1,23,2301,230102,', '道里区', '10709', '230102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230103', '2301', '0,1,23,2301,230103,', '南岗区', '10710', '230103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230104', '2301', '0,1,23,2301,230104,', '道外区', '10711', '230104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230105', '2301', '0,1,23,2301,230105,', '太平区', '10712', '230105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230106', '2301', '0,1,23,2301,230106,', '香坊区', '10713', '230106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230107', '2301', '0,1,23,2301,230107,', '动力区', '10714', '230107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230108', '2301', '0,1,23,2301,230108,', '平房区', '10715', '230108', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230121', '2301', '0,1,23,2301,230121,', '呼兰县', '10716', '230121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230123', '2301', '0,1,23,2301,230123,', '依兰县', '10717', '230123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230124', '2301', '0,1,23,2301,230124,', '方正县', '10718', '230124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230125', '2301', '0,1,23,2301,230125,', '宾  县', '10719', '230125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230126', '2301', '0,1,23,2301,230126,', '巴彦县', '10720', '230126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230127', '2301', '0,1,23,2301,230127,', '木兰县', '10721', '230127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230128', '2301', '0,1,23,2301,230128,', '通河县', '10722', '230128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230129', '2301', '0,1,23,2301,230129,', '延寿县', '10723', '230129', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230181', '2301', '0,1,23,2301,230181,', '阿城市', '10724', '230181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230182', '2301', '0,1,23,2301,230182,', '双城市', '10725', '230182', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230183', '2301', '0,1,23,2301,230183,', '尚志市', '10726', '230183', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230184', '2301', '0,1,23,2301,230184,', '五常市', '10727', '230184', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2302', '23', '0,1,23,2302,', '齐齐哈尔', '10728', '2302', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230201', '2302', '0,1,23,2302,230201,', '市辖区', '10729', '230201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230202', '2302', '0,1,23,2302,230202,', '龙沙区', '10730', '230202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230203', '2302', '0,1,23,2302,230203,', '建华区', '10731', '230203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230204', '2302', '0,1,23,2302,230204,', '铁锋区', '10732', '230204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230205', '2302', '0,1,23,2302,230205,', '昂昂溪区', '10733', '230205', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230206', '2302', '0,1,23,2302,230206,', '富拉尔基区', '10734', '230206', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230207', '2302', '0,1,23,2302,230207,', '碾子山区', '10735', '230207', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230208', '2302', '0,1,23,2302,230208,', '梅里斯达斡尔族区', '10736', '230208', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230221', '2302', '0,1,23,2302,230221,', '龙江县', '10737', '230221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230223', '2302', '0,1,23,2302,230223,', '依安县', '10738', '230223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230224', '2302', '0,1,23,2302,230224,', '泰来县', '10739', '230224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230225', '2302', '0,1,23,2302,230225,', '甘南县', '10740', '230225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230227', '2302', '0,1,23,2302,230227,', '富裕县', '10741', '230227', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230229', '2302', '0,1,23,2302,230229,', '克山县', '10742', '230229', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230230', '2302', '0,1,23,2302,230230,', '克东县', '10743', '230230', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230231', '2302', '0,1,23,2302,230231,', '拜泉县', '10744', '230231', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230281', '2302', '0,1,23,2302,230281,', '讷河市', '10745', '230281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2303', '23', '0,1,23,2303,', '鸡西', '10746', '2303', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230301', '2303', '0,1,23,2303,230301,', '市辖区', '10747', '230301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230302', '2303', '0,1,23,2303,230302,', '鸡冠区', '10748', '230302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230303', '2303', '0,1,23,2303,230303,', '恒山区', '10749', '230303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230304', '2303', '0,1,23,2303,230304,', '滴道区', '10750', '230304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230305', '2303', '0,1,23,2303,230305,', '梨树区', '10751', '230305', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230306', '2303', '0,1,23,2303,230306,', '城子河区', '10752', '230306', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230307', '2303', '0,1,23,2303,230307,', '麻山区', '10753', '230307', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230321', '2303', '0,1,23,2303,230321,', '鸡东县', '10754', '230321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230381', '2303', '0,1,23,2303,230381,', '虎林市', '10755', '230381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230382', '2303', '0,1,23,2303,230382,', '密山市', '10756', '230382', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2304', '23', '0,1,23,2304,', '鹤岗', '10757', '2304', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230401', '2304', '0,1,23,2304,230401,', '市辖区', '10758', '230401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230402', '2304', '0,1,23,2304,230402,', '向阳区', '10759', '230402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230403', '2304', '0,1,23,2304,230403,', '工农区', '10760', '230403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230404', '2304', '0,1,23,2304,230404,', '南山区', '10761', '230404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230405', '2304', '0,1,23,2304,230405,', '兴安区', '10762', '230405', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230406', '2304', '0,1,23,2304,230406,', '东山区', '10763', '230406', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230407', '2304', '0,1,23,2304,230407,', '兴山区', '10764', '230407', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230421', '2304', '0,1,23,2304,230421,', '萝北县', '10765', '230421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230422', '2304', '0,1,23,2304,230422,', '绥滨县', '10766', '230422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2305', '23', '0,1,23,2305,', '双鸭山', '10767', '2305', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230501', '2305', '0,1,23,2305,230501,', '市辖区', '10768', '230501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230502', '2305', '0,1,23,2305,230502,', '尖山区', '10769', '230502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230503', '2305', '0,1,23,2305,230503,', '岭东区', '10770', '230503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230505', '2305', '0,1,23,2305,230505,', '四方台区', '10771', '230505', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230506', '2305', '0,1,23,2305,230506,', '宝山区', '10772', '230506', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230521', '2305', '0,1,23,2305,230521,', '集贤县', '10773', '230521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230522', '2305', '0,1,23,2305,230522,', '友谊县', '10774', '230522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230523', '2305', '0,1,23,2305,230523,', '宝清县', '10775', '230523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230524', '2305', '0,1,23,2305,230524,', '饶河县', '10776', '230524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2306', '23', '0,1,23,2306,', '大庆', '10777', '2306', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230601', '2306', '0,1,23,2306,230601,', '市辖区', '10778', '230601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230602', '2306', '0,1,23,2306,230602,', '萨尔图区', '10779', '230602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230603', '2306', '0,1,23,2306,230603,', '龙凤区', '10780', '230603', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230604', '2306', '0,1,23,2306,230604,', '让胡路区', '10781', '230604', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230605', '2306', '0,1,23,2306,230605,', '红岗区', '10782', '230605', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230606', '2306', '0,1,23,2306,230606,', '大同区', '10783', '230606', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230621', '2306', '0,1,23,2306,230621,', '肇州县', '10784', '230621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230622', '2306', '0,1,23,2306,230622,', '肇源县', '10785', '230622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230623', '2306', '0,1,23,2306,230623,', '林甸县', '10786', '230623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230624', '2306', '0,1,23,2306,230624,', '杜尔伯特蒙古族自治县', '10787', '230624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2307', '23', '0,1,23,2307,', '伊春', '10788', '2307', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230701', '2307', '0,1,23,2307,230701,', '市辖区', '10789', '230701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230702', '2307', '0,1,23,2307,230702,', '伊春区', '10790', '230702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230703', '2307', '0,1,23,2307,230703,', '南岔区', '10791', '230703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230704', '2307', '0,1,23,2307,230704,', '友好区', '10792', '230704', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230705', '2307', '0,1,23,2307,230705,', '西林区', '10793', '230705', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230706', '2307', '0,1,23,2307,230706,', '翠峦区', '10794', '230706', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230707', '2307', '0,1,23,2307,230707,', '新青区', '10795', '230707', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230708', '2307', '0,1,23,2307,230708,', '美溪区', '10796', '230708', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230709', '2307', '0,1,23,2307,230709,', '金山屯区', '10797', '230709', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230710', '2307', '0,1,23,2307,230710,', '五营区', '10798', '230710', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230711', '2307', '0,1,23,2307,230711,', '乌马河区', '10799', '230711', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230712', '2307', '0,1,23,2307,230712,', '汤旺河区', '10800', '230712', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230713', '2307', '0,1,23,2307,230713,', '带岭区', '10801', '230713', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230714', '2307', '0,1,23,2307,230714,', '乌伊岭区', '10802', '230714', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230715', '2307', '0,1,23,2307,230715,', '红星区', '10803', '230715', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230716', '2307', '0,1,23,2307,230716,', '上甘岭区', '10804', '230716', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230722', '2307', '0,1,23,2307,230722,', '嘉荫县', '10805', '230722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230781', '2307', '0,1,23,2307,230781,', '铁力市', '10806', '230781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2308', '23', '0,1,23,2308,', '佳木斯', '10807', '2308', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230801', '2308', '0,1,23,2308,230801,', '市辖区', '10808', '230801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230802', '2308', '0,1,23,2308,230802,', '永红区', '10809', '230802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230803', '2308', '0,1,23,2308,230803,', '向阳区', '10810', '230803', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230804', '2308', '0,1,23,2308,230804,', '前进区', '10811', '230804', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230805', '2308', '0,1,23,2308,230805,', '东风区', '10812', '230805', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230811', '2308', '0,1,23,2308,230811,', '郊  区', '10813', '230811', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230822', '2308', '0,1,23,2308,230822,', '桦南县', '10814', '230822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230826', '2308', '0,1,23,2308,230826,', '桦川县', '10815', '230826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230828', '2308', '0,1,23,2308,230828,', '汤原县', '10816', '230828', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230833', '2308', '0,1,23,2308,230833,', '抚远县', '10817', '230833', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230881', '2308', '0,1,23,2308,230881,', '同江市', '10818', '230881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230882', '2308', '0,1,23,2308,230882,', '富锦市', '10819', '230882', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2309', '23', '0,1,23,2309,', '七台河', '10820', '2309', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230901', '2309', '0,1,23,2309,230901,', '市辖区', '10821', '230901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230902', '2309', '0,1,23,2309,230902,', '新兴区', '10822', '230902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230903', '2309', '0,1,23,2309,230903,', '桃山区', '10823', '230903', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230904', '2309', '0,1,23,2309,230904,', '茄子河区', '10824', '230904', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('230921', '2309', '0,1,23,2309,230921,', '勃利县', '10825', '230921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2310', '23', '0,1,23,2310,', '牡丹江', '10826', '2310', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231001', '2310', '0,1,23,2310,231001,', '市辖区', '10827', '231001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231002', '2310', '0,1,23,2310,231002,', '东安区', '10828', '231002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231003', '2310', '0,1,23,2310,231003,', '阳明区', '10829', '231003', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231004', '2310', '0,1,23,2310,231004,', '爱民区', '10830', '231004', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231005', '2310', '0,1,23,2310,231005,', '西安区', '10831', '231005', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231024', '2310', '0,1,23,2310,231024,', '东宁县', '10832', '231024', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231025', '2310', '0,1,23,2310,231025,', '林口县', '10833', '231025', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231081', '2310', '0,1,23,2310,231081,', '绥芬河市', '10834', '231081', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231083', '2310', '0,1,23,2310,231083,', '海林市', '10835', '231083', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231084', '2310', '0,1,23,2310,231084,', '宁安市', '10836', '231084', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231085', '2310', '0,1,23,2310,231085,', '穆棱市', '10837', '231085', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2311', '23', '0,1,23,2311,', '黑河', '10838', '2311', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231101', '2311', '0,1,23,2311,231101,', '市辖区', '10839', '231101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231102', '2311', '0,1,23,2311,231102,', '爱辉区', '10840', '231102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231121', '2311', '0,1,23,2311,231121,', '嫩江县', '10841', '231121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231123', '2311', '0,1,23,2311,231123,', '逊克县', '10842', '231123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231124', '2311', '0,1,23,2311,231124,', '孙吴县', '10843', '231124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231181', '2311', '0,1,23,2311,231181,', '北安市', '10844', '231181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231182', '2311', '0,1,23,2311,231182,', '五大连池市', '10845', '231182', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2312', '23', '0,1,23,2312,', '绥化', '10846', '2312', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231201', '2312', '0,1,23,2312,231201,', '市辖区', '10847', '231201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231202', '2312', '0,1,23,2312,231202,', '北林区', '10848', '231202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231221', '2312', '0,1,23,2312,231221,', '望奎县', '10849', '231221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231222', '2312', '0,1,23,2312,231222,', '兰西县', '10850', '231222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231223', '2312', '0,1,23,2312,231223,', '青冈县', '10851', '231223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231224', '2312', '0,1,23,2312,231224,', '庆安县', '10852', '231224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231225', '2312', '0,1,23,2312,231225,', '明水县', '10853', '231225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231226', '2312', '0,1,23,2312,231226,', '绥棱县', '10854', '231226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231281', '2312', '0,1,23,2312,231281,', '安达市', '10855', '231281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231282', '2312', '0,1,23,2312,231282,', '肇东市', '10856', '231282', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('231283', '2312', '0,1,23,2312,231283,', '海伦市', '10857', '231283', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('2327', '23', '0,1,23,2327,', '大兴安岭地区', '10858', '2327', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('232721', '2327', '0,1,23,2327,232721,', '呼玛县', '10859', '232721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('232722', '2327', '0,1,23,2327,232722,', '塔河县', '10860', '232722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('232723', '2327', '0,1,23,2327,232723,', '漠河县', '10861', '232723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('31', '1', '0,1,31,', '上海', '10862', '31', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3101', '31', '0,1,31,3101,', '上海市辖', '10863', '3101', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310101', '3101', '0,1,31,3101,310101,', '黄浦区', '10864', '310101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310103', '3101', '0,1,31,3101,310103,', '卢湾区', '10865', '310103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310104', '3101', '0,1,31,3101,310104,', '徐汇区', '10866', '310104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310105', '3101', '0,1,31,3101,310105,', '长宁区', '10867', '310105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310106', '3101', '0,1,31,3101,310106,', '静安区', '10868', '310106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310107', '3101', '0,1,31,3101,310107,', '普陀区', '10869', '310107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310108', '3101', '0,1,31,3101,310108,', '闸北区', '10870', '310108', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310109', '3101', '0,1,31,3101,310109,', '虹口区', '10871', '310109', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310110', '3101', '0,1,31,3101,310110,', '杨浦区', '10872', '310110', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310112', '3101', '0,1,31,3101,310112,', '闵行区', '10873', '310112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310113', '3101', '0,1,31,3101,310113,', '宝山区', '10874', '310113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310114', '3101', '0,1,31,3101,310114,', '嘉定区', '10875', '310114', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310115', '3101', '0,1,31,3101,310115,', '浦东新区', '10876', '310115', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310116', '3101', '0,1,31,3101,310116,', '金山区', '10877', '310116', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310117', '3101', '0,1,31,3101,310117,', '松江区', '10878', '310117', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310118', '3101', '0,1,31,3101,310118,', '青浦区', '10879', '310118', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3102', '31', '0,1,31,3102,', '上海县辖', '10880', '3102', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310225', '3102', '0,1,31,3102,310225,', '南汇县', '10881', '310225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310226', '3102', '0,1,31,3102,310226,', '奉贤县', '10882', '310226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('310230', '3102', '0,1,31,3102,310230,', '崇明县', '10883', '310230', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('32', '1', '0,1,32,', '江苏', '10884', '32', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3201', '32', '0,1,32,3201,', '南京', '10885', '3201', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320101', '3201', '0,1,32,3201,320101,', '市辖区', '10886', '320101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320102', '3201', '0,1,32,3201,320102,', '玄武区', '10887', '320102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320103', '3201', '0,1,32,3201,320103,', '白下区', '10888', '320103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320104', '3201', '0,1,32,3201,320104,', '秦淮区', '10889', '320104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320105', '3201', '0,1,32,3201,320105,', '建邺区', '10890', '320105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320106', '3201', '0,1,32,3201,320106,', '鼓楼区', '10891', '320106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320107', '3201', '0,1,32,3201,320107,', '下关区', '10892', '320107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320111', '3201', '0,1,32,3201,320111,', '浦口区', '10893', '320111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320112', '3201', '0,1,32,3201,320112,', '大厂区', '10894', '320112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320113', '3201', '0,1,32,3201,320113,', '栖霞区', '10895', '320113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320114', '3201', '0,1,32,3201,320114,', '雨花台区', '10896', '320114', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320115', '3201', '0,1,32,3201,320115,', '江宁区', '10897', '320115', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320122', '3201', '0,1,32,3201,320122,', '江浦县', '10898', '320122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320123', '3201', '0,1,32,3201,320123,', '六合县', '10899', '320123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320124', '3201', '0,1,32,3201,320124,', '溧水县', '10900', '320124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320125', '3201', '0,1,32,3201,320125,', '高淳县', '10901', '320125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3202', '32', '0,1,32,3202,', '无锡', '10902', '3202', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320201', '3202', '0,1,32,3202,320201,', '市辖区', '10903', '320201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320202', '3202', '0,1,32,3202,320202,', '崇安区', '10904', '320202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320203', '3202', '0,1,32,3202,320203,', '南长区', '10905', '320203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320204', '3202', '0,1,32,3202,320204,', '北塘区', '10906', '320204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320205', '3202', '0,1,32,3202,320205,', '锡山区', '10907', '320205', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320206', '3202', '0,1,32,3202,320206,', '惠山区', '10908', '320206', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320211', '3202', '0,1,32,3202,320211,', '滨湖区', '10909', '320211', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320281', '3202', '0,1,32,3202,320281,', '江阴市', '10910', '320281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320282', '3202', '0,1,32,3202,320282,', '宜兴市', '10911', '320282', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3203', '32', '0,1,32,3203,', '徐州', '10912', '3203', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320301', '3203', '0,1,32,3203,320301,', '市辖区', '10913', '320301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320302', '3203', '0,1,32,3203,320302,', '鼓楼区', '10914', '320302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320303', '3203', '0,1,32,3203,320303,', '云龙区', '10915', '320303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320304', '3203', '0,1,32,3203,320304,', '九里区', '10916', '320304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320305', '3203', '0,1,32,3203,320305,', '贾汪区', '10917', '320305', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320311', '3203', '0,1,32,3203,320311,', '泉山区', '10918', '320311', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320321', '3203', '0,1,32,3203,320321,', '丰  县', '10919', '320321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320322', '3203', '0,1,32,3203,320322,', '沛  县', '10920', '320322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320323', '3203', '0,1,32,3203,320323,', '铜山县', '10921', '320323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320324', '3203', '0,1,32,3203,320324,', '睢宁县', '10922', '320324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320381', '3203', '0,1,32,3203,320381,', '新沂市', '10923', '320381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320382', '3203', '0,1,32,3203,320382,', '邳州市', '10924', '320382', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3204', '32', '0,1,32,3204,', '常州', '10925', '3204', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320401', '3204', '0,1,32,3204,320401,', '市辖区', '10926', '320401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320402', '3204', '0,1,32,3204,320402,', '天宁区', '10927', '320402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320404', '3204', '0,1,32,3204,320404,', '钟楼区', '10928', '320404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320405', '3204', '0,1,32,3204,320405,', '戚墅堰区', '10929', '320405', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320411', '3204', '0,1,32,3204,320411,', '郊  区', '10930', '320411', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320481', '3204', '0,1,32,3204,320481,', '溧阳市', '10931', '320481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320482', '3204', '0,1,32,3204,320482,', '金坛市', '10932', '320482', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320483', '3204', '0,1,32,3204,320483,', '武进市', '10933', '320483', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3205', '32', '0,1,32,3205,', '苏州', '10934', '3205', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320501', '3205', '0,1,32,3205,320501,', '市辖区', '10935', '320501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320502', '3205', '0,1,32,3205,320502,', '沧浪区', '10936', '320502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320503', '3205', '0,1,32,3205,320503,', '平江区', '10937', '320503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320504', '3205', '0,1,32,3205,320504,', '金阊区', '10938', '320504', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320505', '3205', '0,1,32,3205,320505,', '虎丘区', '10939', '320505', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320506', '3205', '0,1,32,3205,320506,', '吴中区', '10940', '320506', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320507', '3205', '0,1,32,3205,320507,', '相城区', '10941', '320507', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320581', '3205', '0,1,32,3205,320581,', '常熟市', '10942', '320581', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320582', '3205', '0,1,32,3205,320582,', '张家港市', '10943', '320582', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320583', '3205', '0,1,32,3205,320583,', '昆山市', '10944', '320583', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320584', '3205', '0,1,32,3205,320584,', '吴江市', '10945', '320584', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320585', '3205', '0,1,32,3205,320585,', '太仓市', '10946', '320585', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3206', '32', '0,1,32,3206,', '南通', '10947', '3206', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320601', '3206', '0,1,32,3206,320601,', '市辖区', '10948', '320601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320602', '3206', '0,1,32,3206,320602,', '崇川区', '10949', '320602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320611', '3206', '0,1,32,3206,320611,', '港闸区', '10950', '320611', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320621', '3206', '0,1,32,3206,320621,', '海安县', '10951', '320621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320623', '3206', '0,1,32,3206,320623,', '如东县', '10952', '320623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320681', '3206', '0,1,32,3206,320681,', '启东市', '10953', '320681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320682', '3206', '0,1,32,3206,320682,', '如皋市', '10954', '320682', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320683', '3206', '0,1,32,3206,320683,', '通州市', '10955', '320683', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320684', '3206', '0,1,32,3206,320684,', '海门市', '10956', '320684', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3207', '32', '0,1,32,3207,', '连云港', '10957', '3207', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320701', '3207', '0,1,32,3207,320701,', '市辖区', '10958', '320701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320703', '3207', '0,1,32,3207,320703,', '连云区', '10959', '320703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320704', '3207', '0,1,32,3207,320704,', '云台区', '10960', '320704', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320705', '3207', '0,1,32,3207,320705,', '新浦区', '10961', '320705', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320706', '3207', '0,1,32,3207,320706,', '海州区', '10962', '320706', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320721', '3207', '0,1,32,3207,320721,', '赣榆县', '10963', '320721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320722', '3207', '0,1,32,3207,320722,', '东海县', '10964', '320722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320723', '3207', '0,1,32,3207,320723,', '灌云县', '10965', '320723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320724', '3207', '0,1,32,3207,320724,', '灌南县', '10966', '320724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3208', '32', '0,1,32,3208,', '淮安', '10967', '3208', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320801', '3208', '0,1,32,3208,320801,', '市辖区', '10968', '320801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320802', '3208', '0,1,32,3208,320802,', '清河区', '10969', '320802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320803', '3208', '0,1,32,3208,320803,', '楚州区', '10970', '320803', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320804', '3208', '0,1,32,3208,320804,', '淮阴区', '10971', '320804', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320811', '3208', '0,1,32,3208,320811,', '清浦区', '10972', '320811', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320826', '3208', '0,1,32,3208,320826,', '涟水县', '10973', '320826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320829', '3208', '0,1,32,3208,320829,', '洪泽县', '10974', '320829', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320830', '3208', '0,1,32,3208,320830,', '盱眙县', '10975', '320830', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320831', '3208', '0,1,32,3208,320831,', '金湖县', '10976', '320831', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3209', '32', '0,1,32,3209,', '盐城', '10977', '3209', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320901', '3209', '0,1,32,3209,320901,', '市辖区', '10978', '320901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320902', '3209', '0,1,32,3209,320902,', '城  区', '10979', '320902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320921', '3209', '0,1,32,3209,320921,', '响水县', '10980', '320921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320922', '3209', '0,1,32,3209,320922,', '滨海县', '10981', '320922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320923', '3209', '0,1,32,3209,320923,', '阜宁县', '10982', '320923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320924', '3209', '0,1,32,3209,320924,', '射阳县', '10983', '320924', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320925', '3209', '0,1,32,3209,320925,', '建湖县', '10984', '320925', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320928', '3209', '0,1,32,3209,320928,', '盐都县', '10985', '320928', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320981', '3209', '0,1,32,3209,320981,', '东台市', '10986', '320981', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('320982', '3209', '0,1,32,3209,320982,', '大丰市', '10987', '320982', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3210', '32', '0,1,32,3210,', '扬州', '10988', '3210', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321001', '3210', '0,1,32,3210,321001,', '市辖区', '10989', '321001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321002', '3210', '0,1,32,3210,321002,', '广陵区', '10990', '321002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321003', '3210', '0,1,32,3210,321003,', '邗江区', '10991', '321003', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321011', '3210', '0,1,32,3210,321011,', '郊  区', '10992', '321011', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321023', '3210', '0,1,32,3210,321023,', '宝应县', '10993', '321023', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321081', '3210', '0,1,32,3210,321081,', '仪征市', '10994', '321081', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321084', '3210', '0,1,32,3210,321084,', '高邮市', '10995', '321084', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321088', '3210', '0,1,32,3210,321088,', '江都市', '10996', '321088', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3211', '32', '0,1,32,3211,', '镇江', '10997', '3211', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321101', '3211', '0,1,32,3211,321101,', '市辖区', '10998', '321101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321102', '3211', '0,1,32,3211,321102,', '京口区', '10999', '321102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321111', '3211', '0,1,32,3211,321111,', '润州区', '11000', '321111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321121', '3211', '0,1,32,3211,321121,', '丹徒县', '11001', '321121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321181', '3211', '0,1,32,3211,321181,', '丹阳市', '11002', '321181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321182', '3211', '0,1,32,3211,321182,', '扬中市', '11003', '321182', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321183', '3211', '0,1,32,3211,321183,', '句容市', '11004', '321183', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3212', '32', '0,1,32,3212,', '泰州', '11005', '3212', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321201', '3212', '0,1,32,3212,321201,', '市辖区', '11006', '321201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321202', '3212', '0,1,32,3212,321202,', '海陵区', '11007', '321202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321203', '3212', '0,1,32,3212,321203,', '高港区', '11008', '321203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321281', '3212', '0,1,32,3212,321281,', '兴化市', '11009', '321281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321282', '3212', '0,1,32,3212,321282,', '靖江市', '11010', '321282', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321283', '3212', '0,1,32,3212,321283,', '泰兴市', '11011', '321283', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321284', '3212', '0,1,32,3212,321284,', '姜堰市', '11012', '321284', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3213', '32', '0,1,32,3213,', '宿迁', '11013', '3213', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321301', '3213', '0,1,32,3213,321301,', '市辖区', '11014', '321301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321302', '3213', '0,1,32,3213,321302,', '宿城区', '11015', '321302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321321', '3213', '0,1,32,3213,321321,', '宿豫县', '11016', '321321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321322', '3213', '0,1,32,3213,321322,', '沭阳县', '11017', '321322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321323', '3213', '0,1,32,3213,321323,', '泗阳县', '11018', '321323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('321324', '3213', '0,1,32,3213,321324,', '泗洪县', '11019', '321324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('33', '1', '0,1,33,', '浙江', '11020', '33', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3301', '33', '0,1,33,3301,', '杭州', '11021', '3301', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330101', '3301', '0,1,33,3301,330101,', '市辖区', '11022', '330101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330102', '3301', '0,1,33,3301,330102,', '上城区', '11023', '330102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330103', '3301', '0,1,33,3301,330103,', '下城区', '11024', '330103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330104', '3301', '0,1,33,3301,330104,', '江干区', '11025', '330104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330105', '3301', '0,1,33,3301,330105,', '拱墅区', '11026', '330105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330106', '3301', '0,1,33,3301,330106,', '西湖区', '11027', '330106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330108', '3301', '0,1,33,3301,330108,', '滨江区', '11028', '330108', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330122', '3301', '0,1,33,3301,330122,', '桐庐县', '11029', '330122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330127', '3301', '0,1,33,3301,330127,', '淳安县', '11030', '330127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330181', '3301', '0,1,33,3301,330181,', '萧山市', '11031', '330181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330182', '3301', '0,1,33,3301,330182,', '建德市', '11032', '330182', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330183', '3301', '0,1,33,3301,330183,', '富阳市', '11033', '330183', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330184', '3301', '0,1,33,3301,330184,', '余杭市', '11034', '330184', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330185', '3301', '0,1,33,3301,330185,', '临安市', '11035', '330185', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3302', '33', '0,1,33,3302,', '宁波', '11036', '3302', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330201', '3302', '0,1,33,3302,330201,', '市辖区', '11037', '330201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330203', '3302', '0,1,33,3302,330203,', '海曙区', '11038', '330203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330204', '3302', '0,1,33,3302,330204,', '江东区', '11039', '330204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330205', '3302', '0,1,33,3302,330205,', '江北区', '11040', '330205', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330206', '3302', '0,1,33,3302,330206,', '北仑区', '11041', '330206', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330211', '3302', '0,1,33,3302,330211,', '镇海区', '11042', '330211', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330225', '3302', '0,1,33,3302,330225,', '象山县', '11043', '330225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330226', '3302', '0,1,33,3302,330226,', '宁海县', '11044', '330226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330227', '3302', '0,1,33,3302,330227,', '鄞  县', '11045', '330227', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330281', '3302', '0,1,33,3302,330281,', '余姚市', '11046', '330281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330282', '3302', '0,1,33,3302,330282,', '慈溪市', '11047', '330282', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330283', '3302', '0,1,33,3302,330283,', '奉化市', '11048', '330283', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3303', '33', '0,1,33,3303,', '温州', '11049', '3303', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330301', '3303', '0,1,33,3303,330301,', '市辖区', '11050', '330301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330302', '3303', '0,1,33,3303,330302,', '鹿城区', '11051', '330302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330303', '3303', '0,1,33,3303,330303,', '龙湾区', '11052', '330303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330304', '3303', '0,1,33,3303,330304,', '瓯海区', '11053', '330304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330322', '3303', '0,1,33,3303,330322,', '洞头县', '11054', '330322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330324', '3303', '0,1,33,3303,330324,', '永嘉县', '11055', '330324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330326', '3303', '0,1,33,3303,330326,', '平阳县', '11056', '330326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330327', '3303', '0,1,33,3303,330327,', '苍南县', '11057', '330327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330328', '3303', '0,1,33,3303,330328,', '文成县', '11058', '330328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330329', '3303', '0,1,33,3303,330329,', '泰顺县', '11059', '330329', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330381', '3303', '0,1,33,3303,330381,', '瑞安市', '11060', '330381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330382', '3303', '0,1,33,3303,330382,', '乐清市', '11061', '330382', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3304', '33', '0,1,33,3304,', '嘉兴', '11062', '3304', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330401', '3304', '0,1,33,3304,330401,', '市辖区', '11063', '330401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330402', '3304', '0,1,33,3304,330402,', '秀城区', '11064', '330402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330411', '3304', '0,1,33,3304,330411,', '秀洲区', '11065', '330411', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330421', '3304', '0,1,33,3304,330421,', '嘉善县', '11066', '330421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330424', '3304', '0,1,33,3304,330424,', '海盐县', '11067', '330424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330481', '3304', '0,1,33,3304,330481,', '海宁市', '11068', '330481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330482', '3304', '0,1,33,3304,330482,', '平湖市', '11069', '330482', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330483', '3304', '0,1,33,3304,330483,', '桐乡市', '11070', '330483', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3305', '33', '0,1,33,3305,', '湖州', '11071', '3305', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330501', '3305', '0,1,33,3305,330501,', '市辖区', '11072', '330501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330521', '3305', '0,1,33,3305,330521,', '德清县', '11073', '330521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330522', '3305', '0,1,33,3305,330522,', '长兴县', '11074', '330522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330523', '3305', '0,1,33,3305,330523,', '安吉县', '11075', '330523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3306', '33', '0,1,33,3306,', '绍兴', '11076', '3306', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330601', '3306', '0,1,33,3306,330601,', '市辖区', '11077', '330601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330602', '3306', '0,1,33,3306,330602,', '越城区', '11078', '330602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330621', '3306', '0,1,33,3306,330621,', '绍兴县', '11079', '330621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330624', '3306', '0,1,33,3306,330624,', '新昌县', '11080', '330624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330681', '3306', '0,1,33,3306,330681,', '诸暨市', '11081', '330681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330682', '3306', '0,1,33,3306,330682,', '上虞市', '11082', '330682', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330683', '3306', '0,1,33,3306,330683,', '嵊州市', '11083', '330683', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3307', '33', '0,1,33,3307,', '金华', '11084', '3307', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330701', '3307', '0,1,33,3307,330701,', '市辖区', '11085', '330701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330702', '3307', '0,1,33,3307,330702,', '婺城区', '11086', '330702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330703', '3307', '0,1,33,3307,330703,', '金东区', '11087', '330703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330723', '3307', '0,1,33,3307,330723,', '武义县', '11088', '330723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330726', '3307', '0,1,33,3307,330726,', '浦江县', '11089', '330726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330727', '3307', '0,1,33,3307,330727,', '磐安县', '11090', '330727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330781', '3307', '0,1,33,3307,330781,', '兰溪市', '11091', '330781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330782', '3307', '0,1,33,3307,330782,', '义乌市', '11092', '330782', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330783', '3307', '0,1,33,3307,330783,', '东阳市', '11093', '330783', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330784', '3307', '0,1,33,3307,330784,', '永康市', '11094', '330784', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3308', '33', '0,1,33,3308,', '衢州', '11095', '3308', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330801', '3308', '0,1,33,3308,330801,', '市辖区', '11096', '330801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330802', '3308', '0,1,33,3308,330802,', '柯城区', '11097', '330802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330821', '3308', '0,1,33,3308,330821,', '衢  县', '11098', '330821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330822', '3308', '0,1,33,3308,330822,', '常山县', '11099', '330822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330824', '3308', '0,1,33,3308,330824,', '开化县', '11100', '330824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330825', '3308', '0,1,33,3308,330825,', '龙游县', '11101', '330825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330881', '3308', '0,1,33,3308,330881,', '江山市', '11102', '330881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3309', '33', '0,1,33,3309,', '舟山', '11103', '3309', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330901', '3309', '0,1,33,3309,330901,', '市辖区', '11104', '330901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330902', '3309', '0,1,33,3309,330902,', '定海区', '11105', '330902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330903', '3309', '0,1,33,3309,330903,', '普陀区', '11106', '330903', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330921', '3309', '0,1,33,3309,330921,', '岱山县', '11107', '330921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('330922', '3309', '0,1,33,3309,330922,', '嵊泗县', '11108', '330922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3310', '33', '0,1,33,3310,', '台州', '11109', '3310', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331001', '3310', '0,1,33,3310,331001,', '市辖区', '11110', '331001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331002', '3310', '0,1,33,3310,331002,', '椒江区', '11111', '331002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331003', '3310', '0,1,33,3310,331003,', '黄岩区', '11112', '331003', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331004', '3310', '0,1,33,3310,331004,', '路桥区', '11113', '331004', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331021', '3310', '0,1,33,3310,331021,', '玉环县', '11114', '331021', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331022', '3310', '0,1,33,3310,331022,', '三门县', '11115', '331022', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331023', '3310', '0,1,33,3310,331023,', '天台县', '11116', '331023', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331024', '3310', '0,1,33,3310,331024,', '仙居县', '11117', '331024', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331081', '3310', '0,1,33,3310,331081,', '温岭市', '11118', '331081', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331082', '3310', '0,1,33,3310,331082,', '临海市', '11119', '331082', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3311', '33', '0,1,33,3311,', '丽水', '11120', '3311', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331101', '3311', '0,1,33,3311,331101,', '市辖区', '11121', '331101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331102', '3311', '0,1,33,3311,331102,', '莲都区', '11122', '331102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331121', '3311', '0,1,33,3311,331121,', '青田县', '11123', '331121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331122', '3311', '0,1,33,3311,331122,', '缙云县', '11124', '331122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331123', '3311', '0,1,33,3311,331123,', '遂昌县', '11125', '331123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331124', '3311', '0,1,33,3311,331124,', '松阳县', '11126', '331124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331125', '3311', '0,1,33,3311,331125,', '云和县', '11127', '331125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331126', '3311', '0,1,33,3311,331126,', '庆元县', '11128', '331126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331127', '3311', '0,1,33,3311,331127,', '景宁畲族自治县', '11129', '331127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('331181', '3311', '0,1,33,3311,331181,', '龙泉市', '11130', '331181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('34', '1', '0,1,34,', '安徽', '11131', '34', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3401', '34', '0,1,34,3401,', '合肥', '11132', '3401', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340101', '3401', '0,1,34,3401,340101,', '市辖区', '11133', '340101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340102', '3401', '0,1,34,3401,340102,', '东市区', '11134', '340102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340103', '3401', '0,1,34,3401,340103,', '中市区', '11135', '340103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340104', '3401', '0,1,34,3401,340104,', '西市区', '11136', '340104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340111', '3401', '0,1,34,3401,340111,', '郊  区', '11137', '340111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340121', '3401', '0,1,34,3401,340121,', '长丰县', '11138', '340121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340122', '3401', '0,1,34,3401,340122,', '肥东县', '11139', '340122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340123', '3401', '0,1,34,3401,340123,', '肥西县', '11140', '340123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3402', '34', '0,1,34,3402,', '芜湖', '11141', '3402', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340201', '3402', '0,1,34,3402,340201,', '市辖区', '11142', '340201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340202', '3402', '0,1,34,3402,340202,', '镜湖区', '11143', '340202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340203', '3402', '0,1,34,3402,340203,', '马塘区', '11144', '340203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340204', '3402', '0,1,34,3402,340204,', '新芜区', '11145', '340204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340207', '3402', '0,1,34,3402,340207,', '鸠江区', '11146', '340207', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340221', '3402', '0,1,34,3402,340221,', '芜湖县', '11147', '340221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340222', '3402', '0,1,34,3402,340222,', '繁昌县', '11148', '340222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340223', '3402', '0,1,34,3402,340223,', '南陵县', '11149', '340223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3403', '34', '0,1,34,3403,', '蚌埠', '11150', '3403', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340301', '3403', '0,1,34,3403,340301,', '市辖区', '11151', '340301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340302', '3403', '0,1,34,3403,340302,', '东市区', '11152', '340302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340303', '3403', '0,1,34,3403,340303,', '中市区', '11153', '340303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340304', '3403', '0,1,34,3403,340304,', '西市区', '11154', '340304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340311', '3403', '0,1,34,3403,340311,', '郊  区', '11155', '340311', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340321', '3403', '0,1,34,3403,340321,', '怀远县', '11156', '340321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340322', '3403', '0,1,34,3403,340322,', '五河县', '11157', '340322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340323', '3403', '0,1,34,3403,340323,', '固镇县', '11158', '340323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3404', '34', '0,1,34,3404,', '淮南', '11159', '3404', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340401', '3404', '0,1,34,3404,340401,', '市辖区', '11160', '340401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340402', '3404', '0,1,34,3404,340402,', '大通区', '11161', '340402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340403', '3404', '0,1,34,3404,340403,', '田家庵区', '11162', '340403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340404', '3404', '0,1,34,3404,340404,', '谢家集区', '11163', '340404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340405', '3404', '0,1,34,3404,340405,', '八公山区', '11164', '340405', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340406', '3404', '0,1,34,3404,340406,', '潘集区', '11165', '340406', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340421', '3404', '0,1,34,3404,340421,', '凤台县', '11166', '340421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3405', '34', '0,1,34,3405,', '马鞍山', '11167', '3405', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340501', '3405', '0,1,34,3405,340501,', '市辖区', '11168', '340501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340502', '3405', '0,1,34,3405,340502,', '金家庄区', '11169', '340502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340503', '3405', '0,1,34,3405,340503,', '花山区', '11170', '340503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340504', '3405', '0,1,34,3405,340504,', '雨山区', '11171', '340504', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340505', '3405', '0,1,34,3405,340505,', '向山区', '11172', '340505', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340521', '3405', '0,1,34,3405,340521,', '当涂县', '11173', '340521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3406', '34', '0,1,34,3406,', '淮北', '11174', '3406', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340601', '3406', '0,1,34,3406,340601,', '市辖区', '11175', '340601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340602', '3406', '0,1,34,3406,340602,', '杜集区', '11176', '340602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340603', '3406', '0,1,34,3406,340603,', '相山区', '11177', '340603', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340604', '3406', '0,1,34,3406,340604,', '烈山区', '11178', '340604', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340621', '3406', '0,1,34,3406,340621,', '濉溪县', '11179', '340621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3407', '34', '0,1,34,3407,', '铜陵', '11180', '3407', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340701', '3407', '0,1,34,3407,340701,', '市辖区', '11181', '340701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340702', '3407', '0,1,34,3407,340702,', '铜官山区', '11182', '340702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340703', '3407', '0,1,34,3407,340703,', '狮子山区', '11183', '340703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340711', '3407', '0,1,34,3407,340711,', '郊  区', '11184', '340711', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340721', '3407', '0,1,34,3407,340721,', '铜陵县', '11185', '340721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3408', '34', '0,1,34,3408,', '安庆', '11186', '3408', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340801', '3408', '0,1,34,3408,340801,', '市辖区', '11187', '340801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340802', '3408', '0,1,34,3408,340802,', '迎江区', '11188', '340802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340803', '3408', '0,1,34,3408,340803,', '大观区', '11189', '340803', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340811', '3408', '0,1,34,3408,340811,', '郊  区', '11190', '340811', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340822', '3408', '0,1,34,3408,340822,', '怀宁县', '11191', '340822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340823', '3408', '0,1,34,3408,340823,', '枞阳县', '11192', '340823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340824', '3408', '0,1,34,3408,340824,', '潜山县', '11193', '340824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340825', '3408', '0,1,34,3408,340825,', '太湖县', '11194', '340825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340826', '3408', '0,1,34,3408,340826,', '宿松县', '11195', '340826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340827', '3408', '0,1,34,3408,340827,', '望江县', '11196', '340827', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340828', '3408', '0,1,34,3408,340828,', '岳西县', '11197', '340828', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('340881', '3408', '0,1,34,3408,340881,', '桐城市', '11198', '340881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3410', '34', '0,1,34,3410,', '黄山', '11199', '3410', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341001', '3410', '0,1,34,3410,341001,', '市辖区', '11200', '341001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341002', '3410', '0,1,34,3410,341002,', '屯溪区', '11201', '341002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341003', '3410', '0,1,34,3410,341003,', '黄山区', '11202', '341003', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341004', '3410', '0,1,34,3410,341004,', '徽州区', '11203', '341004', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341021', '3410', '0,1,34,3410,341021,', '歙  县', '11204', '341021', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341022', '3410', '0,1,34,3410,341022,', '休宁县', '11205', '341022', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341023', '3410', '0,1,34,3410,341023,', '黟  县', '11206', '341023', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341024', '3410', '0,1,34,3410,341024,', '祁门县', '11207', '341024', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3411', '34', '0,1,34,3411,', '滁州', '11208', '3411', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341101', '3411', '0,1,34,3411,341101,', '市辖区', '11209', '341101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341102', '3411', '0,1,34,3411,341102,', '琅琊区', '11210', '341102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341103', '3411', '0,1,34,3411,341103,', '南谯区', '11211', '341103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341122', '3411', '0,1,34,3411,341122,', '来安县', '11212', '341122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341124', '3411', '0,1,34,3411,341124,', '全椒县', '11213', '341124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341125', '3411', '0,1,34,3411,341125,', '定远县', '11214', '341125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341126', '3411', '0,1,34,3411,341126,', '凤阳县', '11215', '341126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341181', '3411', '0,1,34,3411,341181,', '天长市', '11216', '341181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341182', '3411', '0,1,34,3411,341182,', '明光市', '11217', '341182', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3412', '34', '0,1,34,3412,', '阜阳', '11218', '3412', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341201', '3412', '0,1,34,3412,341201,', '市辖区', '11219', '341201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341202', '3412', '0,1,34,3412,341202,', '颍州区', '11220', '341202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341203', '3412', '0,1,34,3412,341203,', '颍东区', '11221', '341203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341204', '3412', '0,1,34,3412,341204,', '颍泉区', '11222', '341204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341221', '3412', '0,1,34,3412,341221,', '临泉县', '11223', '341221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341222', '3412', '0,1,34,3412,341222,', '太和县', '11224', '341222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341225', '3412', '0,1,34,3412,341225,', '阜南县', '11225', '341225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341226', '3412', '0,1,34,3412,341226,', '颍上县', '11226', '341226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341282', '3412', '0,1,34,3412,341282,', '界首市', '11227', '341282', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3413', '34', '0,1,34,3413,', '宿州', '11228', '3413', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341301', '3413', '0,1,34,3413,341301,', '市辖区', '11229', '341301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341302', '3413', '0,1,34,3413,341302,', '墉桥区', '11230', '341302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341321', '3413', '0,1,34,3413,341321,', '砀山县', '11231', '341321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341322', '3413', '0,1,34,3413,341322,', '萧  县', '11232', '341322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341323', '3413', '0,1,34,3413,341323,', '灵璧县', '11233', '341323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341324', '3413', '0,1,34,3413,341324,', '泗  县', '11234', '341324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3414', '34', '0,1,34,3414,', '巢湖', '11235', '3414', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341401', '3414', '0,1,34,3414,341401,', '市辖区', '11236', '341401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341402', '3414', '0,1,34,3414,341402,', '居巢区', '11237', '341402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341421', '3414', '0,1,34,3414,341421,', '庐江县', '11238', '341421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341422', '3414', '0,1,34,3414,341422,', '无为县', '11239', '341422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341423', '3414', '0,1,34,3414,341423,', '含山县', '11240', '341423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341424', '3414', '0,1,34,3414,341424,', '和  县', '11241', '341424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3415', '34', '0,1,34,3415,', '六安', '11242', '3415', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341501', '3415', '0,1,34,3415,341501,', '市辖区', '11243', '341501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341502', '3415', '0,1,34,3415,341502,', '金安区', '11244', '341502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341503', '3415', '0,1,34,3415,341503,', '裕安区', '11245', '341503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341521', '3415', '0,1,34,3415,341521,', '寿  县', '11246', '341521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341522', '3415', '0,1,34,3415,341522,', '霍邱县', '11247', '341522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341523', '3415', '0,1,34,3415,341523,', '舒城县', '11248', '341523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341524', '3415', '0,1,34,3415,341524,', '金寨县', '11249', '341524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341525', '3415', '0,1,34,3415,341525,', '霍山县', '11250', '341525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3416', '34', '0,1,34,3416,', '亳州', '11251', '3416', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341601', '3416', '0,1,34,3416,341601,', '市辖区', '11252', '341601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341602', '3416', '0,1,34,3416,341602,', '谯城区', '11253', '341602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341621', '3416', '0,1,34,3416,341621,', '涡阳县', '11254', '341621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341622', '3416', '0,1,34,3416,341622,', '蒙城县', '11255', '341622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341623', '3416', '0,1,34,3416,341623,', '利辛县', '11256', '341623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3417', '34', '0,1,34,3417,', '池州', '11257', '3417', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341701', '3417', '0,1,34,3417,341701,', '市辖区', '11258', '341701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341702', '3417', '0,1,34,3417,341702,', '贵池区', '11259', '341702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341721', '3417', '0,1,34,3417,341721,', '东至县', '11260', '341721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341722', '3417', '0,1,34,3417,341722,', '石台县', '11261', '341722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341723', '3417', '0,1,34,3417,341723,', '青阳县', '11262', '341723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3418', '34', '0,1,34,3418,', '宣城', '11263', '3418', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341801', '3418', '0,1,34,3418,341801,', '市辖区', '11264', '341801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341802', '3418', '0,1,34,3418,341802,', '宣州区', '11265', '341802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341821', '3418', '0,1,34,3418,341821,', '郎溪县', '11266', '341821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341822', '3418', '0,1,34,3418,341822,', '广德县', '11267', '341822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341823', '3418', '0,1,34,3418,341823,', '泾  县', '11268', '341823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341824', '3418', '0,1,34,3418,341824,', '绩溪县', '11269', '341824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341825', '3418', '0,1,34,3418,341825,', '旌德县', '11270', '341825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('341881', '3418', '0,1,34,3418,341881,', '宁国市', '11271', '341881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('35', '1', '0,1,35,', '福建', '11272', '35', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3501', '35', '0,1,35,3501,', '福州', '11273', '3501', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350101', '3501', '0,1,35,3501,350101,', '市辖区', '11274', '350101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350102', '3501', '0,1,35,3501,350102,', '鼓楼区', '11275', '350102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350103', '3501', '0,1,35,3501,350103,', '台江区', '11276', '350103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350104', '3501', '0,1,35,3501,350104,', '仓山区', '11277', '350104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350105', '3501', '0,1,35,3501,350105,', '马尾区', '11278', '350105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350111', '3501', '0,1,35,3501,350111,', '晋安区', '11279', '350111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350121', '3501', '0,1,35,3501,350121,', '闽侯县', '11280', '350121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350122', '3501', '0,1,35,3501,350122,', '连江县', '11281', '350122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350123', '3501', '0,1,35,3501,350123,', '罗源县', '11282', '350123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350124', '3501', '0,1,35,3501,350124,', '闽清县', '11283', '350124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350125', '3501', '0,1,35,3501,350125,', '永泰县', '11284', '350125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350128', '3501', '0,1,35,3501,350128,', '平潭县', '11285', '350128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350181', '3501', '0,1,35,3501,350181,', '福清市', '11286', '350181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350182', '3501', '0,1,35,3501,350182,', '长乐市', '11287', '350182', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3502', '35', '0,1,35,3502,', '厦门', '11288', '3502', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350201', '3502', '0,1,35,3502,350201,', '市辖区', '11289', '350201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350202', '3502', '0,1,35,3502,350202,', '鼓浪屿区', '11290', '350202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350203', '3502', '0,1,35,3502,350203,', '思明区', '11291', '350203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350204', '3502', '0,1,35,3502,350204,', '开元区', '11292', '350204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350205', '3502', '0,1,35,3502,350205,', '杏林区', '11293', '350205', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350206', '3502', '0,1,35,3502,350206,', '湖里区', '11294', '350206', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350211', '3502', '0,1,35,3502,350211,', '集美区', '11295', '350211', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350212', '3502', '0,1,35,3502,350212,', '同安区', '11296', '350212', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3503', '35', '0,1,35,3503,', '莆田', '11297', '3503', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350301', '3503', '0,1,35,3503,350301,', '市辖区', '11298', '350301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350302', '3503', '0,1,35,3503,350302,', '城厢区', '11299', '350302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350303', '3503', '0,1,35,3503,350303,', '涵江区', '11300', '350303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350321', '3503', '0,1,35,3503,350321,', '莆田县', '11301', '350321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350322', '3503', '0,1,35,3503,350322,', '仙游县', '11302', '350322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3504', '35', '0,1,35,3504,', '三明', '11303', '3504', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350401', '3504', '0,1,35,3504,350401,', '市辖区', '11304', '350401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350402', '3504', '0,1,35,3504,350402,', '梅列区', '11305', '350402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350403', '3504', '0,1,35,3504,350403,', '三元区', '11306', '350403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350421', '3504', '0,1,35,3504,350421,', '明溪县', '11307', '350421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350423', '3504', '0,1,35,3504,350423,', '清流县', '11308', '350423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350424', '3504', '0,1,35,3504,350424,', '宁化县', '11309', '350424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350425', '3504', '0,1,35,3504,350425,', '大田县', '11310', '350425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350426', '3504', '0,1,35,3504,350426,', '尤溪县', '11311', '350426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350427', '3504', '0,1,35,3504,350427,', '沙  县', '11312', '350427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350428', '3504', '0,1,35,3504,350428,', '将乐县', '11313', '350428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350429', '3504', '0,1,35,3504,350429,', '泰宁县', '11314', '350429', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350430', '3504', '0,1,35,3504,350430,', '建宁县', '11315', '350430', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350481', '3504', '0,1,35,3504,350481,', '永安市', '11316', '350481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3505', '35', '0,1,35,3505,', '泉州', '11317', '3505', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350501', '3505', '0,1,35,3505,350501,', '市辖区', '11318', '350501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350502', '3505', '0,1,35,3505,350502,', '鲤城区', '11319', '350502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350503', '3505', '0,1,35,3505,350503,', '丰泽区', '11320', '350503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350504', '3505', '0,1,35,3505,350504,', '洛江区', '11321', '350504', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350505', '3505', '0,1,35,3505,350505,', '泉港区', '11322', '350505', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350521', '3505', '0,1,35,3505,350521,', '惠安县', '11323', '350521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350524', '3505', '0,1,35,3505,350524,', '安溪县', '11324', '350524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350525', '3505', '0,1,35,3505,350525,', '永春县', '11325', '350525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350526', '3505', '0,1,35,3505,350526,', '德化县', '11326', '350526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350527', '3505', '0,1,35,3505,350527,', '金门县', '11327', '350527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350581', '3505', '0,1,35,3505,350581,', '石狮市', '11328', '350581', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350582', '3505', '0,1,35,3505,350582,', '晋江市', '11329', '350582', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350583', '3505', '0,1,35,3505,350583,', '南安市', '11330', '350583', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3506', '35', '0,1,35,3506,', '漳州', '11331', '3506', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350601', '3506', '0,1,35,3506,350601,', '市辖区', '11332', '350601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350602', '3506', '0,1,35,3506,350602,', '芗城区', '11333', '350602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350603', '3506', '0,1,35,3506,350603,', '龙文区', '11334', '350603', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350622', '3506', '0,1,35,3506,350622,', '云霄县', '11335', '350622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350623', '3506', '0,1,35,3506,350623,', '漳浦县', '11336', '350623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350624', '3506', '0,1,35,3506,350624,', '诏安县', '11337', '350624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350625', '3506', '0,1,35,3506,350625,', '长泰县', '11338', '350625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350626', '3506', '0,1,35,3506,350626,', '东山县', '11339', '350626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350627', '3506', '0,1,35,3506,350627,', '南靖县', '11340', '350627', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350628', '3506', '0,1,35,3506,350628,', '平和县', '11341', '350628', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350629', '3506', '0,1,35,3506,350629,', '华安县', '11342', '350629', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350681', '3506', '0,1,35,3506,350681,', '龙海市', '11343', '350681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3507', '35', '0,1,35,3507,', '南平', '11344', '3507', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350701', '3507', '0,1,35,3507,350701,', '市辖区', '11345', '350701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350702', '3507', '0,1,35,3507,350702,', '延平区', '11346', '350702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350721', '3507', '0,1,35,3507,350721,', '顺昌县', '11347', '350721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350722', '3507', '0,1,35,3507,350722,', '浦城县', '11348', '350722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350723', '3507', '0,1,35,3507,350723,', '光泽县', '11349', '350723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350724', '3507', '0,1,35,3507,350724,', '松溪县', '11350', '350724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350725', '3507', '0,1,35,3507,350725,', '政和县', '11351', '350725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350781', '3507', '0,1,35,3507,350781,', '邵武市', '11352', '350781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350782', '3507', '0,1,35,3507,350782,', '武夷山市', '11353', '350782', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350783', '3507', '0,1,35,3507,350783,', '建瓯市', '11354', '350783', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350784', '3507', '0,1,35,3507,350784,', '建阳市', '11355', '350784', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3508', '35', '0,1,35,3508,', '龙岩', '11356', '3508', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350801', '3508', '0,1,35,3508,350801,', '市辖区', '11357', '350801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350802', '3508', '0,1,35,3508,350802,', '新罗区', '11358', '350802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350821', '3508', '0,1,35,3508,350821,', '长汀县', '11359', '350821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350822', '3508', '0,1,35,3508,350822,', '永定县', '11360', '350822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350823', '3508', '0,1,35,3508,350823,', '上杭县', '11361', '350823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350824', '3508', '0,1,35,3508,350824,', '武平县', '11362', '350824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350825', '3508', '0,1,35,3508,350825,', '连城县', '11363', '350825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350881', '3508', '0,1,35,3508,350881,', '漳平市', '11364', '350881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3509', '35', '0,1,35,3509,', '宁德', '11365', '3509', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350901', '3509', '0,1,35,3509,350901,', '市辖区', '11366', '350901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350902', '3509', '0,1,35,3509,350902,', '蕉城区', '11367', '350902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350921', '3509', '0,1,35,3509,350921,', '霞浦县', '11368', '350921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350922', '3509', '0,1,35,3509,350922,', '古田县', '11369', '350922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350923', '3509', '0,1,35,3509,350923,', '屏南县', '11370', '350923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350924', '3509', '0,1,35,3509,350924,', '寿宁县', '11371', '350924', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350925', '3509', '0,1,35,3509,350925,', '周宁县', '11372', '350925', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350926', '3509', '0,1,35,3509,350926,', '柘荣县', '11373', '350926', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350981', '3509', '0,1,35,3509,350981,', '福安市', '11374', '350981', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('350982', '3509', '0,1,35,3509,350982,', '福鼎市', '11375', '350982', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('36', '1', '0,1,36,', '江西', '11376', '36', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3601', '36', '0,1,36,3601,', '南昌', '11377', '3601', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360101', '3601', '0,1,36,3601,360101,', '市辖区', '11378', '360101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360102', '3601', '0,1,36,3601,360102,', '东湖区', '11379', '360102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360103', '3601', '0,1,36,3601,360103,', '西湖区', '11380', '360103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360104', '3601', '0,1,36,3601,360104,', '青云谱区', '11381', '360104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360105', '3601', '0,1,36,3601,360105,', '湾里区', '11382', '360105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360111', '3601', '0,1,36,3601,360111,', '郊  区', '11383', '360111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360121', '3601', '0,1,36,3601,360121,', '南昌县', '11384', '360121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360122', '3601', '0,1,36,3601,360122,', '新建县', '11385', '360122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360123', '3601', '0,1,36,3601,360123,', '安义县', '11386', '360123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360124', '3601', '0,1,36,3601,360124,', '进贤县', '11387', '360124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3602', '36', '0,1,36,3602,', '景德镇', '11388', '3602', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360201', '3602', '0,1,36,3602,360201,', '市辖区', '11389', '360201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360202', '3602', '0,1,36,3602,360202,', '昌江区', '11390', '360202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360203', '3602', '0,1,36,3602,360203,', '珠山区', '11391', '360203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360222', '3602', '0,1,36,3602,360222,', '浮梁县', '11392', '360222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360281', '3602', '0,1,36,3602,360281,', '乐平市', '11393', '360281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3603', '36', '0,1,36,3603,', '萍乡', '11394', '3603', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360301', '3603', '0,1,36,3603,360301,', '市辖区', '11395', '360301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360302', '3603', '0,1,36,3603,360302,', '安源区', '11396', '360302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360313', '3603', '0,1,36,3603,360313,', '湘东区', '11397', '360313', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360321', '3603', '0,1,36,3603,360321,', '莲花县', '11398', '360321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360322', '3603', '0,1,36,3603,360322,', '上栗县', '11399', '360322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360323', '3603', '0,1,36,3603,360323,', '芦溪县', '11400', '360323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3604', '36', '0,1,36,3604,', '九江', '11401', '3604', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360401', '3604', '0,1,36,3604,360401,', '市辖区', '11402', '360401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360402', '3604', '0,1,36,3604,360402,', '庐山区', '11403', '360402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360403', '3604', '0,1,36,3604,360403,', '浔阳区', '11404', '360403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360421', '3604', '0,1,36,3604,360421,', '九江县', '11405', '360421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360423', '3604', '0,1,36,3604,360423,', '武宁县', '11406', '360423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360424', '3604', '0,1,36,3604,360424,', '修水县', '11407', '360424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360425', '3604', '0,1,36,3604,360425,', '永修县', '11408', '360425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360426', '3604', '0,1,36,3604,360426,', '德安县', '11409', '360426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360427', '3604', '0,1,36,3604,360427,', '星子县', '11410', '360427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360428', '3604', '0,1,36,3604,360428,', '都昌县', '11411', '360428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360429', '3604', '0,1,36,3604,360429,', '湖口县', '11412', '360429', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360430', '3604', '0,1,36,3604,360430,', '彭泽县', '11413', '360430', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360481', '3604', '0,1,36,3604,360481,', '瑞昌市', '11414', '360481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3605', '36', '0,1,36,3605,', '新余', '11415', '3605', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360501', '3605', '0,1,36,3605,360501,', '市辖区', '11416', '360501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360502', '3605', '0,1,36,3605,360502,', '渝水区', '11417', '360502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360521', '3605', '0,1,36,3605,360521,', '分宜县', '11418', '360521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3606', '36', '0,1,36,3606,', '鹰潭', '11419', '3606', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360601', '3606', '0,1,36,3606,360601,', '市辖区', '11420', '360601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360602', '3606', '0,1,36,3606,360602,', '月湖区', '11421', '360602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360622', '3606', '0,1,36,3606,360622,', '余江县', '11422', '360622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360681', '3606', '0,1,36,3606,360681,', '贵溪市', '11423', '360681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3607', '36', '0,1,36,3607,', '赣州', '11424', '3607', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360701', '3607', '0,1,36,3607,360701,', '市辖区', '11425', '360701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360702', '3607', '0,1,36,3607,360702,', '章贡区', '11426', '360702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360721', '3607', '0,1,36,3607,360721,', '赣  县', '11427', '360721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360722', '3607', '0,1,36,3607,360722,', '信丰县', '11428', '360722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360723', '3607', '0,1,36,3607,360723,', '大余县', '11429', '360723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360724', '3607', '0,1,36,3607,360724,', '上犹县', '11430', '360724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360725', '3607', '0,1,36,3607,360725,', '崇义县', '11431', '360725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360726', '3607', '0,1,36,3607,360726,', '安远县', '11432', '360726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360727', '3607', '0,1,36,3607,360727,', '龙南县', '11433', '360727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360728', '3607', '0,1,36,3607,360728,', '定南县', '11434', '360728', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360729', '3607', '0,1,36,3607,360729,', '全南县', '11435', '360729', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360730', '3607', '0,1,36,3607,360730,', '宁都县', '11436', '360730', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360731', '3607', '0,1,36,3607,360731,', '于都县', '11437', '360731', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360732', '3607', '0,1,36,3607,360732,', '兴国县', '11438', '360732', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360733', '3607', '0,1,36,3607,360733,', '会昌县', '11439', '360733', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360734', '3607', '0,1,36,3607,360734,', '寻乌县', '11440', '360734', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360735', '3607', '0,1,36,3607,360735,', '石城县', '11441', '360735', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360781', '3607', '0,1,36,3607,360781,', '瑞金市', '11442', '360781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360782', '3607', '0,1,36,3607,360782,', '南康市', '11443', '360782', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3608', '36', '0,1,36,3608,', '吉安', '11444', '3608', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360801', '3608', '0,1,36,3608,360801,', '市辖区', '11445', '360801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360802', '3608', '0,1,36,3608,360802,', '吉州区', '11446', '360802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360803', '3608', '0,1,36,3608,360803,', '青原区', '11447', '360803', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360821', '3608', '0,1,36,3608,360821,', '吉安县', '11448', '360821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360822', '3608', '0,1,36,3608,360822,', '吉水县', '11449', '360822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360823', '3608', '0,1,36,3608,360823,', '峡江县', '11450', '360823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360824', '3608', '0,1,36,3608,360824,', '新干县', '11451', '360824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360825', '3608', '0,1,36,3608,360825,', '永丰县', '11452', '360825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360826', '3608', '0,1,36,3608,360826,', '泰和县', '11453', '360826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360827', '3608', '0,1,36,3608,360827,', '遂川县', '11454', '360827', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360828', '3608', '0,1,36,3608,360828,', '万安县', '11455', '360828', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360829', '3608', '0,1,36,3608,360829,', '安福县', '11456', '360829', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360830', '3608', '0,1,36,3608,360830,', '永新县', '11457', '360830', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360881', '3608', '0,1,36,3608,360881,', '井冈山市', '11458', '360881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3609', '36', '0,1,36,3609,', '宜春', '11459', '3609', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360901', '3609', '0,1,36,3609,360901,', '市辖区', '11460', '360901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360902', '3609', '0,1,36,3609,360902,', '袁州区', '11461', '360902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360921', '3609', '0,1,36,3609,360921,', '奉新县', '11462', '360921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360922', '3609', '0,1,36,3609,360922,', '万载县', '11463', '360922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360923', '3609', '0,1,36,3609,360923,', '上高县', '11464', '360923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360924', '3609', '0,1,36,3609,360924,', '宜丰县', '11465', '360924', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360925', '3609', '0,1,36,3609,360925,', '靖安县', '11466', '360925', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360926', '3609', '0,1,36,3609,360926,', '铜鼓县', '11467', '360926', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360981', '3609', '0,1,36,3609,360981,', '丰城市', '11468', '360981', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360982', '3609', '0,1,36,3609,360982,', '樟树市', '11469', '360982', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('360983', '3609', '0,1,36,3609,360983,', '高安市', '11470', '360983', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3610', '36', '0,1,36,3610,', '抚州', '11471', '3610', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361001', '3610', '0,1,36,3610,361001,', '市辖区', '11472', '361001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361002', '3610', '0,1,36,3610,361002,', '临川区', '11473', '361002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361021', '3610', '0,1,36,3610,361021,', '南城县', '11474', '361021', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361022', '3610', '0,1,36,3610,361022,', '黎川县', '11475', '361022', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361023', '3610', '0,1,36,3610,361023,', '南丰县', '11476', '361023', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361024', '3610', '0,1,36,3610,361024,', '崇仁县', '11477', '361024', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361025', '3610', '0,1,36,3610,361025,', '乐安县', '11478', '361025', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361026', '3610', '0,1,36,3610,361026,', '宜黄县', '11479', '361026', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361027', '3610', '0,1,36,3610,361027,', '金溪县', '11480', '361027', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361028', '3610', '0,1,36,3610,361028,', '资溪县', '11481', '361028', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361029', '3610', '0,1,36,3610,361029,', '东乡县', '11482', '361029', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361030', '3610', '0,1,36,3610,361030,', '广昌县', '11483', '361030', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3611', '36', '0,1,36,3611,', '上饶', '11484', '3611', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361101', '3611', '0,1,36,3611,361101,', '市辖区', '11485', '361101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361102', '3611', '0,1,36,3611,361102,', '信州区', '11486', '361102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361121', '3611', '0,1,36,3611,361121,', '上饶县', '11487', '361121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361122', '3611', '0,1,36,3611,361122,', '广丰县', '11488', '361122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361123', '3611', '0,1,36,3611,361123,', '玉山县', '11489', '361123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361124', '3611', '0,1,36,3611,361124,', '铅山县', '11490', '361124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361125', '3611', '0,1,36,3611,361125,', '横峰县', '11491', '361125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361126', '3611', '0,1,36,3611,361126,', '弋阳县', '11492', '361126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361127', '3611', '0,1,36,3611,361127,', '余干县', '11493', '361127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361128', '3611', '0,1,36,3611,361128,', '波阳县', '11494', '361128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361129', '3611', '0,1,36,3611,361129,', '万年县', '11495', '361129', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361130', '3611', '0,1,36,3611,361130,', '婺源县', '11496', '361130', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('361181', '3611', '0,1,36,3611,361181,', '德兴市', '11497', '361181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('37', '1', '0,1,37,', '山东', '11498', '37', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3701', '37', '0,1,37,3701,', '济南', '11499', '3701', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370101', '3701', '0,1,37,3701,370101,', '市辖区', '11500', '370101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370102', '3701', '0,1,37,3701,370102,', '历下区', '11501', '370102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370103', '3701', '0,1,37,3701,370103,', '市中区', '11502', '370103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370104', '3701', '0,1,37,3701,370104,', '槐荫区', '11503', '370104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370105', '3701', '0,1,37,3701,370105,', '天桥区', '11504', '370105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370112', '3701', '0,1,37,3701,370112,', '历城区', '11505', '370112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370123', '3701', '0,1,37,3701,370123,', '长清县', '11506', '370123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370124', '3701', '0,1,37,3701,370124,', '平阴县', '11507', '370124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370125', '3701', '0,1,37,3701,370125,', '济阳县', '11508', '370125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370126', '3701', '0,1,37,3701,370126,', '商河县', '11509', '370126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370181', '3701', '0,1,37,3701,370181,', '章丘市', '11510', '370181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3702', '37', '0,1,37,3702,', '青岛', '11511', '3702', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370201', '3702', '0,1,37,3702,370201,', '市辖区', '11512', '370201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370202', '3702', '0,1,37,3702,370202,', '市南区', '11513', '370202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370203', '3702', '0,1,37,3702,370203,', '市北区', '11514', '370203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370205', '3702', '0,1,37,3702,370205,', '四方区', '11515', '370205', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370211', '3702', '0,1,37,3702,370211,', '黄岛区', '11516', '370211', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370212', '3702', '0,1,37,3702,370212,', '崂山区', '11517', '370212', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370213', '3702', '0,1,37,3702,370213,', '李沧区', '11518', '370213', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370214', '3702', '0,1,37,3702,370214,', '城阳区', '11519', '370214', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370281', '3702', '0,1,37,3702,370281,', '胶州市', '11520', '370281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370282', '3702', '0,1,37,3702,370282,', '即墨市', '11521', '370282', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370283', '3702', '0,1,37,3702,370283,', '平度市', '11522', '370283', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370284', '3702', '0,1,37,3702,370284,', '胶南市', '11523', '370284', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370285', '3702', '0,1,37,3702,370285,', '莱西市', '11524', '370285', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3703', '37', '0,1,37,3703,', '淄博', '11525', '3703', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370301', '3703', '0,1,37,3703,370301,', '市辖区', '11526', '370301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370302', '3703', '0,1,37,3703,370302,', '淄川区', '11527', '370302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370303', '3703', '0,1,37,3703,370303,', '张店区', '11528', '370303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370304', '3703', '0,1,37,3703,370304,', '博山区', '11529', '370304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370305', '3703', '0,1,37,3703,370305,', '临淄区', '11530', '370305', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370306', '3703', '0,1,37,3703,370306,', '周村区', '11531', '370306', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370321', '3703', '0,1,37,3703,370321,', '桓台县', '11532', '370321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370322', '3703', '0,1,37,3703,370322,', '高青县', '11533', '370322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370323', '3703', '0,1,37,3703,370323,', '沂源县', '11534', '370323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3704', '37', '0,1,37,3704,', '枣庄', '11535', '3704', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370401', '3704', '0,1,37,3704,370401,', '市辖区', '11536', '370401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370402', '3704', '0,1,37,3704,370402,', '市中区', '11537', '370402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370403', '3704', '0,1,37,3704,370403,', '薛城区', '11538', '370403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370404', '3704', '0,1,37,3704,370404,', '峄城区', '11539', '370404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370405', '3704', '0,1,37,3704,370405,', '台儿庄区', '11540', '370405', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370406', '3704', '0,1,37,3704,370406,', '山亭区', '11541', '370406', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370481', '3704', '0,1,37,3704,370481,', '滕州市', '11542', '370481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3705', '37', '0,1,37,3705,', '东营', '11543', '3705', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370501', '3705', '0,1,37,3705,370501,', '市辖区', '11544', '370501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370502', '3705', '0,1,37,3705,370502,', '东营区', '11545', '370502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370503', '3705', '0,1,37,3705,370503,', '河口区', '11546', '370503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370521', '3705', '0,1,37,3705,370521,', '垦利县', '11547', '370521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370522', '3705', '0,1,37,3705,370522,', '利津县', '11548', '370522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370523', '3705', '0,1,37,3705,370523,', '广饶县', '11549', '370523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3706', '37', '0,1,37,3706,', '烟台', '11550', '3706', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370601', '3706', '0,1,37,3706,370601,', '市辖区', '11551', '370601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370602', '3706', '0,1,37,3706,370602,', '芝罘区', '11552', '370602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370611', '3706', '0,1,37,3706,370611,', '福山区', '11553', '370611', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370612', '3706', '0,1,37,3706,370612,', '牟平区', '11554', '370612', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370613', '3706', '0,1,37,3706,370613,', '莱山区', '11555', '370613', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370634', '3706', '0,1,37,3706,370634,', '长岛县', '11556', '370634', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370681', '3706', '0,1,37,3706,370681,', '龙口市', '11557', '370681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370682', '3706', '0,1,37,3706,370682,', '莱阳市', '11558', '370682', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370683', '3706', '0,1,37,3706,370683,', '莱州市', '11559', '370683', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370684', '3706', '0,1,37,3706,370684,', '蓬莱市', '11560', '370684', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370685', '3706', '0,1,37,3706,370685,', '招远市', '11561', '370685', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370686', '3706', '0,1,37,3706,370686,', '栖霞市', '11562', '370686', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370687', '3706', '0,1,37,3706,370687,', '海阳市', '11563', '370687', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3707', '37', '0,1,37,3707,', '潍坊', '11564', '3707', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370701', '3707', '0,1,37,3707,370701,', '市辖区', '11565', '370701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370702', '3707', '0,1,37,3707,370702,', '潍城区', '11566', '370702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370703', '3707', '0,1,37,3707,370703,', '寒亭区', '11567', '370703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370704', '3707', '0,1,37,3707,370704,', '坊子区', '11568', '370704', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370705', '3707', '0,1,37,3707,370705,', '奎文区', '11569', '370705', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370724', '3707', '0,1,37,3707,370724,', '临朐县', '11570', '370724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370725', '3707', '0,1,37,3707,370725,', '昌乐县', '11571', '370725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370781', '3707', '0,1,37,3707,370781,', '青州市', '11572', '370781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370782', '3707', '0,1,37,3707,370782,', '诸城市', '11573', '370782', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370783', '3707', '0,1,37,3707,370783,', '寿光市', '11574', '370783', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370784', '3707', '0,1,37,3707,370784,', '安丘市', '11575', '370784', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370785', '3707', '0,1,37,3707,370785,', '高密市', '11576', '370785', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370786', '3707', '0,1,37,3707,370786,', '昌邑市', '11577', '370786', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3708', '37', '0,1,37,3708,', '济宁', '11578', '3708', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370801', '3708', '0,1,37,3708,370801,', '市辖区', '11579', '370801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370802', '3708', '0,1,37,3708,370802,', '市中区', '11580', '370802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370811', '3708', '0,1,37,3708,370811,', '任城区', '11581', '370811', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370826', '3708', '0,1,37,3708,370826,', '微山县', '11582', '370826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370827', '3708', '0,1,37,3708,370827,', '鱼台县', '11583', '370827', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370828', '3708', '0,1,37,3708,370828,', '金乡县', '11584', '370828', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370829', '3708', '0,1,37,3708,370829,', '嘉祥县', '11585', '370829', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370830', '3708', '0,1,37,3708,370830,', '汶上县', '11586', '370830', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370831', '3708', '0,1,37,3708,370831,', '泗水县', '11587', '370831', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370832', '3708', '0,1,37,3708,370832,', '梁山县', '11588', '370832', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370881', '3708', '0,1,37,3708,370881,', '曲阜市', '11589', '370881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370882', '3708', '0,1,37,3708,370882,', '兖州市', '11590', '370882', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370883', '3708', '0,1,37,3708,370883,', '邹城市', '11591', '370883', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3709', '37', '0,1,37,3709,', '泰安', '11592', '3709', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370901', '3709', '0,1,37,3709,370901,', '市辖区', '11593', '370901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370902', '3709', '0,1,37,3709,370902,', '泰山区', '11594', '370902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370903', '3709', '0,1,37,3709,370903,', '岱岳区', '11595', '370903', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370921', '3709', '0,1,37,3709,370921,', '宁阳县', '11596', '370921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370923', '3709', '0,1,37,3709,370923,', '东平县', '11597', '370923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370982', '3709', '0,1,37,3709,370982,', '新泰市', '11598', '370982', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('370983', '3709', '0,1,37,3709,370983,', '肥城市', '11599', '370983', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3710', '37', '0,1,37,3710,', '威海', '11600', '3710', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371001', '3710', '0,1,37,3710,371001,', '市辖区', '11601', '371001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371002', '3710', '0,1,37,3710,371002,', '环翠区', '11602', '371002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371081', '3710', '0,1,37,3710,371081,', '文登市', '11603', '371081', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371082', '3710', '0,1,37,3710,371082,', '荣成市', '11604', '371082', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371083', '3710', '0,1,37,3710,371083,', '乳山市', '11605', '371083', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3711', '37', '0,1,37,3711,', '日照', '11606', '3711', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371101', '3711', '0,1,37,3711,371101,', '市辖区', '11607', '371101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371102', '3711', '0,1,37,3711,371102,', '东港区', '11608', '371102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371121', '3711', '0,1,37,3711,371121,', '五莲县', '11609', '371121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371122', '3711', '0,1,37,3711,371122,', '莒  县', '11610', '371122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3712', '37', '0,1,37,3712,', '莱芜', '11611', '3712', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371201', '3712', '0,1,37,3712,371201,', '市辖区', '11612', '371201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371202', '3712', '0,1,37,3712,371202,', '莱城区', '11613', '371202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371203', '3712', '0,1,37,3712,371203,', '钢城区', '11614', '371203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3713', '37', '0,1,37,3713,', '临沂', '11615', '3713', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371301', '3713', '0,1,37,3713,371301,', '市辖区', '11616', '371301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371302', '3713', '0,1,37,3713,371302,', '兰山区', '11617', '371302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371311', '3713', '0,1,37,3713,371311,', '罗庄区', '11618', '371311', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371312', '3713', '0,1,37,3713,371312,', '河东区', '11619', '371312', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371321', '3713', '0,1,37,3713,371321,', '沂南县', '11620', '371321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371322', '3713', '0,1,37,3713,371322,', '郯城县', '11621', '371322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371323', '3713', '0,1,37,3713,371323,', '沂水县', '11622', '371323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371324', '3713', '0,1,37,3713,371324,', '苍山县', '11623', '371324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371325', '3713', '0,1,37,3713,371325,', '费  县', '11624', '371325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371326', '3713', '0,1,37,3713,371326,', '平邑县', '11625', '371326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371327', '3713', '0,1,37,3713,371327,', '莒南县', '11626', '371327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371328', '3713', '0,1,37,3713,371328,', '蒙阴县', '11627', '371328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371329', '3713', '0,1,37,3713,371329,', '临沭县', '11628', '371329', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3714', '37', '0,1,37,3714,', '德州', '11629', '3714', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371401', '3714', '0,1,37,3714,371401,', '市辖区', '11630', '371401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371402', '3714', '0,1,37,3714,371402,', '德城区', '11631', '371402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371421', '3714', '0,1,37,3714,371421,', '陵  县', '11632', '371421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371422', '3714', '0,1,37,3714,371422,', '宁津县', '11633', '371422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371423', '3714', '0,1,37,3714,371423,', '庆云县', '11634', '371423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371424', '3714', '0,1,37,3714,371424,', '临邑县', '11635', '371424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371425', '3714', '0,1,37,3714,371425,', '齐河县', '11636', '371425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371426', '3714', '0,1,37,3714,371426,', '平原县', '11637', '371426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371427', '3714', '0,1,37,3714,371427,', '夏津县', '11638', '371427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371428', '3714', '0,1,37,3714,371428,', '武城县', '11639', '371428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371481', '3714', '0,1,37,3714,371481,', '乐陵市', '11640', '371481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371482', '3714', '0,1,37,3714,371482,', '禹城市', '11641', '371482', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3715', '37', '0,1,37,3715,', '聊城', '11642', '3715', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371501', '3715', '0,1,37,3715,371501,', '市辖区', '11643', '371501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371502', '3715', '0,1,37,3715,371502,', '东昌府区', '11644', '371502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371521', '3715', '0,1,37,3715,371521,', '阳谷县', '11645', '371521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371522', '3715', '0,1,37,3715,371522,', '莘  县', '11646', '371522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371523', '3715', '0,1,37,3715,371523,', '茌平县', '11647', '371523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371524', '3715', '0,1,37,3715,371524,', '东阿县', '11648', '371524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371525', '3715', '0,1,37,3715,371525,', '冠  县', '11649', '371525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371526', '3715', '0,1,37,3715,371526,', '高唐县', '11650', '371526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371581', '3715', '0,1,37,3715,371581,', '临清市', '11651', '371581', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3716', '37', '0,1,37,3716,', '滨州', '11652', '3716', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371601', '3716', '0,1,37,3716,371601,', '市辖区', '11653', '371601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371603', '3716', '0,1,37,3716,371603,', '滨城区', '11654', '371603', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371621', '3716', '0,1,37,3716,371621,', '惠民县', '11655', '371621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371622', '3716', '0,1,37,3716,371622,', '阳信县', '11656', '371622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371623', '3716', '0,1,37,3716,371623,', '无棣县', '11657', '371623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371624', '3716', '0,1,37,3716,371624,', '沾化县', '11658', '371624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371625', '3716', '0,1,37,3716,371625,', '博兴县', '11659', '371625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371626', '3716', '0,1,37,3716,371626,', '邹平县', '11660', '371626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('3717', '37', '0,1,37,3717,', '菏泽', '11661', '3717', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371701', '3717', '0,1,37,3717,371701,', '市辖区', '11662', '371701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371702', '3717', '0,1,37,3717,371702,', '牡丹区', '11663', '371702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371721', '3717', '0,1,37,3717,371721,', '曹  县', '11664', '371721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371722', '3717', '0,1,37,3717,371722,', '单  县', '11665', '371722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371723', '3717', '0,1,37,3717,371723,', '成武县', '11666', '371723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371724', '3717', '0,1,37,3717,371724,', '巨野县', '11667', '371724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371725', '3717', '0,1,37,3717,371725,', '郓城县', '11668', '371725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371726', '3717', '0,1,37,3717,371726,', '鄄城县', '11669', '371726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371727', '3717', '0,1,37,3717,371727,', '定陶县', '11670', '371727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('371728', '3717', '0,1,37,3717,371728,', '东明县', '11671', '371728', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('41', '1', '0,1,41,', '河南', '11672', '41', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4101', '41', '0,1,41,4101,', '郑州', '11673', '4101', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410101', '4101', '0,1,41,4101,410101,', '市辖区', '11674', '410101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410102', '4101', '0,1,41,4101,410102,', '中原区', '11675', '410102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410103', '4101', '0,1,41,4101,410103,', '二七区', '11676', '410103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410104', '4101', '0,1,41,4101,410104,', '管城回族区', '11677', '410104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410105', '4101', '0,1,41,4101,410105,', '金水区', '11678', '410105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410106', '4101', '0,1,41,4101,410106,', '上街区', '11679', '410106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410108', '4101', '0,1,41,4101,410108,', '邙山区', '11680', '410108', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410122', '4101', '0,1,41,4101,410122,', '中牟县', '11681', '410122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410181', '4101', '0,1,41,4101,410181,', '巩义市', '11682', '410181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410182', '4101', '0,1,41,4101,410182,', '荥阳市', '11683', '410182', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410183', '4101', '0,1,41,4101,410183,', '新密市', '11684', '410183', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410184', '4101', '0,1,41,4101,410184,', '新郑市', '11685', '410184', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410185', '4101', '0,1,41,4101,410185,', '登封市', '11686', '410185', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4102', '41', '0,1,41,4102,', '开封', '11687', '4102', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410201', '4102', '0,1,41,4102,410201,', '市辖区', '11688', '410201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410202', '4102', '0,1,41,4102,410202,', '龙亭区', '11689', '410202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410203', '4102', '0,1,41,4102,410203,', '顺河回族区', '11690', '410203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410204', '4102', '0,1,41,4102,410204,', '鼓楼区', '11691', '410204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410205', '4102', '0,1,41,4102,410205,', '南关区', '11692', '410205', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410211', '4102', '0,1,41,4102,410211,', '郊  区', '11693', '410211', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410221', '4102', '0,1,41,4102,410221,', '杞  县', '11694', '410221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410222', '4102', '0,1,41,4102,410222,', '通许县', '11695', '410222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410223', '4102', '0,1,41,4102,410223,', '尉氏县', '11696', '410223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410224', '4102', '0,1,41,4102,410224,', '开封县', '11697', '410224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410225', '4102', '0,1,41,4102,410225,', '兰考县', '11698', '410225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4103', '41', '0,1,41,4103,', '洛阳', '11699', '4103', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410301', '4103', '0,1,41,4103,410301,', '市辖区', '11700', '410301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410302', '4103', '0,1,41,4103,410302,', '老城区', '11701', '410302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410303', '4103', '0,1,41,4103,410303,', '西工区', '11702', '410303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410304', '4103', '0,1,41,4103,410304,', '廛河回族区', '11703', '410304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410305', '4103', '0,1,41,4103,410305,', '涧西区', '11704', '410305', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410306', '4103', '0,1,41,4103,410306,', '吉利区', '11705', '410306', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410307', '4103', '0,1,41,4103,410307,', '洛龙区', '11706', '410307', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410322', '4103', '0,1,41,4103,410322,', '孟津县', '11707', '410322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410323', '4103', '0,1,41,4103,410323,', '新安县', '11708', '410323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410324', '4103', '0,1,41,4103,410324,', '栾川县', '11709', '410324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410325', '4103', '0,1,41,4103,410325,', '嵩  县', '11710', '410325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410326', '4103', '0,1,41,4103,410326,', '汝阳县', '11711', '410326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410327', '4103', '0,1,41,4103,410327,', '宜阳县', '11712', '410327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410328', '4103', '0,1,41,4103,410328,', '洛宁县', '11713', '410328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410329', '4103', '0,1,41,4103,410329,', '伊川县', '11714', '410329', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410381', '4103', '0,1,41,4103,410381,', '偃师市', '11715', '410381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4104', '41', '0,1,41,4104,', '平顶山', '11716', '4104', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410401', '4104', '0,1,41,4104,410401,', '市辖区', '11717', '410401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410402', '4104', '0,1,41,4104,410402,', '新华区', '11718', '410402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410403', '4104', '0,1,41,4104,410403,', '卫东区', '11719', '410403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410404', '4104', '0,1,41,4104,410404,', '石龙区', '11720', '410404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410411', '4104', '0,1,41,4104,410411,', '湛河区', '11721', '410411', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410421', '4104', '0,1,41,4104,410421,', '宝丰县', '11722', '410421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410422', '4104', '0,1,41,4104,410422,', '叶  县', '11723', '410422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410423', '4104', '0,1,41,4104,410423,', '鲁山县', '11724', '410423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410425', '4104', '0,1,41,4104,410425,', '郏  县', '11725', '410425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410481', '4104', '0,1,41,4104,410481,', '舞钢市', '11726', '410481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410482', '4104', '0,1,41,4104,410482,', '汝州市', '11727', '410482', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4105', '41', '0,1,41,4105,', '安阳', '11728', '4105', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410501', '4105', '0,1,41,4105,410501,', '市辖区', '11729', '410501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410502', '4105', '0,1,41,4105,410502,', '文峰区', '11730', '410502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410503', '4105', '0,1,41,4105,410503,', '北关区', '11731', '410503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410504', '4105', '0,1,41,4105,410504,', '铁西区', '11732', '410504', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410511', '4105', '0,1,41,4105,410511,', '郊  区', '11733', '410511', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410522', '4105', '0,1,41,4105,410522,', '安阳县', '11734', '410522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410523', '4105', '0,1,41,4105,410523,', '汤阴县', '11735', '410523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410526', '4105', '0,1,41,4105,410526,', '滑  县', '11736', '410526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410527', '4105', '0,1,41,4105,410527,', '内黄县', '11737', '410527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410581', '4105', '0,1,41,4105,410581,', '林州市', '11738', '410581', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4106', '41', '0,1,41,4106,', '鹤壁', '11739', '4106', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410601', '4106', '0,1,41,4106,410601,', '市辖区', '11740', '410601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410602', '4106', '0,1,41,4106,410602,', '鹤山区', '11741', '410602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410603', '4106', '0,1,41,4106,410603,', '山城区', '11742', '410603', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410611', '4106', '0,1,41,4106,410611,', '郊  区', '11743', '410611', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410621', '4106', '0,1,41,4106,410621,', '浚  县', '11744', '410621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410622', '4106', '0,1,41,4106,410622,', '淇  县', '11745', '410622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4107', '41', '0,1,41,4107,', '新乡', '11746', '4107', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410701', '4107', '0,1,41,4107,410701,', '市辖区', '11747', '410701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410702', '4107', '0,1,41,4107,410702,', '红旗区', '11748', '410702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410703', '4107', '0,1,41,4107,410703,', '新华区', '11749', '410703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410704', '4107', '0,1,41,4107,410704,', '北站区', '11750', '410704', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410711', '4107', '0,1,41,4107,410711,', '郊  区', '11751', '410711', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410721', '4107', '0,1,41,4107,410721,', '新乡县', '11752', '410721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410724', '4107', '0,1,41,4107,410724,', '获嘉县', '11753', '410724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410725', '4107', '0,1,41,4107,410725,', '原阳县', '11754', '410725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410726', '4107', '0,1,41,4107,410726,', '延津县', '11755', '410726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410727', '4107', '0,1,41,4107,410727,', '封丘县', '11756', '410727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410728', '4107', '0,1,41,4107,410728,', '长垣县', '11757', '410728', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410781', '4107', '0,1,41,4107,410781,', '卫辉市', '11758', '410781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410782', '4107', '0,1,41,4107,410782,', '辉县市', '11759', '410782', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4108', '41', '0,1,41,4108,', '焦作', '11760', '4108', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410801', '4108', '0,1,41,4108,410801,', '市辖区', '11761', '410801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410802', '4108', '0,1,41,4108,410802,', '解放区', '11762', '410802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410803', '4108', '0,1,41,4108,410803,', '中站区', '11763', '410803', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410804', '4108', '0,1,41,4108,410804,', '马村区', '11764', '410804', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410811', '4108', '0,1,41,4108,410811,', '山阳区', '11765', '410811', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410821', '4108', '0,1,41,4108,410821,', '修武县', '11766', '410821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410822', '4108', '0,1,41,4108,410822,', '博爱县', '11767', '410822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410823', '4108', '0,1,41,4108,410823,', '武陟县', '11768', '410823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410825', '4108', '0,1,41,4108,410825,', '温  县', '11769', '410825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410881', '4108', '0,1,41,4108,410881,', '济源市', '11770', '410881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410882', '4108', '0,1,41,4108,410882,', '沁阳市', '11771', '410882', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410883', '4108', '0,1,41,4108,410883,', '孟州市', '11772', '410883', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4109', '41', '0,1,41,4109,', '濮阳', '11773', '4109', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410901', '4109', '0,1,41,4109,410901,', '市辖区', '11774', '410901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410902', '4109', '0,1,41,4109,410902,', '市  区', '11775', '410902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410922', '4109', '0,1,41,4109,410922,', '清丰县', '11776', '410922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410923', '4109', '0,1,41,4109,410923,', '南乐县', '11777', '410923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410926', '4109', '0,1,41,4109,410926,', '范  县', '11778', '410926', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410927', '4109', '0,1,41,4109,410927,', '台前县', '11779', '410927', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('410928', '4109', '0,1,41,4109,410928,', '濮阳县', '11780', '410928', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4110', '41', '0,1,41,4110,', '许昌', '11781', '4110', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411001', '4110', '0,1,41,4110,411001,', '市辖区', '11782', '411001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411002', '4110', '0,1,41,4110,411002,', '魏都区', '11783', '411002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411023', '4110', '0,1,41,4110,411023,', '许昌县', '11784', '411023', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411024', '4110', '0,1,41,4110,411024,', '鄢陵县', '11785', '411024', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411025', '4110', '0,1,41,4110,411025,', '襄城县', '11786', '411025', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411081', '4110', '0,1,41,4110,411081,', '禹州市', '11787', '411081', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411082', '4110', '0,1,41,4110,411082,', '长葛市', '11788', '411082', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4111', '41', '0,1,41,4111,', '漯河', '11789', '4111', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411101', '4111', '0,1,41,4111,411101,', '市辖区', '11790', '411101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411102', '4111', '0,1,41,4111,411102,', '源汇区', '11791', '411102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411121', '4111', '0,1,41,4111,411121,', '舞阳县', '11792', '411121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411122', '4111', '0,1,41,4111,411122,', '临颍县', '11793', '411122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411123', '4111', '0,1,41,4111,411123,', '郾城县', '11794', '411123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4112', '41', '0,1,41,4112,', '三门峡', '11795', '4112', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411201', '4112', '0,1,41,4112,411201,', '市辖区', '11796', '411201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411202', '4112', '0,1,41,4112,411202,', '湖滨区', '11797', '411202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411221', '4112', '0,1,41,4112,411221,', '渑池县', '11798', '411221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411222', '4112', '0,1,41,4112,411222,', '陕  县', '11799', '411222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411224', '4112', '0,1,41,4112,411224,', '卢氏县', '11800', '411224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411281', '4112', '0,1,41,4112,411281,', '义马市', '11801', '411281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411282', '4112', '0,1,41,4112,411282,', '灵宝市', '11802', '411282', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4113', '41', '0,1,41,4113,', '南阳', '11803', '4113', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411301', '4113', '0,1,41,4113,411301,', '市辖区', '11804', '411301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411302', '4113', '0,1,41,4113,411302,', '宛城区', '11805', '411302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411303', '4113', '0,1,41,4113,411303,', '卧龙区', '11806', '411303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411321', '4113', '0,1,41,4113,411321,', '南召县', '11807', '411321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411322', '4113', '0,1,41,4113,411322,', '方城县', '11808', '411322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411323', '4113', '0,1,41,4113,411323,', '西峡县', '11809', '411323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411324', '4113', '0,1,41,4113,411324,', '镇平县', '11810', '411324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411325', '4113', '0,1,41,4113,411325,', '内乡县', '11811', '411325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411326', '4113', '0,1,41,4113,411326,', '淅川县', '11812', '411326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411327', '4113', '0,1,41,4113,411327,', '社旗县', '11813', '411327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411328', '4113', '0,1,41,4113,411328,', '唐河县', '11814', '411328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411329', '4113', '0,1,41,4113,411329,', '新野县', '11815', '411329', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411330', '4113', '0,1,41,4113,411330,', '桐柏县', '11816', '411330', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411381', '4113', '0,1,41,4113,411381,', '邓州市', '11817', '411381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4114', '41', '0,1,41,4114,', '商丘', '11818', '4114', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411401', '4114', '0,1,41,4114,411401,', '市辖区', '11819', '411401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411402', '4114', '0,1,41,4114,411402,', '梁园区', '11820', '411402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411403', '4114', '0,1,41,4114,411403,', '睢阳区', '11821', '411403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411421', '4114', '0,1,41,4114,411421,', '民权县', '11822', '411421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411422', '4114', '0,1,41,4114,411422,', '睢  县', '11823', '411422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411423', '4114', '0,1,41,4114,411423,', '宁陵县', '11824', '411423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411424', '4114', '0,1,41,4114,411424,', '柘城县', '11825', '411424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411425', '4114', '0,1,41,4114,411425,', '虞城县', '11826', '411425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411426', '4114', '0,1,41,4114,411426,', '夏邑县', '11827', '411426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411481', '4114', '0,1,41,4114,411481,', '永城市', '11828', '411481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4115', '41', '0,1,41,4115,', '信阳', '11829', '4115', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411501', '4115', '0,1,41,4115,411501,', '市辖区', '11830', '411501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411502', '4115', '0,1,41,4115,411502,', '师河区', '11831', '411502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411503', '4115', '0,1,41,4115,411503,', '平桥区', '11832', '411503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411521', '4115', '0,1,41,4115,411521,', '罗山县', '11833', '411521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411522', '4115', '0,1,41,4115,411522,', '光山县', '11834', '411522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411523', '4115', '0,1,41,4115,411523,', '新  县', '11835', '411523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411524', '4115', '0,1,41,4115,411524,', '商城县', '11836', '411524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411525', '4115', '0,1,41,4115,411525,', '固始县', '11837', '411525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411526', '4115', '0,1,41,4115,411526,', '潢川县', '11838', '411526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411527', '4115', '0,1,41,4115,411527,', '淮滨县', '11839', '411527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411528', '4115', '0,1,41,4115,411528,', '息  县', '11840', '411528', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4116', '41', '0,1,41,4116,', '周口', '11841', '4116', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411601', '4116', '0,1,41,4116,411601,', '市辖区', '11842', '411601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411602', '4116', '0,1,41,4116,411602,', '川汇区', '11843', '411602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411621', '4116', '0,1,41,4116,411621,', '扶沟县', '11844', '411621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411622', '4116', '0,1,41,4116,411622,', '西华县', '11845', '411622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411623', '4116', '0,1,41,4116,411623,', '商水县', '11846', '411623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411624', '4116', '0,1,41,4116,411624,', '沈丘县', '11847', '411624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411625', '4116', '0,1,41,4116,411625,', '郸城县', '11848', '411625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411626', '4116', '0,1,41,4116,411626,', '淮阳县', '11849', '411626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411627', '4116', '0,1,41,4116,411627,', '太康县', '11850', '411627', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411628', '4116', '0,1,41,4116,411628,', '鹿邑县', '11851', '411628', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411681', '4116', '0,1,41,4116,411681,', '项城市', '11852', '411681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4117', '41', '0,1,41,4117,', '驻马店', '11853', '4117', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411701', '4117', '0,1,41,4117,411701,', '市辖区', '11854', '411701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411702', '4117', '0,1,41,4117,411702,', '驿城区', '11855', '411702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411721', '4117', '0,1,41,4117,411721,', '西平县', '11856', '411721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411722', '4117', '0,1,41,4117,411722,', '上蔡县', '11857', '411722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411723', '4117', '0,1,41,4117,411723,', '平舆县', '11858', '411723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411724', '4117', '0,1,41,4117,411724,', '正阳县', '11859', '411724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411725', '4117', '0,1,41,4117,411725,', '确山县', '11860', '411725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411726', '4117', '0,1,41,4117,411726,', '泌阳县', '11861', '411726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411727', '4117', '0,1,41,4117,411727,', '汝南县', '11862', '411727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411728', '4117', '0,1,41,4117,411728,', '遂平县', '11863', '411728', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('411729', '4117', '0,1,41,4117,411729,', '新蔡县', '11864', '411729', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('42', '1', '0,1,42,', '湖北', '11865', '42', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4201', '42', '0,1,42,4201,', '武汉', '11866', '4201', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420101', '4201', '0,1,42,4201,420101,', '市辖区', '11867', '420101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420102', '4201', '0,1,42,4201,420102,', '江岸区', '11868', '420102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420103', '4201', '0,1,42,4201,420103,', '江汉区', '11869', '420103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420104', '4201', '0,1,42,4201,420104,', '乔口区', '11870', '420104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420105', '4201', '0,1,42,4201,420105,', '汉阳区', '11871', '420105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420106', '4201', '0,1,42,4201,420106,', '武昌区', '11872', '420106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420107', '4201', '0,1,42,4201,420107,', '青山区', '11873', '420107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420111', '4201', '0,1,42,4201,420111,', '洪山区', '11874', '420111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420112', '4201', '0,1,42,4201,420112,', '东西湖区', '11875', '420112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420113', '4201', '0,1,42,4201,420113,', '汉南区', '11876', '420113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420114', '4201', '0,1,42,4201,420114,', '蔡甸区', '11877', '420114', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420115', '4201', '0,1,42,4201,420115,', '江夏区', '11878', '420115', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420116', '4201', '0,1,42,4201,420116,', '黄陂区', '11879', '420116', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420117', '4201', '0,1,42,4201,420117,', '新洲区', '11880', '420117', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4202', '42', '0,1,42,4202,', '黄石', '11881', '4202', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420201', '4202', '0,1,42,4202,420201,', '市辖区', '11882', '420201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420202', '4202', '0,1,42,4202,420202,', '黄石港区', '11883', '420202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420203', '4202', '0,1,42,4202,420203,', '石灰窑区', '11884', '420203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420204', '4202', '0,1,42,4202,420204,', '下陆区', '11885', '420204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420205', '4202', '0,1,42,4202,420205,', '铁山区', '11886', '420205', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420222', '4202', '0,1,42,4202,420222,', '阳新县', '11887', '420222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420281', '4202', '0,1,42,4202,420281,', '大冶市', '11888', '420281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4203', '42', '0,1,42,4203,', '十堰', '11889', '4203', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420301', '4203', '0,1,42,4203,420301,', '市辖区', '11890', '420301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420302', '4203', '0,1,42,4203,420302,', '茅箭区', '11891', '420302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420303', '4203', '0,1,42,4203,420303,', '张湾区', '11892', '420303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420321', '4203', '0,1,42,4203,420321,', '郧  县', '11893', '420321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420322', '4203', '0,1,42,4203,420322,', '郧西县', '11894', '420322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420323', '4203', '0,1,42,4203,420323,', '竹山县', '11895', '420323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420324', '4203', '0,1,42,4203,420324,', '竹溪县', '11896', '420324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420325', '4203', '0,1,42,4203,420325,', '房  县', '11897', '420325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420381', '4203', '0,1,42,4203,420381,', '丹江口市', '11898', '420381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4205', '42', '0,1,42,4205,', '宜昌', '11899', '4205', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420501', '4205', '0,1,42,4205,420501,', '市辖区', '11900', '420501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420502', '4205', '0,1,42,4205,420502,', '西陵区', '11901', '420502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420503', '4205', '0,1,42,4205,420503,', '伍家岗区', '11902', '420503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420504', '4205', '0,1,42,4205,420504,', '点军区', '11903', '420504', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420505', '4205', '0,1,42,4205,420505,', '虎亭区', '11904', '420505', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420521', '4205', '0,1,42,4205,420521,', '宜昌县', '11905', '420521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420525', '4205', '0,1,42,4205,420525,', '远安县', '11906', '420525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420526', '4205', '0,1,42,4205,420526,', '兴山县', '11907', '420526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420527', '4205', '0,1,42,4205,420527,', '秭归县', '11908', '420527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420528', '4205', '0,1,42,4205,420528,', '长阳土家族自治县', '11909', '420528', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420529', '4205', '0,1,42,4205,420529,', '五峰土家族自治县', '11910', '420529', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420581', '4205', '0,1,42,4205,420581,', '宜都市', '11911', '420581', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420582', '4205', '0,1,42,4205,420582,', '当阳市', '11912', '420582', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420583', '4205', '0,1,42,4205,420583,', '枝江市', '11913', '420583', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4206', '42', '0,1,42,4206,', '襄樊', '11914', '4206', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420601', '4206', '0,1,42,4206,420601,', '市辖区', '11915', '420601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420602', '4206', '0,1,42,4206,420602,', '襄城区', '11916', '420602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420606', '4206', '0,1,42,4206,420606,', '樊城区', '11917', '420606', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420621', '4206', '0,1,42,4206,420621,', '襄阳县', '11918', '420621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420624', '4206', '0,1,42,4206,420624,', '南漳县', '11919', '420624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420625', '4206', '0,1,42,4206,420625,', '谷城县', '11920', '420625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420626', '4206', '0,1,42,4206,420626,', '保康县', '11921', '420626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420682', '4206', '0,1,42,4206,420682,', '老河口市', '11922', '420682', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420683', '4206', '0,1,42,4206,420683,', '枣阳市', '11923', '420683', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420684', '4206', '0,1,42,4206,420684,', '宜城市', '11924', '420684', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4207', '42', '0,1,42,4207,', '鄂州', '11925', '4207', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420701', '4207', '0,1,42,4207,420701,', '市辖区', '11926', '420701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420702', '4207', '0,1,42,4207,420702,', '梁子湖区', '11927', '420702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420703', '4207', '0,1,42,4207,420703,', '华容区', '11928', '420703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420704', '4207', '0,1,42,4207,420704,', '鄂城区', '11929', '420704', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4208', '42', '0,1,42,4208,', '荆门', '11930', '4208', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420801', '4208', '0,1,42,4208,420801,', '市辖区', '11931', '420801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420802', '4208', '0,1,42,4208,420802,', '东宝区', '11932', '420802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420821', '4208', '0,1,42,4208,420821,', '京山县', '11933', '420821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420822', '4208', '0,1,42,4208,420822,', '沙洋县', '11934', '420822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420881', '4208', '0,1,42,4208,420881,', '钟祥市', '11935', '420881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4209', '42', '0,1,42,4209,', '孝感', '11936', '4209', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420901', '4209', '0,1,42,4209,420901,', '市辖区', '11937', '420901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420902', '4209', '0,1,42,4209,420902,', '孝南区', '11938', '420902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420921', '4209', '0,1,42,4209,420921,', '孝昌县', '11939', '420921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420922', '4209', '0,1,42,4209,420922,', '大悟县', '11940', '420922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420923', '4209', '0,1,42,4209,420923,', '云梦县', '11941', '420923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420981', '4209', '0,1,42,4209,420981,', '应城市', '11942', '420981', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420982', '4209', '0,1,42,4209,420982,', '安陆市', '11943', '420982', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('420984', '4209', '0,1,42,4209,420984,', '汉川市', '11944', '420984', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4210', '42', '0,1,42,4210,', '荆州', '11945', '4210', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421001', '4210', '0,1,42,4210,421001,', '市辖区', '11946', '421001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421002', '4210', '0,1,42,4210,421002,', '沙市区', '11947', '421002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421003', '4210', '0,1,42,4210,421003,', '荆州区', '11948', '421003', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421022', '4210', '0,1,42,4210,421022,', '公安县', '11949', '421022', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421023', '4210', '0,1,42,4210,421023,', '监利县', '11950', '421023', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421024', '4210', '0,1,42,4210,421024,', '江陵县', '11951', '421024', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421081', '4210', '0,1,42,4210,421081,', '石首市', '11952', '421081', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421083', '4210', '0,1,42,4210,421083,', '洪湖市', '11953', '421083', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421087', '4210', '0,1,42,4210,421087,', '松滋市', '11954', '421087', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4211', '42', '0,1,42,4211,', '黄冈', '11955', '4211', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421101', '4211', '0,1,42,4211,421101,', '市辖区', '11956', '421101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421102', '4211', '0,1,42,4211,421102,', '黄州区', '11957', '421102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421121', '4211', '0,1,42,4211,421121,', '团风县', '11958', '421121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421122', '4211', '0,1,42,4211,421122,', '红安县', '11959', '421122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421123', '4211', '0,1,42,4211,421123,', '罗田县', '11960', '421123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421124', '4211', '0,1,42,4211,421124,', '英山县', '11961', '421124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421125', '4211', '0,1,42,4211,421125,', '浠水县', '11962', '421125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421126', '4211', '0,1,42,4211,421126,', '蕲春县', '11963', '421126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421127', '4211', '0,1,42,4211,421127,', '黄梅县', '11964', '421127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421181', '4211', '0,1,42,4211,421181,', '麻城市', '11965', '421181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421182', '4211', '0,1,42,4211,421182,', '武穴市', '11966', '421182', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4212', '42', '0,1,42,4212,', '咸宁', '11967', '4212', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421201', '4212', '0,1,42,4212,421201,', '市辖区', '11968', '421201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421202', '4212', '0,1,42,4212,421202,', '咸安区', '11969', '421202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421221', '4212', '0,1,42,4212,421221,', '嘉鱼县', '11970', '421221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421222', '4212', '0,1,42,4212,421222,', '通城县', '11971', '421222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421223', '4212', '0,1,42,4212,421223,', '崇阳县', '11972', '421223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421224', '4212', '0,1,42,4212,421224,', '通山县', '11973', '421224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421281', '4212', '0,1,42,4212,421281,', '赤壁市', '11974', '421281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4213', '42', '0,1,42,4213,', '随州', '11975', '4213', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421301', '4213', '0,1,42,4213,421301,', '市辖区', '11976', '421301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421302', '4213', '0,1,42,4213,421302,', '曾都区', '11977', '421302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('421381', '4213', '0,1,42,4213,421381,', '广水市', '11978', '421381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4228', '42', '0,1,42,4228,', '恩施土家族苗族自治州', '11979', '4228', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('422801', '4228', '0,1,42,4228,422801,', '恩施市', '11980', '422801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('422802', '4228', '0,1,42,4228,422802,', '利川市', '11981', '422802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('422822', '4228', '0,1,42,4228,422822,', '建始县', '11982', '422822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('422823', '4228', '0,1,42,4228,422823,', '巴东县', '11983', '422823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('422825', '4228', '0,1,42,4228,422825,', '宣恩县', '11984', '422825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('422826', '4228', '0,1,42,4228,422826,', '咸丰县', '11985', '422826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('422827', '4228', '0,1,42,4228,422827,', '来凤县', '11986', '422827', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('422828', '4228', '0,1,42,4228,422828,', '鹤峰县', '11987', '422828', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4290', '42', '0,1,42,4290,', '省直辖行政单位', '11988', '4290', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('429004', '4290', '0,1,42,4290,429004,', '仙桃市', '11989', '429004', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('429005', '4290', '0,1,42,4290,429005,', '潜江市', '11990', '429005', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('429006', '4290', '0,1,42,4290,429006,', '天门市', '11991', '429006', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('429021', '4290', '0,1,42,4290,429021,', '神农架林区', '11992', '429021', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('43', '1', '0,1,43,', '湖南', '11993', '43', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4301', '43', '0,1,43,4301,', '长沙', '11994', '4301', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430101', '4301', '0,1,43,4301,430101,', '市辖区', '11995', '430101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430102', '4301', '0,1,43,4301,430102,', '芙蓉区', '11996', '430102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430103', '4301', '0,1,43,4301,430103,', '天心区', '11997', '430103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430104', '4301', '0,1,43,4301,430104,', '岳麓区', '11998', '430104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430105', '4301', '0,1,43,4301,430105,', '开福区', '11999', '430105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430111', '4301', '0,1,43,4301,430111,', '雨花区', '12000', '430111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430121', '4301', '0,1,43,4301,430121,', '长沙县', '12001', '430121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430122', '4301', '0,1,43,4301,430122,', '望城县', '12002', '430122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430124', '4301', '0,1,43,4301,430124,', '宁乡县', '12003', '430124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430181', '4301', '0,1,43,4301,430181,', '浏阳市', '12004', '430181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4302', '43', '0,1,43,4302,', '株洲', '12005', '4302', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430201', '4302', '0,1,43,4302,430201,', '市辖区', '12006', '430201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430202', '4302', '0,1,43,4302,430202,', '荷塘区', '12007', '430202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430203', '4302', '0,1,43,4302,430203,', '芦淞区', '12008', '430203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430204', '4302', '0,1,43,4302,430204,', '石峰区', '12009', '430204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430211', '4302', '0,1,43,4302,430211,', '天元区', '12010', '430211', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430221', '4302', '0,1,43,4302,430221,', '株洲县', '12011', '430221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430223', '4302', '0,1,43,4302,430223,', '攸  县', '12012', '430223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430224', '4302', '0,1,43,4302,430224,', '茶陵县', '12013', '430224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430225', '4302', '0,1,43,4302,430225,', '炎陵县', '12014', '430225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430281', '4302', '0,1,43,4302,430281,', '醴陵市', '12015', '430281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4303', '43', '0,1,43,4303,', '湘潭', '12016', '4303', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430301', '4303', '0,1,43,4303,430301,', '市辖区', '12017', '430301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430302', '4303', '0,1,43,4303,430302,', '雨湖区', '12018', '430302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430304', '4303', '0,1,43,4303,430304,', '岳塘区', '12019', '430304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430321', '4303', '0,1,43,4303,430321,', '湘潭县', '12020', '430321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430381', '4303', '0,1,43,4303,430381,', '湘乡市', '12021', '430381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430382', '4303', '0,1,43,4303,430382,', '韶山市', '12022', '430382', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4304', '43', '0,1,43,4304,', '衡阳', '12023', '4304', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430401', '4304', '0,1,43,4304,430401,', '市辖区', '12024', '430401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430402', '4304', '0,1,43,4304,430402,', '江东区', '12025', '430402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430403', '4304', '0,1,43,4304,430403,', '城南区', '12026', '430403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430404', '4304', '0,1,43,4304,430404,', '城北区', '12027', '430404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430411', '4304', '0,1,43,4304,430411,', '郊   区', '12028', '430411', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430412', '4304', '0,1,43,4304,430412,', '南岳区', '12029', '430412', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430421', '4304', '0,1,43,4304,430421,', '衡阳县', '12030', '430421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430422', '4304', '0,1,43,4304,430422,', '衡南县', '12031', '430422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430423', '4304', '0,1,43,4304,430423,', '衡山县', '12032', '430423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430424', '4304', '0,1,43,4304,430424,', '衡东县', '12033', '430424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430426', '4304', '0,1,43,4304,430426,', '祁东县', '12034', '430426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430481', '4304', '0,1,43,4304,430481,', '耒阳市', '12035', '430481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430482', '4304', '0,1,43,4304,430482,', '常宁市', '12036', '430482', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4305', '43', '0,1,43,4305,', '邵阳', '12037', '4305', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430501', '4305', '0,1,43,4305,430501,', '市辖区', '12038', '430501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430502', '4305', '0,1,43,4305,430502,', '双清区', '12039', '430502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430503', '4305', '0,1,43,4305,430503,', '大祥区', '12040', '430503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430511', '4305', '0,1,43,4305,430511,', '北塔区', '12041', '430511', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430521', '4305', '0,1,43,4305,430521,', '邵东县', '12042', '430521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430522', '4305', '0,1,43,4305,430522,', '新邵县', '12043', '430522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430523', '4305', '0,1,43,4305,430523,', '邵阳县', '12044', '430523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430524', '4305', '0,1,43,4305,430524,', '隆回县', '12045', '430524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430525', '4305', '0,1,43,4305,430525,', '洞口县', '12046', '430525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430527', '4305', '0,1,43,4305,430527,', '绥宁县', '12047', '430527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430528', '4305', '0,1,43,4305,430528,', '新宁县', '12048', '430528', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430529', '4305', '0,1,43,4305,430529,', '城步苗族自治县', '12049', '430529', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430581', '4305', '0,1,43,4305,430581,', '武冈市', '12050', '430581', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4306', '43', '0,1,43,4306,', '岳阳', '12051', '4306', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430601', '4306', '0,1,43,4306,430601,', '市辖区', '12052', '430601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430602', '4306', '0,1,43,4306,430602,', '岳阳楼区', '12053', '430602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430603', '4306', '0,1,43,4306,430603,', '云溪区', '12054', '430603', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430611', '4306', '0,1,43,4306,430611,', '君山区', '12055', '430611', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430621', '4306', '0,1,43,4306,430621,', '岳阳县', '12056', '430621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430623', '4306', '0,1,43,4306,430623,', '华容县', '12057', '430623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430624', '4306', '0,1,43,4306,430624,', '湘阴县', '12058', '430624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430626', '4306', '0,1,43,4306,430626,', '平江县', '12059', '430626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430681', '4306', '0,1,43,4306,430681,', '汨罗市', '12060', '430681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430682', '4306', '0,1,43,4306,430682,', '临湘市', '12061', '430682', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4307', '43', '0,1,43,4307,', '常德', '12062', '4307', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430701', '4307', '0,1,43,4307,430701,', '市辖区', '12063', '430701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430702', '4307', '0,1,43,4307,430702,', '武陵区', '12064', '430702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430703', '4307', '0,1,43,4307,430703,', '鼎城区', '12065', '430703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430721', '4307', '0,1,43,4307,430721,', '安乡县', '12066', '430721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430722', '4307', '0,1,43,4307,430722,', '汉寿县', '12067', '430722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430723', '4307', '0,1,43,4307,430723,', '澧  县', '12068', '430723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430724', '4307', '0,1,43,4307,430724,', '临澧县', '12069', '430724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430725', '4307', '0,1,43,4307,430725,', '桃源县', '12070', '430725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430726', '4307', '0,1,43,4307,430726,', '石门县', '12071', '430726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430781', '4307', '0,1,43,4307,430781,', '津市市', '12072', '430781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4308', '43', '0,1,43,4308,', '张家界', '12073', '4308', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430801', '4308', '0,1,43,4308,430801,', '市辖区', '12074', '430801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430802', '4308', '0,1,43,4308,430802,', '永定区', '12075', '430802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430811', '4308', '0,1,43,4308,430811,', '武陵源区', '12076', '430811', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430821', '4308', '0,1,43,4308,430821,', '慈利县', '12077', '430821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430822', '4308', '0,1,43,4308,430822,', '桑植县', '12078', '430822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4309', '43', '0,1,43,4309,', '益阳', '12079', '4309', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430901', '4309', '0,1,43,4309,430901,', '市辖区', '12080', '430901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430902', '4309', '0,1,43,4309,430902,', '资阳区', '12081', '430902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430903', '4309', '0,1,43,4309,430903,', '赫山区', '12082', '430903', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430921', '4309', '0,1,43,4309,430921,', '南  县', '12083', '430921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430922', '4309', '0,1,43,4309,430922,', '桃江县', '12084', '430922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430923', '4309', '0,1,43,4309,430923,', '安化县', '12085', '430923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('430981', '4309', '0,1,43,4309,430981,', '沅江市', '12086', '430981', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4310', '43', '0,1,43,4310,', '郴州', '12087', '4310', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431001', '4310', '0,1,43,4310,431001,', '市辖区', '12088', '431001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431002', '4310', '0,1,43,4310,431002,', '北湖区', '12089', '431002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431003', '4310', '0,1,43,4310,431003,', '苏仙区', '12090', '431003', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431021', '4310', '0,1,43,4310,431021,', '桂阳县', '12091', '431021', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431022', '4310', '0,1,43,4310,431022,', '宜章县', '12092', '431022', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431023', '4310', '0,1,43,4310,431023,', '永兴县', '12093', '431023', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431024', '4310', '0,1,43,4310,431024,', '嘉禾县', '12094', '431024', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431025', '4310', '0,1,43,4310,431025,', '临武县', '12095', '431025', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431026', '4310', '0,1,43,4310,431026,', '汝城县', '12096', '431026', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431027', '4310', '0,1,43,4310,431027,', '桂东县', '12097', '431027', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431028', '4310', '0,1,43,4310,431028,', '安仁县', '12098', '431028', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431081', '4310', '0,1,43,4310,431081,', '资兴市', '12099', '431081', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4311', '43', '0,1,43,4311,', '永州', '12100', '4311', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431101', '4311', '0,1,43,4311,431101,', '市辖区', '12101', '431101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431102', '4311', '0,1,43,4311,431102,', '芝山区', '12102', '431102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431103', '4311', '0,1,43,4311,431103,', '冷水滩区', '12103', '431103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431121', '4311', '0,1,43,4311,431121,', '祁阳县', '12104', '431121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431122', '4311', '0,1,43,4311,431122,', '东安县', '12105', '431122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431123', '4311', '0,1,43,4311,431123,', '双牌县', '12106', '431123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431124', '4311', '0,1,43,4311,431124,', '道  县', '12107', '431124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431125', '4311', '0,1,43,4311,431125,', '江永县', '12108', '431125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431126', '4311', '0,1,43,4311,431126,', '宁远县', '12109', '431126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431127', '4311', '0,1,43,4311,431127,', '蓝山县', '12110', '431127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431128', '4311', '0,1,43,4311,431128,', '新田县', '12111', '431128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431129', '4311', '0,1,43,4311,431129,', '江华瑶族自治县', '12112', '431129', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4312', '43', '0,1,43,4312,', '怀化', '12113', '4312', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431201', '4312', '0,1,43,4312,431201,', '市辖区', '12114', '431201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431202', '4312', '0,1,43,4312,431202,', '鹤城区', '12115', '431202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431221', '4312', '0,1,43,4312,431221,', '中方县', '12116', '431221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431222', '4312', '0,1,43,4312,431222,', '沅陵县', '12117', '431222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431223', '4312', '0,1,43,4312,431223,', '辰溪县', '12118', '431223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431224', '4312', '0,1,43,4312,431224,', '溆浦县', '12119', '431224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431225', '4312', '0,1,43,4312,431225,', '会同县', '12120', '431225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431226', '4312', '0,1,43,4312,431226,', '麻阳苗族自治县', '12121', '431226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431227', '4312', '0,1,43,4312,431227,', '新晃侗族自治县', '12122', '431227', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431228', '4312', '0,1,43,4312,431228,', '芷江侗族自治县', '12123', '431228', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431229', '4312', '0,1,43,4312,431229,', '靖州苗族侗族自治县', '12124', '431229', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431230', '4312', '0,1,43,4312,431230,', '通道侗族自治县', '12125', '431230', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431281', '4312', '0,1,43,4312,431281,', '洪江市', '12126', '431281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4313', '43', '0,1,43,4313,', '娄底', '12127', '4313', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431301', '4313', '0,1,43,4313,431301,', '市辖区', '12128', '431301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431302', '4313', '0,1,43,4313,431302,', '娄星区', '12129', '431302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431321', '4313', '0,1,43,4313,431321,', '双峰县', '12130', '431321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431322', '4313', '0,1,43,4313,431322,', '新化县', '12131', '431322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431381', '4313', '0,1,43,4313,431381,', '冷水江市', '12132', '431381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('431382', '4313', '0,1,43,4313,431382,', '涟源市', '12133', '431382', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4331', '43', '0,1,43,4331,', '湘西土家族苗族自治州', '12134', '4331', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('433101', '4331', '0,1,43,4331,433101,', '吉首市', '12135', '433101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('433122', '4331', '0,1,43,4331,433122,', '泸溪县', '12136', '433122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('433123', '4331', '0,1,43,4331,433123,', '凤凰县', '12137', '433123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('433124', '4331', '0,1,43,4331,433124,', '花垣县', '12138', '433124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('433125', '4331', '0,1,43,4331,433125,', '保靖县', '12139', '433125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('433126', '4331', '0,1,43,4331,433126,', '古丈县', '12140', '433126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('433127', '4331', '0,1,43,4331,433127,', '永顺县', '12141', '433127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('433130', '4331', '0,1,43,4331,433130,', '龙山县', '12142', '433130', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('44', '1', '0,1,44,', '广东', '12143', '44', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4401', '44', '0,1,44,4401,', '广州', '12144', '4401', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440101', '4401', '0,1,44,4401,440101,', '市辖区', '12145', '440101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440102', '4401', '0,1,44,4401,440102,', '东山区', '12146', '440102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440103', '4401', '0,1,44,4401,440103,', '荔湾区', '12147', '440103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440104', '4401', '0,1,44,4401,440104,', '越秀区', '12148', '440104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440105', '4401', '0,1,44,4401,440105,', '海珠区', '12149', '440105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440106', '4401', '0,1,44,4401,440106,', '天河区', '12150', '440106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440107', '4401', '0,1,44,4401,440107,', '芳村区', '12151', '440107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440111', '4401', '0,1,44,4401,440111,', '白云区', '12152', '440111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440112', '4401', '0,1,44,4401,440112,', '黄埔区', '12153', '440112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440113', '4401', '0,1,44,4401,440113,', '番禺区', '12154', '440113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440114', '4401', '0,1,44,4401,440114,', '花都区', '12155', '440114', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440183', '4401', '0,1,44,4401,440183,', '增城市', '12156', '440183', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440184', '4401', '0,1,44,4401,440184,', '从化市', '12157', '440184', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4402', '44', '0,1,44,4402,', '韶关', '12158', '4402', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440201', '4402', '0,1,44,4402,440201,', '市辖区', '12159', '440201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440202', '4402', '0,1,44,4402,440202,', '北江区', '12160', '440202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440203', '4402', '0,1,44,4402,440203,', '武江区', '12161', '440203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440204', '4402', '0,1,44,4402,440204,', '浈江区', '12162', '440204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440221', '4402', '0,1,44,4402,440221,', '曲江县', '12163', '440221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440222', '4402', '0,1,44,4402,440222,', '始兴县', '12164', '440222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440224', '4402', '0,1,44,4402,440224,', '仁化县', '12165', '440224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440229', '4402', '0,1,44,4402,440229,', '翁源县', '12166', '440229', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440232', '4402', '0,1,44,4402,440232,', '乳源瑶族自治县', '12167', '440232', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440233', '4402', '0,1,44,4402,440233,', '新丰县', '12168', '440233', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440281', '4402', '0,1,44,4402,440281,', '乐昌市', '12169', '440281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440282', '4402', '0,1,44,4402,440282,', '南雄市', '12170', '440282', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4403', '44', '0,1,44,4403,', '深圳', '12171', '4403', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440301', '4403', '0,1,44,4403,440301,', '市辖区', '12172', '440301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440303', '4403', '0,1,44,4403,440303,', '罗湖区', '12173', '440303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440304', '4403', '0,1,44,4403,440304,', '福田区', '12174', '440304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440305', '4403', '0,1,44,4403,440305,', '南山区', '12175', '440305', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440306', '4403', '0,1,44,4403,440306,', '宝安区', '12176', '440306', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440307', '4403', '0,1,44,4403,440307,', '龙岗区', '12177', '440307', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440308', '4403', '0,1,44,4403,440308,', '盐田区', '12178', '440308', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4404', '44', '0,1,44,4404,', '珠海', '12179', '4404', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440401', '4404', '0,1,44,4404,440401,', '市辖区', '12180', '440401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440402', '4404', '0,1,44,4404,440402,', '香洲区', '12181', '440402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440421', '4404', '0,1,44,4404,440421,', '斗门县', '12182', '440421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4405', '44', '0,1,44,4405,', '汕头', '12183', '4405', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440501', '4405', '0,1,44,4405,440501,', '市辖区', '12184', '440501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440506', '4405', '0,1,44,4405,440506,', '达濠区', '12185', '440506', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440507', '4405', '0,1,44,4405,440507,', '龙湖区', '12186', '440507', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440508', '4405', '0,1,44,4405,440508,', '金园区', '12187', '440508', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440509', '4405', '0,1,44,4405,440509,', '升平区', '12188', '440509', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440510', '4405', '0,1,44,4405,440510,', '河浦区', '12189', '440510', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440523', '4405', '0,1,44,4405,440523,', '南澳县', '12190', '440523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440582', '4405', '0,1,44,4405,440582,', '潮阳市', '12191', '440582', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440583', '4405', '0,1,44,4405,440583,', '澄海市', '12192', '440583', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4406', '44', '0,1,44,4406,', '佛山', '12193', '4406', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440601', '4406', '0,1,44,4406,440601,', '市辖区', '12194', '440601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440602', '4406', '0,1,44,4406,440602,', '城  区', '12195', '440602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440603', '4406', '0,1,44,4406,440603,', '石湾区', '12196', '440603', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440681', '4406', '0,1,44,4406,440681,', '顺德市', '12197', '440681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440682', '4406', '0,1,44,4406,440682,', '南海市', '12198', '440682', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440683', '4406', '0,1,44,4406,440683,', '三水市', '12199', '440683', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440684', '4406', '0,1,44,4406,440684,', '高明市', '12200', '440684', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4407', '44', '0,1,44,4407,', '江门', '12201', '4407', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440701', '4407', '0,1,44,4407,440701,', '市辖区', '12202', '440701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440703', '4407', '0,1,44,4407,440703,', '蓬江区', '12203', '440703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440704', '4407', '0,1,44,4407,440704,', '江海区', '12204', '440704', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440781', '4407', '0,1,44,4407,440781,', '台山市', '12205', '440781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440782', '4407', '0,1,44,4407,440782,', '新会市', '12206', '440782', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440783', '4407', '0,1,44,4407,440783,', '开平市', '12207', '440783', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440784', '4407', '0,1,44,4407,440784,', '鹤山市', '12208', '440784', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440785', '4407', '0,1,44,4407,440785,', '恩平市', '12209', '440785', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4408', '44', '0,1,44,4408,', '湛江', '12210', '4408', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440801', '4408', '0,1,44,4408,440801,', '市辖区', '12211', '440801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440802', '4408', '0,1,44,4408,440802,', '赤坎区', '12212', '440802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440803', '4408', '0,1,44,4408,440803,', '霞山区', '12213', '440803', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440804', '4408', '0,1,44,4408,440804,', '坡头区', '12214', '440804', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440811', '4408', '0,1,44,4408,440811,', '麻章区', '12215', '440811', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440823', '4408', '0,1,44,4408,440823,', '遂溪县', '12216', '440823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440825', '4408', '0,1,44,4408,440825,', '徐闻县', '12217', '440825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440881', '4408', '0,1,44,4408,440881,', '廉江市', '12218', '440881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440882', '4408', '0,1,44,4408,440882,', '雷州市', '12219', '440882', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440883', '4408', '0,1,44,4408,440883,', '吴川市', '12220', '440883', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4409', '44', '0,1,44,4409,', '茂名', '12221', '4409', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440901', '4409', '0,1,44,4409,440901,', '市辖区', '12222', '440901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440902', '4409', '0,1,44,4409,440902,', '茂南区', '12223', '440902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440923', '4409', '0,1,44,4409,440923,', '电白县', '12224', '440923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440981', '4409', '0,1,44,4409,440981,', '高州市', '12225', '440981', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440982', '4409', '0,1,44,4409,440982,', '化州市', '12226', '440982', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('440983', '4409', '0,1,44,4409,440983,', '信宜市', '12227', '440983', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4412', '44', '0,1,44,4412,', '肇庆', '12228', '4412', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441201', '4412', '0,1,44,4412,441201,', '市辖区', '12229', '441201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441202', '4412', '0,1,44,4412,441202,', '端州区', '12230', '441202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441203', '4412', '0,1,44,4412,441203,', '鼎湖区', '12231', '441203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441223', '4412', '0,1,44,4412,441223,', '广宁县', '12232', '441223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441224', '4412', '0,1,44,4412,441224,', '怀集县', '12233', '441224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441225', '4412', '0,1,44,4412,441225,', '封开县', '12234', '441225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441226', '4412', '0,1,44,4412,441226,', '德庆县', '12235', '441226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441283', '4412', '0,1,44,4412,441283,', '高要市', '12236', '441283', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441284', '4412', '0,1,44,4412,441284,', '四会市', '12237', '441284', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4413', '44', '0,1,44,4413,', '惠州', '12238', '4413', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441301', '4413', '0,1,44,4413,441301,', '市辖区', '12239', '441301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441302', '4413', '0,1,44,4413,441302,', '惠城区', '12240', '441302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441322', '4413', '0,1,44,4413,441322,', '博罗县', '12241', '441322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441323', '4413', '0,1,44,4413,441323,', '惠东县', '12242', '441323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441324', '4413', '0,1,44,4413,441324,', '龙门县', '12243', '441324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441381', '4413', '0,1,44,4413,441381,', '惠阳市', '12244', '441381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4414', '44', '0,1,44,4414,', '梅州', '12245', '4414', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441401', '4414', '0,1,44,4414,441401,', '市辖区', '12246', '441401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441402', '4414', '0,1,44,4414,441402,', '梅江区', '12247', '441402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441421', '4414', '0,1,44,4414,441421,', '梅  县', '12248', '441421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441422', '4414', '0,1,44,4414,441422,', '大埔县', '12249', '441422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441423', '4414', '0,1,44,4414,441423,', '丰顺县', '12250', '441423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441424', '4414', '0,1,44,4414,441424,', '五华县', '12251', '441424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441426', '4414', '0,1,44,4414,441426,', '平远县', '12252', '441426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441427', '4414', '0,1,44,4414,441427,', '蕉岭县', '12253', '441427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441481', '4414', '0,1,44,4414,441481,', '兴宁市', '12254', '441481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4415', '44', '0,1,44,4415,', '汕尾', '12255', '4415', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441501', '4415', '0,1,44,4415,441501,', '市辖区', '12256', '441501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441502', '4415', '0,1,44,4415,441502,', '城  区', '12257', '441502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441521', '4415', '0,1,44,4415,441521,', '海丰县', '12258', '441521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441523', '4415', '0,1,44,4415,441523,', '陆河县', '12259', '441523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441581', '4415', '0,1,44,4415,441581,', '陆丰市', '12260', '441581', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4416', '44', '0,1,44,4416,', '河源', '12261', '4416', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441601', '4416', '0,1,44,4416,441601,', '市辖区', '12262', '441601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441602', '4416', '0,1,44,4416,441602,', '源城区', '12263', '441602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441621', '4416', '0,1,44,4416,441621,', '紫金县', '12264', '441621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441622', '4416', '0,1,44,4416,441622,', '龙川县', '12265', '441622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441623', '4416', '0,1,44,4416,441623,', '连平县', '12266', '441623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441624', '4416', '0,1,44,4416,441624,', '和平县', '12267', '441624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441625', '4416', '0,1,44,4416,441625,', '东源县', '12268', '441625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4417', '44', '0,1,44,4417,', '阳江', '12269', '4417', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441701', '4417', '0,1,44,4417,441701,', '市辖区', '12270', '441701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441702', '4417', '0,1,44,4417,441702,', '江城区', '12271', '441702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441721', '4417', '0,1,44,4417,441721,', '阳西县', '12272', '441721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441723', '4417', '0,1,44,4417,441723,', '阳东县', '12273', '441723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441781', '4417', '0,1,44,4417,441781,', '阳春市', '12274', '441781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4418', '44', '0,1,44,4418,', '清远', '12275', '4418', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441801', '4418', '0,1,44,4418,441801,', '市辖区', '12276', '441801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441802', '4418', '0,1,44,4418,441802,', '清城区', '12277', '441802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441821', '4418', '0,1,44,4418,441821,', '佛冈县', '12278', '441821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441823', '4418', '0,1,44,4418,441823,', '阳山县', '12279', '441823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441825', '4418', '0,1,44,4418,441825,', '连山壮族瑶族自治县', '12280', '441825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441826', '4418', '0,1,44,4418,441826,', '连南瑶族自治县', '12281', '441826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441827', '4418', '0,1,44,4418,441827,', '清新县', '12282', '441827', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441881', '4418', '0,1,44,4418,441881,', '英德市', '12283', '441881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441882', '4418', '0,1,44,4418,441882,', '连州市', '12284', '441882', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4419', '44', '0,1,44,4419,', '东莞', '12285', '4419', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441901', '4419', '0,1,44,4419,441901,', '莞城区', '12286', '441901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441902', '4419', '0,1,44,4419,441902,', '东城区', '12287', '441902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441903', '4419', '0,1,44,4419,441903,', '南城区', '12288', '441903', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('441904', '4419', '0,1,44,4419,441904,', '万江区', '12289', '441904', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4420', '44', '0,1,44,4420,', '中山', '12290', '4420', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('442001', '4420', '0,1,44,4420,442001,', '石岐区', '12291', '442001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('442002', '4420', '0,1,44,4420,442002,', '东区', '12292', '442002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('442003', '4420', '0,1,44,4420,442003,', '西区', '12293', '442003', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('442004', '4420', '0,1,44,4420,442004,', '南区', '12294', '442004', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('442005', '4420', '0,1,44,4420,442005,', '五桂山', '12295', '442005', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4451', '44', '0,1,44,4451,', '潮州', '12296', '4451', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445101', '4451', '0,1,44,4451,445101,', '市辖区', '12297', '445101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445102', '4451', '0,1,44,4451,445102,', '湘桥区', '12298', '445102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445121', '4451', '0,1,44,4451,445121,', '潮安县', '12299', '445121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445122', '4451', '0,1,44,4451,445122,', '饶平县', '12300', '445122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4452', '44', '0,1,44,4452,', '揭阳', '12301', '4452', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445201', '4452', '0,1,44,4452,445201,', '市辖区', '12302', '445201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445202', '4452', '0,1,44,4452,445202,', '榕城区', '12303', '445202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445221', '4452', '0,1,44,4452,445221,', '揭东县', '12304', '445221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445222', '4452', '0,1,44,4452,445222,', '揭西县', '12305', '445222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445224', '4452', '0,1,44,4452,445224,', '惠来县', '12306', '445224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445281', '4452', '0,1,44,4452,445281,', '普宁市', '12307', '445281', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4453', '44', '0,1,44,4453,', '云浮', '12308', '4453', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445301', '4453', '0,1,44,4453,445301,', '市辖区', '12309', '445301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445302', '4453', '0,1,44,4453,445302,', '云城区', '12310', '445302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445321', '4453', '0,1,44,4453,445321,', '新兴县', '12311', '445321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445322', '4453', '0,1,44,4453,445322,', '郁南县', '12312', '445322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445323', '4453', '0,1,44,4453,445323,', '云安县', '12313', '445323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('445381', '4453', '0,1,44,4453,445381,', '罗定市', '12314', '445381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('45', '1', '0,1,45,', '广西', '12315', '45', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4501', '45', '0,1,45,4501,', '南宁', '12316', '4501', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450101', '4501', '0,1,45,4501,450101,', '市辖区', '12317', '450101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450102', '4501', '0,1,45,4501,450102,', '兴宁区', '12318', '450102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450103', '4501', '0,1,45,4501,450103,', '新城区', '12319', '450103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450104', '4501', '0,1,45,4501,450104,', '城北区', '12320', '450104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450105', '4501', '0,1,45,4501,450105,', '江南区', '12321', '450105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450106', '4501', '0,1,45,4501,450106,', '永新区', '12322', '450106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450111', '4501', '0,1,45,4501,450111,', '市郊区', '12323', '450111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450121', '4501', '0,1,45,4501,450121,', '邕宁县', '12324', '450121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450122', '4501', '0,1,45,4501,450122,', '武鸣县', '12325', '450122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4502', '45', '0,1,45,4502,', '柳州', '12326', '4502', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450201', '4502', '0,1,45,4502,450201,', '市辖区', '12327', '450201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450202', '4502', '0,1,45,4502,450202,', '城中区', '12328', '450202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450203', '4502', '0,1,45,4502,450203,', '鱼峰区', '12329', '450203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450204', '4502', '0,1,45,4502,450204,', '柳南区', '12330', '450204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450205', '4502', '0,1,45,4502,450205,', '柳北区', '12331', '450205', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450211', '4502', '0,1,45,4502,450211,', '市郊区', '12332', '450211', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450221', '4502', '0,1,45,4502,450221,', '柳江县', '12333', '450221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450222', '4502', '0,1,45,4502,450222,', '柳城县', '12334', '450222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4503', '45', '0,1,45,4503,', '桂林', '12335', '4503', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450301', '4503', '0,1,45,4503,450301,', '市辖区', '12336', '450301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450302', '4503', '0,1,45,4503,450302,', '秀峰区', '12337', '450302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450303', '4503', '0,1,45,4503,450303,', '叠彩区', '12338', '450303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450304', '4503', '0,1,45,4503,450304,', '象山区', '12339', '450304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450305', '4503', '0,1,45,4503,450305,', '七星区', '12340', '450305', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450311', '4503', '0,1,45,4503,450311,', '雁山区', '12341', '450311', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450321', '4503', '0,1,45,4503,450321,', '阳朔县', '12342', '450321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450322', '4503', '0,1,45,4503,450322,', '临桂县', '12343', '450322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450323', '4503', '0,1,45,4503,450323,', '灵川县', '12344', '450323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450324', '4503', '0,1,45,4503,450324,', '全州县', '12345', '450324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450325', '4503', '0,1,45,4503,450325,', '兴安县', '12346', '450325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450326', '4503', '0,1,45,4503,450326,', '永福县', '12347', '450326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450327', '4503', '0,1,45,4503,450327,', '灌阳县', '12348', '450327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450328', '4503', '0,1,45,4503,450328,', '龙胜各县自治区', '12349', '450328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450329', '4503', '0,1,45,4503,450329,', '资源县', '12350', '450329', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450330', '4503', '0,1,45,4503,450330,', '平乐县', '12351', '450330', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450331', '4503', '0,1,45,4503,450331,', '荔蒲县', '12352', '450331', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450332', '4503', '0,1,45,4503,450332,', '恭城瑶族自治县', '12353', '450332', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4504', '45', '0,1,45,4504,', '梧州', '12354', '4504', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450401', '4504', '0,1,45,4504,450401,', '市辖区', '12355', '450401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450403', '4504', '0,1,45,4504,450403,', '万秀区', '12356', '450403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450404', '4504', '0,1,45,4504,450404,', '蝶山区', '12357', '450404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450411', '4504', '0,1,45,4504,450411,', '市郊区', '12358', '450411', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450421', '4504', '0,1,45,4504,450421,', '苍梧县', '12359', '450421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450422', '4504', '0,1,45,4504,450422,', '藤  县', '12360', '450422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450423', '4504', '0,1,45,4504,450423,', '蒙山县', '12361', '450423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450481', '4504', '0,1,45,4504,450481,', '岑溪市', '12362', '450481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4505', '45', '0,1,45,4505,', '北海', '12363', '4505', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450501', '4505', '0,1,45,4505,450501,', '市辖区', '12364', '450501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450502', '4505', '0,1,45,4505,450502,', '海城区', '12365', '450502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450503', '4505', '0,1,45,4505,450503,', '银海区', '12366', '450503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450512', '4505', '0,1,45,4505,450512,', '铁山港区', '12367', '450512', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450521', '4505', '0,1,45,4505,450521,', '合浦县', '12368', '450521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4506', '45', '0,1,45,4506,', '防城港', '12369', '4506', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450601', '4506', '0,1,45,4506,450601,', '市辖区', '12370', '450601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450602', '4506', '0,1,45,4506,450602,', '港口区', '12371', '450602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450603', '4506', '0,1,45,4506,450603,', '防城区', '12372', '450603', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450621', '4506', '0,1,45,4506,450621,', '上思县', '12373', '450621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450681', '4506', '0,1,45,4506,450681,', '东兴市', '12374', '450681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4507', '45', '0,1,45,4507,', '钦州', '12375', '4507', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450701', '4507', '0,1,45,4507,450701,', '市辖区', '12376', '450701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450702', '4507', '0,1,45,4507,450702,', '钦南区', '12377', '450702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450703', '4507', '0,1,45,4507,450703,', '钦北区', '12378', '450703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450721', '4507', '0,1,45,4507,450721,', '浦北县', '12379', '450721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450722', '4507', '0,1,45,4507,450722,', '灵山县', '12380', '450722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4508', '45', '0,1,45,4508,', '贵港', '12381', '4508', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450801', '4508', '0,1,45,4508,450801,', '市辖区', '12382', '450801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450802', '4508', '0,1,45,4508,450802,', '港北区', '12383', '450802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450803', '4508', '0,1,45,4508,450803,', '港南区', '12384', '450803', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450821', '4508', '0,1,45,4508,450821,', '平南县', '12385', '450821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450881', '4508', '0,1,45,4508,450881,', '桂平市', '12386', '450881', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4509', '45', '0,1,45,4509,', '玉林', '12387', '4509', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450901', '4509', '0,1,45,4509,450901,', '市辖区', '12388', '450901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450902', '4509', '0,1,45,4509,450902,', '玉州区', '12389', '450902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450921', '4509', '0,1,45,4509,450921,', '容  县', '12390', '450921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450922', '4509', '0,1,45,4509,450922,', '陆川县', '12391', '450922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450923', '4509', '0,1,45,4509,450923,', '博白县', '12392', '450923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450924', '4509', '0,1,45,4509,450924,', '兴业县', '12393', '450924', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('450981', '4509', '0,1,45,4509,450981,', '北流市', '12394', '450981', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4521', '45', '0,1,45,4521,', '南宁地区', '12395', '4521', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452101', '4521', '0,1,45,4521,452101,', '凭祥市', '12396', '452101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452122', '4521', '0,1,45,4521,452122,', '横  县', '12397', '452122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452123', '4521', '0,1,45,4521,452123,', '宾阳县', '12398', '452123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452124', '4521', '0,1,45,4521,452124,', '上林县', '12399', '452124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452126', '4521', '0,1,45,4521,452126,', '隆安县', '12400', '452126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452127', '4521', '0,1,45,4521,452127,', '马山县', '12401', '452127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452128', '4521', '0,1,45,4521,452128,', '扶绥县', '12402', '452128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452129', '4521', '0,1,45,4521,452129,', '崇左县', '12403', '452129', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452130', '4521', '0,1,45,4521,452130,', '大新县', '12404', '452130', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452131', '4521', '0,1,45,4521,452131,', '天等县', '12405', '452131', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452132', '4521', '0,1,45,4521,452132,', '宁明县', '12406', '452132', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452133', '4521', '0,1,45,4521,452133,', '龙州县', '12407', '452133', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4522', '45', '0,1,45,4522,', '柳州地区', '12408', '4522', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452201', '4522', '0,1,45,4522,452201,', '合山市', '12409', '452201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452223', '4522', '0,1,45,4522,452223,', '鹿寨县', '12410', '452223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452224', '4522', '0,1,45,4522,452224,', '象州县', '12411', '452224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452225', '4522', '0,1,45,4522,452225,', '武宣县', '12412', '452225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452226', '4522', '0,1,45,4522,452226,', '来宾县', '12413', '452226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452227', '4522', '0,1,45,4522,452227,', '融安县', '12414', '452227', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452228', '4522', '0,1,45,4522,452228,', '三江侗族自治县', '12415', '452228', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452229', '4522', '0,1,45,4522,452229,', '融水苗族自治县', '12416', '452229', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452230', '4522', '0,1,45,4522,452230,', '金秀瑶族自治县', '12417', '452230', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452231', '4522', '0,1,45,4522,452231,', '忻城县', '12418', '452231', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4524', '45', '0,1,45,4524,', '贺州地区', '12419', '4524', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452402', '4524', '0,1,45,4524,452402,', '贺州市', '12420', '452402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452424', '4524', '0,1,45,4524,452424,', '昭平县', '12421', '452424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452427', '4524', '0,1,45,4524,452427,', '钟山县', '12422', '452427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452428', '4524', '0,1,45,4524,452428,', '富川瑶族自治县', '12423', '452428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4526', '45', '0,1,45,4526,', '百色地区', '12424', '4526', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452601', '4526', '0,1,45,4526,452601,', '百色市', '12425', '452601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452622', '4526', '0,1,45,4526,452622,', '田阳县', '12426', '452622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452623', '4526', '0,1,45,4526,452623,', '田东县', '12427', '452623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452624', '4526', '0,1,45,4526,452624,', '平果县', '12428', '452624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452625', '4526', '0,1,45,4526,452625,', '德保县', '12429', '452625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452626', '4526', '0,1,45,4526,452626,', '靖西县', '12430', '452626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452627', '4526', '0,1,45,4526,452627,', '那坡县', '12431', '452627', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452628', '4526', '0,1,45,4526,452628,', '凌云县', '12432', '452628', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452629', '4526', '0,1,45,4526,452629,', '乐业县', '12433', '452629', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452630', '4526', '0,1,45,4526,452630,', '田林县', '12434', '452630', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452631', '4526', '0,1,45,4526,452631,', '隆林各族自治县', '12435', '452631', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452632', '4526', '0,1,45,4526,452632,', '西林县', '12436', '452632', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4527', '45', '0,1,45,4527,', '河池地区', '12437', '4527', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452701', '4527', '0,1,45,4527,452701,', '河池市', '12438', '452701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452702', '4527', '0,1,45,4527,452702,', '宜州市', '12439', '452702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452723', '4527', '0,1,45,4527,452723,', '罗城仫佬族自治县', '12440', '452723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452724', '4527', '0,1,45,4527,452724,', '环江毛南族自治县', '12441', '452724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452725', '4527', '0,1,45,4527,452725,', '南丹县', '12442', '452725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452726', '4527', '0,1,45,4527,452726,', '天峨县', '12443', '452726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452727', '4527', '0,1,45,4527,452727,', '凤山县', '12444', '452727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452728', '4527', '0,1,45,4527,452728,', '东兰县', '12445', '452728', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452729', '4527', '0,1,45,4527,452729,', '巴马瑶族自治县', '12446', '452729', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452730', '4527', '0,1,45,4527,452730,', '都安瑶族自治县', '12447', '452730', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('452731', '4527', '0,1,45,4527,452731,', '大化瑶族自治县', '12448', '452731', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('46', '1', '0,1,46,', '海南', '12449', '46', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4601', '46', '0,1,46,4601,', '海南', '12450', '4601', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460101', '4601', '0,1,46,4601,460101,', '通什市', '12451', '460101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460102', '4601', '0,1,46,4601,460102,', '琼海市', '12452', '460102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460103', '4601', '0,1,46,4601,460103,', '儋州市', '12453', '460103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460104', '4601', '0,1,46,4601,460104,', '琼山市', '12454', '460104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460105', '4601', '0,1,46,4601,460105,', '文昌市', '12455', '460105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460106', '4601', '0,1,46,4601,460106,', '万宁市', '12456', '460106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460107', '4601', '0,1,46,4601,460107,', '东方市', '12457', '460107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460125', '4601', '0,1,46,4601,460125,', '定安县', '12458', '460125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460126', '4601', '0,1,46,4601,460126,', '屯昌县', '12459', '460126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460127', '4601', '0,1,46,4601,460127,', '澄迈县', '12460', '460127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460128', '4601', '0,1,46,4601,460128,', '临高县', '12461', '460128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460130', '4601', '0,1,46,4601,460130,', '白沙黎族自治县', '12462', '460130', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460131', '4601', '0,1,46,4601,460131,', '昌江黎族自治县', '12463', '460131', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460133', '4601', '0,1,46,4601,460133,', '乐东黎族自治县', '12464', '460133', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460134', '4601', '0,1,46,4601,460134,', '陵水黎族自治县', '12465', '460134', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460135', '4601', '0,1,46,4601,460135,', '保亭黎族苗族自治县', '12466', '460135', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460136', '4601', '0,1,46,4601,460136,', '琼中黎族苗族自治县', '12467', '460136', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460137', '4601', '0,1,46,4601,460137,', '西沙群岛', '12468', '460137', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460138', '4601', '0,1,46,4601,460138,', '南沙群岛', '12469', '460138', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460139', '4601', '0,1,46,4601,460139,', '中沙群岛的岛礁及其海', '12470', '460139', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4602', '46', '0,1,46,4602,', '海口', '12471', '4602', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460201', '4602', '0,1,46,4602,460201,', '市辖区', '12472', '460201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460202', '4602', '0,1,46,4602,460202,', '振东区', '12473', '460202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460203', '4602', '0,1,46,4602,460203,', '新华区', '12474', '460203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460204', '4602', '0,1,46,4602,460204,', '秀英区', '12475', '460204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('4603', '46', '0,1,46,4603,', '三亚', '12476', '4603', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('460301', '4603', '0,1,46,4603,460301,', '市辖区', '12477', '460301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('50', '1', '0,1,50,', '重庆', '12478', '50', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5001', '50', '0,1,50,5001,', '重庆市辖', '12479', '5001', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500101', '5001', '0,1,50,5001,500101,', '万州区', '12480', '500101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500102', '5001', '0,1,50,5001,500102,', '涪陵区', '12481', '500102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500103', '5001', '0,1,50,5001,500103,', '渝中区', '12482', '500103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500104', '5001', '0,1,50,5001,500104,', '大渡口区', '12483', '500104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500105', '5001', '0,1,50,5001,500105,', '江北区', '12484', '500105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500106', '5001', '0,1,50,5001,500106,', '沙坪坝区', '12485', '500106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500107', '5001', '0,1,50,5001,500107,', '九龙坡区', '12486', '500107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500108', '5001', '0,1,50,5001,500108,', '南岸区', '12487', '500108', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500109', '5001', '0,1,50,5001,500109,', '北碚区', '12488', '500109', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500110', '5001', '0,1,50,5001,500110,', '万盛区', '12489', '500110', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500111', '5001', '0,1,50,5001,500111,', '双桥区', '12490', '500111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500112', '5001', '0,1,50,5001,500112,', '渝北区', '12491', '500112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500113', '5001', '0,1,50,5001,500113,', '巴南区', '12492', '500113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500114', '5001', '0,1,50,5001,500114,', '黔江区', '12493', '500114', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5002', '50', '0,1,50,5002,', '重庆县辖', '12494', '5002', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500221', '5002', '0,1,50,5002,500221,', '长寿县', '12495', '500221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500222', '5002', '0,1,50,5002,500222,', '綦江县', '12496', '500222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500223', '5002', '0,1,50,5002,500223,', '潼南县', '12497', '500223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500224', '5002', '0,1,50,5002,500224,', '铜梁县', '12498', '500224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500225', '5002', '0,1,50,5002,500225,', '大足县', '12499', '500225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500226', '5002', '0,1,50,5002,500226,', '荣昌县', '12500', '500226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500227', '5002', '0,1,50,5002,500227,', '璧山县', '12501', '500227', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500228', '5002', '0,1,50,5002,500228,', '梁平县', '12502', '500228', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500229', '5002', '0,1,50,5002,500229,', '城口县', '12503', '500229', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500230', '5002', '0,1,50,5002,500230,', '丰都县', '12504', '500230', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500231', '5002', '0,1,50,5002,500231,', '垫江县', '12505', '500231', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500232', '5002', '0,1,50,5002,500232,', '武隆县', '12506', '500232', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500233', '5002', '0,1,50,5002,500233,', '忠  县', '12507', '500233', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500234', '5002', '0,1,50,5002,500234,', '开  县', '12508', '500234', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500235', '5002', '0,1,50,5002,500235,', '云阳县', '12509', '500235', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500236', '5002', '0,1,50,5002,500236,', '奉节县', '12510', '500236', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500237', '5002', '0,1,50,5002,500237,', '巫山县', '12511', '500237', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500238', '5002', '0,1,50,5002,500238,', '巫溪县', '12512', '500238', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500240', '5002', '0,1,50,5002,500240,', '石柱土家族自治县', '12513', '500240', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500241', '5002', '0,1,50,5002,500241,', '秀山土家族苗族自治县', '12514', '500241', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500242', '5002', '0,1,50,5002,500242,', '酉阳土家族苗族自治县', '12515', '500242', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500243', '5002', '0,1,50,5002,500243,', '彭水苗族土家族自治县', '12516', '500243', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5003', '50', '0,1,50,5003,', '重庆县级', '12517', '5003', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500381', '5003', '0,1,50,5003,500381,', '江津市', '12518', '500381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500382', '5003', '0,1,50,5003,500382,', '合川市', '12519', '500382', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500383', '5003', '0,1,50,5003,500383,', '永川市', '12520', '500383', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('500384', '5003', '0,1,50,5003,500384,', '南川市', '12521', '500384', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('51', '1', '0,1,51,', '四川', '12522', '51', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5101', '51', '0,1,51,5101,', '成都', '12523', '5101', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510101', '5101', '0,1,51,5101,510101,', '市辖区', '12524', '510101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510103', '5101', '0,1,51,5101,510103,', '高新区', '12525', '510103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510104', '5101', '0,1,51,5101,510104,', '锦江区', '12526', '510104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510105', '5101', '0,1,51,5101,510105,', '青羊区', '12527', '510105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510106', '5101', '0,1,51,5101,510106,', '金牛区', '12528', '510106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510107', '5101', '0,1,51,5101,510107,', '武侯区', '12529', '510107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510108', '5101', '0,1,51,5101,510108,', '成华区', '12530', '510108', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510112', '5101', '0,1,51,5101,510112,', '龙泉驿区', '12531', '510112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510113', '5101', '0,1,51,5101,510113,', '青白江区', '12532', '510113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510121', '5101', '0,1,51,5101,510121,', '金堂县', '12533', '510121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510122', '5101', '0,1,51,5101,510122,', '双流县', '12534', '510122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510123', '5101', '0,1,51,5101,510123,', '温江县', '12535', '510123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510124', '5101', '0,1,51,5101,510124,', '郫  县', '12536', '510124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510125', '5101', '0,1,51,5101,510125,', '新都县', '12537', '510125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510129', '5101', '0,1,51,5101,510129,', '大邑县', '12538', '510129', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510131', '5101', '0,1,51,5101,510131,', '蒲江县', '12539', '510131', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510132', '5101', '0,1,51,5101,510132,', '新津县', '12540', '510132', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510181', '5101', '0,1,51,5101,510181,', '都江堰市', '12541', '510181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510182', '5101', '0,1,51,5101,510182,', '彭州市', '12542', '510182', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510183', '5101', '0,1,51,5101,510183,', '邛崃市', '12543', '510183', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510184', '5101', '0,1,51,5101,510184,', '崇州市', '12544', '510184', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5103', '51', '0,1,51,5103,', '自贡', '12545', '5103', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510301', '5103', '0,1,51,5103,510301,', '市辖区', '12546', '510301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510302', '5103', '0,1,51,5103,510302,', '自流井区', '12547', '510302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510303', '5103', '0,1,51,5103,510303,', '贡井区', '12548', '510303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510304', '5103', '0,1,51,5103,510304,', '大安区', '12549', '510304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510311', '5103', '0,1,51,5103,510311,', '沿滩区', '12550', '510311', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510321', '5103', '0,1,51,5103,510321,', '荣  县', '12551', '510321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510322', '5103', '0,1,51,5103,510322,', '富顺县', '12552', '510322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5104', '51', '0,1,51,5104,', '攀枝花', '12553', '5104', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510401', '5104', '0,1,51,5104,510401,', '市辖区', '12554', '510401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510402', '5104', '0,1,51,5104,510402,', '东  区', '12555', '510402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510403', '5104', '0,1,51,5104,510403,', '西  区', '12556', '510403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510411', '5104', '0,1,51,5104,510411,', '仁和区', '12557', '510411', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510421', '5104', '0,1,51,5104,510421,', '米易县', '12558', '510421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510422', '5104', '0,1,51,5104,510422,', '盐边县', '12559', '510422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5105', '51', '0,1,51,5105,', '泸州', '12560', '5105', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510501', '5105', '0,1,51,5105,510501,', '市辖区', '12561', '510501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510502', '5105', '0,1,51,5105,510502,', '江阳区', '12562', '510502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510503', '5105', '0,1,51,5105,510503,', '纳溪区', '12563', '510503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510504', '5105', '0,1,51,5105,510504,', '龙马潭区', '12564', '510504', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510521', '5105', '0,1,51,5105,510521,', '泸  县', '12565', '510521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510522', '5105', '0,1,51,5105,510522,', '合江县', '12566', '510522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510524', '5105', '0,1,51,5105,510524,', '叙永县', '12567', '510524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510525', '5105', '0,1,51,5105,510525,', '古蔺县', '12568', '510525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5106', '51', '0,1,51,5106,', '德阳', '12569', '5106', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510601', '5106', '0,1,51,5106,510601,', '市辖区', '12570', '510601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510603', '5106', '0,1,51,5106,510603,', '旌阳区', '12571', '510603', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510623', '5106', '0,1,51,5106,510623,', '中江县', '12572', '510623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510626', '5106', '0,1,51,5106,510626,', '罗江县', '12573', '510626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510681', '5106', '0,1,51,5106,510681,', '广汉市', '12574', '510681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510682', '5106', '0,1,51,5106,510682,', '什邡市', '12575', '510682', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510683', '5106', '0,1,51,5106,510683,', '绵竹市', '12576', '510683', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5107', '51', '0,1,51,5107,', '绵阳', '12577', '5107', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510701', '5107', '0,1,51,5107,510701,', '市辖区', '12578', '510701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510703', '5107', '0,1,51,5107,510703,', '涪城区', '12579', '510703', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510704', '5107', '0,1,51,5107,510704,', '游仙区', '12580', '510704', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510710', '5107', '0,1,51,5107,510710,', '科学城区', '12581', '510710', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510722', '5107', '0,1,51,5107,510722,', '三台县', '12582', '510722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510723', '5107', '0,1,51,5107,510723,', '盐亭县', '12583', '510723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510724', '5107', '0,1,51,5107,510724,', '安  县', '12584', '510724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510725', '5107', '0,1,51,5107,510725,', '梓潼县', '12585', '510725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510726', '5107', '0,1,51,5107,510726,', '北川县', '12586', '510726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510727', '5107', '0,1,51,5107,510727,', '平武县', '12587', '510727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510781', '5107', '0,1,51,5107,510781,', '江油市', '12588', '510781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5108', '51', '0,1,51,5108,', '广元', '12589', '5108', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510801', '5108', '0,1,51,5108,510801,', '市辖区', '12590', '510801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510802', '5108', '0,1,51,5108,510802,', '市中区', '12591', '510802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510811', '5108', '0,1,51,5108,510811,', '元坝区', '12592', '510811', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510812', '5108', '0,1,51,5108,510812,', '朝天区', '12593', '510812', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510821', '5108', '0,1,51,5108,510821,', '旺苍县', '12594', '510821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510822', '5108', '0,1,51,5108,510822,', '青川县', '12595', '510822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510823', '5108', '0,1,51,5108,510823,', '剑阁县', '12596', '510823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510824', '5108', '0,1,51,5108,510824,', '苍溪县', '12597', '510824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5109', '51', '0,1,51,5109,', '遂宁', '12598', '5109', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510901', '5109', '0,1,51,5109,510901,', '市辖区', '12599', '510901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510902', '5109', '0,1,51,5109,510902,', '市中区', '12600', '510902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510921', '5109', '0,1,51,5109,510921,', '蓬溪县', '12601', '510921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510922', '5109', '0,1,51,5109,510922,', '射洪县', '12602', '510922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('510923', '5109', '0,1,51,5109,510923,', '大英县', '12603', '510923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5110', '51', '0,1,51,5110,', '内江', '12604', '5110', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511001', '5110', '0,1,51,5110,511001,', '市辖区', '12605', '511001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511002', '5110', '0,1,51,5110,511002,', '市中区', '12606', '511002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511011', '5110', '0,1,51,5110,511011,', '东兴区', '12607', '511011', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511024', '5110', '0,1,51,5110,511024,', '威远县', '12608', '511024', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511025', '5110', '0,1,51,5110,511025,', '资中县', '12609', '511025', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511028', '5110', '0,1,51,5110,511028,', '隆昌县', '12610', '511028', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5111', '51', '0,1,51,5111,', '乐山', '12611', '5111', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511101', '5111', '0,1,51,5111,511101,', '市辖区', '12612', '511101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511102', '5111', '0,1,51,5111,511102,', '市中区', '12613', '511102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511111', '5111', '0,1,51,5111,511111,', '沙湾区', '12614', '511111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511112', '5111', '0,1,51,5111,511112,', '五通桥区', '12615', '511112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511113', '5111', '0,1,51,5111,511113,', '金口河区', '12616', '511113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511123', '5111', '0,1,51,5111,511123,', '犍为县', '12617', '511123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511124', '5111', '0,1,51,5111,511124,', '井研县', '12618', '511124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511126', '5111', '0,1,51,5111,511126,', '夹江县', '12619', '511126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511129', '5111', '0,1,51,5111,511129,', '沐川县', '12620', '511129', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511132', '5111', '0,1,51,5111,511132,', '峨边彝族自治县', '12621', '511132', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511133', '5111', '0,1,51,5111,511133,', '马边彝族自治县', '12622', '511133', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511181', '5111', '0,1,51,5111,511181,', '峨眉山市', '12623', '511181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5113', '51', '0,1,51,5113,', '南充', '12624', '5113', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511301', '5113', '0,1,51,5113,511301,', '市辖区', '12625', '511301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511302', '5113', '0,1,51,5113,511302,', '顺庆区', '12626', '511302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511303', '5113', '0,1,51,5113,511303,', '高坪区', '12627', '511303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511304', '5113', '0,1,51,5113,511304,', '嘉陵区', '12628', '511304', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511321', '5113', '0,1,51,5113,511321,', '南部县', '12629', '511321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511322', '5113', '0,1,51,5113,511322,', '营山县', '12630', '511322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511323', '5113', '0,1,51,5113,511323,', '蓬安县', '12631', '511323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511324', '5113', '0,1,51,5113,511324,', '仪陇县', '12632', '511324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511325', '5113', '0,1,51,5113,511325,', '西充县', '12633', '511325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511381', '5113', '0,1,51,5113,511381,', '阆中市', '12634', '511381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5114', '51', '0,1,51,5114,', '眉山', '12635', '5114', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511401', '5114', '0,1,51,5114,511401,', '市辖区', '12636', '511401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511402', '5114', '0,1,51,5114,511402,', '东坡区', '12637', '511402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511421', '5114', '0,1,51,5114,511421,', '仁寿县', '12638', '511421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511422', '5114', '0,1,51,5114,511422,', '彭山县', '12639', '511422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511423', '5114', '0,1,51,5114,511423,', '洪雅县', '12640', '511423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511424', '5114', '0,1,51,5114,511424,', '丹棱县', '12641', '511424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511425', '5114', '0,1,51,5114,511425,', '青神县', '12642', '511425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5115', '51', '0,1,51,5115,', '宜宾', '12643', '5115', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511501', '5115', '0,1,51,5115,511501,', '市辖区', '12644', '511501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511502', '5115', '0,1,51,5115,511502,', '翠屏区', '12645', '511502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511521', '5115', '0,1,51,5115,511521,', '宜宾县', '12646', '511521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511522', '5115', '0,1,51,5115,511522,', '南溪县', '12647', '511522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511523', '5115', '0,1,51,5115,511523,', '江安县', '12648', '511523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511524', '5115', '0,1,51,5115,511524,', '长宁县', '12649', '511524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511525', '5115', '0,1,51,5115,511525,', '高  县', '12650', '511525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511526', '5115', '0,1,51,5115,511526,', '珙  县', '12651', '511526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511527', '5115', '0,1,51,5115,511527,', '筠连县', '12652', '511527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511528', '5115', '0,1,51,5115,511528,', '兴文县', '12653', '511528', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511529', '5115', '0,1,51,5115,511529,', '屏山县', '12654', '511529', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5116', '51', '0,1,51,5116,', '广安', '12655', '5116', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511601', '5116', '0,1,51,5116,511601,', '市辖区', '12656', '511601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511602', '5116', '0,1,51,5116,511602,', '广安区', '12657', '511602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511621', '5116', '0,1,51,5116,511621,', '岳池县', '12658', '511621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511622', '5116', '0,1,51,5116,511622,', '武胜县', '12659', '511622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511623', '5116', '0,1,51,5116,511623,', '邻水县', '12660', '511623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511681', '5116', '0,1,51,5116,511681,', '华蓥市', '12661', '511681', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5117', '51', '0,1,51,5117,', '达州', '12662', '5117', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511701', '5117', '0,1,51,5117,511701,', '市辖区', '12663', '511701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511702', '5117', '0,1,51,5117,511702,', '通川区', '12664', '511702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511721', '5117', '0,1,51,5117,511721,', '达  县', '12665', '511721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511722', '5117', '0,1,51,5117,511722,', '宣汉县', '12666', '511722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511723', '5117', '0,1,51,5117,511723,', '开江县', '12667', '511723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511724', '5117', '0,1,51,5117,511724,', '大竹县', '12668', '511724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511725', '5117', '0,1,51,5117,511725,', '渠  县', '12669', '511725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511781', '5117', '0,1,51,5117,511781,', '万源市', '12670', '511781', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5118', '51', '0,1,51,5118,', '雅安', '12671', '5118', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511801', '5118', '0,1,51,5118,511801,', '市辖区', '12672', '511801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511802', '5118', '0,1,51,5118,511802,', '雨城区', '12673', '511802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511821', '5118', '0,1,51,5118,511821,', '名山县', '12674', '511821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511822', '5118', '0,1,51,5118,511822,', '荥经县', '12675', '511822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511823', '5118', '0,1,51,5118,511823,', '汉源县', '12676', '511823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511824', '5118', '0,1,51,5118,511824,', '石棉县', '12677', '511824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511825', '5118', '0,1,51,5118,511825,', '天全县', '12678', '511825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511826', '5118', '0,1,51,5118,511826,', '芦山县', '12679', '511826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511827', '5118', '0,1,51,5118,511827,', '宝兴县', '12680', '511827', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5119', '51', '0,1,51,5119,', '巴中', '12681', '5119', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511901', '5119', '0,1,51,5119,511901,', '市辖区', '12682', '511901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511902', '5119', '0,1,51,5119,511902,', '巴州区', '12683', '511902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511921', '5119', '0,1,51,5119,511921,', '通江县', '12684', '511921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511922', '5119', '0,1,51,5119,511922,', '南江县', '12685', '511922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('511923', '5119', '0,1,51,5119,511923,', '平昌县', '12686', '511923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5120', '51', '0,1,51,5120,', '资阳', '12687', '5120', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('512001', '5120', '0,1,51,5120,512001,', '市辖区', '12688', '512001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('512002', '5120', '0,1,51,5120,512002,', '雁江区', '12689', '512002', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('512021', '5120', '0,1,51,5120,512021,', '安岳县', '12690', '512021', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('512022', '5120', '0,1,51,5120,512022,', '乐至县', '12691', '512022', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('512081', '5120', '0,1,51,5120,512081,', '简阳市', '12692', '512081', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5132', '51', '0,1,51,5132,', '阿坝藏族羌族自治州', '12693', '5132', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513221', '5132', '0,1,51,5132,513221,', '汶川县', '12694', '513221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513222', '5132', '0,1,51,5132,513222,', '理  县', '12695', '513222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513223', '5132', '0,1,51,5132,513223,', '茂  县', '12696', '513223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513224', '5132', '0,1,51,5132,513224,', '松潘县', '12697', '513224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513225', '5132', '0,1,51,5132,513225,', '九寨沟县', '12698', '513225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513226', '5132', '0,1,51,5132,513226,', '金川县', '12699', '513226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513227', '5132', '0,1,51,5132,513227,', '小金县', '12700', '513227', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513228', '5132', '0,1,51,5132,513228,', '黑水县', '12701', '513228', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513229', '5132', '0,1,51,5132,513229,', '马尔康县', '12702', '513229', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513230', '5132', '0,1,51,5132,513230,', '壤塘县', '12703', '513230', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513231', '5132', '0,1,51,5132,513231,', '阿坝县', '12704', '513231', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513232', '5132', '0,1,51,5132,513232,', '若尔盖县', '12705', '513232', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513233', '5132', '0,1,51,5132,513233,', '红原县', '12706', '513233', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5133', '51', '0,1,51,5133,', '甘孜藏族自治州', '12707', '5133', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513321', '5133', '0,1,51,5133,513321,', '康定县', '12708', '513321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513322', '5133', '0,1,51,5133,513322,', '泸定县', '12709', '513322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513323', '5133', '0,1,51,5133,513323,', '丹巴县', '12710', '513323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513324', '5133', '0,1,51,5133,513324,', '九龙县', '12711', '513324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513325', '5133', '0,1,51,5133,513325,', '雅江县', '12712', '513325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513326', '5133', '0,1,51,5133,513326,', '道孚县', '12713', '513326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513327', '5133', '0,1,51,5133,513327,', '炉霍县', '12714', '513327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513328', '5133', '0,1,51,5133,513328,', '甘孜县', '12715', '513328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513329', '5133', '0,1,51,5133,513329,', '新龙县', '12716', '513329', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513330', '5133', '0,1,51,5133,513330,', '德格县', '12717', '513330', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513331', '5133', '0,1,51,5133,513331,', '白玉县', '12718', '513331', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513332', '5133', '0,1,51,5133,513332,', '石渠县', '12719', '513332', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513333', '5133', '0,1,51,5133,513333,', '色达县', '12720', '513333', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513334', '5133', '0,1,51,5133,513334,', '理塘县', '12721', '513334', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513335', '5133', '0,1,51,5133,513335,', '巴塘县', '12722', '513335', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513336', '5133', '0,1,51,5133,513336,', '乡城县', '12723', '513336', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513337', '5133', '0,1,51,5133,513337,', '稻城县', '12724', '513337', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513338', '5133', '0,1,51,5133,513338,', '得荣县', '12725', '513338', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5134', '51', '0,1,51,5134,', '凉山彝族自治州', '12726', '5134', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513401', '5134', '0,1,51,5134,513401,', '西昌市', '12727', '513401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513422', '5134', '0,1,51,5134,513422,', '木里藏族自治县', '12728', '513422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513423', '5134', '0,1,51,5134,513423,', '盐源县', '12729', '513423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513424', '5134', '0,1,51,5134,513424,', '德昌县', '12730', '513424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513425', '5134', '0,1,51,5134,513425,', '会理县', '12731', '513425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513426', '5134', '0,1,51,5134,513426,', '会东县', '12732', '513426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513427', '5134', '0,1,51,5134,513427,', '宁南县', '12733', '513427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513428', '5134', '0,1,51,5134,513428,', '普格县', '12734', '513428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513429', '5134', '0,1,51,5134,513429,', '布拖县', '12735', '513429', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513430', '5134', '0,1,51,5134,513430,', '金阳县', '12736', '513430', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513431', '5134', '0,1,51,5134,513431,', '昭觉县', '12737', '513431', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513432', '5134', '0,1,51,5134,513432,', '喜德县', '12738', '513432', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513433', '5134', '0,1,51,5134,513433,', '冕宁县', '12739', '513433', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513434', '5134', '0,1,51,5134,513434,', '越西县', '12740', '513434', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513435', '5134', '0,1,51,5134,513435,', '甘洛县', '12741', '513435', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513436', '5134', '0,1,51,5134,513436,', '美姑县', '12742', '513436', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('513437', '5134', '0,1,51,5134,513437,', '雷波县', '12743', '513437', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('52', '1', '0,1,52,', '贵州', '12744', '52', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5201', '52', '0,1,52,5201,', '贵阳', '12745', '5201', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520101', '5201', '0,1,52,5201,520101,', '市辖区', '12746', '520101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520102', '5201', '0,1,52,5201,520102,', '南明区', '12747', '520102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520103', '5201', '0,1,52,5201,520103,', '云岩区', '12748', '520103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520111', '5201', '0,1,52,5201,520111,', '花溪区', '12749', '520111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520112', '5201', '0,1,52,5201,520112,', '乌当区', '12750', '520112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520113', '5201', '0,1,52,5201,520113,', '白云区', '12751', '520113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520114', '5201', '0,1,52,5201,520114,', '小河区', '12752', '520114', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520121', '5201', '0,1,52,5201,520121,', '开阳县', '12753', '520121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520122', '5201', '0,1,52,5201,520122,', '息烽县', '12754', '520122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520123', '5201', '0,1,52,5201,520123,', '修文县', '12755', '520123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520181', '5201', '0,1,52,5201,520181,', '清镇市', '12756', '520181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5202', '52', '0,1,52,5202,', '六盘水', '12757', '5202', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520201', '5202', '0,1,52,5202,520201,', '钟山区', '12758', '520201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520203', '5202', '0,1,52,5202,520203,', '六枝特区', '12759', '520203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520221', '5202', '0,1,52,5202,520221,', '水城县', '12760', '520221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520222', '5202', '0,1,52,5202,520222,', '盘  县', '12761', '520222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5203', '52', '0,1,52,5203,', '遵义', '12762', '5203', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520301', '5203', '0,1,52,5203,520301,', '市辖区', '12763', '520301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520302', '5203', '0,1,52,5203,520302,', '红花岗区', '12764', '520302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520321', '5203', '0,1,52,5203,520321,', '遵义县', '12765', '520321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520322', '5203', '0,1,52,5203,520322,', '桐梓县', '12766', '520322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520323', '5203', '0,1,52,5203,520323,', '绥阳县', '12767', '520323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520324', '5203', '0,1,52,5203,520324,', '正安县', '12768', '520324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520325', '5203', '0,1,52,5203,520325,', '道真仡佬族苗族自治县', '12769', '520325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520326', '5203', '0,1,52,5203,520326,', '务川仡佬族苗族自治县', '12770', '520326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520327', '5203', '0,1,52,5203,520327,', '凤冈县', '12771', '520327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520328', '5203', '0,1,52,5203,520328,', '湄潭县', '12772', '520328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520329', '5203', '0,1,52,5203,520329,', '余庆县', '12773', '520329', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520330', '5203', '0,1,52,5203,520330,', '习水县', '12774', '520330', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520381', '5203', '0,1,52,5203,520381,', '赤水市', '12775', '520381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520382', '5203', '0,1,52,5203,520382,', '仁怀市', '12776', '520382', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5204', '52', '0,1,52,5204,', '安顺', '12777', '5204', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520401', '5204', '0,1,52,5204,520401,', '市辖区', '12778', '520401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520402', '5204', '0,1,52,5204,520402,', '西秀区', '12779', '520402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520421', '5204', '0,1,52,5204,520421,', '平坝县', '12780', '520421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520422', '5204', '0,1,52,5204,520422,', '普定县', '12781', '520422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520423', '5204', '0,1,52,5204,520423,', '镇宁布依族苗族自治县', '12782', '520423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520424', '5204', '0,1,52,5204,520424,', '关岭布依族苗族自治县', '12783', '520424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('520425', '5204', '0,1,52,5204,520425,', '紫云苗族布依族自治县', '12784', '520425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5222', '52', '0,1,52,5222,', '铜仁地区', '12785', '5222', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522201', '5222', '0,1,52,5222,522201,', '铜仁市', '12786', '522201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522222', '5222', '0,1,52,5222,522222,', '江口县', '12787', '522222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522223', '5222', '0,1,52,5222,522223,', '玉屏侗族自治县', '12788', '522223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522224', '5222', '0,1,52,5222,522224,', '石阡县', '12789', '522224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522225', '5222', '0,1,52,5222,522225,', '思南县', '12790', '522225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522226', '5222', '0,1,52,5222,522226,', '印江土家族苗族自治县', '12791', '522226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522227', '5222', '0,1,52,5222,522227,', '德江县', '12792', '522227', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522228', '5222', '0,1,52,5222,522228,', '沿河土家族自治县', '12793', '522228', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522229', '5222', '0,1,52,5222,522229,', '松桃苗族自治县', '12794', '522229', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522230', '5222', '0,1,52,5222,522230,', '万山特区', '12795', '522230', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5223', '52', '0,1,52,5223,', '黔西南布依族苗族自治', '12796', '5223', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522301', '5223', '0,1,52,5223,522301,', '兴义市', '12797', '522301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522322', '5223', '0,1,52,5223,522322,', '兴仁县', '12798', '522322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522323', '5223', '0,1,52,5223,522323,', '普安县', '12799', '522323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522324', '5223', '0,1,52,5223,522324,', '晴隆县', '12800', '522324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522325', '5223', '0,1,52,5223,522325,', '贞丰县', '12801', '522325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522326', '5223', '0,1,52,5223,522326,', '望谟县', '12802', '522326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522327', '5223', '0,1,52,5223,522327,', '册亨县', '12803', '522327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522328', '5223', '0,1,52,5223,522328,', '安龙县', '12804', '522328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5224', '52', '0,1,52,5224,', '毕节地区', '12805', '5224', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522401', '5224', '0,1,52,5224,522401,', '毕节市', '12806', '522401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522422', '5224', '0,1,52,5224,522422,', '大方县', '12807', '522422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522423', '5224', '0,1,52,5224,522423,', '黔西县', '12808', '522423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522424', '5224', '0,1,52,5224,522424,', '金沙县', '12809', '522424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522425', '5224', '0,1,52,5224,522425,', '织金县', '12810', '522425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522426', '5224', '0,1,52,5224,522426,', '纳雍县', '12811', '522426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522427', '5224', '0,1,52,5224,522427,', '威宁彝族回族苗族自治', '12812', '522427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522428', '5224', '0,1,52,5224,522428,', '赫章县', '12813', '522428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5226', '52', '0,1,52,5226,', '黔东南苗族侗族自治州', '12814', '5226', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522601', '5226', '0,1,52,5226,522601,', '凯里市', '12815', '522601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522622', '5226', '0,1,52,5226,522622,', '黄平县', '12816', '522622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522623', '5226', '0,1,52,5226,522623,', '施秉县', '12817', '522623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522624', '5226', '0,1,52,5226,522624,', '三穗县', '12818', '522624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522625', '5226', '0,1,52,5226,522625,', '镇远县', '12819', '522625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522626', '5226', '0,1,52,5226,522626,', '岑巩县', '12820', '522626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522627', '5226', '0,1,52,5226,522627,', '天柱县', '12821', '522627', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522628', '5226', '0,1,52,5226,522628,', '锦屏县', '12822', '522628', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522629', '5226', '0,1,52,5226,522629,', '剑河县', '12823', '522629', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522630', '5226', '0,1,52,5226,522630,', '台江县', '12824', '522630', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522631', '5226', '0,1,52,5226,522631,', '黎平县', '12825', '522631', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522632', '5226', '0,1,52,5226,522632,', '榕江县', '12826', '522632', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522633', '5226', '0,1,52,5226,522633,', '从江县', '12827', '522633', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522634', '5226', '0,1,52,5226,522634,', '雷山县', '12828', '522634', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522635', '5226', '0,1,52,5226,522635,', '麻江县', '12829', '522635', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522636', '5226', '0,1,52,5226,522636,', '丹寨县', '12830', '522636', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5227', '52', '0,1,52,5227,', '黔南布依族苗族自治州', '12831', '5227', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522701', '5227', '0,1,52,5227,522701,', '都匀市', '12832', '522701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522702', '5227', '0,1,52,5227,522702,', '福泉市', '12833', '522702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522722', '5227', '0,1,52,5227,522722,', '荔波县', '12834', '522722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522723', '5227', '0,1,52,5227,522723,', '贵定县', '12835', '522723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522725', '5227', '0,1,52,5227,522725,', '瓮安县', '12836', '522725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522726', '5227', '0,1,52,5227,522726,', '独山县', '12837', '522726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522727', '5227', '0,1,52,5227,522727,', '平塘县', '12838', '522727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522728', '5227', '0,1,52,5227,522728,', '罗甸县', '12839', '522728', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522729', '5227', '0,1,52,5227,522729,', '长顺县', '12840', '522729', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522730', '5227', '0,1,52,5227,522730,', '龙里县', '12841', '522730', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522731', '5227', '0,1,52,5227,522731,', '惠水县', '12842', '522731', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('522732', '5227', '0,1,52,5227,522732,', '三都水族自治县', '12843', '522732', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('53', '1', '0,1,53,', '云南', '12844', '53', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5301', '53', '0,1,53,5301,', '昆明', '12845', '5301', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530101', '5301', '0,1,53,5301,530101,', '市辖区', '12846', '530101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530102', '5301', '0,1,53,5301,530102,', '五华区', '12847', '530102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530103', '5301', '0,1,53,5301,530103,', '盘龙区', '12848', '530103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530111', '5301', '0,1,53,5301,530111,', '官渡区', '12849', '530111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530112', '5301', '0,1,53,5301,530112,', '西山区', '12850', '530112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530113', '5301', '0,1,53,5301,530113,', '东川区', '12851', '530113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530121', '5301', '0,1,53,5301,530121,', '呈贡县', '12852', '530121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530122', '5301', '0,1,53,5301,530122,', '晋宁县', '12853', '530122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530124', '5301', '0,1,53,5301,530124,', '富民县', '12854', '530124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530125', '5301', '0,1,53,5301,530125,', '宜良县', '12855', '530125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530126', '5301', '0,1,53,5301,530126,', '石林彝族自治县', '12856', '530126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530127', '5301', '0,1,53,5301,530127,', '嵩明县', '12857', '530127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530128', '5301', '0,1,53,5301,530128,', '禄劝彝族苗族自治县', '12858', '530128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530129', '5301', '0,1,53,5301,530129,', '寻甸回族彝族自治县', '12859', '530129', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530181', '5301', '0,1,53,5301,530181,', '安宁市', '12860', '530181', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5303', '53', '0,1,53,5303,', '曲靖', '12861', '5303', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530301', '5303', '0,1,53,5303,530301,', '市辖区', '12862', '530301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530302', '5303', '0,1,53,5303,530302,', '麒麟区', '12863', '530302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530321', '5303', '0,1,53,5303,530321,', '马龙县', '12864', '530321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530322', '5303', '0,1,53,5303,530322,', '陆良县', '12865', '530322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530323', '5303', '0,1,53,5303,530323,', '师宗县', '12866', '530323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530324', '5303', '0,1,53,5303,530324,', '罗平县', '12867', '530324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530325', '5303', '0,1,53,5303,530325,', '富源县', '12868', '530325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530326', '5303', '0,1,53,5303,530326,', '会泽县', '12869', '530326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530328', '5303', '0,1,53,5303,530328,', '沾益县', '12870', '530328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530381', '5303', '0,1,53,5303,530381,', '宣威市', '12871', '530381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5304', '53', '0,1,53,5304,', '玉溪', '12872', '5304', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530401', '5304', '0,1,53,5304,530401,', '市辖区', '12873', '530401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530402', '5304', '0,1,53,5304,530402,', '红塔区', '12874', '530402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530421', '5304', '0,1,53,5304,530421,', '江川县', '12875', '530421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530422', '5304', '0,1,53,5304,530422,', '澄江县', '12876', '530422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530423', '5304', '0,1,53,5304,530423,', '通海县', '12877', '530423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530424', '5304', '0,1,53,5304,530424,', '华宁县', '12878', '530424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530425', '5304', '0,1,53,5304,530425,', '易门县', '12879', '530425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530426', '5304', '0,1,53,5304,530426,', '峨山彝族自治县', '12880', '530426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530427', '5304', '0,1,53,5304,530427,', '新平彝族傣族自治县', '12881', '530427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530428', '5304', '0,1,53,5304,530428,', '元江哈尼族彝族傣族自', '12882', '530428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5305', '53', '0,1,53,5305,', '保山', '12883', '5305', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530501', '5305', '0,1,53,5305,530501,', '市辖区', '12884', '530501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530502', '5305', '0,1,53,5305,530502,', '隆阳区', '12885', '530502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530521', '5305', '0,1,53,5305,530521,', '施甸县', '12886', '530521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530522', '5305', '0,1,53,5305,530522,', '腾冲县', '12887', '530522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530523', '5305', '0,1,53,5305,530523,', '龙陵县', '12888', '530523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('530524', '5305', '0,1,53,5305,530524,', '昌宁县', '12889', '530524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5321', '53', '0,1,53,5321,', '昭通地区', '12890', '5321', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532101', '5321', '0,1,53,5321,532101,', '昭通市', '12891', '532101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532122', '5321', '0,1,53,5321,532122,', '鲁甸县', '12892', '532122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532123', '5321', '0,1,53,5321,532123,', '巧家县', '12893', '532123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532124', '5321', '0,1,53,5321,532124,', '盐津县', '12894', '532124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532125', '5321', '0,1,53,5321,532125,', '大关县', '12895', '532125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532126', '5321', '0,1,53,5321,532126,', '永善县', '12896', '532126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532127', '5321', '0,1,53,5321,532127,', '绥江县', '12897', '532127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532128', '5321', '0,1,53,5321,532128,', '镇雄县', '12898', '532128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532129', '5321', '0,1,53,5321,532129,', '彝良县', '12899', '532129', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532130', '5321', '0,1,53,5321,532130,', '威信县', '12900', '532130', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532131', '5321', '0,1,53,5321,532131,', '水富县', '12901', '532131', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5323', '53', '0,1,53,5323,', '楚雄彝族自治州', '12902', '5323', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532301', '5323', '0,1,53,5323,532301,', '楚雄市', '12903', '532301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532322', '5323', '0,1,53,5323,532322,', '双柏县', '12904', '532322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532323', '5323', '0,1,53,5323,532323,', '牟定县', '12905', '532323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532324', '5323', '0,1,53,5323,532324,', '南华县', '12906', '532324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532325', '5323', '0,1,53,5323,532325,', '姚安县', '12907', '532325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532326', '5323', '0,1,53,5323,532326,', '大姚县', '12908', '532326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532327', '5323', '0,1,53,5323,532327,', '永仁县', '12909', '532327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532328', '5323', '0,1,53,5323,532328,', '元谋县', '12910', '532328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532329', '5323', '0,1,53,5323,532329,', '武定县', '12911', '532329', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532331', '5323', '0,1,53,5323,532331,', '禄丰县', '12912', '532331', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5325', '53', '0,1,53,5325,', '红河哈尼族彝族自治州', '12913', '5325', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532501', '5325', '0,1,53,5325,532501,', '个旧市', '12914', '532501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532502', '5325', '0,1,53,5325,532502,', '开远市', '12915', '532502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532522', '5325', '0,1,53,5325,532522,', '蒙自县', '12916', '532522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532523', '5325', '0,1,53,5325,532523,', '屏边苗族自治县', '12917', '532523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532524', '5325', '0,1,53,5325,532524,', '建水县', '12918', '532524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532525', '5325', '0,1,53,5325,532525,', '石屏县', '12919', '532525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532526', '5325', '0,1,53,5325,532526,', '弥勒县', '12920', '532526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532527', '5325', '0,1,53,5325,532527,', '泸西县', '12921', '532527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532528', '5325', '0,1,53,5325,532528,', '元阳县', '12922', '532528', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532529', '5325', '0,1,53,5325,532529,', '红河县', '12923', '532529', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532530', '5325', '0,1,53,5325,532530,', '金平苗族瑶族傣族自治', '12924', '532530', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532531', '5325', '0,1,53,5325,532531,', '绿春县', '12925', '532531', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532532', '5325', '0,1,53,5325,532532,', '河口瑶族自治县', '12926', '532532', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5326', '53', '0,1,53,5326,', '文山壮族苗族自治州', '12927', '5326', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532621', '5326', '0,1,53,5326,532621,', '文山县', '12928', '532621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532622', '5326', '0,1,53,5326,532622,', '砚山县', '12929', '532622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532623', '5326', '0,1,53,5326,532623,', '西畴县', '12930', '532623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532624', '5326', '0,1,53,5326,532624,', '麻栗坡县', '12931', '532624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532625', '5326', '0,1,53,5326,532625,', '马关县', '12932', '532625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532626', '5326', '0,1,53,5326,532626,', '丘北县', '12933', '532626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532627', '5326', '0,1,53,5326,532627,', '广南县', '12934', '532627', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532628', '5326', '0,1,53,5326,532628,', '富宁县', '12935', '532628', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5327', '53', '0,1,53,5327,', '思茅地区', '12936', '5327', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532701', '5327', '0,1,53,5327,532701,', '思茅市', '12937', '532701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532722', '5327', '0,1,53,5327,532722,', '普洱哈尼族彝族自治县', '12938', '532722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532723', '5327', '0,1,53,5327,532723,', '墨江哈尼族自治县', '12939', '532723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532724', '5327', '0,1,53,5327,532724,', '景东彝族自治县', '12940', '532724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532725', '5327', '0,1,53,5327,532725,', '景谷傣族彝族自治县', '12941', '532725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532726', '5327', '0,1,53,5327,532726,', '镇沅彝族哈尼族拉祜族', '12942', '532726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532727', '5327', '0,1,53,5327,532727,', '江城哈尼族彝族自治县', '12943', '532727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532728', '5327', '0,1,53,5327,532728,', '孟连傣族拉祜族佤族自', '12944', '532728', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532729', '5327', '0,1,53,5327,532729,', '澜沧拉祜族自治县', '12945', '532729', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532730', '5327', '0,1,53,5327,532730,', '西盟佤族自治县', '12946', '532730', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5328', '53', '0,1,53,5328,', '西双版纳傣族自治州', '12947', '5328', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532801', '5328', '0,1,53,5328,532801,', '景洪市', '12948', '532801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532822', '5328', '0,1,53,5328,532822,', '勐海县', '12949', '532822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532823', '5328', '0,1,53,5328,532823,', '勐腊县', '12950', '532823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5329', '53', '0,1,53,5329,', '大理白族自治州', '12951', '5329', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532901', '5329', '0,1,53,5329,532901,', '大理市', '12952', '532901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532922', '5329', '0,1,53,5329,532922,', '漾濞彝族自治县', '12953', '532922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532923', '5329', '0,1,53,5329,532923,', '祥云县', '12954', '532923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532924', '5329', '0,1,53,5329,532924,', '宾川县', '12955', '532924', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532925', '5329', '0,1,53,5329,532925,', '弥渡县', '12956', '532925', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532926', '5329', '0,1,53,5329,532926,', '南涧彝族自治县', '12957', '532926', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532927', '5329', '0,1,53,5329,532927,', '巍山彝族回族自治县', '12958', '532927', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532928', '5329', '0,1,53,5329,532928,', '永平县', '12959', '532928', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532929', '5329', '0,1,53,5329,532929,', '云龙县', '12960', '532929', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532930', '5329', '0,1,53,5329,532930,', '洱源县', '12961', '532930', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532931', '5329', '0,1,53,5329,532931,', '剑川县', '12962', '532931', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('532932', '5329', '0,1,53,5329,532932,', '鹤庆县', '12963', '532932', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5331', '53', '0,1,53,5331,', '德宏傣族景颇族自治州', '12964', '5331', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533102', '5331', '0,1,53,5331,533102,', '瑞丽市', '12965', '533102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533103', '5331', '0,1,53,5331,533103,', '潞西市', '12966', '533103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533122', '5331', '0,1,53,5331,533122,', '梁河县', '12967', '533122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533123', '5331', '0,1,53,5331,533123,', '盈江县', '12968', '533123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533124', '5331', '0,1,53,5331,533124,', '陇川县', '12969', '533124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5332', '53', '0,1,53,5332,', '丽江地区', '12970', '5332', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533221', '5332', '0,1,53,5332,533221,', '丽江纳西族自治县', '12971', '533221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533222', '5332', '0,1,53,5332,533222,', '永胜县', '12972', '533222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533223', '5332', '0,1,53,5332,533223,', '华坪县', '12973', '533223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533224', '5332', '0,1,53,5332,533224,', '宁蒗彝族自治县', '12974', '533224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5333', '53', '0,1,53,5333,', '怒江傈僳族自治州', '12975', '5333', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533321', '5333', '0,1,53,5333,533321,', '泸水县', '12976', '533321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533323', '5333', '0,1,53,5333,533323,', '福贡县', '12977', '533323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533324', '5333', '0,1,53,5333,533324,', '贡山独龙族怒族自治县', '12978', '533324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533325', '5333', '0,1,53,5333,533325,', '兰坪白族普米族自治县', '12979', '533325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5334', '53', '0,1,53,5334,', '迪庆藏族自治州', '12980', '5334', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533421', '5334', '0,1,53,5334,533421,', '中甸县', '12981', '533421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533422', '5334', '0,1,53,5334,533422,', '德钦县', '12982', '533422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533423', '5334', '0,1,53,5334,533423,', '维西傈僳族自治县', '12983', '533423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5335', '53', '0,1,53,5335,', '临沧地区', '12984', '5335', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533521', '5335', '0,1,53,5335,533521,', '临沧县', '12985', '533521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533522', '5335', '0,1,53,5335,533522,', '凤庆县', '12986', '533522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533523', '5335', '0,1,53,5335,533523,', '云  县', '12987', '533523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533524', '5335', '0,1,53,5335,533524,', '永德县', '12988', '533524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533525', '5335', '0,1,53,5335,533525,', '镇康县', '12989', '533525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533526', '5335', '0,1,53,5335,533526,', '双江拉祜族佤族布朗族', '12990', '533526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533527', '5335', '0,1,53,5335,533527,', '耿马傣族佤族自治县', '12991', '533527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('533528', '5335', '0,1,53,5335,533528,', '沧源佤族自治县', '12992', '533528', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('54', '1', '0,1,54,', '西藏', '12993', '54', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5401', '54', '0,1,54,5401,', '拉萨', '12994', '5401', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('540101', '5401', '0,1,54,5401,540101,', '市辖区', '12995', '540101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('540102', '5401', '0,1,54,5401,540102,', '城关区', '12996', '540102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('540121', '5401', '0,1,54,5401,540121,', '林周县', '12997', '540121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('540122', '5401', '0,1,54,5401,540122,', '当雄县', '12998', '540122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('540123', '5401', '0,1,54,5401,540123,', '尼木县', '12999', '540123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('540124', '5401', '0,1,54,5401,540124,', '曲水县', '13000', '540124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('540125', '5401', '0,1,54,5401,540125,', '堆龙德庆县', '13001', '540125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('540126', '5401', '0,1,54,5401,540126,', '达孜县', '13002', '540126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('540127', '5401', '0,1,54,5401,540127,', '墨竹工卡县', '13003', '540127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5421', '54', '0,1,54,5421,', '昌都地区', '13004', '5421', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542121', '5421', '0,1,54,5421,542121,', '昌都县', '13005', '542121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542122', '5421', '0,1,54,5421,542122,', '江达县', '13006', '542122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542123', '5421', '0,1,54,5421,542123,', '贡觉县', '13007', '542123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542124', '5421', '0,1,54,5421,542124,', '类乌齐县', '13008', '542124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542125', '5421', '0,1,54,5421,542125,', '丁青县', '13009', '542125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542126', '5421', '0,1,54,5421,542126,', '察雅县', '13010', '542126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542127', '5421', '0,1,54,5421,542127,', '八宿县', '13011', '542127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542128', '5421', '0,1,54,5421,542128,', '左贡县', '13012', '542128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542129', '5421', '0,1,54,5421,542129,', '芒康县', '13013', '542129', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542132', '5421', '0,1,54,5421,542132,', '洛隆县', '13014', '542132', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542133', '5421', '0,1,54,5421,542133,', '边坝县', '13015', '542133', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5422', '54', '0,1,54,5422,', '山南地区', '13016', '5422', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542221', '5422', '0,1,54,5422,542221,', '乃东县', '13017', '542221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542222', '5422', '0,1,54,5422,542222,', '扎囊县', '13018', '542222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542223', '5422', '0,1,54,5422,542223,', '贡嘎县', '13019', '542223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542224', '5422', '0,1,54,5422,542224,', '桑日县', '13020', '542224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542225', '5422', '0,1,54,5422,542225,', '琼结县', '13021', '542225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542226', '5422', '0,1,54,5422,542226,', '曲松县', '13022', '542226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542227', '5422', '0,1,54,5422,542227,', '措美县', '13023', '542227', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542228', '5422', '0,1,54,5422,542228,', '洛扎县', '13024', '542228', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542229', '5422', '0,1,54,5422,542229,', '加查县', '13025', '542229', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542231', '5422', '0,1,54,5422,542231,', '隆子县', '13026', '542231', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542232', '5422', '0,1,54,5422,542232,', '错那县', '13027', '542232', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542233', '5422', '0,1,54,5422,542233,', '浪卡子县', '13028', '542233', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5423', '54', '0,1,54,5423,', '日喀则地区', '13029', '5423', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542301', '5423', '0,1,54,5423,542301,', '日喀则市', '13030', '542301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542322', '5423', '0,1,54,5423,542322,', '南木林县', '13031', '542322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542323', '5423', '0,1,54,5423,542323,', '江孜县', '13032', '542323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542324', '5423', '0,1,54,5423,542324,', '定日县', '13033', '542324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542325', '5423', '0,1,54,5423,542325,', '萨迦县', '13034', '542325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542326', '5423', '0,1,54,5423,542326,', '拉孜县', '13035', '542326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542327', '5423', '0,1,54,5423,542327,', '昂仁县', '13036', '542327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542328', '5423', '0,1,54,5423,542328,', '谢通门县', '13037', '542328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542329', '5423', '0,1,54,5423,542329,', '白朗县', '13038', '542329', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542330', '5423', '0,1,54,5423,542330,', '仁布县', '13039', '542330', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542331', '5423', '0,1,54,5423,542331,', '康马县', '13040', '542331', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542332', '5423', '0,1,54,5423,542332,', '定结县', '13041', '542332', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542333', '5423', '0,1,54,5423,542333,', '仲巴县', '13042', '542333', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542334', '5423', '0,1,54,5423,542334,', '亚东县', '13043', '542334', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542335', '5423', '0,1,54,5423,542335,', '吉隆县', '13044', '542335', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542336', '5423', '0,1,54,5423,542336,', '聂拉木县', '13045', '542336', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542337', '5423', '0,1,54,5423,542337,', '萨嘎县', '13046', '542337', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542338', '5423', '0,1,54,5423,542338,', '岗巴县', '13047', '542338', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5424', '54', '0,1,54,5424,', '那曲地区', '13048', '5424', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542421', '5424', '0,1,54,5424,542421,', '那曲县', '13049', '542421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542422', '5424', '0,1,54,5424,542422,', '嘉黎县', '13050', '542422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542423', '5424', '0,1,54,5424,542423,', '比如县', '13051', '542423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542424', '5424', '0,1,54,5424,542424,', '聂荣县', '13052', '542424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542425', '5424', '0,1,54,5424,542425,', '安多县', '13053', '542425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542426', '5424', '0,1,54,5424,542426,', '申扎县', '13054', '542426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542427', '5424', '0,1,54,5424,542427,', '索  县', '13055', '542427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542428', '5424', '0,1,54,5424,542428,', '班戈县', '13056', '542428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542429', '5424', '0,1,54,5424,542429,', '巴青县', '13057', '542429', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542430', '5424', '0,1,54,5424,542430,', '尼玛县', '13058', '542430', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5425', '54', '0,1,54,5425,', '阿里地区', '13059', '5425', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542521', '5425', '0,1,54,5425,542521,', '普兰县', '13060', '542521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542522', '5425', '0,1,54,5425,542522,', '札达县', '13061', '542522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542523', '5425', '0,1,54,5425,542523,', '噶尔县', '13062', '542523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542524', '5425', '0,1,54,5425,542524,', '日土县', '13063', '542524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542525', '5425', '0,1,54,5425,542525,', '革吉县', '13064', '542525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542526', '5425', '0,1,54,5425,542526,', '改则县', '13065', '542526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542527', '5425', '0,1,54,5425,542527,', '措勤县', '13066', '542527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('5426', '54', '0,1,54,5426,', '林芝地区', '13067', '5426', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542621', '5426', '0,1,54,5426,542621,', '林芝县', '13068', '542621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542622', '5426', '0,1,54,5426,542622,', '工布江达县', '13069', '542622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542623', '5426', '0,1,54,5426,542623,', '米林县', '13070', '542623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542624', '5426', '0,1,54,5426,542624,', '墨脱县', '13071', '542624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542625', '5426', '0,1,54,5426,542625,', '波密县', '13072', '542625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542626', '5426', '0,1,54,5426,542626,', '察隅县', '13073', '542626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('542627', '5426', '0,1,54,5426,542627,', '朗  县', '13074', '542627', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('61', '1', '0,1,61,', '陕西', '13075', '61', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6101', '61', '0,1,61,6101,', '西安', '13076', '6101', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610101', '6101', '0,1,61,6101,610101,', '市辖区', '13077', '610101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610102', '6101', '0,1,61,6101,610102,', '新城区', '13078', '610102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610103', '6101', '0,1,61,6101,610103,', '碑林区', '13079', '610103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610104', '6101', '0,1,61,6101,610104,', '莲湖区', '13080', '610104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610111', '6101', '0,1,61,6101,610111,', '灞桥区', '13081', '610111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610112', '6101', '0,1,61,6101,610112,', '未央区', '13082', '610112', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610113', '6101', '0,1,61,6101,610113,', '雁塔区', '13083', '610113', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610114', '6101', '0,1,61,6101,610114,', '阎良区', '13084', '610114', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610115', '6101', '0,1,61,6101,610115,', '临潼区', '13085', '610115', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610121', '6101', '0,1,61,6101,610121,', '长安县', '13086', '610121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610122', '6101', '0,1,61,6101,610122,', '蓝田县', '13087', '610122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610124', '6101', '0,1,61,6101,610124,', '周至县', '13088', '610124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610125', '6101', '0,1,61,6101,610125,', '户  县', '13089', '610125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610126', '6101', '0,1,61,6101,610126,', '高陵县', '13090', '610126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6102', '61', '0,1,61,6102,', '铜川', '13091', '6102', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610201', '6102', '0,1,61,6102,610201,', '市辖区', '13092', '610201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610202', '6102', '0,1,61,6102,610202,', '王益区', '13093', '610202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610203', '6102', '0,1,61,6102,610203,', '印台区', '13094', '610203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610221', '6102', '0,1,61,6102,610221,', '耀  县', '13095', '610221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610222', '6102', '0,1,61,6102,610222,', '宜君县', '13096', '610222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6103', '61', '0,1,61,6103,', '宝鸡', '13097', '6103', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610301', '6103', '0,1,61,6103,610301,', '市辖区', '13098', '610301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610302', '6103', '0,1,61,6103,610302,', '渭滨区', '13099', '610302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610303', '6103', '0,1,61,6103,610303,', '金台区', '13100', '610303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610321', '6103', '0,1,61,6103,610321,', '宝鸡县', '13101', '610321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610322', '6103', '0,1,61,6103,610322,', '凤翔县', '13102', '610322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610323', '6103', '0,1,61,6103,610323,', '岐山县', '13103', '610323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610324', '6103', '0,1,61,6103,610324,', '扶风县', '13104', '610324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610326', '6103', '0,1,61,6103,610326,', '眉  县', '13105', '610326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610327', '6103', '0,1,61,6103,610327,', '陇  县', '13106', '610327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610328', '6103', '0,1,61,6103,610328,', '千阳县', '13107', '610328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610329', '6103', '0,1,61,6103,610329,', '麟游县', '13108', '610329', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610330', '6103', '0,1,61,6103,610330,', '凤  县', '13109', '610330', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610331', '6103', '0,1,61,6103,610331,', '太白县', '13110', '610331', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6104', '61', '0,1,61,6104,', '咸阳', '13111', '6104', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610401', '6104', '0,1,61,6104,610401,', '市辖区', '13112', '610401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610402', '6104', '0,1,61,6104,610402,', '秦都区', '13113', '610402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610403', '6104', '0,1,61,6104,610403,', '杨陵区', '13114', '610403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610404', '6104', '0,1,61,6104,610404,', '渭城区', '13115', '610404', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610422', '6104', '0,1,61,6104,610422,', '三原县', '13116', '610422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610423', '6104', '0,1,61,6104,610423,', '泾阳县', '13117', '610423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610424', '6104', '0,1,61,6104,610424,', '乾  县', '13118', '610424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610425', '6104', '0,1,61,6104,610425,', '礼泉县', '13119', '610425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610426', '6104', '0,1,61,6104,610426,', '永寿县', '13120', '610426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610427', '6104', '0,1,61,6104,610427,', '彬  县', '13121', '610427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610428', '6104', '0,1,61,6104,610428,', '长武县', '13122', '610428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610429', '6104', '0,1,61,6104,610429,', '旬邑县', '13123', '610429', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610430', '6104', '0,1,61,6104,610430,', '淳化县', '13124', '610430', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610431', '6104', '0,1,61,6104,610431,', '武功县', '13125', '610431', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610481', '6104', '0,1,61,6104,610481,', '兴平市', '13126', '610481', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6105', '61', '0,1,61,6105,', '渭南', '13127', '6105', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610501', '6105', '0,1,61,6105,610501,', '市辖区', '13128', '610501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610502', '6105', '0,1,61,6105,610502,', '临渭区', '13129', '610502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610521', '6105', '0,1,61,6105,610521,', '华  县', '13130', '610521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610522', '6105', '0,1,61,6105,610522,', '潼关县', '13131', '610522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610523', '6105', '0,1,61,6105,610523,', '大荔县', '13132', '610523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610524', '6105', '0,1,61,6105,610524,', '合阳县', '13133', '610524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610525', '6105', '0,1,61,6105,610525,', '澄城县', '13134', '610525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610526', '6105', '0,1,61,6105,610526,', '蒲城县', '13135', '610526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610527', '6105', '0,1,61,6105,610527,', '白水县', '13136', '610527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610528', '6105', '0,1,61,6105,610528,', '富平县', '13137', '610528', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610581', '6105', '0,1,61,6105,610581,', '韩城市', '13138', '610581', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610582', '6105', '0,1,61,6105,610582,', '华阴市', '13139', '610582', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6106', '61', '0,1,61,6106,', '延安', '13140', '6106', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610601', '6106', '0,1,61,6106,610601,', '市辖区', '13141', '610601', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610602', '6106', '0,1,61,6106,610602,', '宝塔区', '13142', '610602', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610621', '6106', '0,1,61,6106,610621,', '延长县', '13143', '610621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610622', '6106', '0,1,61,6106,610622,', '延川县', '13144', '610622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610623', '6106', '0,1,61,6106,610623,', '子长县', '13145', '610623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610624', '6106', '0,1,61,6106,610624,', '安塞县', '13146', '610624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610625', '6106', '0,1,61,6106,610625,', '志丹县', '13147', '610625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610626', '6106', '0,1,61,6106,610626,', '吴旗县', '13148', '610626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610627', '6106', '0,1,61,6106,610627,', '甘泉县', '13149', '610627', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610628', '6106', '0,1,61,6106,610628,', '富  县', '13150', '610628', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610629', '6106', '0,1,61,6106,610629,', '洛川县', '13151', '610629', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610630', '6106', '0,1,61,6106,610630,', '宜川县', '13152', '610630', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610631', '6106', '0,1,61,6106,610631,', '黄龙县', '13153', '610631', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610632', '6106', '0,1,61,6106,610632,', '黄陵县', '13154', '610632', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6107', '61', '0,1,61,6107,', '汉中', '13155', '6107', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610701', '6107', '0,1,61,6107,610701,', '市辖区', '13156', '610701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610702', '6107', '0,1,61,6107,610702,', '汉台区', '13157', '610702', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610721', '6107', '0,1,61,6107,610721,', '南郑县', '13158', '610721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610722', '6107', '0,1,61,6107,610722,', '城固县', '13159', '610722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610723', '6107', '0,1,61,6107,610723,', '洋  县', '13160', '610723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610724', '6107', '0,1,61,6107,610724,', '西乡县', '13161', '610724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610725', '6107', '0,1,61,6107,610725,', '勉  县', '13162', '610725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610726', '6107', '0,1,61,6107,610726,', '宁强县', '13163', '610726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610727', '6107', '0,1,61,6107,610727,', '略阳县', '13164', '610727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610728', '6107', '0,1,61,6107,610728,', '镇巴县', '13165', '610728', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610729', '6107', '0,1,61,6107,610729,', '留坝县', '13166', '610729', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610730', '6107', '0,1,61,6107,610730,', '佛坪县', '13167', '610730', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6108', '61', '0,1,61,6108,', '榆林', '13168', '6108', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610801', '6108', '0,1,61,6108,610801,', '市辖区', '13169', '610801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610802', '6108', '0,1,61,6108,610802,', '榆阳区', '13170', '610802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610821', '6108', '0,1,61,6108,610821,', '神木县', '13171', '610821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610822', '6108', '0,1,61,6108,610822,', '府谷县', '13172', '610822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610823', '6108', '0,1,61,6108,610823,', '横山县', '13173', '610823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610824', '6108', '0,1,61,6108,610824,', '靖边县', '13174', '610824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610825', '6108', '0,1,61,6108,610825,', '定边县', '13175', '610825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610826', '6108', '0,1,61,6108,610826,', '绥德县', '13176', '610826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610827', '6108', '0,1,61,6108,610827,', '米脂县', '13177', '610827', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610828', '6108', '0,1,61,6108,610828,', '佳  县', '13178', '610828', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610829', '6108', '0,1,61,6108,610829,', '吴堡县', '13179', '610829', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610830', '6108', '0,1,61,6108,610830,', '清涧县', '13180', '610830', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610831', '6108', '0,1,61,6108,610831,', '子洲县', '13181', '610831', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6109', '61', '0,1,61,6109,', '安康', '13182', '6109', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610901', '6109', '0,1,61,6109,610901,', '市辖区', '13183', '610901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610902', '6109', '0,1,61,6109,610902,', '汉滨区', '13184', '610902', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610921', '6109', '0,1,61,6109,610921,', '汉阴县', '13185', '610921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610922', '6109', '0,1,61,6109,610922,', '石泉县', '13186', '610922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610923', '6109', '0,1,61,6109,610923,', '宁陕县', '13187', '610923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610924', '6109', '0,1,61,6109,610924,', '紫阳县', '13188', '610924', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610925', '6109', '0,1,61,6109,610925,', '岚皋县', '13189', '610925', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610926', '6109', '0,1,61,6109,610926,', '平利县', '13190', '610926', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610927', '6109', '0,1,61,6109,610927,', '镇坪县', '13191', '610927', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610928', '6109', '0,1,61,6109,610928,', '旬阳县', '13192', '610928', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('610929', '6109', '0,1,61,6109,610929,', '白河县', '13193', '610929', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6125', '61', '0,1,61,6125,', '商洛地区', '13194', '6125', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('612501', '6125', '0,1,61,6125,612501,', '商州市', '13195', '612501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('612522', '6125', '0,1,61,6125,612522,', '洛南县', '13196', '612522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('612523', '6125', '0,1,61,6125,612523,', '丹凤县', '13197', '612523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('612524', '6125', '0,1,61,6125,612524,', '商南县', '13198', '612524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('612525', '6125', '0,1,61,6125,612525,', '山阳县', '13199', '612525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('612526', '6125', '0,1,61,6125,612526,', '镇安县', '13200', '612526', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('612527', '6125', '0,1,61,6125,612527,', '柞水县', '13201', '612527', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('62', '1', '0,1,62,', '甘肃', '13202', '62', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6201', '62', '0,1,62,6201,', '兰州', '13203', '6201', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620101', '6201', '0,1,62,6201,620101,', '市辖区', '13204', '620101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620102', '6201', '0,1,62,6201,620102,', '城关区', '13205', '620102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620103', '6201', '0,1,62,6201,620103,', '七里河区', '13206', '620103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620104', '6201', '0,1,62,6201,620104,', '西固区', '13207', '620104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620105', '6201', '0,1,62,6201,620105,', '安宁区', '13208', '620105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620111', '6201', '0,1,62,6201,620111,', '红古区', '13209', '620111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620121', '6201', '0,1,62,6201,620121,', '永登县', '13210', '620121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620122', '6201', '0,1,62,6201,620122,', '皋兰县', '13211', '620122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620123', '6201', '0,1,62,6201,620123,', '榆中县', '13212', '620123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6202', '62', '0,1,62,6202,', '嘉峪关', '13213', '6202', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620201', '6202', '0,1,62,6202,620201,', '市辖区', '13214', '620201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6203', '62', '0,1,62,6203,', '金昌', '13215', '6203', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620301', '6203', '0,1,62,6203,620301,', '市辖区', '13216', '620301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620302', '6203', '0,1,62,6203,620302,', '金川区', '13217', '620302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620321', '6203', '0,1,62,6203,620321,', '永昌县', '13218', '620321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6204', '62', '0,1,62,6204,', '白银', '13219', '6204', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620401', '6204', '0,1,62,6204,620401,', '市辖区', '13220', '620401', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620402', '6204', '0,1,62,6204,620402,', '白银区', '13221', '620402', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620403', '6204', '0,1,62,6204,620403,', '平川区', '13222', '620403', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620421', '6204', '0,1,62,6204,620421,', '靖远县', '13223', '620421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620422', '6204', '0,1,62,6204,620422,', '会宁县', '13224', '620422', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620423', '6204', '0,1,62,6204,620423,', '景泰县', '13225', '620423', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6205', '62', '0,1,62,6205,', '天水', '13226', '6205', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620501', '6205', '0,1,62,6205,620501,', '市辖区', '13227', '620501', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620502', '6205', '0,1,62,6205,620502,', '秦城区', '13228', '620502', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620503', '6205', '0,1,62,6205,620503,', '北道区', '13229', '620503', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620521', '6205', '0,1,62,6205,620521,', '清水县', '13230', '620521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620522', '6205', '0,1,62,6205,620522,', '秦安县', '13231', '620522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620523', '6205', '0,1,62,6205,620523,', '甘谷县', '13232', '620523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620524', '6205', '0,1,62,6205,620524,', '武山县', '13233', '620524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('620525', '6205', '0,1,62,6205,620525,', '张家川回族自治县', '13234', '620525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6221', '62', '0,1,62,6221,', '酒泉地区', '13235', '6221', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622101', '6221', '0,1,62,6221,622101,', '玉门市', '13236', '622101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622102', '6221', '0,1,62,6221,622102,', '酒泉市', '13237', '622102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622103', '6221', '0,1,62,6221,622103,', '敦煌市', '13238', '622103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622123', '6221', '0,1,62,6221,622123,', '金塔县', '13239', '622123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622124', '6221', '0,1,62,6221,622124,', '肃北蒙古族自治县', '13240', '622124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622125', '6221', '0,1,62,6221,622125,', '阿克塞哈萨克族自治县', '13241', '622125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622126', '6221', '0,1,62,6221,622126,', '安西县', '13242', '622126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6222', '62', '0,1,62,6222,', '张掖地区', '13243', '6222', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622201', '6222', '0,1,62,6222,622201,', '张掖市', '13244', '622201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622222', '6222', '0,1,62,6222,622222,', '肃南裕固族自治县', '13245', '622222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622223', '6222', '0,1,62,6222,622223,', '民乐县', '13246', '622223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622224', '6222', '0,1,62,6222,622224,', '临泽县', '13247', '622224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622225', '6222', '0,1,62,6222,622225,', '高台县', '13248', '622225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622226', '6222', '0,1,62,6222,622226,', '山丹县', '13249', '622226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6223', '62', '0,1,62,6223,', '武威地区', '13250', '6223', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622301', '6223', '0,1,62,6223,622301,', '武威市', '13251', '622301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622322', '6223', '0,1,62,6223,622322,', '民勤县', '13252', '622322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622323', '6223', '0,1,62,6223,622323,', '古浪县', '13253', '622323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622326', '6223', '0,1,62,6223,622326,', '天祝藏族自治县', '13254', '622326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6224', '62', '0,1,62,6224,', '定西地区', '13255', '6224', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622421', '6224', '0,1,62,6224,622421,', '定西县', '13256', '622421', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622424', '6224', '0,1,62,6224,622424,', '通渭县', '13257', '622424', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622425', '6224', '0,1,62,6224,622425,', '陇西县', '13258', '622425', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622426', '6224', '0,1,62,6224,622426,', '渭源县', '13259', '622426', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622427', '6224', '0,1,62,6224,622427,', '临洮县', '13260', '622427', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622428', '6224', '0,1,62,6224,622428,', '漳  县', '13261', '622428', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622429', '6224', '0,1,62,6224,622429,', '岷  县', '13262', '622429', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6226', '62', '0,1,62,6226,', '陇南地区', '13263', '6226', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622621', '6226', '0,1,62,6226,622621,', '武都县', '13264', '622621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622623', '6226', '0,1,62,6226,622623,', '宕昌县', '13265', '622623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622624', '6226', '0,1,62,6226,622624,', '成  县', '13266', '622624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622625', '6226', '0,1,62,6226,622625,', '康  县', '13267', '622625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622626', '6226', '0,1,62,6226,622626,', '文  县', '13268', '622626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622627', '6226', '0,1,62,6226,622627,', '西和县', '13269', '622627', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622628', '6226', '0,1,62,6226,622628,', '礼  县', '13270', '622628', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622629', '6226', '0,1,62,6226,622629,', '两当县', '13271', '622629', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622630', '6226', '0,1,62,6226,622630,', '徽  县', '13272', '622630', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6227', '62', '0,1,62,6227,', '平凉地区', '13273', '6227', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622701', '6227', '0,1,62,6227,622701,', '平凉市', '13274', '622701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622722', '6227', '0,1,62,6227,622722,', '泾川县', '13275', '622722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622723', '6227', '0,1,62,6227,622723,', '灵台县', '13276', '622723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622724', '6227', '0,1,62,6227,622724,', '崇信县', '13277', '622724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622725', '6227', '0,1,62,6227,622725,', '华亭县', '13278', '622725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622726', '6227', '0,1,62,6227,622726,', '庄浪县', '13279', '622726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622727', '6227', '0,1,62,6227,622727,', '静宁县', '13280', '622727', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6228', '62', '0,1,62,6228,', '庆阳地区', '13281', '6228', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622801', '6228', '0,1,62,6228,622801,', '西峰市', '13282', '622801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622821', '6228', '0,1,62,6228,622821,', '庆阳县', '13283', '622821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622822', '6228', '0,1,62,6228,622822,', '环  县', '13284', '622822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622823', '6228', '0,1,62,6228,622823,', '华池县', '13285', '622823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622824', '6228', '0,1,62,6228,622824,', '合水县', '13286', '622824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622825', '6228', '0,1,62,6228,622825,', '正宁县', '13287', '622825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622826', '6228', '0,1,62,6228,622826,', '宁  县', '13288', '622826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622827', '6228', '0,1,62,6228,622827,', '镇原县', '13289', '622827', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6229', '62', '0,1,62,6229,', '临夏回族自治州', '13290', '6229', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622901', '6229', '0,1,62,6229,622901,', '临夏市', '13291', '622901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622921', '6229', '0,1,62,6229,622921,', '临夏县', '13292', '622921', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622922', '6229', '0,1,62,6229,622922,', '康乐县', '13293', '622922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622923', '6229', '0,1,62,6229,622923,', '永靖县', '13294', '622923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622924', '6229', '0,1,62,6229,622924,', '广河县', '13295', '622924', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622925', '6229', '0,1,62,6229,622925,', '和政县', '13296', '622925', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622926', '6229', '0,1,62,6229,622926,', '东乡族自治县', '13297', '622926', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('622927', '6229', '0,1,62,6229,622927,', '积石山保安族东乡族撒', '13298', '622927', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6230', '62', '0,1,62,6230,', '甘南藏族自治州', '13299', '6230', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('623001', '6230', '0,1,62,6230,623001,', '合作市', '13300', '623001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('623021', '6230', '0,1,62,6230,623021,', '临潭县', '13301', '623021', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('623022', '6230', '0,1,62,6230,623022,', '卓尼县', '13302', '623022', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('623023', '6230', '0,1,62,6230,623023,', '舟曲县', '13303', '623023', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('623024', '6230', '0,1,62,6230,623024,', '迭部县', '13304', '623024', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('623025', '6230', '0,1,62,6230,623025,', '玛曲县', '13305', '623025', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('623026', '6230', '0,1,62,6230,623026,', '碌曲县', '13306', '623026', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('623027', '6230', '0,1,62,6230,623027,', '夏河县', '13307', '623027', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('63', '1', '0,1,63,', '青海', '13308', '63', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6301', '63', '0,1,63,6301,', '西宁', '13309', '6301', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('630101', '6301', '0,1,63,6301,630101,', '市辖区', '13310', '630101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('630102', '6301', '0,1,63,6301,630102,', '城东区', '13311', '630102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('630103', '6301', '0,1,63,6301,630103,', '城中区', '13312', '630103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('630104', '6301', '0,1,63,6301,630104,', '城西区', '13313', '630104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('630105', '6301', '0,1,63,6301,630105,', '城北区', '13314', '630105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('630121', '6301', '0,1,63,6301,630121,', '大通回族土族自治县', '13315', '630121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('630122', '6301', '0,1,63,6301,630122,', '湟中县', '13316', '630122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('630123', '6301', '0,1,63,6301,630123,', '湟源县', '13317', '630123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6321', '63', '0,1,63,6321,', '海东地区', '13318', '6321', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632121', '6321', '0,1,63,6321,632121,', '平安县', '13319', '632121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632122', '6321', '0,1,63,6321,632122,', '民和回族土族自治县', '13320', '632122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632123', '6321', '0,1,63,6321,632123,', '乐都县', '13321', '632123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632126', '6321', '0,1,63,6321,632126,', '互助土族自治县', '13322', '632126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632127', '6321', '0,1,63,6321,632127,', '化隆回族自治县', '13323', '632127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632128', '6321', '0,1,63,6321,632128,', '循化撒拉族自治县', '13324', '632128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6322', '63', '0,1,63,6322,', '海北藏族自治州', '13325', '6322', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632221', '6322', '0,1,63,6322,632221,', '门源回族自治县', '13326', '632221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632222', '6322', '0,1,63,6322,632222,', '祁连县', '13327', '632222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632223', '6322', '0,1,63,6322,632223,', '海晏县', '13328', '632223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632224', '6322', '0,1,63,6322,632224,', '刚察县', '13329', '632224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6323', '63', '0,1,63,6323,', '黄南藏族自治州', '13330', '6323', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632321', '6323', '0,1,63,6323,632321,', '同仁县', '13331', '632321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632322', '6323', '0,1,63,6323,632322,', '尖扎县', '13332', '632322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632323', '6323', '0,1,63,6323,632323,', '泽库县', '13333', '632323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632324', '6323', '0,1,63,6323,632324,', '河南蒙古族自治县', '13334', '632324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6325', '63', '0,1,63,6325,', '海南藏族自治州', '13335', '6325', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632521', '6325', '0,1,63,6325,632521,', '共和县', '13336', '632521', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632522', '6325', '0,1,63,6325,632522,', '同德县', '13337', '632522', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632523', '6325', '0,1,63,6325,632523,', '贵德县', '13338', '632523', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632524', '6325', '0,1,63,6325,632524,', '兴海县', '13339', '632524', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632525', '6325', '0,1,63,6325,632525,', '贵南县', '13340', '632525', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6326', '63', '0,1,63,6326,', '果洛藏族自治州', '13341', '6326', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632621', '6326', '0,1,63,6326,632621,', '玛沁县', '13342', '632621', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632622', '6326', '0,1,63,6326,632622,', '班玛县', '13343', '632622', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632623', '6326', '0,1,63,6326,632623,', '甘德县', '13344', '632623', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632624', '6326', '0,1,63,6326,632624,', '达日县', '13345', '632624', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632625', '6326', '0,1,63,6326,632625,', '久治县', '13346', '632625', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632626', '6326', '0,1,63,6326,632626,', '玛多县', '13347', '632626', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6327', '63', '0,1,63,6327,', '玉树藏族自治州', '13348', '6327', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632721', '6327', '0,1,63,6327,632721,', '玉树县', '13349', '632721', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632722', '6327', '0,1,63,6327,632722,', '杂多县', '13350', '632722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632723', '6327', '0,1,63,6327,632723,', '称多县', '13351', '632723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632724', '6327', '0,1,63,6327,632724,', '治多县', '13352', '632724', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632725', '6327', '0,1,63,6327,632725,', '囊谦县', '13353', '632725', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632726', '6327', '0,1,63,6327,632726,', '曲麻莱县', '13354', '632726', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6328', '63', '0,1,63,6328,', '海西蒙古族藏族自治州', '13355', '6328', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632801', '6328', '0,1,63,6328,632801,', '格尔木市', '13356', '632801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632802', '6328', '0,1,63,6328,632802,', '德令哈市', '13357', '632802', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632821', '6328', '0,1,63,6328,632821,', '乌兰县', '13358', '632821', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632822', '6328', '0,1,63,6328,632822,', '都兰县', '13359', '632822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('632823', '6328', '0,1,63,6328,632823,', '天峻县', '13360', '632823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('64', '1', '0,1,64,', '宁夏回族自治区', '13361', '64', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6401', '64', '0,1,64,6401,', '银川', '13362', '6401', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640101', '6401', '0,1,64,6401,640101,', '市辖区', '13363', '640101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640102', '6401', '0,1,64,6401,640102,', '城  区', '13364', '640102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640103', '6401', '0,1,64,6401,640103,', '新城区', '13365', '640103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640111', '6401', '0,1,64,6401,640111,', '郊  区', '13366', '640111', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640121', '6401', '0,1,64,6401,640121,', '永宁县', '13367', '640121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640122', '6401', '0,1,64,6401,640122,', '贺兰县', '13368', '640122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6402', '64', '0,1,64,6402,', '石嘴山', '13369', '6402', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640201', '6402', '0,1,64,6402,640201,', '市辖区', '13370', '640201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640202', '6402', '0,1,64,6402,640202,', '大武口区', '13371', '640202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640203', '6402', '0,1,64,6402,640203,', '石嘴山区', '13372', '640203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640204', '6402', '0,1,64,6402,640204,', '石炭井区', '13373', '640204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640221', '6402', '0,1,64,6402,640221,', '平罗县', '13374', '640221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640222', '6402', '0,1,64,6402,640222,', '陶乐县', '13375', '640222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640223', '6402', '0,1,64,6402,640223,', '惠农县', '13376', '640223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6403', '64', '0,1,64,6403,', '吴忠', '13377', '6403', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640301', '6403', '0,1,64,6403,640301,', '市辖区', '13378', '640301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640302', '6403', '0,1,64,6403,640302,', '利通区', '13379', '640302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640321', '6403', '0,1,64,6403,640321,', '中卫县', '13380', '640321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640322', '6403', '0,1,64,6403,640322,', '中宁县', '13381', '640322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640323', '6403', '0,1,64,6403,640323,', '盐池县', '13382', '640323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640324', '6403', '0,1,64,6403,640324,', '同心县', '13383', '640324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640381', '6403', '0,1,64,6403,640381,', '青铜峡市', '13384', '640381', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('640382', '6403', '0,1,64,6403,640382,', '灵武市', '13385', '640382', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6422', '64', '0,1,64,6422,', '固原地区', '13386', '6422', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('642221', '6422', '0,1,64,6422,642221,', '固原县', '13387', '642221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('642222', '6422', '0,1,64,6422,642222,', '海原县', '13388', '642222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('642223', '6422', '0,1,64,6422,642223,', '西吉县', '13389', '642223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('642224', '6422', '0,1,64,6422,642224,', '隆德县', '13390', '642224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('642225', '6422', '0,1,64,6422,642225,', '泾源县', '13391', '642225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('642226', '6422', '0,1,64,6422,642226,', '彭阳县', '13392', '642226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('65', '1', '0,1,65,', '新疆维吾尔自治区', '13393', '65', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6501', '65', '0,1,65,6501,', '乌鲁木齐', '13394', '6501', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650101', '6501', '0,1,65,6501,650101,', '市辖区', '13395', '650101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650102', '6501', '0,1,65,6501,650102,', '天山区', '13396', '650102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650103', '6501', '0,1,65,6501,650103,', '沙依巴克区', '13397', '650103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650104', '6501', '0,1,65,6501,650104,', '新市区', '13398', '650104', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650105', '6501', '0,1,65,6501,650105,', '水磨沟区', '13399', '650105', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650106', '6501', '0,1,65,6501,650106,', '头屯河区', '13400', '650106', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650107', '6501', '0,1,65,6501,650107,', '南泉区', '13401', '650107', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650108', '6501', '0,1,65,6501,650108,', '东山区', '13402', '650108', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650121', '6501', '0,1,65,6501,650121,', '乌鲁木齐县', '13403', '650121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6502', '65', '0,1,65,6502,', '克拉玛依', '13404', '6502', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650201', '6502', '0,1,65,6502,650201,', '市辖区', '13405', '650201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650202', '6502', '0,1,65,6502,650202,', '独山子区', '13406', '650202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650203', '6502', '0,1,65,6502,650203,', '克拉玛依区', '13407', '650203', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650204', '6502', '0,1,65,6502,650204,', '白碱滩区', '13408', '650204', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('650205', '6502', '0,1,65,6502,650205,', '乌尔禾区', '13409', '650205', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6521', '65', '0,1,65,6521,', '吐鲁番地区', '13410', '6521', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652101', '6521', '0,1,65,6521,652101,', '吐鲁番市', '13411', '652101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652122', '6521', '0,1,65,6521,652122,', '鄯善县', '13412', '652122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652123', '6521', '0,1,65,6521,652123,', '托克逊县', '13413', '652123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6522', '65', '0,1,65,6522,', '哈密地区', '13414', '6522', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652201', '6522', '0,1,65,6522,652201,', '哈密市', '13415', '652201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652222', '6522', '0,1,65,6522,652222,', '巴里坤哈萨克自治县', '13416', '652222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652223', '6522', '0,1,65,6522,652223,', '伊吾县', '13417', '652223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6523', '65', '0,1,65,6523,', '昌吉回族自治州', '13418', '6523', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652301', '6523', '0,1,65,6523,652301,', '昌吉市', '13419', '652301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652302', '6523', '0,1,65,6523,652302,', '阜康市', '13420', '652302', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652303', '6523', '0,1,65,6523,652303,', '米泉市', '13421', '652303', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652323', '6523', '0,1,65,6523,652323,', '呼图壁县', '13422', '652323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652324', '6523', '0,1,65,6523,652324,', '玛纳斯县', '13423', '652324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652325', '6523', '0,1,65,6523,652325,', '奇台县', '13424', '652325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652327', '6523', '0,1,65,6523,652327,', '吉木萨尔县', '13425', '652327', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652328', '6523', '0,1,65,6523,652328,', '木垒哈萨克自治县', '13426', '652328', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6527', '65', '0,1,65,6527,', '博尔塔拉蒙古自治州', '13427', '6527', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652701', '6527', '0,1,65,6527,652701,', '博乐市', '13428', '652701', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652722', '6527', '0,1,65,6527,652722,', '精河县', '13429', '652722', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652723', '6527', '0,1,65,6527,652723,', '温泉县', '13430', '652723', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6528', '65', '0,1,65,6528,', '巴音郭楞蒙古自治州', '13431', '6528', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652801', '6528', '0,1,65,6528,652801,', '库尔勒市', '13432', '652801', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652822', '6528', '0,1,65,6528,652822,', '轮台县', '13433', '652822', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652823', '6528', '0,1,65,6528,652823,', '尉犁县', '13434', '652823', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652824', '6528', '0,1,65,6528,652824,', '若羌县', '13435', '652824', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652825', '6528', '0,1,65,6528,652825,', '且末县', '13436', '652825', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652826', '6528', '0,1,65,6528,652826,', '焉耆回族自治县', '13437', '652826', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652827', '6528', '0,1,65,6528,652827,', '和静县', '13438', '652827', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652828', '6528', '0,1,65,6528,652828,', '和硕县', '13439', '652828', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652829', '6528', '0,1,65,6528,652829,', '博湖县', '13440', '652829', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6529', '65', '0,1,65,6529,', '阿克苏地区', '13441', '6529', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652901', '6529', '0,1,65,6529,652901,', '阿克苏市', '13442', '652901', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652922', '6529', '0,1,65,6529,652922,', '温宿县', '13443', '652922', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652923', '6529', '0,1,65,6529,652923,', '库车县', '13444', '652923', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652924', '6529', '0,1,65,6529,652924,', '沙雅县', '13445', '652924', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652925', '6529', '0,1,65,6529,652925,', '新和县', '13446', '652925', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652926', '6529', '0,1,65,6529,652926,', '拜城县', '13447', '652926', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652927', '6529', '0,1,65,6529,652927,', '乌什县', '13448', '652927', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652928', '6529', '0,1,65,6529,652928,', '阿瓦提县', '13449', '652928', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('652929', '6529', '0,1,65,6529,652929,', '柯坪县', '13450', '652929', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6530', '65', '0,1,65,6530,', '克孜勒苏柯尔克孜自治', '13451', '6530', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653001', '6530', '0,1,65,6530,653001,', '阿图什市', '13452', '653001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653022', '6530', '0,1,65,6530,653022,', '阿克陶县', '13453', '653022', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653023', '6530', '0,1,65,6530,653023,', '阿合奇县', '13454', '653023', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653024', '6530', '0,1,65,6530,653024,', '乌恰县', '13455', '653024', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6531', '65', '0,1,65,6531,', '喀什地区', '13456', '6531', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653101', '6531', '0,1,65,6531,653101,', '喀什市', '13457', '653101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653121', '6531', '0,1,65,6531,653121,', '疏附县', '13458', '653121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653122', '6531', '0,1,65,6531,653122,', '疏勒县', '13459', '653122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653123', '6531', '0,1,65,6531,653123,', '英吉沙县', '13460', '653123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653124', '6531', '0,1,65,6531,653124,', '泽普县', '13461', '653124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653125', '6531', '0,1,65,6531,653125,', '莎车县', '13462', '653125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653126', '6531', '0,1,65,6531,653126,', '叶城县', '13463', '653126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653127', '6531', '0,1,65,6531,653127,', '麦盖提县', '13464', '653127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653128', '6531', '0,1,65,6531,653128,', '岳普湖县', '13465', '653128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653129', '6531', '0,1,65,6531,653129,', '伽师县', '13466', '653129', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653130', '6531', '0,1,65,6531,653130,', '巴楚县', '13467', '653130', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653131', '6531', '0,1,65,6531,653131,', '塔什库尔干塔吉克自治', '13468', '653131', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6532', '65', '0,1,65,6532,', '和田地区', '13469', '6532', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653201', '6532', '0,1,65,6532,653201,', '和田市', '13470', '653201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653221', '6532', '0,1,65,6532,653221,', '和田县', '13471', '653221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653222', '6532', '0,1,65,6532,653222,', '墨玉县', '13472', '653222', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653223', '6532', '0,1,65,6532,653223,', '皮山县', '13473', '653223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653224', '6532', '0,1,65,6532,653224,', '洛浦县', '13474', '653224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653225', '6532', '0,1,65,6532,653225,', '策勒县', '13475', '653225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653226', '6532', '0,1,65,6532,653226,', '于田县', '13476', '653226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('653227', '6532', '0,1,65,6532,653227,', '民丰县', '13477', '653227', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6540', '65', '0,1,65,6540,', '伊犁哈萨克自治州', '13478', '6540', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654001', '6540', '0,1,65,6540,654001,', '奎屯市', '13479', '654001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6541', '65', '0,1,65,6541,', '伊犁地区', '13480', '6541', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654101', '6541', '0,1,65,6541,654101,', '伊宁市', '13481', '654101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654121', '6541', '0,1,65,6541,654121,', '伊宁县', '13482', '654121', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654122', '6541', '0,1,65,6541,654122,', '察布查尔锡伯自治县', '13483', '654122', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654123', '6541', '0,1,65,6541,654123,', '霍城县', '13484', '654123', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654124', '6541', '0,1,65,6541,654124,', '巩留县', '13485', '654124', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654125', '6541', '0,1,65,6541,654125,', '新源县', '13486', '654125', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654126', '6541', '0,1,65,6541,654126,', '昭苏县', '13487', '654126', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654127', '6541', '0,1,65,6541,654127,', '特克斯县', '13488', '654127', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654128', '6541', '0,1,65,6541,654128,', '尼勒克县', '13489', '654128', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6542', '65', '0,1,65,6542,', '塔城地区', '13490', '6542', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654201', '6542', '0,1,65,6542,654201,', '塔城市', '13491', '654201', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654202', '6542', '0,1,65,6542,654202,', '乌苏市', '13492', '654202', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654221', '6542', '0,1,65,6542,654221,', '额敏县', '13493', '654221', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654223', '6542', '0,1,65,6542,654223,', '沙湾县', '13494', '654223', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654224', '6542', '0,1,65,6542,654224,', '托里县', '13495', '654224', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654225', '6542', '0,1,65,6542,654225,', '裕民县', '13496', '654225', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654226', '6542', '0,1,65,6542,654226,', '和布克赛尔蒙古自治县', '13497', '654226', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6543', '65', '0,1,65,6543,', '阿勒泰地区', '13498', '6543', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654301', '6543', '0,1,65,6543,654301,', '阿勒泰市', '13499', '654301', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654321', '6543', '0,1,65,6543,654321,', '布尔津县', '13500', '654321', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654322', '6543', '0,1,65,6543,654322,', '富蕴县', '13501', '654322', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654323', '6543', '0,1,65,6543,654323,', '福海县', '13502', '654323', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654324', '6543', '0,1,65,6543,654324,', '哈巴河县', '13503', '654324', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654325', '6543', '0,1,65,6543,654325,', '青河县', '13504', '654325', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('654326', '6543', '0,1,65,6543,654326,', '吉木乃县', '13505', '654326', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('6590', '65', '0,1,65,6590,', '省直辖行政单位', '13506', '6590', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('659001', '6590', '0,1,65,6590,659001,', '石河子市', '13507', '659001', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('71', '1', '0,1,71,', '台湾', '13508', '71', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('7101', '71', '0,1,71,7101,', '台湾市辖', '13509', '7101', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('710101', '7101', '0,1,71,7101,710101,', '请选择', '13510', '710101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('710102', '7101', '0,1,71,7101,710102,', '市辖区', '13511', '710102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('710103', '7101', '0,1,71,7101,710103,', '台湾省', '13512', '710103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('81', '1', '0,1,81,', '香港', '13513', '81', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('8101', '81', '0,1,81,8101,', '香港特区', '13514', '8101', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('810101', '8101', '0,1,81,8101,810101,', '请选择', '13515', '810101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('810102', '8101', '0,1,81,8101,810102,', '市辖区', '13516', '810102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('810103', '8101', '0,1,81,8101,810103,', '香港特区', '13517', '810103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('91', '1', '0,1,91,', '澳门', '13518', '91', '2', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('9101', '91', '0,1,91,9101,', '澳门特区', '13519', '9101', '3', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('910101', '9101', '0,1,91,9101,910101,', '请选择', '13520', '910101', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('910102', '9101', '0,1,91,9101,910102,', '市辖区', '13521', '910102', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');
INSERT INTO `sys_area` VALUES ('910103', '9101', '0,1,91,9101,910103,', '澳门特区', '13522', '910103', '4', '1', '2017-03-10 15:48:56', '1', '2017-03-10 15:48:56', null, '0');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL COMMENT '父部门id',
  `pids` varchar(255) DEFAULT NULL COMMENT '父级ids',
  `simplename` varchar(45) DEFAULT NULL COMMENT '简称',
  `fullname` varchar(255) DEFAULT NULL COMMENT '全称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  `version` int(11) DEFAULT NULL COMMENT '版本（乐观锁保留字段）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('25', '2', '24', '[0],[24],', '开发部', '开发部', '', null);
INSERT INTO `sys_dept` VALUES ('26', '3', '24', '[0],[24],', '运营部', '运营部', '', null);
INSERT INTO `sys_dept` VALUES ('27', '4', '24', '[0],[24],', '战略部', '战略部', '', null);
INSERT INTO `sys_dept` VALUES ('28', '5', '1', null, '测试', '测试fullname', '测试tips', '1');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '排序',
  `pid` int(11) DEFAULT NULL COMMENT '父级字典',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  `code` varchar(255) DEFAULT NULL COMMENT '值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('50', '0', '0', '性别', null, 'sys_sex');
INSERT INTO `sys_dict` VALUES ('51', '1', '50', '男', null, '1');
INSERT INTO `sys_dict` VALUES ('52', '2', '50', '女', null, '2');
INSERT INTO `sys_dict` VALUES ('53', '0', '0', '状态', null, 'sys_state');
INSERT INTO `sys_dict` VALUES ('54', '1', '53', '启用', null, '1');
INSERT INTO `sys_dict` VALUES ('55', '2', '53', '禁用', null, '2');
INSERT INTO `sys_dict` VALUES ('56', '0', '0', '账号状态', null, 'account_state');
INSERT INTO `sys_dict` VALUES ('57', '1', '56', '启用', null, '1');
INSERT INTO `sys_dict` VALUES ('58', '2', '56', '冻结', null, '2');
INSERT INTO `sys_dict` VALUES ('59', '3', '56', '已删除', null, '3');
INSERT INTO `sys_dict` VALUES ('69', '0', '0', '字典测试', '这是一个字典测试', 'test');
INSERT INTO `sys_dict` VALUES ('70', '1', '69', '测试1', null, '1');
INSERT INTO `sys_dict` VALUES ('71', '2', '69', '测试2', null, '2');

-- ----------------------------
-- Table structure for sys_expense
-- ----------------------------
DROP TABLE IF EXISTS `sys_expense`;
CREATE TABLE `sys_expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `money` decimal(20,2) DEFAULT NULL COMMENT '报销金额',
  `desc` varchar(255) DEFAULT '' COMMENT '描述',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `state` int(11) DEFAULT NULL COMMENT '状态: 1.待提交  2:待审核   3.审核通过 4:驳回',
  `userid` int(11) DEFAULT NULL COMMENT '用户id',
  `processId` varchar(255) DEFAULT NULL COMMENT '流程定义id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报销表';

-- ----------------------------
-- Records of sys_expense
-- ----------------------------

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log` (
  `id` int(65) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `logname` varchar(255) DEFAULT NULL COMMENT '日志名称',
  `userid` int(65) DEFAULT NULL COMMENT '管理员id',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `succeed` varchar(255) DEFAULT NULL COMMENT '是否执行成功',
  `message` text COMMENT '具体消息',
  `ip` varchar(255) DEFAULT NULL COMMENT '登录ip',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=258 DEFAULT CHARSET=utf8 COMMENT='登录记录';

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------
INSERT INTO `sys_login_log` VALUES ('217', '登录失败日志', null, '2018-07-06 00:01:19', '成功', '账号:root,账号密码错误', '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('218', '登录失败日志', null, '2018-07-06 00:01:54', '成功', '账号:root,账号密码错误', '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('219', '登录失败日志', null, '2018-07-06 00:02:28', '成功', '账号:test,账号密码错误', '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('220', '登录失败日志', null, '2018-07-06 00:03:30', '成功', '账号:test,账号密码错误', '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('221', '登录日志', '1', '2018-07-06 00:03:45', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('222', '登录日志', '1', '2018-07-06 00:08:01', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('223', '退出日志', '1', '2018-07-06 00:08:24', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('224', '登录日志', '1', '2018-07-06 00:08:25', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('225', '登录日志', '1', '2018-07-06 00:11:08', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('226', '登录日志', '1', '2018-07-06 00:12:48', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('227', '登录日志', '1', '2018-07-06 00:17:16', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('228', '登录日志', '1', '2018-07-06 00:17:16', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('229', '登录日志', '1', '2018-07-06 00:18:10', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('230', '退出日志', '1', '2018-07-06 00:18:15', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('231', '登录日志', '1', '2018-07-06 00:18:17', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('232', '退出日志', '1', '2018-07-06 00:18:55', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('233', '登录日志', '1', '2018-07-06 00:18:56', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('234', '登录日志', '1', '2018-07-06 00:19:36', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('235', '退出日志', '1', '2018-07-06 00:19:40', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('236', '登录日志', '1', '2018-07-06 00:19:43', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('237', '退出日志', '1', '2018-07-07 02:08:13', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('238', '登录日志', '1', '2018-07-07 02:08:14', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('239', '登录日志', '1', '2018-07-07 02:09:57', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('240', '退出日志', '1', '2018-07-07 02:10:50', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('241', '登录日志', '1', '2018-07-07 02:10:53', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('242', '退出日志', '1', '2018-07-08 00:12:20', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('243', '登录日志', '1', '2018-07-08 00:12:22', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('244', '登录日志', '1', '2018-07-08 00:31:36', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('245', '登录日志', '1', '2018-07-08 01:12:53', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('246', '登录日志', '1', '2018-07-08 01:15:40', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('247', '登录失败日志', null, '2018-07-11 03:59:20', '成功', '账号:13803490306,账号密码错误', '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('248', '登录失败日志', null, '2018-07-11 03:59:24', '成功', '账号:13803490306,账号密码错误', '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('249', '登录失败日志', null, '2018-07-11 03:59:27', '成功', '账号:admin,账号密码错误', '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('250', '登录日志', '1', '2018-07-11 03:59:46', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('251', '退出日志', '1', '2018-07-11 04:09:33', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('252', '登录日志', '1', '2018-07-11 04:09:35', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('253', '登录日志', '1', '2018-07-11 04:22:22', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('254', '登录日志', '1', '2018-07-11 04:26:04', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('255', '登录日志', '1', '2018-07-11 05:58:28', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('256', '登录日志', '1', '2018-07-11 06:01:13', '成功', null, '0:0:0:0:0:0:0:1');
INSERT INTO `sys_login_log` VALUES ('257', '登录日志', '1', '2018-07-11 06:06:48', '成功', null, '0:0:0:0:0:0:0:1');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `code` varchar(255) DEFAULT NULL COMMENT '菜单编号',
  `pcode` varchar(255) DEFAULT NULL COMMENT '菜单父编号',
  `pcodes` varchar(255) DEFAULT NULL COMMENT '当前菜单的所有父菜单编号',
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `url` varchar(255) DEFAULT NULL COMMENT 'url地址',
  `num` int(65) DEFAULT NULL COMMENT '菜单排序号',
  `levels` int(65) DEFAULT NULL COMMENT '菜单层级',
  `ismenu` int(11) DEFAULT NULL COMMENT '是否是菜单（1：是  0：不是）',
  `tips` varchar(255) DEFAULT NULL COMMENT '备注',
  `status` int(65) DEFAULT NULL COMMENT '菜单状态 :  1:启用   0:不启用',
  `isopen` int(11) DEFAULT NULL COMMENT '是否打开:    1:打开   0:不打开',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1016901161182593032 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('105', 'system', '0', '[0],', '系统管理', 'fa-user', '#', '4', '1', '1', null, '1', '1');
INSERT INTO `sys_menu` VALUES ('106', 'mgr', 'system', '[0],[system],', '用户管理', '', '/mgr', '1', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('107', 'mgr_add', 'mgr', '[0],[system],[mgr],', '添加用户', null, '/mgr/add', '1', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('108', 'mgr_edit', 'mgr', '[0],[system],[mgr],', '修改用户', null, '/mgr/edit', '2', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('109', 'mgr_delete', 'mgr', '[0],[system],[mgr],', '删除用户', null, '/mgr/delete', '3', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('110', 'mgr_reset', 'mgr', '[0],[system],[mgr],', '重置密码', null, '/mgr/reset', '4', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('111', 'mgr_freeze', 'mgr', '[0],[system],[mgr],', '冻结用户', null, '/mgr/freeze', '5', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('112', 'mgr_unfreeze', 'mgr', '[0],[system],[mgr],', '解除冻结用户', null, '/mgr/unfreeze', '6', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('113', 'mgr_setRole', 'mgr', '[0],[system],[mgr],', '分配角色', null, '/mgr/setRole', '7', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('114', 'role', 'system', '[0],[system],', '角色管理', null, '/role', '2', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('115', 'role_add', 'role', '[0],[system],[role],', '添加角色', null, '/role/add', '1', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('116', 'role_edit', 'role', '[0],[system],[role],', '修改角色', null, '/role/edit', '2', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('117', 'role_remove', 'role', '[0],[system],[role],', '删除角色', null, '/role/remove', '3', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('118', 'role_setAuthority', 'role', '[0],[system],[role],', '配置权限', null, '/role/setAuthority', '4', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('119', 'menu', 'system', '[0],[system],', '菜单管理', null, '/menu', '4', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('120', 'menu_add', 'menu', '[0],[system],[menu],', '添加菜单', null, '/menu/add', '1', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('121', 'menu_edit', 'menu', '[0],[system],[menu],', '修改菜单', null, '/menu/edit', '2', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('122', 'menu_remove', 'menu', '[0],[system],[menu],', '删除菜单', null, '/menu/remove', '3', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('128', 'log', 'system', '[0],[system],', '业务日志', null, '/log', '6', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('130', 'druid', 'system', '[0],[system],', '监控管理', null, '/druid', '7', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('131', 'dept', 'system', '[0],[system],', '部门管理', null, '/dept', '3', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('132', 'dict', 'system', '[0],[system],', '字典管理', null, '/dict', '4', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('133', 'loginLog', 'system', '[0],[system],', '登录日志', null, '/loginLog', '6', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('134', 'log_clean', 'log', '[0],[system],[log],', '清空日志', null, '/log/delLog', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('135', 'dept_add', 'dept', '[0],[system],[dept],', '添加部门', null, '/dept/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('136', 'dept_update', 'dept', '[0],[system],[dept],', '修改部门', null, '/dept/update', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('137', 'dept_delete', 'dept', '[0],[system],[dept],', '删除部门', null, '/dept/delete', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('138', 'dict_add', 'dict', '[0],[system],[dict],', '添加字典', null, '/dict/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('139', 'dict_update', 'dict', '[0],[system],[dict],', '修改字典', null, '/dict/update', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('140', 'dict_delete', 'dict', '[0],[system],[dict],', '删除字典', null, '/dict/delete', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('141', 'notice', 'system', '[0],[system],', '通知管理', null, '/notice', '9', '2', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('142', 'notice_add', 'notice', '[0],[system],[notice],', '添加通知', null, '/notice/add', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('143', 'notice_update', 'notice', '[0],[system],[notice],', '修改通知', null, '/notice/update', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('144', 'notice_delete', 'notice', '[0],[system],[notice],', '删除通知', null, '/notice/delete', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('145', 'hello', '0', '[0],', '通知', 'fa-rocket', '/notice/hello', '1', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('148', 'code', '0', '[0],', '代码生成', 'fa-code', '/code', '3', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('149', 'api_mgr', '0', '[0],', '接口文档', 'fa-leaf', '/swagger-ui.html', '2', '1', '1', null, '1', null);
INSERT INTO `sys_menu` VALUES ('150', 'to_menu_edit', 'menu', '[0],[system],[menu],', '菜单编辑跳转', '', '/menu/menu_edit', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('151', 'menu_list', 'menu', '[0],[system],[menu],', '菜单列表', '', '/menu/list', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('152', 'to_dept_update', 'dept', '[0],[system],[dept],', '修改部门跳转', '', '/dept/dept_update', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('153', 'dept_list', 'dept', '[0],[system],[dept],', '部门列表', '', '/dept/list', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('154', 'dept_detail', 'dept', '[0],[system],[dept],', '部门详情', '', '/dept/detail', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('155', 'to_dict_edit', 'dict', '[0],[system],[dict],', '修改菜单跳转', '', '/dict/dict_edit', '4', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('156', 'dict_list', 'dict', '[0],[system],[dict],', '字典列表', '', '/dict/list', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('157', 'dict_detail', 'dict', '[0],[system],[dict],', '字典详情', '', '/dict/detail', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('158', 'log_list', 'log', '[0],[system],[log],', '日志列表', '', '/log/list', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('159', 'log_detail', 'log', '[0],[system],[log],', '日志详情', '', '/log/detail', '3', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('160', 'del_login_log', 'loginLog', '[0],[system],[loginLog],', '清空登录日志', '', '/loginLog/delLoginLog', '1', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('161', 'login_log_list', 'loginLog', '[0],[system],[loginLog],', '登录日志列表', '', '/loginLog/list', '2', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('162', 'to_role_edit', 'role', '[0],[system],[role],', '修改角色跳转', '', '/role/role_edit', '5', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('163', 'to_role_assign', 'role', '[0],[system],[role],', '角色分配跳转', '', '/role/role_assign', '6', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('164', 'role_list', 'role', '[0],[system],[role],', '角色列表', '', '/role/list', '7', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('165', 'to_assign_role', 'mgr', '[0],[system],[mgr],', '分配角色跳转', '', '/mgr/role_assign', '8', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('166', 'to_user_edit', 'mgr', '[0],[system],[mgr],', '编辑用户跳转', '', '/mgr/user_edit', '9', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('167', 'mgr_list', 'mgr', '[0],[system],[mgr],', '用户列表', '', '/mgr/list', '10', '3', '0', null, '1', null);
INSERT INTO `sys_menu` VALUES ('1015025486529028105', 'mall', '0', '[0],', '商城管理', 'fa-user', '/', '5', '1', '1', null, '1', '1');
INSERT INTO `sys_menu` VALUES ('1016895452801982465', 'category', 'mall', '[0],[mall],', '分类管理', '', '/category', '99', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895452801982466', 'category_list', 'category', '[0],[mall],[category],', '分类管理列表', '', '/category/list', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895452801982467', 'category_add', 'category', '[0],[mall],[category],', '分类管理添加', '', '/category/add', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895452801982468', 'category_update', 'category', '[0],[mall],[category],', '分类管理更新', '', '/category/update', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895452801982469', 'category_delete', 'category', '[0],[mall],[category],', '分类管理删除', '', '/category/delete', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895452801982470', 'category_detail', 'category', '[0],[mall],[category],', '分类管理详情', '', '/category/detail', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895575640563713', 'brand', 'mall', '[0],[mall],', '品牌管理', '', '/brand', '99', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895765277630466', 'productProperty', 'mall', '[0],[mall],', '属性管理', '', '/productProperty', '99', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895765277630467', 'productProperty_list', 'productProperty', '[0],[mall],[productProperty],', '属性管理列表', '', '/productProperty/list', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895765277630468', 'productProperty_add', 'productProperty', '[0],[mall],[productProperty],', '属性管理添加', '', '/productProperty/add', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895765277630469', 'productProperty_update', 'productProperty', '[0],[mall],[productProperty],', '属性管理更新', '', '/productProperty/update', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895765277630470', 'productProperty_delete', 'productProperty', '[0],[mall],[productProperty],', '属性管理删除', '', '/productProperty/delete', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895765277630471', 'productProperty_detail', 'productProperty', '[0],[mall],[productProperty],', '属性管理详情', '', '/productProperty/detail', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895830058655746', 'productAttrbute', 'mall', '[0],[mall],', '属性值管理', '', '/productAttrbute', '99', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895830058655747', 'productAttrbute_list', 'productAttrbute', '[0],[mall],[productAttrbute],', '属性值管理列表', '', '/productAttrbute/list', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895830058655748', 'productAttrbute_add', 'productAttrbute', '[0],[mall],[productAttrbute],', '属性值管理添加', '', '/productAttrbute/add', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895830058655749', 'productAttrbute_update', 'productAttrbute', '[0],[mall],[productAttrbute],', '属性值管理更新', '', '/productAttrbute/update', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895830058655750', 'productAttrbute_delete', 'productAttrbute', '[0],[mall],[productAttrbute],', '属性值管理删除', '', '/productAttrbute/delete', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016895830058655751', 'productAttrbute_detail', 'productAttrbute', '[0],[mall],[productAttrbute],', '属性值管理详情', '', '/productAttrbute/detail', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016899635525513218', 'skuProduct', 'mall', '[0],[mall],', '商品SKU管理', '', '/skuProduct', '99', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016899635525513219', 'skuProduct_list', 'skuProduct', '[0],[mall],[skuProduct],', '商品SKU管理列表', '', '/skuProduct/list', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016899635525513220', 'skuProduct_add', 'skuProduct', '[0],[mall],[skuProduct],', '商品SKU管理添加', '', '/skuProduct/add', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016899635525513221', 'skuProduct_update', 'skuProduct', '[0],[mall],[skuProduct],', '商品SKU管理更新', '', '/skuProduct/update', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016899635525513222', 'skuProduct_delete', 'skuProduct', '[0],[mall],[skuProduct],', '商品SKU管理删除', '', '/skuProduct/delete', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016899635525513223', 'skuProduct_detail', 'skuProduct', '[0],[mall],[skuProduct],', '商品SKU管理详情', '', '/skuProduct/detail', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016901161182593026', 'shop', 'mall', '[0],[mall],', '店铺管理', '', '/shop', '99', '2', '1', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016901161182593027', 'shop_list', 'shop', '[0],[mall],[shop],', '店铺管理列表', '', '/shop/list', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016901161182593028', 'shop_add', 'shop', '[0],[mall],[shop],', '店铺管理添加', '', '/shop/add', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016901161182593029', 'shop_update', 'shop', '[0],[mall],[shop],', '店铺管理更新', '', '/shop/update', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016901161182593030', 'shop_delete', 'shop', '[0],[mall],[shop],', '店铺管理删除', '', '/shop/delete', '99', '3', '0', null, '1', '0');
INSERT INTO `sys_menu` VALUES ('1016901161182593031', 'shop_detail', 'shop', '[0],[mall],[shop],', '店铺管理详情', '', '/shop/detail', '99', '3', '0', null, '1', '0');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `type` int(11) DEFAULT NULL COMMENT '类型',
  `content` text COMMENT '内容',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `creater` int(11) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='通知表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('6', '世界', '10', '欢迎使用Guns管理系统', '2017-01-11 08:53:20', '1');
INSERT INTO `sys_notice` VALUES ('8', '你好', null, '你好', '2017-05-10 19:28:57', '1');

-- ----------------------------
-- Table structure for sys_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_operation_log`;
CREATE TABLE `sys_operation_log` (
  `id` int(65) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `logtype` varchar(255) DEFAULT NULL COMMENT '日志类型',
  `logname` varchar(255) DEFAULT NULL COMMENT '日志名称',
  `userid` int(65) DEFAULT NULL COMMENT '用户id',
  `classname` varchar(255) DEFAULT NULL COMMENT '类名称',
  `method` text COMMENT '方法名称',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `succeed` varchar(255) DEFAULT NULL COMMENT '是否成功',
  `message` text COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=588 DEFAULT CHARSET=utf8 COMMENT='操作日志';

-- ----------------------------
-- Records of sys_operation_log
-- ----------------------------
INSERT INTO `sys_operation_log` VALUES ('554', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-06 00:18:50', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('555', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-06 00:19:21', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('556', '业务日志', '删除菜单', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'remove', '2018-07-06 15:44:42', '成功', '菜单id=1015025486529028098');
INSERT INTO `sys_operation_log` VALUES ('557', '业务日志', '菜单新增', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'add', '2018-07-06 15:52:11', '成功', '菜单名称=商城管理');
INSERT INTO `sys_operation_log` VALUES ('558', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-06 15:52:54', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('559', '业务日志', '修改菜单', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'edit', '2018-07-06 15:54:04', '成功', '菜单名称=商城管理;;;');
INSERT INTO `sys_operation_log` VALUES ('560', '业务日志', '修改菜单', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'edit', '2018-07-06 15:54:49', '成功', '菜单名称=商城管理;;;字段名称:url地址,旧值:#,新值:###');
INSERT INTO `sys_operation_log` VALUES ('561', '业务日志', '修改菜单', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'edit', '2018-07-06 15:55:15', '成功', '菜单名称=商城管理;;;字段名称:菜单父编号,旧值:0,新值:105');
INSERT INTO `sys_operation_log` VALUES ('562', '业务日志', '删除菜单', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'remove', '2018-07-06 15:56:15', '成功', '菜单id=1015025486529028104');
INSERT INTO `sys_operation_log` VALUES ('563', '业务日志', '菜单新增', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'add', '2018-07-07 00:59:00', '成功', '菜单名称=商城管理');
INSERT INTO `sys_operation_log` VALUES ('564', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-07 00:59:17', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('565', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-07 01:08:07', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('566', '异常日志', '', '1', null, null, '2018-07-07 01:39:00', '失败', 'org.springframework.jdbc.UncategorizedSQLException: \r\n### Error querying database.  Cause: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC\r\n### The error may exist in com/stylefeng/guns/modular/system/dao/ProductMapper.java (best guess)\r\n### The error may involve com.stylefeng.guns.modular.system.dao.ProductMapper.selectMapsPage\r\n### The error occurred while executing a query\r\n### SQL: SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product   WHERE  (productName like \'%0%\') ORDER BY update_time DESC, update_time asc ASC\r\n### Cause: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC\n; uncategorized SQLException; SQL state [null]; error code [0]; sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC; nested exception is java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:89)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:81)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:81)\r\n	at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:73)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:446)\r\n	at com.sun.proxy.TProxy94.selectList(Unknown Source)\r\n	at org.mybatis.spring.SqlSessionTemplate.selectList(SqlSessionTemplate.java:238)\r\n	at org.apache.ibatis.binding.MapperMethod.executeForMany(MapperMethod.java:137)\r\n	at org.apache.ibatis.binding.MapperMethod.execute(MapperMethod.java:76)\r\n	at org.apache.ibatis.binding.MapperProxy.invoke(MapperProxy.java:59)\r\n	at com.sun.proxy.TProxy106.selectMapsPage(Unknown Source)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImpl.selectMapsPage(ServiceImpl.java:415)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImplTTFastClassBySpringCGLIBTT3e2398a4.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at com.alibaba.druid.support.spring.stat.DruidStatInterceptor.invoke(DruidStatInterceptor.java:72)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.service.impl.ProductServiceImplTTEnhancerBySpringCGLIBTT7e801f0a.selectMapsPage(<generated>)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductController.list(ProductController.java:71)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductControllerTTFastClassBySpringCGLIBTT3362bf26.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint.proceed(MethodInvocationProceedingJoinPoint.java:89)\r\n	at com.stylefeng.guns.core.intercept.SessionHolderInterceptor.sessionKit(SessionHolderInterceptor.java:29)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:644)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:633)\r\n	at org.springframework.aop.aspectj.AspectJAroundAdvice.invoke(AspectJAroundAdvice.java:70)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:92)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductControllerTTEnhancerBySpringCGLIBTTab185397.list(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:209)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:136)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:102)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:877)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:783)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:991)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:925)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:974)\r\n	at org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:877)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:661)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:851)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:742)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:52)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:449)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilterT1.call(AbstractShiroFilter.java:365)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:362)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.stylefeng.guns.core.xss.XssFilter.doFilter(XssFilter.java:31)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.alibaba.druid.support.http.WebStatFilter.doFilter(WebStatFilter.java:123)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:99)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HttpPutFormContentFilter.doFilterInternal(HttpPutFormContentFilter.java:109)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:81)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:200)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:198)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:496)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:140)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:81)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:87)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:342)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:803)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)\r\n	at org.apache.coyote.AbstractProtocolTConnectionHandler.process(AbstractProtocol.java:790)\r\n	at org.apache.tomcat.util.net.NioEndpointTSocketProcessor.doRun(NioEndpoint.java:1459)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\r\n	at java.util.concurrent.ThreadPoolExecutorTWorker.run(ThreadPoolExecutor.java:617)\r\n	at org.apache.tomcat.util.threads.TaskThreadTWrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC\r\n	at com.alibaba.druid.wall.WallFilter.checkInternal(WallFilter.java:798)\r\n	at com.alibaba.druid.wall.WallFilter.connection_prepareStatement(WallFilter.java:251)\r\n	at com.alibaba.druid.filter.FilterChainImpl.connection_prepareStatement(FilterChainImpl.java:568)\r\n	at com.alibaba.druid.proxy.jdbc.ConnectionProxyImpl.prepareStatement(ConnectionProxyImpl.java:342)\r\n	at com.alibaba.druid.pool.DruidPooledConnection.prepareStatement(DruidPooledConnection.java:349)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.logging.jdbc.ConnectionLogger.invoke(ConnectionLogger.java:55)\r\n	at com.sun.proxy.TProxy124.prepareStatement(Unknown Source)\r\n	at org.apache.ibatis.executor.statement.PreparedStatementHandler.instantiateStatement(PreparedStatementHandler.java:87)\r\n	at org.apache.ibatis.executor.statement.BaseStatementHandler.prepare(BaseStatementHandler.java:88)\r\n	at org.apache.ibatis.executor.statement.RoutingStatementHandler.prepare(RoutingStatementHandler.java:59)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Invocation.proceed(Invocation.java:49)\r\n	at com.baomidou.mybatisplus.plugins.PaginationInterceptor.intercept(PaginationInterceptor.java:128)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:61)\r\n	at com.sun.proxy.TProxy123.prepare(Unknown Source)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Invocation.proceed(Invocation.java:49)\r\n	at com.stylefeng.guns.core.datascope.DataScopeInterceptor.intercept(DataScopeInterceptor.java:46)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:61)\r\n	at com.sun.proxy.TProxy123.prepare(Unknown Source)\r\n	at org.apache.ibatis.executor.SimpleExecutor.prepareStatement(SimpleExecutor.java:85)\r\n	at org.apache.ibatis.executor.SimpleExecutor.doQuery(SimpleExecutor.java:62)\r\n	at org.apache.ibatis.executor.BaseExecutor.queryFromDatabase(BaseExecutor.java:326)\r\n	at org.apache.ibatis.executor.BaseExecutor.query(BaseExecutor.java:156)\r\n	at org.apache.ibatis.executor.CachingExecutor.query(CachingExecutor.java:109)\r\n	at org.apache.ibatis.executor.CachingExecutor.query(CachingExecutor.java:83)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.TProxy122.query(Unknown Source)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.selectList(DefaultSqlSession.java:148)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:433)\r\n	... 108 more\r\nCaused by: com.alibaba.druid.sql.parser.ParserException: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC\r\n	at com.alibaba.druid.sql.parser.SQLParser.printError(SQLParser.java:284)\r\n	at com.alibaba.druid.sql.parser.SQLStatementParser.parseStatementList(SQLStatementParser.java:426)\r\n	at com.alibaba.druid.sql.parser.SQLStatementParser.parseStatementList(SQLStatementParser.java:83)\r\n	at com.alibaba.druid.wall.WallProvider.checkInternal(WallProvider.java:624)\r\n	at com.alibaba.druid.wall.WallProvider.check(WallProvider.java:578)\r\n	at com.alibaba.druid.wall.WallFilter.checkInternal(WallFilter.java:785)\r\n	... 155 more\r\n');
INSERT INTO `sys_operation_log` VALUES ('567', '异常日志', '', '1', null, null, '2018-07-07 01:39:57', '失败', 'org.springframework.jdbc.UncategorizedSQLException: \r\n### Error querying database.  Cause: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC\r\n### The error may exist in com/stylefeng/guns/modular/system/dao/ProductMapper.java (best guess)\r\n### The error may involve com.stylefeng.guns.modular.system.dao.ProductMapper.selectMapsPage\r\n### The error occurred while executing a query\r\n### SQL: SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product   WHERE  (productName like \'%0%\') ORDER BY update_time DESC, update_time asc ASC\r\n### Cause: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC\n; uncategorized SQLException; SQL state [null]; error code [0]; sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC; nested exception is java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:89)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:81)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:81)\r\n	at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:73)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:446)\r\n	at com.sun.proxy.TProxy94.selectList(Unknown Source)\r\n	at org.mybatis.spring.SqlSessionTemplate.selectList(SqlSessionTemplate.java:238)\r\n	at org.apache.ibatis.binding.MapperMethod.executeForMany(MapperMethod.java:137)\r\n	at org.apache.ibatis.binding.MapperMethod.execute(MapperMethod.java:76)\r\n	at org.apache.ibatis.binding.MapperProxy.invoke(MapperProxy.java:59)\r\n	at com.sun.proxy.TProxy106.selectMapsPage(Unknown Source)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImpl.selectMapsPage(ServiceImpl.java:415)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImplTTFastClassBySpringCGLIBTT3e2398a4.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at com.alibaba.druid.support.spring.stat.DruidStatInterceptor.invoke(DruidStatInterceptor.java:72)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.service.impl.ProductServiceImplTTEnhancerBySpringCGLIBTTa96e4612.selectMapsPage(<generated>)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductController.list(ProductController.java:72)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductControllerTTFastClassBySpringCGLIBTT3362bf26.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint.proceed(MethodInvocationProceedingJoinPoint.java:89)\r\n	at com.stylefeng.guns.core.intercept.SessionHolderInterceptor.sessionKit(SessionHolderInterceptor.java:29)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:644)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:633)\r\n	at org.springframework.aop.aspectj.AspectJAroundAdvice.invoke(AspectJAroundAdvice.java:70)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:92)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductControllerTTEnhancerBySpringCGLIBTT23da1d8b.list(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:209)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:136)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:102)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:877)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:783)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:991)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:925)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:974)\r\n	at org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:877)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:661)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:851)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:742)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:52)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:449)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilterT1.call(AbstractShiroFilter.java:365)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:362)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.stylefeng.guns.core.xss.XssFilter.doFilter(XssFilter.java:31)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.alibaba.druid.support.http.WebStatFilter.doFilter(WebStatFilter.java:123)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:99)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HttpPutFormContentFilter.doFilterInternal(HttpPutFormContentFilter.java:109)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:81)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:200)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:198)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:496)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:140)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:81)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:87)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:342)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:803)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)\r\n	at org.apache.coyote.AbstractProtocolTConnectionHandler.process(AbstractProtocol.java:790)\r\n	at org.apache.tomcat.util.net.NioEndpointTSocketProcessor.doRun(NioEndpoint.java:1459)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\r\n	at java.util.concurrent.ThreadPoolExecutorTWorker.run(ThreadPoolExecutor.java:617)\r\n	at org.apache.tomcat.util.threads.TaskThreadTWrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC\r\n	at com.alibaba.druid.wall.WallFilter.checkInternal(WallFilter.java:798)\r\n	at com.alibaba.druid.wall.WallFilter.connection_prepareStatement(WallFilter.java:251)\r\n	at com.alibaba.druid.filter.FilterChainImpl.connection_prepareStatement(FilterChainImpl.java:568)\r\n	at com.alibaba.druid.proxy.jdbc.ConnectionProxyImpl.prepareStatement(ConnectionProxyImpl.java:342)\r\n	at com.alibaba.druid.pool.DruidPooledConnection.prepareStatement(DruidPooledConnection.java:349)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.logging.jdbc.ConnectionLogger.invoke(ConnectionLogger.java:55)\r\n	at com.sun.proxy.TProxy124.prepareStatement(Unknown Source)\r\n	at org.apache.ibatis.executor.statement.PreparedStatementHandler.instantiateStatement(PreparedStatementHandler.java:87)\r\n	at org.apache.ibatis.executor.statement.BaseStatementHandler.prepare(BaseStatementHandler.java:88)\r\n	at org.apache.ibatis.executor.statement.RoutingStatementHandler.prepare(RoutingStatementHandler.java:59)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Invocation.proceed(Invocation.java:49)\r\n	at com.baomidou.mybatisplus.plugins.PaginationInterceptor.intercept(PaginationInterceptor.java:128)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:61)\r\n	at com.sun.proxy.TProxy123.prepare(Unknown Source)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Invocation.proceed(Invocation.java:49)\r\n	at com.stylefeng.guns.core.datascope.DataScopeInterceptor.intercept(DataScopeInterceptor.java:46)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:61)\r\n	at com.sun.proxy.TProxy123.prepare(Unknown Source)\r\n	at org.apache.ibatis.executor.SimpleExecutor.prepareStatement(SimpleExecutor.java:85)\r\n	at org.apache.ibatis.executor.SimpleExecutor.doQuery(SimpleExecutor.java:62)\r\n	at org.apache.ibatis.executor.BaseExecutor.queryFromDatabase(BaseExecutor.java:326)\r\n	at org.apache.ibatis.executor.BaseExecutor.query(BaseExecutor.java:156)\r\n	at org.apache.ibatis.executor.CachingExecutor.query(CachingExecutor.java:109)\r\n	at org.apache.ibatis.executor.CachingExecutor.query(CachingExecutor.java:83)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.TProxy122.query(Unknown Source)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.selectList(DefaultSqlSession.java:148)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:433)\r\n	... 108 more\r\nCaused by: com.alibaba.druid.sql.parser.ParserException: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC\r\n	at com.alibaba.druid.sql.parser.SQLParser.printError(SQLParser.java:284)\r\n	at com.alibaba.druid.sql.parser.SQLStatementParser.parseStatementList(SQLStatementParser.java:426)\r\n	at com.alibaba.druid.sql.parser.SQLStatementParser.parseStatementList(SQLStatementParser.java:83)\r\n	at com.alibaba.druid.wall.WallProvider.checkInternal(WallProvider.java:624)\r\n	at com.alibaba.druid.wall.WallProvider.check(WallProvider.java:578)\r\n	at com.alibaba.druid.wall.WallFilter.checkInternal(WallFilter.java:785)\r\n	... 155 more\r\n');
INSERT INTO `sys_operation_log` VALUES ('568', '异常日志', '', '1', null, null, '2018-07-07 01:40:37', '失败', 'org.springframework.jdbc.UncategorizedSQLException: \r\n### Error querying database.  Cause: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC\r\n### The error may exist in com/stylefeng/guns/modular/system/dao/ProductMapper.java (best guess)\r\n### The error may involve com.stylefeng.guns.modular.system.dao.ProductMapper.selectMapsPage\r\n### The error occurred while executing a query\r\n### SQL: SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product   WHERE  (productName like \'%0%\') ORDER BY update_time DESC, update_time asc ASC\r\n### Cause: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC\n; uncategorized SQLException; SQL state [null]; error code [0]; sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC; nested exception is java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:89)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:81)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:81)\r\n	at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:73)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:446)\r\n	at com.sun.proxy.TProxy94.selectList(Unknown Source)\r\n	at org.mybatis.spring.SqlSessionTemplate.selectList(SqlSessionTemplate.java:238)\r\n	at org.apache.ibatis.binding.MapperMethod.executeForMany(MapperMethod.java:137)\r\n	at org.apache.ibatis.binding.MapperMethod.execute(MapperMethod.java:76)\r\n	at org.apache.ibatis.binding.MapperProxy.invoke(MapperProxy.java:59)\r\n	at com.sun.proxy.TProxy106.selectMapsPage(Unknown Source)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImpl.selectMapsPage(ServiceImpl.java:415)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImplTTFastClassBySpringCGLIBTT3e2398a4.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at com.alibaba.druid.support.spring.stat.DruidStatInterceptor.invoke(DruidStatInterceptor.java:72)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.service.impl.ProductServiceImplTTEnhancerBySpringCGLIBTTa96e4612.selectMapsPage(<generated>)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductController.list(ProductController.java:72)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductControllerTTFastClassBySpringCGLIBTT3362bf26.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint.proceed(MethodInvocationProceedingJoinPoint.java:89)\r\n	at com.stylefeng.guns.core.intercept.SessionHolderInterceptor.sessionKit(SessionHolderInterceptor.java:29)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:644)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:633)\r\n	at org.springframework.aop.aspectj.AspectJAroundAdvice.invoke(AspectJAroundAdvice.java:70)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:92)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductControllerTTEnhancerBySpringCGLIBTT23da1d8b.list(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:209)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:136)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:102)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:877)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:783)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:991)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:925)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:974)\r\n	at org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:877)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:661)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:851)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:742)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:52)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:449)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilterT1.call(AbstractShiroFilter.java:365)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:362)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.stylefeng.guns.core.xss.XssFilter.doFilter(XssFilter.java:31)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.alibaba.druid.support.http.WebStatFilter.doFilter(WebStatFilter.java:123)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:99)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HttpPutFormContentFilter.doFilterInternal(HttpPutFormContentFilter.java:109)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:81)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:200)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:198)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:496)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:140)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:81)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:87)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:342)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:803)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)\r\n	at org.apache.coyote.AbstractProtocolTConnectionHandler.process(AbstractProtocol.java:790)\r\n	at org.apache.tomcat.util.net.NioEndpointTSocketProcessor.doRun(NioEndpoint.java:1459)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\r\n	at java.util.concurrent.ThreadPoolExecutorTWorker.run(ThreadPoolExecutor.java:617)\r\n	at org.apache.tomcat.util.threads.TaskThreadTWrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (productName like \'%0%\')\nORDER BY update_time DESC, update_time asc ASC\r\n	at com.alibaba.druid.wall.WallFilter.checkInternal(WallFilter.java:798)\r\n	at com.alibaba.druid.wall.WallFilter.connection_prepareStatement(WallFilter.java:251)\r\n	at com.alibaba.druid.filter.FilterChainImpl.connection_prepareStatement(FilterChainImpl.java:568)\r\n	at com.alibaba.druid.proxy.jdbc.ConnectionProxyImpl.prepareStatement(ConnectionProxyImpl.java:342)\r\n	at com.alibaba.druid.pool.DruidPooledConnection.prepareStatement(DruidPooledConnection.java:349)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.logging.jdbc.ConnectionLogger.invoke(ConnectionLogger.java:55)\r\n	at com.sun.proxy.TProxy124.prepareStatement(Unknown Source)\r\n	at org.apache.ibatis.executor.statement.PreparedStatementHandler.instantiateStatement(PreparedStatementHandler.java:87)\r\n	at org.apache.ibatis.executor.statement.BaseStatementHandler.prepare(BaseStatementHandler.java:88)\r\n	at org.apache.ibatis.executor.statement.RoutingStatementHandler.prepare(RoutingStatementHandler.java:59)\r\n	at sun.reflect.GeneratedMethodAccessor72.invoke(Unknown Source)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Invocation.proceed(Invocation.java:49)\r\n	at com.baomidou.mybatisplus.plugins.PaginationInterceptor.intercept(PaginationInterceptor.java:128)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:61)\r\n	at com.sun.proxy.TProxy123.prepare(Unknown Source)\r\n	at sun.reflect.GeneratedMethodAccessor72.invoke(Unknown Source)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Invocation.proceed(Invocation.java:49)\r\n	at com.stylefeng.guns.core.datascope.DataScopeInterceptor.intercept(DataScopeInterceptor.java:46)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:61)\r\n	at com.sun.proxy.TProxy123.prepare(Unknown Source)\r\n	at org.apache.ibatis.executor.SimpleExecutor.prepareStatement(SimpleExecutor.java:85)\r\n	at org.apache.ibatis.executor.SimpleExecutor.doQuery(SimpleExecutor.java:62)\r\n	at org.apache.ibatis.executor.BaseExecutor.queryFromDatabase(BaseExecutor.java:326)\r\n	at org.apache.ibatis.executor.BaseExecutor.query(BaseExecutor.java:156)\r\n	at org.apache.ibatis.executor.CachingExecutor.query(CachingExecutor.java:109)\r\n	at org.apache.ibatis.executor.CachingExecutor.query(CachingExecutor.java:83)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.TProxy122.query(Unknown Source)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.selectList(DefaultSqlSession.java:148)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:433)\r\n	... 108 more\r\nCaused by: com.alibaba.druid.sql.parser.ParserException: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 380, line 3, column 44, token ASC\r\n	at com.alibaba.druid.sql.parser.SQLParser.printError(SQLParser.java:284)\r\n	at com.alibaba.druid.sql.parser.SQLStatementParser.parseStatementList(SQLStatementParser.java:426)\r\n	at com.alibaba.druid.sql.parser.SQLStatementParser.parseStatementList(SQLStatementParser.java:83)\r\n	at com.alibaba.druid.wall.WallProvider.checkInternal(WallProvider.java:624)\r\n	at com.alibaba.druid.wall.WallProvider.check(WallProvider.java:578)\r\n	at com.alibaba.druid.wall.WallFilter.checkInternal(WallFilter.java:785)\r\n	at com.alibaba.druid.wall.WallFilter.connection_prepareStatement(WallFilter.java:251)\r\n	at com.alibaba.druid.filter.FilterChainImpl.connection_prepareStatement(FilterChainImpl.java:568)\r\n	at com.alibaba.druid.proxy.jdbc.ConnectionProxyImpl.prepareStatement(ConnectionProxyImpl.java:342)\r\n	at com.alibaba.druid.pool.DruidPooledConnection.prepareStatement(DruidPooledConnection.java:349)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.logging.jdbc.ConnectionLogger.invoke(ConnectionLogger.java:55)\r\n	at com.sun.proxy.TProxy124.prepareStatement(Unknown Source)\r\n	at org.apache.ibatis.executor.statement.PreparedStatementHandler.instantiateStatement(PreparedStatementHandler.java:87)\r\n	at org.apache.ibatis.executor.statement.BaseStatementHandler.prepare(BaseStatementHandler.java:88)\r\n	at org.apache.ibatis.executor.statement.RoutingStatementHandler.prepare(RoutingStatementHandler.java:59)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Invocation.proceed(Invocation.java:49)\r\n	at com.baomidou.mybatisplus.plugins.PaginationInterceptor.intercept(PaginationInterceptor.java:128)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:61)\r\n	at com.sun.proxy.TProxy123.prepare(Unknown Source)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	... 132 more\r\n');
INSERT INTO `sys_operation_log` VALUES ('569', '异常日志', '', '1', null, null, '2018-07-07 01:41:36', '失败', 'org.springframework.jdbc.UncategorizedSQLException: \r\n### Error querying database.  Cause: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 376, line 3, column 39, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (product_name like \'%0%\')\nORDER BY update_time, update_time asc ASC\r\n### The error may exist in com/stylefeng/guns/modular/system/dao/ProductMapper.java (best guess)\r\n### The error may involve com.stylefeng.guns.modular.system.dao.ProductMapper.selectMapsPage\r\n### The error occurred while executing a query\r\n### SQL: SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product   WHERE  (product_name like \'%0%\') ORDER BY update_time, update_time asc ASC\r\n### Cause: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 376, line 3, column 39, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (product_name like \'%0%\')\nORDER BY update_time, update_time asc ASC\n; uncategorized SQLException; SQL state [null]; error code [0]; sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 376, line 3, column 39, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (product_name like \'%0%\')\nORDER BY update_time, update_time asc ASC; nested exception is java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 376, line 3, column 39, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (product_name like \'%0%\')\nORDER BY update_time, update_time asc ASC\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:89)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:81)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:81)\r\n	at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:73)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:446)\r\n	at com.sun.proxy.TProxy94.selectList(Unknown Source)\r\n	at org.mybatis.spring.SqlSessionTemplate.selectList(SqlSessionTemplate.java:238)\r\n	at org.apache.ibatis.binding.MapperMethod.executeForMany(MapperMethod.java:137)\r\n	at org.apache.ibatis.binding.MapperMethod.execute(MapperMethod.java:76)\r\n	at org.apache.ibatis.binding.MapperProxy.invoke(MapperProxy.java:59)\r\n	at com.sun.proxy.TProxy106.selectMapsPage(Unknown Source)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImpl.selectMapsPage(ServiceImpl.java:415)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImplTTFastClassBySpringCGLIBTT3e2398a4.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at com.alibaba.druid.support.spring.stat.DruidStatInterceptor.invoke(DruidStatInterceptor.java:72)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.service.impl.ProductServiceImplTTEnhancerBySpringCGLIBTTf7f85239.selectMapsPage(<generated>)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductController.list(ProductController.java:72)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductControllerTTFastClassBySpringCGLIBTT3362bf26.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint.proceed(MethodInvocationProceedingJoinPoint.java:89)\r\n	at com.stylefeng.guns.core.intercept.SessionHolderInterceptor.sessionKit(SessionHolderInterceptor.java:29)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:644)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:633)\r\n	at org.springframework.aop.aspectj.AspectJAroundAdvice.invoke(AspectJAroundAdvice.java:70)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:92)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductControllerTTEnhancerBySpringCGLIBTTb6986bb1.list(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:209)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:136)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:102)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:877)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:783)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:991)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:925)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:974)\r\n	at org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:877)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:661)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:851)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:742)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:52)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:449)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilterT1.call(AbstractShiroFilter.java:365)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:362)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.stylefeng.guns.core.xss.XssFilter.doFilter(XssFilter.java:31)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.alibaba.druid.support.http.WebStatFilter.doFilter(WebStatFilter.java:123)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:99)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HttpPutFormContentFilter.doFilterInternal(HttpPutFormContentFilter.java:109)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:81)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:200)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:198)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:496)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:140)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:81)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:87)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:342)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:803)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)\r\n	at org.apache.coyote.AbstractProtocolTConnectionHandler.process(AbstractProtocol.java:790)\r\n	at org.apache.tomcat.util.net.NioEndpointTSocketProcessor.doRun(NioEndpoint.java:1459)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\r\n	at java.util.concurrent.ThreadPoolExecutorTWorker.run(ThreadPoolExecutor.java:617)\r\n	at org.apache.tomcat.util.threads.TaskThreadTWrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.sql.SQLException: sql injection violation, syntax error: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 376, line 3, column 39, token ASC : SELECT  id,product_name AS productName,prodcut_code AS prodcutCode,category_id AS categoryId,shop_id AS shopId,brand_id AS brandId,main_image AS mainImage,sub_images AS subImages,product_type AS productType,detail,create_time AS createTime,update_time AS updateTime,del_flag AS delFlag  FROM product \n WHERE  (product_name like \'%0%\')\nORDER BY update_time, update_time asc ASC\r\n	at com.alibaba.druid.wall.WallFilter.checkInternal(WallFilter.java:798)\r\n	at com.alibaba.druid.wall.WallFilter.connection_prepareStatement(WallFilter.java:251)\r\n	at com.alibaba.druid.filter.FilterChainImpl.connection_prepareStatement(FilterChainImpl.java:568)\r\n	at com.alibaba.druid.proxy.jdbc.ConnectionProxyImpl.prepareStatement(ConnectionProxyImpl.java:342)\r\n	at com.alibaba.druid.pool.DruidPooledConnection.prepareStatement(DruidPooledConnection.java:349)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.logging.jdbc.ConnectionLogger.invoke(ConnectionLogger.java:55)\r\n	at com.sun.proxy.TProxy124.prepareStatement(Unknown Source)\r\n	at org.apache.ibatis.executor.statement.PreparedStatementHandler.instantiateStatement(PreparedStatementHandler.java:87)\r\n	at org.apache.ibatis.executor.statement.BaseStatementHandler.prepare(BaseStatementHandler.java:88)\r\n	at org.apache.ibatis.executor.statement.RoutingStatementHandler.prepare(RoutingStatementHandler.java:59)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Invocation.proceed(Invocation.java:49)\r\n	at com.baomidou.mybatisplus.plugins.PaginationInterceptor.intercept(PaginationInterceptor.java:128)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:61)\r\n	at com.sun.proxy.TProxy123.prepare(Unknown Source)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Invocation.proceed(Invocation.java:49)\r\n	at com.stylefeng.guns.core.datascope.DataScopeInterceptor.intercept(DataScopeInterceptor.java:46)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:61)\r\n	at com.sun.proxy.TProxy123.prepare(Unknown Source)\r\n	at org.apache.ibatis.executor.SimpleExecutor.prepareStatement(SimpleExecutor.java:85)\r\n	at org.apache.ibatis.executor.SimpleExecutor.doQuery(SimpleExecutor.java:62)\r\n	at org.apache.ibatis.executor.BaseExecutor.queryFromDatabase(BaseExecutor.java:326)\r\n	at org.apache.ibatis.executor.BaseExecutor.query(BaseExecutor.java:156)\r\n	at org.apache.ibatis.executor.CachingExecutor.query(CachingExecutor.java:109)\r\n	at org.apache.ibatis.executor.CachingExecutor.query(CachingExecutor.java:83)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.TProxy122.query(Unknown Source)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.selectList(DefaultSqlSession.java:148)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:433)\r\n	... 108 more\r\nCaused by: com.alibaba.druid.sql.parser.ParserException: syntax error, error in :\' asc ASC\', expect ASC, actual ASC pos 376, line 3, column 39, token ASC\r\n	at com.alibaba.druid.sql.parser.SQLParser.printError(SQLParser.java:284)\r\n	at com.alibaba.druid.sql.parser.SQLStatementParser.parseStatementList(SQLStatementParser.java:426)\r\n	at com.alibaba.druid.sql.parser.SQLStatementParser.parseStatementList(SQLStatementParser.java:83)\r\n	at com.alibaba.druid.wall.WallProvider.checkInternal(WallProvider.java:624)\r\n	at com.alibaba.druid.wall.WallProvider.check(WallProvider.java:578)\r\n	at com.alibaba.druid.wall.WallFilter.checkInternal(WallFilter.java:785)\r\n	... 155 more\r\n');
INSERT INTO `sys_operation_log` VALUES ('570', '异常日志', '', '1', null, null, '2018-07-07 01:43:49', '失败', 'org.mybatis.spring.MyBatisSystemException: nested exception is org.apache.ibatis.type.TypeException: Could not set parameters for mapping: ParameterMapping{property=\'ew.paramNameValuePairs.MPGENVAL1\', mode=IN, javaType=class java.lang.Object, jdbcType=null, numericScale=null, resultMapId=\'null\', jdbcTypeName=\'null\', expression=\'null\'}. Cause: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: java.sql.SQLException: Parameter index out of range (1 > number of parameters, which is 0).\r\n	at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:77)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:446)\r\n	at com.sun.proxy.TProxy94.selectList(Unknown Source)\r\n	at org.mybatis.spring.SqlSessionTemplate.selectList(SqlSessionTemplate.java:238)\r\n	at org.apache.ibatis.binding.MapperMethod.executeForMany(MapperMethod.java:137)\r\n	at org.apache.ibatis.binding.MapperMethod.execute(MapperMethod.java:76)\r\n	at org.apache.ibatis.binding.MapperProxy.invoke(MapperProxy.java:59)\r\n	at com.sun.proxy.TProxy106.selectMapsPage(Unknown Source)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImpl.selectMapsPage(ServiceImpl.java:415)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImplTTFastClassBySpringCGLIBTT3e2398a4.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at com.alibaba.druid.support.spring.stat.DruidStatInterceptor.invoke(DruidStatInterceptor.java:72)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.service.impl.ProductServiceImplTTEnhancerBySpringCGLIBTTdac39b9c.selectMapsPage(<generated>)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductController.list(ProductController.java:72)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductControllerTTFastClassBySpringCGLIBTT3362bf26.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint.proceed(MethodInvocationProceedingJoinPoint.java:89)\r\n	at com.stylefeng.guns.core.intercept.SessionHolderInterceptor.sessionKit(SessionHolderInterceptor.java:29)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:644)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:633)\r\n	at org.springframework.aop.aspectj.AspectJAroundAdvice.invoke(AspectJAroundAdvice.java:70)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:92)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.controller.ProductControllerTTEnhancerBySpringCGLIBTT320ee56d.list(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:209)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:136)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:102)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:877)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:783)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:991)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:925)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:974)\r\n	at org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:877)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:661)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:851)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:742)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:52)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:449)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilterT1.call(AbstractShiroFilter.java:365)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:362)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.stylefeng.guns.core.xss.XssFilter.doFilter(XssFilter.java:31)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.alibaba.druid.support.http.WebStatFilter.doFilter(WebStatFilter.java:123)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:99)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HttpPutFormContentFilter.doFilterInternal(HttpPutFormContentFilter.java:109)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:81)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:200)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:198)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:496)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:140)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:81)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:87)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:342)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:803)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)\r\n	at org.apache.coyote.AbstractProtocolTConnectionHandler.process(AbstractProtocol.java:790)\r\n	at org.apache.tomcat.util.net.NioEndpointTSocketProcessor.doRun(NioEndpoint.java:1459)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\r\n	at java.util.concurrent.ThreadPoolExecutorTWorker.run(ThreadPoolExecutor.java:617)\r\n	at org.apache.tomcat.util.threads.TaskThreadTWrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: org.apache.ibatis.type.TypeException: Could not set parameters for mapping: ParameterMapping{property=\'ew.paramNameValuePairs.MPGENVAL1\', mode=IN, javaType=class java.lang.Object, jdbcType=null, numericScale=null, resultMapId=\'null\', jdbcTypeName=\'null\', expression=\'null\'}. Cause: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: java.sql.SQLException: Parameter index out of range (1 > number of parameters, which is 0).\r\n	at com.baomidou.mybatisplus.MybatisDefaultParameterHandler.setParameters(MybatisDefaultParameterHandler.java:276)\r\n	at org.apache.ibatis.executor.statement.PreparedStatementHandler.parameterize(PreparedStatementHandler.java:93)\r\n	at org.apache.ibatis.executor.statement.RoutingStatementHandler.parameterize(RoutingStatementHandler.java:64)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.TProxy123.parameterize(Unknown Source)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.TProxy123.parameterize(Unknown Source)\r\n	at org.apache.ibatis.executor.SimpleExecutor.prepareStatement(SimpleExecutor.java:86)\r\n	at org.apache.ibatis.executor.SimpleExecutor.doQuery(SimpleExecutor.java:62)\r\n	at org.apache.ibatis.executor.BaseExecutor.queryFromDatabase(BaseExecutor.java:326)\r\n	at org.apache.ibatis.executor.BaseExecutor.query(BaseExecutor.java:156)\r\n	at org.apache.ibatis.executor.CachingExecutor.query(CachingExecutor.java:109)\r\n	at org.apache.ibatis.executor.CachingExecutor.query(CachingExecutor.java:83)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.TProxy122.query(Unknown Source)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.selectList(DefaultSqlSession.java:148)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:433)\r\n	... 108 more\r\nCaused by: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: java.sql.SQLException: Parameter index out of range (1 > number of parameters, which is 0).\r\n	at org.apache.ibatis.type.BaseTypeHandler.setParameter(BaseTypeHandler.java:55)\r\n	at com.baomidou.mybatisplus.MybatisDefaultParameterHandler.setParameters(MybatisDefaultParameterHandler.java:274)\r\n	... 140 more\r\nCaused by: org.apache.ibatis.type.TypeException: Error setting non null for parameter #1 with JdbcType null . Try setting a different JdbcType for this parameter or a different configuration property. Cause: java.sql.SQLException: Parameter index out of range (1 > number of parameters, which is 0).\r\n	at org.apache.ibatis.type.BaseTypeHandler.setParameter(BaseTypeHandler.java:55)\r\n	at org.apache.ibatis.type.UnknownTypeHandler.setNonNullParameter(UnknownTypeHandler.java:45)\r\n	at org.apache.ibatis.type.BaseTypeHandler.setParameter(BaseTypeHandler.java:53)\r\n	... 141 more\r\nCaused by: java.sql.SQLException: Parameter index out of range (1 > number of parameters, which is 0).\r\n	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:127)\r\n	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:95)\r\n	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:87)\r\n	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:61)\r\n	at com.mysql.cj.jdbc.ClientPreparedStatement.checkBounds(ClientPreparedStatement.java:1419)\r\n	at com.mysql.cj.jdbc.ClientPreparedStatement.getCoreParameterIndex(ClientPreparedStatement.java:1432)\r\n	at com.mysql.cj.jdbc.ClientPreparedStatement.setString(ClientPreparedStatement.java:1762)\r\n	at com.alibaba.druid.filter.FilterChainImpl.preparedStatement_setString(FilterChainImpl.java:3301)\r\n	at com.alibaba.druid.filter.FilterAdapter.preparedStatement_setString(FilterAdapter.java:1362)\r\n	at com.alibaba.druid.filter.FilterChainImpl.preparedStatement_setString(FilterChainImpl.java:3298)\r\n	at com.alibaba.druid.filter.FilterAdapter.preparedStatement_setString(FilterAdapter.java:1362)\r\n	at com.alibaba.druid.filter.FilterChainImpl.preparedStatement_setString(FilterChainImpl.java:3298)\r\n	at com.alibaba.druid.proxy.jdbc.PreparedStatementProxyImpl.setString(PreparedStatementProxyImpl.java:611)\r\n	at com.alibaba.druid.pool.DruidPooledPreparedStatement.setString(DruidPooledPreparedStatement.java:370)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.logging.jdbc.PreparedStatementLogger.invoke(PreparedStatementLogger.java:67)\r\n	at com.sun.proxy.TProxy125.setString(Unknown Source)\r\n	at org.apache.ibatis.type.StringTypeHandler.setNonNullParameter(StringTypeHandler.java:31)\r\n	at org.apache.ibatis.type.StringTypeHandler.setNonNullParameter(StringTypeHandler.java:26)\r\n	at org.apache.ibatis.type.BaseTypeHandler.setParameter(BaseTypeHandler.java:53)\r\n	... 143 more\r\n');
INSERT INTO `sys_operation_log` VALUES ('571', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-07 02:04:43', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('572', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-07 02:05:08', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('573', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-07 02:08:08', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('574', '业务日志', '修改菜单', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'edit', '2018-07-07 02:11:39', '成功', '菜单名称=店铺管理列表;;;字段名称:null,旧值:0,新值:1');
INSERT INTO `sys_operation_log` VALUES ('575', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-07 02:11:56', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('576', '业务日志', '修改菜单', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'edit', '2018-07-07 02:13:04', '成功', '菜单名称=店铺管理;;;字段名称:菜单父编号,旧值:1015401738892869633,新值:1015025486529028105');
INSERT INTO `sys_operation_log` VALUES ('577', '业务日志', '修改菜单', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'edit', '2018-07-07 02:13:33', '成功', '菜单名称=店铺管理;;;字段名称:null,旧值:1,新值:0');
INSERT INTO `sys_operation_log` VALUES ('578', '业务日志', '修改菜单', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'edit', '2018-07-07 02:14:02', '成功', '菜单名称=店铺管理;;;字段名称:null,旧值:0,新值:1');
INSERT INTO `sys_operation_log` VALUES ('579', '业务日志', '修改菜单', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'edit', '2018-07-07 02:14:41', '成功', '菜单名称=店铺管理列表;;;字段名称:null,旧值:1,新值:0');
INSERT INTO `sys_operation_log` VALUES ('580', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-08 01:14:47', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('581', '业务日志', '修改菜单', '1', 'com.stylefeng.guns.modular.system.controller.MenuController', 'edit', '2018-07-08 01:16:18', '成功', '菜单名称=商品SKU;;;字段名称:菜单父编号,旧值:1015401738892869633,新值:1015025486529028105');
INSERT INTO `sys_operation_log` VALUES ('582', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-11 04:09:52', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('583', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-11 04:14:46', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('584', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-11 04:20:10', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('585', '业务日志', '配置权限', '1', 'com.stylefeng.guns.modular.system.controller.RoleController', 'setAuthority', '2018-07-11 04:25:38', '成功', '角色名称=超级管理员,资源名称=系统管理,用户管理,添加用户,修改用户,删除用户,重置密码,冻结用户,解除冻结用户,分配角色,分配角色跳转,编辑用户跳转,用户列表,角色管理,添加角色,修改角色,删除角色,配置权限,修改角色跳转,角色分配跳转,角色列表,菜单管理,添加菜单,修改菜单,删除菜单,菜单编辑跳转,菜单列表,业务日志,清空日志,日志列表,日志详情,监控管理,部门管理,添加部门,修改部门,删除部门,修改部门跳转,部门列表,部门详情,字典管理,添加字典,修改字典,删除字典,修改菜单跳转,字典列表,字典详情,登录日志,清空登录日志,登录日志列表,通知管理,添加通知,修改通知,删除通知,通知,代码生成,接口文档');
INSERT INTO `sys_operation_log` VALUES ('586', '异常日志', '', '1', null, null, '2018-07-11 06:05:31', '失败', 'org.springframework.dao.DataIntegrityViolationException: \r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may involve com.stylefeng.guns.modular.system.dao.CategoryMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO category   ( pid,  category_name,  category_type,  sort,  image,  is_show )  VALUES   ( ?,  ?,  ?,  ?,  ?,  ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; ]; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n	at org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator.doTranslate(SQLErrorCodeSQLExceptionTranslator.java:246)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:72)\r\n	at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:73)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:446)\r\n	at com.sun.proxy.TProxy94.insert(Unknown Source)\r\n	at org.mybatis.spring.SqlSessionTemplate.insert(SqlSessionTemplate.java:278)\r\n	at org.apache.ibatis.binding.MapperMethod.execute(MapperMethod.java:58)\r\n	at org.apache.ibatis.binding.MapperProxy.invoke(MapperProxy.java:59)\r\n	at com.sun.proxy.TProxy107.insert(Unknown Source)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImpl.insert(ServiceImpl.java:98)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImplTTFastClassBySpringCGLIBTT3e2398a4.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.transaction.interceptor.TransactionAspectSupport.invokeWithinTransaction(TransactionAspectSupport.java:294)\r\n	at org.springframework.transaction.interceptor.TransactionInterceptor.invoke(TransactionInterceptor.java:98)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at com.alibaba.druid.support.spring.stat.DruidStatInterceptor.invoke(DruidStatInterceptor.java:72)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.service.impl.CategoryServiceImplTTEnhancerBySpringCGLIBTTfef236f6.insert(<generated>)\r\n	at com.stylefeng.guns.modular.mall.controller.CategoryController.add(CategoryController.java:72)\r\n	at com.stylefeng.guns.modular.mall.controller.CategoryControllerTTFastClassBySpringCGLIBTT684aad5f.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint.proceed(MethodInvocationProceedingJoinPoint.java:89)\r\n	at com.stylefeng.guns.core.intercept.SessionHolderInterceptor.sessionKit(SessionHolderInterceptor.java:29)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:644)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:633)\r\n	at org.springframework.aop.aspectj.AspectJAroundAdvice.invoke(AspectJAroundAdvice.java:70)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:92)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.controller.CategoryControllerTTEnhancerBySpringCGLIBTTe7241161.add(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:209)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:136)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:102)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:877)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:783)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:991)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:925)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:974)\r\n	at org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:877)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:661)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:851)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:742)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:52)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:449)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilterT1.call(AbstractShiroFilter.java:365)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:362)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.stylefeng.guns.core.xss.XssFilter.doFilter(XssFilter.java:31)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.alibaba.druid.support.http.WebStatFilter.doFilter(WebStatFilter.java:123)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:99)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HttpPutFormContentFilter.doFilterInternal(HttpPutFormContentFilter.java:109)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:81)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:200)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:198)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:496)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:140)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:81)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:87)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:342)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:803)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)\r\n	at org.apache.coyote.AbstractProtocolTConnectionHandler.process(AbstractProtocol.java:790)\r\n	at org.apache.tomcat.util.net.NioEndpointTSocketProcessor.doRun(NioEndpoint.java:1459)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\r\n	at java.util.concurrent.ThreadPoolExecutorTWorker.run(ThreadPoolExecutor.java:617)\r\n	at org.apache.tomcat.util.threads.TaskThreadTWrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:127)\r\n	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:95)\r\n	at com.mysql.cj.jdbc.exceptions.SQLExceptionsMapping.translateException(SQLExceptionsMapping.java:122)\r\n	at com.mysql.cj.jdbc.ClientPreparedStatement.executeInternal(ClientPreparedStatement.java:960)\r\n	at com.mysql.cj.jdbc.ClientPreparedStatement.execute(ClientPreparedStatement.java:388)\r\n	at com.alibaba.druid.filter.FilterChainImpl.preparedStatement_execute(FilterChainImpl.java:3409)\r\n	at com.alibaba.druid.filter.FilterEventAdapter.preparedStatement_execute(FilterEventAdapter.java:440)\r\n	at com.alibaba.druid.filter.FilterChainImpl.preparedStatement_execute(FilterChainImpl.java:3407)\r\n	at com.alibaba.druid.wall.WallFilter.preparedStatement_execute(WallFilter.java:619)\r\n	at com.alibaba.druid.filter.FilterChainImpl.preparedStatement_execute(FilterChainImpl.java:3407)\r\n	at com.alibaba.druid.proxy.jdbc.PreparedStatementProxyImpl.execute(PreparedStatementProxyImpl.java:167)\r\n	at com.alibaba.druid.pool.DruidPooledPreparedStatement.execute(DruidPooledPreparedStatement.java:498)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.logging.jdbc.PreparedStatementLogger.invoke(PreparedStatementLogger.java:59)\r\n	at com.sun.proxy.TProxy131.execute(Unknown Source)\r\n	at org.apache.ibatis.executor.statement.PreparedStatementHandler.update(PreparedStatementHandler.java:46)\r\n	at org.apache.ibatis.executor.statement.RoutingStatementHandler.update(RoutingStatementHandler.java:74)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.TProxy129.update(Unknown Source)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.TProxy129.update(Unknown Source)\r\n	at org.apache.ibatis.executor.SimpleExecutor.doUpdate(SimpleExecutor.java:50)\r\n	at org.apache.ibatis.executor.BaseExecutor.update(BaseExecutor.java:117)\r\n	at org.apache.ibatis.executor.CachingExecutor.update(CachingExecutor.java:76)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Invocation.proceed(Invocation.java:49)\r\n	at com.baomidou.mybatisplus.plugins.OptimisticLockerInterceptor.intercept(OptimisticLockerInterceptor.java:71)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:61)\r\n	at com.sun.proxy.TProxy128.update(Unknown Source)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.update(DefaultSqlSession.java:198)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.insert(DefaultSqlSession.java:185)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:433)\r\n	... 110 more\r\n');
INSERT INTO `sys_operation_log` VALUES ('587', '异常日志', '', '1', null, null, '2018-07-11 06:07:01', '失败', 'org.springframework.dao.DataIntegrityViolationException: \r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may involve com.stylefeng.guns.modular.system.dao.CategoryMapper.insert-Inline\r\n### The error occurred while setting parameters\r\n### SQL: INSERT INTO category   ( pid,  category_name,  category_type,  sort,  image,  is_show )  VALUES   ( ?,  ?,  ?,  ?,  ?,  ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; ]; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n	at org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator.doTranslate(SQLErrorCodeSQLExceptionTranslator.java:246)\r\n	at org.springframework.jdbc.support.AbstractFallbackSQLExceptionTranslator.translate(AbstractFallbackSQLExceptionTranslator.java:72)\r\n	at org.mybatis.spring.MyBatisExceptionTranslator.translateExceptionIfPossible(MyBatisExceptionTranslator.java:73)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:446)\r\n	at com.sun.proxy.TProxy94.insert(Unknown Source)\r\n	at org.mybatis.spring.SqlSessionTemplate.insert(SqlSessionTemplate.java:278)\r\n	at org.apache.ibatis.binding.MapperMethod.execute(MapperMethod.java:58)\r\n	at org.apache.ibatis.binding.MapperProxy.invoke(MapperProxy.java:59)\r\n	at com.sun.proxy.TProxy107.insert(Unknown Source)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImpl.insert(ServiceImpl.java:98)\r\n	at com.baomidou.mybatisplus.service.impl.ServiceImplTTFastClassBySpringCGLIBTT3e2398a4.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.transaction.interceptor.TransactionAspectSupport.invokeWithinTransaction(TransactionAspectSupport.java:294)\r\n	at org.springframework.transaction.interceptor.TransactionInterceptor.invoke(TransactionInterceptor.java:98)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at com.alibaba.druid.support.spring.stat.DruidStatInterceptor.invoke(DruidStatInterceptor.java:72)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.service.impl.CategoryServiceImplTTEnhancerBySpringCGLIBTTb9519ed0.insert(<generated>)\r\n	at com.stylefeng.guns.modular.mall.controller.CategoryController.add(CategoryController.java:76)\r\n	at com.stylefeng.guns.modular.mall.controller.CategoryControllerTTFastClassBySpringCGLIBTT684aad5f.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:204)\r\n	at org.springframework.aop.framework.CglibAopProxyTCglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:747)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint.proceed(MethodInvocationProceedingJoinPoint.java:89)\r\n	at com.stylefeng.guns.core.intercept.SessionHolderInterceptor.sessionKit(SessionHolderInterceptor.java:29)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:644)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:633)\r\n	at org.springframework.aop.aspectj.AspectJAroundAdvice.invoke(AspectJAroundAdvice.java:70)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:92)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:185)\r\n	at org.springframework.aop.framework.CglibAopProxyTDynamicAdvisedInterceptor.intercept(CglibAopProxy.java:689)\r\n	at com.stylefeng.guns.modular.mall.controller.CategoryControllerTTEnhancerBySpringCGLIBTTfc0c39c7.add(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:209)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:136)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:102)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:877)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:783)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:991)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:925)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:974)\r\n	at org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:877)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:661)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:851)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:742)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:52)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:61)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.executeChain(AdviceFilter.java:108)\r\n	at org.apache.shiro.web.servlet.AdviceFilter.doFilterInternal(AdviceFilter.java:137)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.shiro.web.servlet.ProxiedFilterChain.doFilter(ProxiedFilterChain.java:66)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.executeChain(AbstractShiroFilter.java:449)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilterT1.call(AbstractShiroFilter.java:365)\r\n	at org.apache.shiro.subject.support.SubjectCallable.doCall(SubjectCallable.java:90)\r\n	at org.apache.shiro.subject.support.SubjectCallable.call(SubjectCallable.java:83)\r\n	at org.apache.shiro.subject.support.DelegatingSubject.execute(DelegatingSubject.java:387)\r\n	at org.apache.shiro.web.servlet.AbstractShiroFilter.doFilterInternal(AbstractShiroFilter.java:362)\r\n	at org.apache.shiro.web.servlet.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:125)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.stylefeng.guns.core.xss.XssFilter.doFilter(XssFilter.java:31)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.alibaba.druid.support.http.WebStatFilter.doFilter(WebStatFilter.java:123)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.RequestContextFilter.doFilterInternal(RequestContextFilter.java:99)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HttpPutFormContentFilter.doFilterInternal(HttpPutFormContentFilter.java:109)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.HiddenHttpMethodFilter.doFilterInternal(HiddenHttpMethodFilter.java:81)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:200)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:107)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:198)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:96)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:496)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:140)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:81)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:87)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:342)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:803)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:66)\r\n	at org.apache.coyote.AbstractProtocolTConnectionHandler.process(AbstractProtocol.java:790)\r\n	at org.apache.tomcat.util.net.NioEndpointTSocketProcessor.doRun(NioEndpoint.java:1459)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)\r\n	at java.util.concurrent.ThreadPoolExecutorTWorker.run(ThreadPoolExecutor.java:617)\r\n	at org.apache.tomcat.util.threads.TaskThreadTWrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Thread.java:745)\r\nCaused by: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:127)\r\n	at com.mysql.cj.jdbc.exceptions.SQLError.createSQLException(SQLError.java:95)\r\n	at com.mysql.cj.jdbc.exceptions.SQLExceptionsMapping.translateException(SQLExceptionsMapping.java:122)\r\n	at com.mysql.cj.jdbc.ClientPreparedStatement.executeInternal(ClientPreparedStatement.java:960)\r\n	at com.mysql.cj.jdbc.ClientPreparedStatement.execute(ClientPreparedStatement.java:388)\r\n	at com.alibaba.druid.filter.FilterChainImpl.preparedStatement_execute(FilterChainImpl.java:3409)\r\n	at com.alibaba.druid.filter.FilterEventAdapter.preparedStatement_execute(FilterEventAdapter.java:440)\r\n	at com.alibaba.druid.filter.FilterChainImpl.preparedStatement_execute(FilterChainImpl.java:3407)\r\n	at com.alibaba.druid.wall.WallFilter.preparedStatement_execute(WallFilter.java:619)\r\n	at com.alibaba.druid.filter.FilterChainImpl.preparedStatement_execute(FilterChainImpl.java:3407)\r\n	at com.alibaba.druid.proxy.jdbc.PreparedStatementProxyImpl.execute(PreparedStatementProxyImpl.java:167)\r\n	at com.alibaba.druid.pool.DruidPooledPreparedStatement.execute(DruidPooledPreparedStatement.java:498)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.logging.jdbc.PreparedStatementLogger.invoke(PreparedStatementLogger.java:59)\r\n	at com.sun.proxy.TProxy131.execute(Unknown Source)\r\n	at org.apache.ibatis.executor.statement.PreparedStatementHandler.update(PreparedStatementHandler.java:46)\r\n	at org.apache.ibatis.executor.statement.RoutingStatementHandler.update(RoutingStatementHandler.java:74)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.TProxy129.update(Unknown Source)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:63)\r\n	at com.sun.proxy.TProxy129.update(Unknown Source)\r\n	at org.apache.ibatis.executor.SimpleExecutor.doUpdate(SimpleExecutor.java:50)\r\n	at org.apache.ibatis.executor.BaseExecutor.update(BaseExecutor.java:117)\r\n	at org.apache.ibatis.executor.CachingExecutor.update(CachingExecutor.java:76)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.apache.ibatis.plugin.Invocation.proceed(Invocation.java:49)\r\n	at com.baomidou.mybatisplus.plugins.OptimisticLockerInterceptor.intercept(OptimisticLockerInterceptor.java:71)\r\n	at org.apache.ibatis.plugin.Plugin.invoke(Plugin.java:61)\r\n	at com.sun.proxy.TProxy128.update(Unknown Source)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.update(DefaultSqlSession.java:198)\r\n	at org.apache.ibatis.session.defaults.DefaultSqlSession.insert(DefaultSqlSession.java:185)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n	at java.lang.reflect.Method.invoke(Method.java:497)\r\n	at org.mybatis.spring.SqlSessionTemplateTSqlSessionInterceptor.invoke(SqlSessionTemplate.java:433)\r\n	... 110 more\r\n');

-- ----------------------------
-- Table structure for sys_relation
-- ----------------------------
DROP TABLE IF EXISTS `sys_relation`;
CREATE TABLE `sys_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menuid` bigint(11) DEFAULT NULL COMMENT '菜单id',
  `roleid` int(11) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4793 DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_relation
-- ----------------------------
INSERT INTO `sys_relation` VALUES ('3377', '105', '5');
INSERT INTO `sys_relation` VALUES ('3378', '106', '5');
INSERT INTO `sys_relation` VALUES ('3379', '107', '5');
INSERT INTO `sys_relation` VALUES ('3380', '108', '5');
INSERT INTO `sys_relation` VALUES ('3381', '109', '5');
INSERT INTO `sys_relation` VALUES ('3382', '110', '5');
INSERT INTO `sys_relation` VALUES ('3383', '111', '5');
INSERT INTO `sys_relation` VALUES ('3384', '112', '5');
INSERT INTO `sys_relation` VALUES ('3385', '113', '5');
INSERT INTO `sys_relation` VALUES ('3386', '114', '5');
INSERT INTO `sys_relation` VALUES ('3387', '115', '5');
INSERT INTO `sys_relation` VALUES ('3388', '116', '5');
INSERT INTO `sys_relation` VALUES ('3389', '117', '5');
INSERT INTO `sys_relation` VALUES ('3390', '118', '5');
INSERT INTO `sys_relation` VALUES ('3391', '119', '5');
INSERT INTO `sys_relation` VALUES ('3392', '120', '5');
INSERT INTO `sys_relation` VALUES ('3393', '121', '5');
INSERT INTO `sys_relation` VALUES ('3394', '122', '5');
INSERT INTO `sys_relation` VALUES ('3395', '150', '5');
INSERT INTO `sys_relation` VALUES ('3396', '151', '5');
INSERT INTO `sys_relation` VALUES ('4706', '105', '1');
INSERT INTO `sys_relation` VALUES ('4707', '106', '1');
INSERT INTO `sys_relation` VALUES ('4708', '107', '1');
INSERT INTO `sys_relation` VALUES ('4709', '108', '1');
INSERT INTO `sys_relation` VALUES ('4710', '109', '1');
INSERT INTO `sys_relation` VALUES ('4711', '110', '1');
INSERT INTO `sys_relation` VALUES ('4712', '111', '1');
INSERT INTO `sys_relation` VALUES ('4713', '112', '1');
INSERT INTO `sys_relation` VALUES ('4714', '113', '1');
INSERT INTO `sys_relation` VALUES ('4715', '165', '1');
INSERT INTO `sys_relation` VALUES ('4716', '166', '1');
INSERT INTO `sys_relation` VALUES ('4717', '167', '1');
INSERT INTO `sys_relation` VALUES ('4718', '114', '1');
INSERT INTO `sys_relation` VALUES ('4719', '115', '1');
INSERT INTO `sys_relation` VALUES ('4720', '116', '1');
INSERT INTO `sys_relation` VALUES ('4721', '117', '1');
INSERT INTO `sys_relation` VALUES ('4722', '118', '1');
INSERT INTO `sys_relation` VALUES ('4723', '162', '1');
INSERT INTO `sys_relation` VALUES ('4724', '163', '1');
INSERT INTO `sys_relation` VALUES ('4725', '164', '1');
INSERT INTO `sys_relation` VALUES ('4726', '119', '1');
INSERT INTO `sys_relation` VALUES ('4727', '120', '1');
INSERT INTO `sys_relation` VALUES ('4728', '121', '1');
INSERT INTO `sys_relation` VALUES ('4729', '122', '1');
INSERT INTO `sys_relation` VALUES ('4730', '150', '1');
INSERT INTO `sys_relation` VALUES ('4731', '151', '1');
INSERT INTO `sys_relation` VALUES ('4732', '128', '1');
INSERT INTO `sys_relation` VALUES ('4733', '134', '1');
INSERT INTO `sys_relation` VALUES ('4734', '158', '1');
INSERT INTO `sys_relation` VALUES ('4735', '159', '1');
INSERT INTO `sys_relation` VALUES ('4736', '130', '1');
INSERT INTO `sys_relation` VALUES ('4737', '131', '1');
INSERT INTO `sys_relation` VALUES ('4738', '135', '1');
INSERT INTO `sys_relation` VALUES ('4739', '136', '1');
INSERT INTO `sys_relation` VALUES ('4740', '137', '1');
INSERT INTO `sys_relation` VALUES ('4741', '152', '1');
INSERT INTO `sys_relation` VALUES ('4742', '153', '1');
INSERT INTO `sys_relation` VALUES ('4743', '154', '1');
INSERT INTO `sys_relation` VALUES ('4744', '132', '1');
INSERT INTO `sys_relation` VALUES ('4745', '138', '1');
INSERT INTO `sys_relation` VALUES ('4746', '139', '1');
INSERT INTO `sys_relation` VALUES ('4747', '140', '1');
INSERT INTO `sys_relation` VALUES ('4748', '155', '1');
INSERT INTO `sys_relation` VALUES ('4749', '156', '1');
INSERT INTO `sys_relation` VALUES ('4750', '157', '1');
INSERT INTO `sys_relation` VALUES ('4751', '133', '1');
INSERT INTO `sys_relation` VALUES ('4752', '160', '1');
INSERT INTO `sys_relation` VALUES ('4753', '161', '1');
INSERT INTO `sys_relation` VALUES ('4754', '141', '1');
INSERT INTO `sys_relation` VALUES ('4755', '142', '1');
INSERT INTO `sys_relation` VALUES ('4756', '143', '1');
INSERT INTO `sys_relation` VALUES ('4757', '144', '1');
INSERT INTO `sys_relation` VALUES ('4758', '145', '1');
INSERT INTO `sys_relation` VALUES ('4759', '148', '1');
INSERT INTO `sys_relation` VALUES ('4760', '149', '1');
INSERT INTO `sys_relation` VALUES ('4761', '1015025486529028105', '1');
INSERT INTO `sys_relation` VALUES ('4762', '1016895452801982465', '1');
INSERT INTO `sys_relation` VALUES ('4763', '1016895452801982466', '1');
INSERT INTO `sys_relation` VALUES ('4764', '1016895452801982467', '1');
INSERT INTO `sys_relation` VALUES ('4765', '1016895452801982468', '1');
INSERT INTO `sys_relation` VALUES ('4766', '1016895452801982469', '1');
INSERT INTO `sys_relation` VALUES ('4767', '1016895452801982470', '1');
INSERT INTO `sys_relation` VALUES ('4768', '1016895575640563713', '1');
INSERT INTO `sys_relation` VALUES ('4769', '1016895765277630466', '1');
INSERT INTO `sys_relation` VALUES ('4770', '1016895765277630467', '1');
INSERT INTO `sys_relation` VALUES ('4771', '1016895765277630468', '1');
INSERT INTO `sys_relation` VALUES ('4772', '1016895765277630469', '1');
INSERT INTO `sys_relation` VALUES ('4773', '1016895765277630470', '1');
INSERT INTO `sys_relation` VALUES ('4774', '1016895765277630471', '1');
INSERT INTO `sys_relation` VALUES ('4775', '1016895830058655746', '1');
INSERT INTO `sys_relation` VALUES ('4776', '1016895830058655747', '1');
INSERT INTO `sys_relation` VALUES ('4777', '1016895830058655748', '1');
INSERT INTO `sys_relation` VALUES ('4778', '1016895830058655749', '1');
INSERT INTO `sys_relation` VALUES ('4779', '1016895830058655750', '1');
INSERT INTO `sys_relation` VALUES ('4780', '1016895830058655751', '1');
INSERT INTO `sys_relation` VALUES ('4781', '1016899635525513218', '1');
INSERT INTO `sys_relation` VALUES ('4782', '1016899635525513219', '1');
INSERT INTO `sys_relation` VALUES ('4783', '1016899635525513220', '1');
INSERT INTO `sys_relation` VALUES ('4784', '1016899635525513221', '1');
INSERT INTO `sys_relation` VALUES ('4785', '1016899635525513222', '1');
INSERT INTO `sys_relation` VALUES ('4786', '1016899635525513223', '1');
INSERT INTO `sys_relation` VALUES ('4787', '1016901161182593026', '1');
INSERT INTO `sys_relation` VALUES ('4788', '1016901161182593027', '1');
INSERT INTO `sys_relation` VALUES ('4789', '1016901161182593028', '1');
INSERT INTO `sys_relation` VALUES ('4790', '1016901161182593029', '1');
INSERT INTO `sys_relation` VALUES ('4791', '1016901161182593030', '1');
INSERT INTO `sys_relation` VALUES ('4792', '1016901161182593031', '1');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `num` int(11) DEFAULT NULL COMMENT '序号',
  `pid` int(11) DEFAULT NULL COMMENT '父角色id',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `deptid` int(11) DEFAULT NULL COMMENT '部门名称',
  `tips` varchar(255) DEFAULT NULL COMMENT '提示',
  `version` int(11) DEFAULT NULL COMMENT '保留字段(暂时没用）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '1', '0', '超级管理员', '24', 'administrator', '1');
INSERT INTO `sys_role` VALUES ('5', '2', '1', '临时', '26', 'temp', null);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `account` varchar(45) DEFAULT NULL COMMENT '账号',
  `password` varchar(45) DEFAULT NULL COMMENT '密码',
  `salt` varchar(45) DEFAULT NULL COMMENT 'md5密码盐',
  `name` varchar(45) DEFAULT NULL COMMENT '名字',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `sex` int(11) DEFAULT NULL COMMENT '性别（1：男 2：女）',
  `email` varchar(45) DEFAULT NULL COMMENT '电子邮件',
  `phone` varchar(45) DEFAULT NULL COMMENT '电话',
  `roleid` varchar(255) DEFAULT NULL COMMENT '角色id',
  `deptid` int(11) DEFAULT NULL COMMENT '部门id',
  `status` int(11) DEFAULT NULL COMMENT '状态(1：启用  2：冻结  3：删除）',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `version` int(11) DEFAULT NULL COMMENT '保留字段',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'girl.gif', 'admin', 'ecfadcde9305f8891bcfe5a1e28c253e', '8pgby', '张三', '2017-05-05 00:00:00', '2', 'sn93@qq.com', '18200000000', '1', '27', '1', '2016-01-29 08:49:53', '25');
INSERT INTO `sys_user` VALUES ('44', null, 'test', '45abb7879f6a8268f1ef600e6038ac73', 'ssts3', 'test', '2017-05-01 00:00:00', '1', 'abc@123.com', '', '5', '26', '3', '2017-05-16 20:33:37', null);
INSERT INTO `sys_user` VALUES ('45', null, 'boss', '71887a5ad666a18f709e1d4e693d5a35', '1f7bf', '老板', '2017-12-04 00:00:00', '1', '', '', '1', '24', '1', '2017-12-04 22:24:02', null);
INSERT INTO `sys_user` VALUES ('46', null, 'manager', 'b53cac62e7175637d4beb3b16b2f7915', 'j3cs9', '经理', '2017-12-04 00:00:00', '1', '', '', '1', '24', '1', '2017-12-04 22:24:24', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(32) NOT NULL COMMENT '用户表id',
  `nick_name` varchar(50) DEFAULT NULL COMMENT '昵称',
  `head` varchar(128) DEFAULT NULL COMMENT '用户头像',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `email` varchar(32) DEFAULT NULL COMMENT '邮箱',
  `password` varchar(32) DEFAULT NULL COMMENT '登录密码',
  `salt` varchar(32) DEFAULT NULL COMMENT '盐值',
  `login_type` int(11) DEFAULT NULL COMMENT '登录方式（1PC端，2安卓，3ios）',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后一次登录时间',
  `login_count` int(11) DEFAULT '0' COMMENT '登录次数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '最后一次更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name_unique` (`nick_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '周半仙1', 'https://gw.alipayobjects.com/zos/rmsportal/qjzOWKfpeBuOHCPncdJv.png', '13803490306', 'meijun_over@163.com', null, null, '1', '2018-07-19 14:18:49', '23', '2018-07-04 14:11:43', '2018-07-04 14:11:45');
INSERT INTO `user` VALUES ('1017606782747979777', null, null, '13800000000', null, null, null, null, null, null, null, null);
INSERT INTO `user` VALUES ('1017614559998414849', null, null, '15555555555', null, null, null, '1', '2018-07-13 13:43:02', null, null, null);
INSERT INTO `user` VALUES ('2', '涨盼望', 'https://gw.alipayobjects.com/zos/rmsportal/qjzOWKfpeBuOHCPncdJv.png', '15313601723', '536304123@qq.com', null, null, '1', '2018-07-11 10:09:41', '7', '2018-07-10 14:31:09', '2018-07-10 14:31:11');
