package com.zmjmall.demo.properties;

import lombok.Data;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zmj
 * @version 2018/7/3
 */
@Component
@ConfigurationProperties(prefix = "mall.login")
@Data
public class LoginProperties {

    /**
     * 要拦截的url
     */
//    @Value("#{${spring.redis.pool.max-active}}")
    private String  interceptorurls;
}
