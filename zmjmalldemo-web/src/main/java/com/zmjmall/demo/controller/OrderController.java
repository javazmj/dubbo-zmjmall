package com.zmjmall.demo.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zmjmall.demo.api.MallOrderService;
import com.zmjmall.demo.dto.OrderDTO;
import com.zmjmall.demo.enums.ResultEnum;
import com.zmjmall.demo.exception.OrderException;
import com.zmjmall.demo.utils.ResultVOUtil;
import com.zmjmall.demo.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 订单控制层
 * @author zmj
 * @version 2018/7/16
 */
@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {

    @Reference
    private MallOrderService mallOrderService;

    /**
     * 创建订单
     * @param userId
     * @param orderDTO
     * @return
     */
    @PostMapping
    @ResponseBody
    public ResultVO createOrder(String userId,@Valid OrderDTO orderDTO) {
        orderDTO.setUserId(userId);
        if(mallOrderService.createOrder(orderDTO)) {
            return ResultVOUtil.success(ResultEnum.SUCCESS);
        }
        throw new OrderException(ResultEnum.RESTFUL_API_ERROR);
    }

    /**
     * 产看所有订单
     * @param userId
     * @return
     */
    @GetMapping
    @ResponseBody
    public ResultVO findAll(String userId) {
       // List<OrderVO> list = mallOrderService.findAll(userId);
        return null;
    }

    /**
     * 产看所有订单
     * @param userId
     * @return
     */
    @GetMapping("/{orderId}")
    @ResponseBody
    public ResultVO findOne(String userId,@PathVariable String orderId) {
        // List<OrderVO> list = mallOrderService.findByUserIdAndOrderId(userId,orderId);
        return null;
    }

}
