package com.zmjmall.demo.controller;

import com.zmjmall.demo.enums.ResultEnum;
import com.zmjmall.demo.utils.ResultVOUtil;
import com.zmjmall.demo.vo.ResultVO;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * api 404控制
 *
 * Created by: meijun
 * Date: 2018/7/5 7:51
 */
@RestController
public class RestNotFoundFilter implements ErrorController{

    private static final String ERROR_PATH = "/error";


    /**
     * 404返回控制
     *
     * @return
     */
    @RequestMapping(value = ERROR_PATH)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ResultVO handleNotfound() {
        return ResultVOUtil.error(ResultEnum.RESTFUL_API_NOTFOUND);
    }



    /**
     * 500返回控制
     *
     * @return
     */
    /*@RequestMapping(value = ERROR_PATH)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResultVO handleError() {
        return ResultVOUtil.error(ResultEnum.RESTFUL_API_ERROR.getCode(),ResultEnum.RESTFUL_API_ERROR.getMsg());
    }*/

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
}
