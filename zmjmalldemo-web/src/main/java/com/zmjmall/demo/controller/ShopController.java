package com.zmjmall.demo.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zmjmall.demo.api.ShopService;
import com.zmjmall.demo.model.Shop;
import com.zmjmall.demo.utils.ResultVOUtil;
import com.zmjmall.demo.vo.ResultVO;
import com.zmjmall.demo.vo.ShopVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;


/**
 * 店铺信息控制层
 *
 * @author zmj
 * @version 2018/6/29
 */
@RestController
@RequestMapping("/shop")
@Slf4j
public class ShopController extends BaseController {

    @Reference
    private ShopService shopService;

    /**
     * 我的店铺详情
     * @param userId
     * @return
     */
    @GetMapping
    @ResponseBody
    public ResultVO<ShopVO> findMyShop(String userId) {
        Shop shop = shopService.findByUserId(userId);
        return ResultVOUtil.success(shop);
    }
}
