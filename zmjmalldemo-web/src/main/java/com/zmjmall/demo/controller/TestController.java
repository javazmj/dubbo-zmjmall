package com.zmjmall.demo.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.zmjmall.demo.api.ProductService;
import com.zmjmall.demo.dto.ProductDTO;
import com.zmjmall.demo.model.Product;
import com.zmjmall.demo.redis.GoodsKey;
import com.zmjmall.demo.utils.RedisUtil;
import com.zmjmall.demo.utils.ResultVOUtil;
import com.zmjmall.demo.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 测试controller
 * 以product示例，实现restful api 风格接口
 * 对于固定商品列表等的缓存用自带注解@Cacheable等（设置过期时间很麻烦）
 * 对于用户登录等信息的缓存用RedisUtil来灵活实现过期时间
 *
 * @author zmj
 * @version 2018/7/2
 */
@RestController
@RequestMapping("/test")
@Slf4j
@CacheConfig(cacheNames = "test")
public class TestController {

    @Reference
    private ProductService productService;

    @GetMapping("/test/{id}")
    @ResponseBody
    public ResultVO test(@PathVariable("id") String id) {
        log.info("[测试接口] ，id: {}" ,id);
        return ResultVOUtil.success(id);
    }


    /**
     * 查询全部
     *
     * @param pageNum
     * @param pageSize
     * @param sort
     * @return
     */
    @GetMapping
    @ResponseBody
    @Cacheable(value="productList")
    public ResultVO findAllPage(@RequestParam(defaultValue = "1",name = "pageNum",required = false)int pageNum
            ,@RequestParam(defaultValue = "1",name = "pageSize",required = false)int pageSize
            ,@RequestParam(defaultValue = "update_time asc",name = "sort",required = false)String sort) {

        List<Product> list = productService.findAllPage(pageNum,pageSize,sort);
        PageInfo<Product> pageInfo = new PageInfo(list);
        log.info("list列表：{}",pageInfo);

        return ResultVOUtil.success(pageInfo);
    }

    /**
     * 查询单个商品
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ResponseBody
    @Cacheable(key = "#p0")
    public ResultVO findById(@PathVariable("id") String id) {

        Product byId = productService.findById(id);
        RedisUtil.set(GoodsKey.getGoodsDetail, id, byId);
        return ResultVOUtil.success(byId);
    }

    /**
     * 新增商品
     *
     * @param product
     */
    @PostMapping
    @ResponseBody
    public void createProduct(@ModelAttribute ProductDTO product) {

        productService.insert(product);
        log.info("restful api接口请求测试 createProduct()");
    }

    /**
     * 修改商品
     *
     * @param product
     */
    @PutMapping
    @ResponseBody
    public void updateProduct(@ModelAttribute ProductDTO product) {

        productService.insert(product);
        log.info("restful api接口请求测试 updateProduct()");
    }

    /**
     * 删除商品
     *
     * @param id
     */
    @DeleteMapping("/{id}")
    @ResponseBody
    public void delProduct(@PathVariable String id) {
        productService.delete(id);
        log.info("restful api接口请求测试 delProduct()：id =  {} ", id);
    }
}
