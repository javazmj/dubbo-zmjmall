package com.zmjmall.demo.controller;

import com.alibaba.dubbo.common.json.JSON;
import com.alibaba.dubbo.common.json.ParseException;
import com.zmjmall.demo.constant.RedisConstant;
import com.zmjmall.demo.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * Created by: meijun
 * Date: 2018/7/8 17:03
 */
@Slf4j
public class BaseController {

    @Autowired
    protected StringRedisTemplate stringRedisTemplate;

    public User getUserInfoByToken(String token) {
        String redisUserInfo = stringRedisTemplate.opsForValue().get(String.format(RedisConstant.TOKEN_PREFIX, token));
        User user = null;
        try {
            user = JSON.parse(redisUserInfo, User.class);
        } catch (ParseException e) {
            e.printStackTrace();
            log.info("[获取token] 转化实体错误");
        }
        return user;
    }
}
