package com.zmjmall.demo.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zmjmall.demo.api.ProductService;
import com.zmjmall.demo.api.SkuProductService;
import com.zmjmall.demo.constant.FinalConstant;
import com.zmjmall.demo.dto.SkuProductDTO;
import com.zmjmall.demo.enums.ProductSortEnum;
import com.zmjmall.demo.enums.ResultEnum;
import com.zmjmall.demo.exception.MallAuthorizeException;
import com.zmjmall.demo.model.Product;
import com.zmjmall.demo.redis.GoodsKey;
import com.zmjmall.demo.util.EnumUtil;
import com.zmjmall.demo.util.JsonUtil;
import com.zmjmall.demo.util.SKUGenerator;
import com.zmjmall.demo.util.SortUtil;
import com.zmjmall.demo.utils.RedisUtil;
import com.zmjmall.demo.utils.ResultVOUtil;
import com.zmjmall.demo.vo.ResultVO;
import com.zmjmall.demo.vo.SkuAttributesVO;
import com.zmjmall.demo.vo.SkuProductVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zmj
 * @version 2018/7/10
 */
@RestController
@RequestMapping("/skuProduct")
@Slf4j
public class SkuProductController {

    @Reference
    private SkuProductService skuProductService;

    @Reference
    private ProductService productService;

    /**
     * 返回所有商品 分页返回
     * @return
     */
    @GetMapping
    @ResponseBody
    //@Cacheable(value="productList")
    public ResultVO<SkuProductVO> findAll(@RequestParam(defaultValue = "1",name = "pageNum",required = false)int pageNum
            ,@RequestParam(defaultValue = "10",name = "pageSize",required = false)int pageSize
            ,@RequestParam(defaultValue = "asc",name = "sort",required = false)String sort
            ,@RequestParam(defaultValue = "0",name = "sortName",required = false)Integer sortName
            , SkuProductDTO skuProductDTO) {
        if(StringUtils.equalsIgnoreCase(sort,FinalConstant.SORT_ASC) || StringUtils.equalsIgnoreCase(sort,FinalConstant.SORT_DESC) ) {
            ProductSortEnum byCode = EnumUtil.getByCode(sortName, ProductSortEnum.class);
            String sortParam = SortUtil.sort(sort, byCode.getMsg());

            List<SkuProductVO> list = skuProductService.findAllByPage(skuProductDTO, pageNum, pageSize, sortParam);
            for (SkuProductVO skuProductVO1:list) {
                //传输后是带转义字符的json串，转为list去掉转义重新设置
                String string = JsonUtil.toListString(skuProductVO1.getAttributeValues(), SkuAttributesVO.class);
                skuProductVO1.setAttributeValues(string);
            }
            return ResultVOUtil.success(new PageInfo<>(list));
        }
        throw new MallAuthorizeException(ResultEnum.PARAM_ERROR);
    }

    /**
     * 查询单个商品
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ResponseBody
    @Cacheable(key = "#p0")
    public ResultVO findById(@PathVariable("id") String id) {
        SkuProductDTO skuProductDTO = new SkuProductDTO();
        skuProductDTO.setId(id);
        SkuProductVO byId = skuProductService.findById(skuProductDTO);
        RedisUtil.set(GoodsKey.getGoodsDetail, id, byId);
        return ResultVOUtil.success(byId);
    }


    /**
     * 根据选择的属性添加产品到购物车
     *
     * @param skuProductDTO
     * @return
     */
    @PostMapping("/cartByAttribute")
    @ResponseBody
    public ResultVO arrributeCheckedCart(SkuProductDTO skuProductDTO ) {
        //根据sku id 获取productId
        SkuProductVO spv = skuProductService.findById(skuProductDTO);
        Product product = productService.findById(spv.getProductId());
        //shopId/ + 分类ID/ + productID/ + 属性ID(排序asc)
        String sku = SKUGenerator.createSKU(product.getShopId(), product.getCategoryId(),
                product.getId(), skuProductDTO.getAttributeValues());
        SkuProductVO skuProductVO1 = new SkuProductVO();
        skuProductVO1.setSku(sku);

        SkuProductVO byId = skuProductService.findById(skuProductDTO);

        if(byId == null) {
            log.error("[sku] 通过sku生成sku错误,未查找到该产品");
            throw new MallAuthorizeException(ResultEnum.RESTFUL_API_ERROR);
        }
        return ResultVOUtil.success(byId);
    }
}

