package com.zmjmall.demo.handle;

import com.zmjmall.demo.exception.MallAuthorizeException;
import com.zmjmall.demo.exception.OrderException;
import com.zmjmall.demo.exception.ResponseBankException;
import com.zmjmall.demo.utils.ResultVOUtil;
import com.zmjmall.demo.vo.ResultVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * 统一异常拦截器
 *
 * Created by: meijun
 * Date: 2018/3/20 19:55
 */
@ControllerAdvice
@Slf4j
public class ExceptionHandle {

    /**
     * 订单异常拦截
     * @param e
     * @return
     */
    @ExceptionHandler(value = OrderException.class)
    @ResponseBody
    public ResultVO handlerSellerException(OrderException e) {
        return ResultVOUtil.error(e.getCode(),e.getMessage());
    }

    /**
     * 商城统一拦截
     * @param e
     * @return
     */
    @ExceptionHandler(value = MallAuthorizeException.class)
    @ResponseBody
    public ResultVO handlerMallAuthorizeException(MallAuthorizeException e) {
        return ResultVOUtil.error(e.getCode(),e.getMessage());
    }


    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResultVO handlerMallAuthorizeExceptionServerError(Exception e) {
        if (e instanceof org.springframework.web.servlet.NoHandlerFoundException) {
            return ResultVOUtil.error(404,e.getMessage());
        }
        return ResultVOUtil.error(500,e.getMessage());

    }

    @ExceptionHandler(value = ResponseBankException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public void handlerResponseBankException() {
    }

    /**
     * 其他错误拦截
     * @param request
     * @param e
     * @return
     */
    /*@ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultVO handlerMallAuthorizeException(HttpServletRequest request,Exception e) {
        e.printStackTrace();
        if (e instanceof org.springframework.web.servlet.NoHandlerFoundException) {
           return ResultVOUtil.error(ResultEnum.RESTFUL_API_NOTFOUND.getCode(),
                   ResultEnum.RESTFUL_API_NOTFOUND.getMsg());
        }
        return ResultVOUtil.error(ResultEnum.RESTFUL_API_ERROR.getCode(),
                ResultEnum.RESTFUL_API_ERROR.getMsg());
    }*/
}
