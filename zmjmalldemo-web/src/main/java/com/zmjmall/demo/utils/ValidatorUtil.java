package com.zmjmall.demo.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author zmj
 * @version 2018/6/20
 */
public class ValidatorUtil {

    private static final Pattern mobile_pattern = Pattern.compile("^1[3|4|5|7|8]\\d{9}$",Pattern.CASE_INSENSITIVE);

    public static boolean isMobile(String src) {
        if(StringUtils.isEmpty(src)) {
            return false;
        }
        Matcher m = mobile_pattern.matcher(src);
        return m.matches();
    }
}
