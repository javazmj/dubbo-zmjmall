package com.zmjmall.demo.utils;

import com.alibaba.fastjson.JSON;
import com.zmjmall.demo.redis.KeyPrefix;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author zmj
 * @version 2018/7/3
 */
public class RedisUtil {

    private static JedisPool jedisPool = null;
    // Redis服务器IP
    private static String ADDR = "127.0.0.1";
    // Redis的端口号
    private static int PORT = 6379;
    // 访问密码
    private static String AUTH = "123456";

    /**
     * 初始化Redis连接池
     */
    static {
        try {
            JedisPoolConfig config = new JedisPoolConfig();
            // 连接耗尽时是否阻塞, false报异常,ture阻塞直到超时, 默认true
            config.setBlockWhenExhausted(true);
            // 设置的逐出策略类名, 默认DefaultEvictionPolicy(当连接超过最大空闲时间,或连接数超过最大空闲连接数)
            config.setEvictionPolicyClassName("org.apache.commons.pool2.impl.DefaultEvictionPolicy");
            // 是否启用pool的jmx管理功能, 默认true
            config.setJmxEnabled(true);
            // 最大空闲连接数, 默认8个 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例。
            config.setMaxIdle(8);
            // 最大连接数, 默认8个
            config.setMaxTotal(200);
            // 表示当borrow(引入)一个jedis实例时，最大的等待时间，如果超过等待时间，则直接抛出JedisConnectionException；
            config.setMaxWaitMillis(1000 * 100);
            // 在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
            config.setTestOnBorrow(true);
            //jedisPool = new JedisPool(config, ADDR, PORT, 3000, AUTH);
            jedisPool = new JedisPool(config, ADDR, PORT, 3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取Jedis实例
     *
     * @return
     */
    public synchronized static Jedis getJedis() {
        try {
            if (jedisPool != null) {
                Jedis resource = jedisPool.getResource();
                return resource;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 释放jedis资源
     *
     * @param jedis
     */
    public static void close(final Jedis jedis) {
        if (jedis != null) {
            jedis.close();
        }
    }

    public static void main(String[] args) {
        Jedis jedis = RedisUtil.getJedis();
      RedisUtil.testString(jedis);
       // RedisUtil.testMap(jedis);
//      RedisUtil.testList(jedis);
//      RedisUtil.testSet(jedis);

        RedisUtil.close(jedis);
    }

    /**
     * 字符串测试
     *
     * @param jedis
     */
    public static void testString(Jedis jedis) {
        jedis.set("name", "xxxx");// 向key-->name中放入了value-->xinxin
        System.out.println(jedis.get("name"));// 执行结果：xinxin

        jedis.append("name", " is my lover"); // 拼接
        System.out.println(jedis.get("name"));

        jedis.del("name"); // 删除某个键
        System.out.println(jedis.get("name"));
        // 设置多个键值对
        jedis.mset("name", "某某某", "age", "24", "qq", "476777XXX");
        jedis.incr("age"); // 进行加1操作
        System.out.println(jedis.get("name") + "-" + jedis.get("age") + "-"
                + jedis.get("qq"));
    }

    /**
     * map 用法
     *
     * @param jedis
     */
    public static void testMap(Jedis jedis) {
        // -----添加数据----------
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", "xinxin");
        map.put("age", "22");
        map.put("qq", "123456");
        jedis.hmset("user", map);
        // 取出user中的name，执行结果:[minxr]-->注意结果是一个泛型的List
        // 第一个参数是存入redis中map对象的key，后面跟的是放入map中的对象的key，后面的key可以跟多个，是可变参数
        List<String> rsmap = jedis.hmget("user", "name", "age", "qq");
        System.out.println(rsmap);

        // 删除map中的某个键值
        jedis.hdel("user", "age");
        System.out.println(jedis.hmget("user", "age")); // 因为删除了，所以返回的是null
        System.out.println(jedis.hlen("user")); // 返回key为user的键中存放的值的个数2
        System.out.println(jedis.exists("user"));// 是否存在key为user的记录 返回true
        System.out.println(jedis.hkeys("user"));// 返回map对象中的所有key
        System.out.println(jedis.hvals("user"));// 返回map对象中的所有value

        Iterator<String> iter = jedis.hkeys("user").iterator();
        while (iter.hasNext()) {
            String key = iter.next();
            System.out.println(key + ":" + jedis.hmget("user", key));
        }
    }

    /**
     * jedis操作List
     */
    public static void testList(Jedis jedis) {
        // 开始前，先移除所有的内容
        jedis.del("java framework");
        System.out.println(jedis.lrange("java framework", 0, -1));
        // 先向key java framework中存放三条数据
        jedis.lpush("java framework", "spring");
        jedis.lpush("java framework", "struts");
        jedis.lpush("java framework", "hibernate");
        // 再取出所有数据jedis.lrange是按范围取出，
        // 第一个是key，第二个是起始位置，第三个是结束位置，jedis.llen获取长度 -1表示取得所有
        System.out.println(jedis.lrange("java framework", 0, -1));

        jedis.del("java framework");
        jedis.rpush("java framework", "spring");
        jedis.rpush("java framework", "struts");
        jedis.rpush("java framework", "hibernate");
        System.out.println(jedis.lrange("java framework", 0, -1));
    }

    /**
     * jedis操作Set
     */
    public static void testSet(Jedis jedis){
        //添加
        jedis.sadd("user","liuling");
        jedis.sadd("user","xinxin");
        jedis.sadd("user","ling");
        jedis.sadd("user","zhangxinxin");
        jedis.sadd("user","who");
        //移除noname
        jedis.srem("user","who");
        System.out.println(jedis.smembers("user"));//获取所有加入的value
        System.out.println(jedis.sismember("user", "who"));//判断 who 是否是user集合的元素
        System.out.println(jedis.srandmember("user"));
        System.out.println(jedis.scard("user"));//返回集合的元素个数
    }

    /**
     * 获取redis值
     * @param prefix
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T get(KeyPrefix prefix, String key, Class<T> clazz) {
        Jedis jedis = null;
        try{
            jedis = jedisPool.getResource();
            //生成key
            String realKey = prefix.getPrefix() + key;
            String str = jedis.get(realKey);
            T t = stringToBean(str,clazz);
            return  t;
        }finally {
            returnToPool(jedis);
        }
    }
    /**
     * 获取redis值
     * @param prefix
     * @param key
     * @return
     */
    public static String  get(KeyPrefix prefix, String key) {
        Jedis jedis = null;
        try{
            jedis = jedisPool.getResource();
            //生成key
            String realKey = prefix.getPrefix() + key;
            String str = jedis.get(realKey);
            return  str;
        }finally {
            returnToPool(jedis);
        }
    }
    /**
     * 设置redis值
     * @param prefix
     * @param key
     * @param value
     * @param <T>
     * @return
     */
    public static <T> boolean set(KeyPrefix prefix,String key,T value) {
        Jedis jedis = null;
        try{
            jedis = jedisPool.getResource();
            String str = beanToString(value);
            if(str == null || str.length() <= 0) {
                return false;
            }
            //生成key
            String realKey = prefix.getPrefix() + key;
            int expireSeconds = prefix.expireSeconds();
            if(expireSeconds <= 0){
                jedis.set(realKey, str);
            }else {
                jedis.setex(realKey,expireSeconds,str);
            }
            return true;
        }finally {
            returnToPool(jedis);
        }
    }
    /**
     * 设置redis值
     * @param prefix
     * @param value
     * @param <T>
     * @return
     */
    public static <T> boolean setList(KeyPrefix prefix,List<T> value) {
        Jedis jedis = null;
        try{
            jedis = jedisPool.getResource();
            if(value == null || value.size() <= 0) {
                return false;
            }
            //生成key
            String realKey = prefix.getPrefix();
            int expireSeconds = prefix.expireSeconds();
            if(expireSeconds <= 0){
                for (T t:value) {
                    jedis.lpush(realKey,t.toString());
                }
            }else {
                for (T t:value) {
                    jedis.lpush(realKey,t.toString());
                }
                jedis.expire(realKey,expireSeconds);
            }
            return true;
        }finally {
            returnToPool(jedis);
        }
    }

    /**
     * 获取redis list
     * @param prefix
     * @param start
     * @param end
     * @return
     */
    public static List<String> getList(KeyPrefix prefix,int start,int end) {
        Jedis jedis = null;
        try{
            jedis = jedisPool.getResource();
            if(!exists(prefix,"")) {
                return null;
            }
            return jedis.lrange(prefix.getPrefix(), start, end);

        }finally {
            returnToPool(jedis);
        }
    }
    /**
     * 获取redis list
     * @param prefix
     * @return
     */
    public static List<String> getList(KeyPrefix prefix) {
        Jedis jedis = null;
        try{
            jedis = jedisPool.getResource();
            if(!exists(prefix,"")) {
                return null;
            }
            return jedis.lrange(prefix.getPrefix(), 0, -1);

        }finally {
            returnToPool(jedis);
        }
    }
    /**
     * 判断key是否存在
     * @param prefix
     * @param key
     * @param <T>
     * @return
     */
    public static <T> boolean exists(KeyPrefix prefix, String key) {
        Jedis jedis = null;
        try {
            jedis =  jedisPool.getResource();
            //生成真正的key
            String realKey  = prefix.getPrefix() + key;
            return  jedis.exists(realKey);
        }finally {
            returnToPool(jedis);
        }
    }

    /**
     * 删除
     * */
    public boolean delete(KeyPrefix prefix, String key) {
        Jedis jedis = null;
        try {
            jedis =  jedisPool.getResource();
            //生成真正的key
            String realKey  = prefix.getPrefix() + key;
            long ret =  jedis.del(key);
            return ret > 0;
        }finally {
            returnToPool(jedis);
        }
    }

    /**
     * 增加值
     * @param prefix
     * @param key
     * @param <T>
     * @return
     */
    public <T> Long incr(KeyPrefix prefix, String key) {
        Jedis jedis = null;
        try {
            jedis =  jedisPool.getResource();
            //生成真正的key
            String realKey  = prefix.getPrefix() + key;
            return  jedis.incr(realKey);
        }finally {
            returnToPool(jedis);
        }
    }

    /**
     * 减少值
     * @param prefix
     * @param key
     * @param <T>
     * @return
     */
    public <T> Long decr(KeyPrefix prefix, String key) {
        Jedis jedis = null;
        try {
            jedis =  jedisPool.getResource();
            //生成真正的key
            String realKey  = prefix.getPrefix() + key;
            return  jedis.decr(realKey);
        }finally {
            returnToPool(jedis);
        }
    }

    private static <T> String beanToString(T value) {
        if (value == null) {
            return null;
        }
        Class<?> aClass = value.getClass();

        if(aClass == int.class || aClass == Integer.class){
            return ""+value;
        }else if(aClass == String.class) {
            return (String)value;
        }else if (aClass == long.class || aClass == Long.class){
            return ""+value;
        }else {
            return JSON.toJSONString(value);
        }
    }

    private static <T> T  stringToBean(String str,Class<T> cClass) {
        if(str == null || str.length() <= 0 || cClass == null){
            return null;
        }
        if(cClass == int.class || cClass == Integer.class) {
            return (T)Integer.valueOf(str);
        }else if(cClass == String.class) {
            return (T)str;
        }else if(cClass == long.class || cClass == Long.class) {
            return  (T)Long.valueOf(str);
        }else {
            return JSON.toJavaObject(JSON.parseObject(str), cClass);
        }
    }

    private static void returnToPool(Jedis jedis) {
        if(jedis != null) {
            jedis.close();
        }
    }
}
