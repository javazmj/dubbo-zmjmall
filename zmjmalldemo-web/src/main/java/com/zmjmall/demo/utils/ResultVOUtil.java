package com.zmjmall.demo.utils;


import com.zmjmall.demo.enums.ResultEnum;
import com.zmjmall.demo.vo.ResultVO;

/**
 * Created by: meijun
 * Date: 2017/12/19 14:29
 */
public class ResultVOUtil {
    /**
     * 成功返回
     * @param o
     * @return
     */
    public  static ResultVO success (Object o) {
        ResultVO  resultVO = new ResultVO();
        resultVO.setCode(ResultEnum.SUCCESS.getCode());
        resultVO.setMsg(ResultEnum.SUCCESS.getMsg());
        resultVO.setData(o);

        return  resultVO;
    }

    /**
     * 失败返回
     * @param resultEnum
     * @return
     */
    public  static ResultVO error (ResultEnum resultEnum) {
        ResultVO  resultVO = new ResultVO();
        resultVO.setCode(resultEnum.getCode());
        resultVO.setMsg(resultEnum.getMsg());
        return resultVO;
    }

    public  static ResultVO error (Integer code, String msg) {
        ResultVO  resultVO = new ResultVO();
        resultVO.setCode(code);
        resultVO.setMsg(msg);
        return  resultVO;
    }
}
