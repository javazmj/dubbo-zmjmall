package com.zmjmall.demo.exception;

import com.zmjmall.demo.enums.ResultEnum;
import lombok.Getter;

/**
 * Created by: meijun
 * Date: 2018/3/20 19:35
 */
@Getter
public class MallAuthorizeException extends RuntimeException {

    private Integer code;

    public MallAuthorizeException(ResultEnum resultEnum) {
        super(resultEnum.getMsg() );
        this.code = resultEnum.getCode();
    }

    public MallAuthorizeException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
