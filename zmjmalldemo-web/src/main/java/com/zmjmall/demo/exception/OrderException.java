package com.zmjmall.demo.exception;


import com.zmjmall.demo.enums.ResultEnum;
import lombok.Getter;

/**
 * Created by: meijun
 * Date: 2017/12/20 16:36
 */
@Getter
public class OrderException extends  RuntimeException {

    private Integer code;

    public OrderException(ResultEnum resultEnum) {
        super(resultEnum.getMsg() );
        this.code = resultEnum.getCode();
    }

    public OrderException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
