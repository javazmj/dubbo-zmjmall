package com.zmjmall.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author zmj
 * @version 2018/7/17
 */
@Configuration
@ConfigurationProperties(prefix = "qiniu")
@Data
public class QiniuConfig {

    private String accesskey;

    private String secretkey;

    private String bucket;

    private String path;
}
