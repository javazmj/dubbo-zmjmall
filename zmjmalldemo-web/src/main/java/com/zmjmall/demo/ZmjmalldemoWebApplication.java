package com.zmjmall.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ImportResource("classpath:consumer.xml") //加载xml配置文件
@ComponentScan(basePackages = "com.zmjmall.demo")
@EnableScheduling
@EnableCaching
@EnableAsync
@EnableJpaAuditing
public class ZmjmalldemoWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZmjmalldemoWebApplication.class, args);
	}
}
