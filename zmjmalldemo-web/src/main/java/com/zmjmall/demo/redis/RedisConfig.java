package com.zmjmall.demo.redis;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by: meijun
 * Date: 2018/4/6 11:26
 */
@Data
@Component
@ConfigurationProperties(prefix = "spring.redis")
public class RedisConfig {

    private String host;

    private int port;

    private int timeout;//秒

    private String password;

    private int poolMaxActive;

    private int poolMaxIdle;

    private int poolMaxWait;//秒

}
