package com.zmjmall.demo.redis;

/**
 * Created by: meijun
 * Date: 2018/4/7 19:25
 */
public class UserKey extends  BasePrefix {

    public static UserKey getById = new UserKey("id");
    public static UserKey getByName = new UserKey("name");
    public static UserKey token = new UserKey("token");
    public static UserKey getCode = new UserKey("loginCode");

    private UserKey( String prefix) {
        super(prefix);
    }

}
