package com.zmjmall.demo.redis;

/**
 * Created by: meijun
 * Date: 2018/4/7 19:25
 */
public class OrderKey extends BasePrefix {

    public OrderKey(int expireSeconds, String prefix) {
        super(expireSeconds, prefix);
    }
}
