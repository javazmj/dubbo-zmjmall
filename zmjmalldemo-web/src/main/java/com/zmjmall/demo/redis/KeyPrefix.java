package com.zmjmall.demo.redis;

/**
 * Created by: meijun
 * Date: 2018/4/7 19:18
 */
public interface KeyPrefix {

    int expireSeconds();

    String getPrefix();
}
