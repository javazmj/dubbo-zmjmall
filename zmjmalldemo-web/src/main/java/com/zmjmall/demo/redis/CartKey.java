package com.zmjmall.demo.redis;

/**
 * @author zmj
 * @version 2018/7/12
 */
public class CartKey extends BasePrefix {

    private CartKey(int expireSeconds, String prefix) {
        super(expireSeconds, prefix);
    }
    public static CartKey cart = new CartKey(36000,"cart");

}
