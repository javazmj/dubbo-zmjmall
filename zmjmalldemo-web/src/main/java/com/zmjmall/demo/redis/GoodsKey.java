package com.zmjmall.demo.redis;

/**
 * @author zmj
 * @version 2018/6/22
 */
public class GoodsKey extends BasePrefix {

    private GoodsKey(int expireSeconds, String prefix) {
        super(expireSeconds, prefix);
    }

    public static GoodsKey getGoodsList = new GoodsKey(60,"gl");

    public static GoodsKey getGoodsDetail = new GoodsKey(60,"gd");

    public static GoodsKey skuProduct = new GoodsKey(36000,"sk");
}
