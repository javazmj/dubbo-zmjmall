package com.zmjmall.demo.redis;

/**
 * Created by: meijun
 * Date: 2018/4/7 19:22
 */
public abstract class BasePrefix implements KeyPrefix {

    private int expireSeconds;

    private String prefix;

    /**
     * 0代表永不过期
     * @param prefix
     */
    public BasePrefix(String prefix) {
       this(0,prefix);
    }


    public BasePrefix(int expireSeconds, String prefix) {
        this.expireSeconds = expireSeconds;
        this.prefix = prefix;
    }

    public int expireSeconds() {
        return expireSeconds;
    }

    public String getPrefix() {
        String className = getClass().getSimpleName();
        return className + ":" + prefix;
    }
}
