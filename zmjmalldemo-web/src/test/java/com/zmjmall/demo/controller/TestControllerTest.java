package com.zmjmall.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



/**
 * Created by: meijun
 * Date: 2018/7/6 22:44
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class TestControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private TestController testController;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void findAllPage() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/test"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        log.info(mvcResult.getResponse().getContentAsString());
        Assert.assertNotNull(mvcResult);
    }

    @Test
    public void findById() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/test/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        log.info(mvcResult.getResponse().getContentAsString());
        Assert.assertNotNull(mvcResult);
    }

    @Test
    public void createProduct() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/test").param("productName","商品"))
                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        log.info("返回新增商品:" + mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void updateProduct() throws Exception {
        MvcResult mvcResult = mockMvc.perform(put("/test").param("id","1"))
                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andReturn();
        log.info("返回新增商品:" + mvcResult.getResponse().getContentAsString());
    }

    @Test
    public void delProduct() throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/test/1"))
                .andExpect(status().isOk())
                .andReturn();
        log.info(mvcResult.getResponse().getContentAsString());
    }

}