package com.zmjmall.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZmjmallAutocodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZmjmallAutocodeApplication.class, args);
	}
}
