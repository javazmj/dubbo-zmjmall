package com.zmjmall.demo.codeBuild;


import com.zmjmall.demo.utils.FileUtil;
import com.zmjmall.demo.utils.PropertiesUtil;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * 功能：生成本框架的基础代码，包括java类，xml文件，前台文件
 * date:2017-4-10
 */
public class CodeEngine {
    //项目公共包名
    private static final String PROJECTPACK = "zmjmall.demo";
    //目录名
    private static final String DirctPack = "crm";
    //数据库表名
    private static final String TABLENAME = "c_sale_business_statistic";
    //生成class名称
    private static String CLASSNAME = "Order";
    //数据库名称
    private static final String TABLESCHEMA = "order";

    private static final String TABLEKEY = "id";   //表主键

    private static final String PACKAGENAME = "com." + PROJECTPACK ;
    private static final String PATHNAME = "com/" + PROJECTPACK;
    private static final String SERVICEAPPNAME = "../zmjmall-service";//service项目名,以及模板文件所在项目名
    private static final String CLIENTAPPRNAME = "../zmjmalldemo-api";//model项目名
    private static final String DUBBOPATH = SERVICEAPPNAME+"/src/main/resources/applicationContextProvider.xml";//dubbo配置文件所在目录


    private String INDEXDISFIELD = ""; //列表显示字段
    private String SEARCHFIELD = "";   //列表搜索字段
    private static final String FUNNAME = "";  //功能名称
    private static final String FUNCPATH = "";

    //功能名称
    private static final String AUTHOR = "system";


    public static final String SRC_PATH = "src/main/java/";  //src根目录
    public static final String ROOT_SAVEPATH = SRC_PATH + PATHNAME; //源码根目录
    public static final String ROOT_SAVEMAPPERXMLPATH = SRC_PATH + "/resources/mappers/"; //源码根目录
    public static final String TEMPLATEPATH = "src/main/java/codeBuild/";  //模板路径

    /**** 生成后文件存放的位置  ******/
    public static final String POJO_SAVEPATH = ROOT_SAVEPATH + "/model";  //pojo类存放的位置
    public static final String QUERYFORM_SAVEPATH = POJO_SAVEPATH + "/query";  //表单查询实体类存放的位置
    public static final String SQLMAP_SAVEPATH = ROOT_SAVEMAPPERXMLPATH;  //ibatis的sql.xml文件存放的位置
    public static final String DAO_SAVEPATH = ROOT_SAVEPATH + "/dao";  //dao接口存放的位置
    public static final String SERVICEIMPL_SAVEPATH = ROOT_SAVEPATH + "/api.impl";  //service实现类存放的位置
    public static final String SERVICEIMPL_SAVEPATH_IMPL = ROOT_SAVEPATH + "/service/impl";  //service实现类存放的位置
    //public static final String CONTROLLER_SAVEPATH = ROOT_SAVEPATH+"/controller"; //controller实体类存放位置


    public static final String WEBROOT_PATH = "src/main/webapp/";  //WEB根目录
    //public static final String WEB_PATH = WEBROOT_PATH+"WEB-INF/view/"+FUNCPATH+"/"; //jsp根目录

    //主键第一个字母大写
    private final String TABLEKEYUPPER;

    //当前日期
    private final String DATE;

    //项目的根目录
    private static String rootpath = PropertiesUtil.getRootpath().replace("target/", "").replace("classes/", "").replace(SERVICEAPPNAME + "/", "");

    //模板路径
    private static String templatePath = TEMPLATEPATH;

    //文件工具类
    private FileUtil fileUtil;

    //数据库工具类
    private DbUtil dbOperate;

    //替换内容
    private String fileContent;

    public CodeEngine() {
        CLASSNAME = oneWordUpper(CLASSNAME);
        TABLEKEYUPPER = oneWordUpper(TABLEKEY);
        DATE = getDate();
        fileUtil = new FileUtil();
        dbOperate = new DbUtil();
    }

    /*
     * 主入口方法
     */
    public static void main(String args[]) {

        CodeEngine codeEngine = new CodeEngine();
        String className = CLASSNAME;//codeEngine.oneWordUpper(TABLENAME);

        ArrayList fieldList = codeEngine.dbOperate.getTableDescList(TABLENAME, TABLESCHEMA);

        //java类、ibatis sql文件自动生成
        codeEngine.javaCode(codeEngine, className, fieldList);

        //前端生成
//		codeEngine.webCode(codeEngine,className,fieldList);
        //dubbo配置生成
//        codeEngine.dubboServiceConfigWrite();
    }

    public void javaCode(CodeEngine codeEngine, String className, ArrayList fieldList) {
        codeEngine.createFile("daoInterface.txt", className + "Dao.java", SERVICEAPPNAME + "/" + DAO_SAVEPATH, fieldList);
        codeEngine.createFile("service.txt", className + "Service.java", CLIENTAPPRNAME + "/" + SERVICEIMPL_SAVEPATH, fieldList);
        codeEngine.createFile("serviceImpl.txt", className + "ServiceImpl.java", SERVICEAPPNAME + "/" + SERVICEIMPL_SAVEPATH_IMPL, fieldList);
        codeEngine.createFile("pojoClass.txt", className + ".java", CLIENTAPPRNAME + "/" + POJO_SAVEPATH, fieldList);
        //codeEngine.createFile("queryFormClass.txt",className+"QueryForm.java",QUERYFORM_SAVEPATH,fieldList);
        codeEngine.createFile("sqlmap.txt", className + "Dao.xml", SERVICEAPPNAME + "/" + SQLMAP_SAVEPATH, fieldList);//.replace("dao", "mapping")
    }

    public void webCode(CodeEngine codeEngine, String className, ArrayList fieldList) {
        //codeEngine.createFile("controller.txt",className+"Controller.java",CONTROLLER_SAVEPATH,fieldList);
//		codeEngine.createFile("view_add.txt","add.jsp",WEB_PATH+TABLENAME,fieldList);
        //codeEngine.createFile("view_index.txt","index.jsp",WEB_PATH+TABLENAME,fieldList);
//		codeEngine.createFile("view_update.txt","update.jsp",WEB_PATH+TABLENAME,fieldList);
        //codeEngine.createFile("view_update.txt","view.jsp",WEB_PATH+TABLENAME,fieldList);
    }

    public void createFoldFile(String creathPath, String templateName, String fileName, ArrayList fieldList) {
        //模板的详细地址
        String file_path = creathPath + CLASSNAME;
        //创建文件夹
        fileUtil.createFolder(file_path);
        //模板的详细地址
        String template_path = rootpath + templatePath + templateName;
        //得到模板的内容
        String fileCon = fileUtil.readTxt(template_path);
        //替换模板中的标签为具体代码
        fileCon = replaceTemplate(fileCon, fieldList);
        //创建文件 和 写入文件内容
        fileUtil.writeTxt(file_path, fileName, fileCon);
        System.out.println(file_path + "/" + fileName + "文件生成成功");
    }


    public void createSqlMapFile(String templateName, String sqlMapFile, String fileName, String filePath, ArrayList fieldList) {
        //模板的详细地址
        String file_path = rootpath + templatePath + templateName;
        //得到模板的内容
        String fileCon = fileUtil.readTxt(file_path);
        //替换模板中的标签为具体代码
        fileContent = replaceTemplate(fileCon, fieldList);
        String oldCon = fileUtil.readTxt(sqlMapFile);
        if (oldCon.indexOf(fileContent) == -1) {
            oldCon = replaceTemplate(oldCon, fieldList);
        }
        //创建文件 和 写入文件内容
        fileUtil.writeTxt(sqlMapFile, "", oldCon);
        System.out.println(sqlMapFile + "文件更新成功");
    }

    /*
     * templateName:模板名称
     * fileName：生成后的文件名
     * fileCon：文件内容
     * filePath：生成后的文件存放地址
     */
    public void createFile(String templateName, String fileName, String filePath, ArrayList fieldList) {
        //模板的详细地址
        String file_path = rootpath + templatePath + templateName;
        //得到模板的内容
        String fileCon = fileUtil.readTxt(file_path);
        //替换模板中的标签为具体代码
        fileCon = replaceTemplate(fileCon, fieldList);
        if(templateName.equals("pojoClass.txt")) fileCon=fileCon.replace("`","");
        //创建文件 和 写入文件内容
        fileUtil.writeTxt(rootpath + filePath, fileName, fileCon);
        System.out.println(rootpath + filePath + "/" + fileName + "文件生成成功");
    }

    public String replaceTemplate(String fileCon, ArrayList fieldList) {
        fileCon = fileCon.replace("{PROJECTPACK}", PROJECTPACK);
        fileCon = fileCon.replace("{PACKAGENAME}", PACKAGENAME);
        fileCon = fileCon.replace("{TABLENAME}", TABLENAME);
        fileCon = fileCon.replace("{DAONAME}", oneWordLower(CLASSNAME));
        fileCon = fileCon.replace("{CLASSNAME}", CLASSNAME);
        fileCon = fileCon.replace("{FUNNAME}", FUNNAME);
        fileCon = fileCon.replace("{FUNCPATH}", FUNCPATH);
        fileCon = fileCon.replace("{AUTHOR}", AUTHOR);
        fileCon = fileCon.replace("{DATE}", DATE);
        fileCon = fileCon.replace("{TABLEKEYUPPER}", TABLEKEYUPPER);
        fileCon = fileCon.replace("{TABLEKEY}", TABLEKEY);
        fileCon = fileCon.replace("<!--tagbody-->", fileContent + "<!--tagbody-->");

        String tagname = "fieldlist";
        while (fileCon.indexOf(tagname) > -1) {
            fileCon = replaceLoopField(fileCon, tagname, fieldList);
        }
        tagname = "fieldkeylist";
        while (fileCon.indexOf(tagname) > -1) {
            fileCon = replaceLoopField(fileCon, tagname, fieldList);
        }
        tagname = "searchlist";
        while (fileCon.indexOf(tagname) > -1) {
            fileCon = replaceLoopField(fileCon, tagname, fieldList);
        }
        tagname = "indexdislist";
        while (fileCon.indexOf(tagname) > -1) {
            fileCon = replaceLoopField(fileCon, tagname, fieldList);
        }
        tagname = "wherelist";
        while (fileCon.indexOf(tagname) > -1) {
            fileCon = replaceLoopField(fileCon, tagname, fieldList);
        }
        return fileCon;
    }

    //循环替换表字段
    @SuppressWarnings("unchecked")
    public String replaceLoopField(String fileCon, String tagname, ArrayList fieldList) {
        String startTag = "{" + tagname + "}";
        String enTag = "{/" + tagname + "}";
        if (fileCon.indexOf(startTag) == -1) return fileCon;
        int i = fileCon.indexOf(startTag);
        int j = fileCon.indexOf(enTag) + enTag.length();
        //得到标签体
        String tagBody = fileCon.substring(i, j);
        String bodyCon = tagBody.replace(startTag, "").replace(enTag, "");

        //取出此表数据库描述
//		ArrayList fieldList = dbOperate.getTableDescList(TABLENAME);

        StringBuffer sb = new StringBuffer();

        if (fieldList != null && fieldList.size() > 0) {
            HashMap fieldMap = new HashMap();
            for (int k = 0; k < fieldList.size(); k++) {
                fieldMap = (HashMap) fieldList.get(k);
                String field = "", Null = "";
                String fieldComment = "";
                String fieldType = "";
                if (fieldMap.get("Null") != null) {
                    Null = fieldMap.get("Null").toString();
                }
                if (fieldMap.get("Field") != null) {
                    field = fieldMap.get("Field").toString();
                }
                if (fieldMap.get("Comment") != null) {
                    fieldComment = fieldMap.get("Comment").toString();
                }
                if (fieldMap.get("Type") != null) {
                    fieldType = fieldMap.get("Type").toString();
                }

                if (field.equals("")) continue;
                if (tagname.equals("fieldlist") && field.equals(TABLEKEY)) {
                    continue;
                }
                if (tagname.equals("searchlist") && !isExistGroup(SEARCHFIELD, field)) {
                    continue;
                }
                if (tagname.equals("indexdislist") && !isExistGroup(INDEXDISFIELD, field)) {
                    continue;
                }
                String temp = bodyCon;
                //直接替换字段
                temp = temp.replace("[field_name]", Tool.lineToHump(field));
                temp = temp.replace("[field_db_name]", field);
                temp = temp.replace("[field_comment]", fieldComment);
                //根据子段类型，替换
                if (fieldType != null && (fieldType.equalsIgnoreCase("smallint")))
                    temp = temp.replace("[field_Type]", "Integer");
                if (fieldType != null && (fieldType.equalsIgnoreCase("int") || fieldType.equalsIgnoreCase("bigint")))
                    temp = temp.replace("[field_Type]", "Long");
                if (fieldType != null && (fieldType.equalsIgnoreCase("float") || fieldType.equalsIgnoreCase("decimal")))
                    temp = temp.replace("[field_Type]", "Double");
                if (fieldType != null && (fieldType.equalsIgnoreCase("varchar") || fieldType.equalsIgnoreCase("char") || fieldType.equalsIgnoreCase("text") || fieldType.equalsIgnoreCase("longtext")))
                    temp = temp.replace("[field_Type]", "String");
                if (fieldType != null && (fieldType.equalsIgnoreCase("date") || fieldType.equalsIgnoreCase("datetime") || fieldType.equalsIgnoreCase("timestamp")))
                    temp = temp.replace("[field_Type]", "java.util.Date");

                //替换第一个字母大写的字段
                temp = temp.replace("[fieldname]", oneWordUpper(field));

                temp = temp.replace("[field_name_must]", Null.equalsIgnoreCase("NO") ? "*" : "&nbsp;");

                //替换SQLMap.txt
                temp = temp.replace("[field_node_u]", field);
                if (k == fieldList.size() - 1) {
                    if(tagname.contains("fieldkeylist")){
                        temp = temp.replace("[field_node],", field +" AS `"+ oneWordLower(field))+"`";
                    }else {
                        temp = temp.replace("[field_node],", field);
                    }
                } else {
                    if(tagname.contains("fieldkeylist")){
                        temp = temp.replace("[field_node],", field +" AS `"+ oneWordLower(field) + "`,\n\t\t");
                    }else {
                        temp = temp.replace("[field_node],", field + ",");
                    }
                }
                if (k == fieldList.size() - 1) {
                    temp = temp.replace("#{[field_u_node]},", "#{" + oneWordLower(field) + "}");
                } else {
                    temp = temp.replace("#{[field_u_node]},", "#{" + oneWordLower(field) + "},");
                }
                sb.append(temp);
            }
        }
        fileCon = fileCon.replace(tagBody, sb.toString());

        return fileCon;
    }

    //让传进来的字符串第一个字母大写
    public String oneWordUpper(String str) {
        if (str.length() == 0) return "";
        str = Tool.lineToHump(str);
        String oneWord = str.substring(0, 1);
        return oneWord.toUpperCase() + str.substring(1, str.length());
    }

    //让传进来的字符串第一个字母小写
    public String oneWordLower(String str) {
        if (str.length() == 0) return "";
        str = Tool.lineToHump(str);
        String oneWord = str.substring(0, 1);
        return oneWord.toLowerCase() + str.substring(1, str.length());
    }


    //得到当前的日期
    public String getDate() {
        SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sFormat.format(new java.util.Date());
    }

    public boolean isExistGroup(String fieldGro, String field) {
        String fid[] = fieldGro.split(",");
        boolean ret = false;
        for (int i = 0; i < fid.length; i++) {
            if (fid[i].equals(field)) {
                ret = true;
                break;
            }
        }
        return ret;
    }


    /**
     * dubbo配置信息-追加写入
     *
     */
    /*public void dubboServiceConfigWrite() {

        String oneWord = CLASSNAME.substring(0, 1);
        String implName= oneWord.toLowerCase() + CLASSNAME.substring(1, CLASSNAME.length());

        String dubboInfo="    <dubbo:service id=\""+implName+"Service\"  interface=\""+PACKAGENAME+".service."+CLASSNAME+"Service\" ref=\""+implName+"ServiceImpl\" timeout=\"60000\"/>\n";

        RandomAccessFile randomFile = null;
        try {
            // 打开一个随机访问文件流，按读写方式
            randomFile = new RandomAccessFile(rootpath+DUBBOPATH, "rw");
            // 文件长度，字节数
            int fileLength = (int)randomFile.length();
            byte[]  buff=new byte[fileLength];
            int r= randomFile.read(buff);
            String s = new String(buff,0,r);
            if(s.contains(CLASSNAME)) {
                System.out.println(s);
                return;
            }
            s=s.replace("</beans>","");
            s+=(dubboInfo+"</beans>");
            System.out.println(s);
            randomFile.seek(0);
            randomFile.write(s.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (randomFile != null) {
                try {
                    randomFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }*/

}
